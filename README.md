**FeverRP - Serveur version compact**

Version du serveur sans les scripts sous licences. Grâce à ce serveur, il te sera possible de développer des scripts via le core Fever.
Bien entendu, cette version ne te donne pas accès à nos scripts les plus développé tel que les garages, la chirurgie ou encore gester. 

**Bdd à créer pour utiliser la version compact**
Database = FXserver
Utiliser = root2
mdp = 123456

Importer le fichier bdd.sql dans cette base de donnée