-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Dim 09 Septembre 2018 à 18:45
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `fxserver`
--

-- --------------------------------------------------------

--
-- Structure de la table `bans`
--

CREATE TABLE `bans` (
  `id` int(10) UNSIGNED NOT NULL,
  `banned` varchar(50) NOT NULL DEFAULT '0',
  `banner` varchar(50) NOT NULL,
  `reason` varchar(150) NOT NULL DEFAULT '0',
  `expires` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `bracelet_elec`
--

CREATE TABLE `bracelet_elec` (
  `identifier` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `coordinates`
--

CREATE TABLE `coordinates` (
  `id` int(11) UNSIGNED NOT NULL,
  `x` double DEFAULT NULL,
  `y` double DEFAULT NULL,
  `z` double DEFAULT NULL,
  `Name` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `coordinates`
--

INSERT INTO `coordinates` (`id`, `x`, `y`, `z`, `Name`) VALUES
(1, 1057.885, -3196.122, -40.162, 'Récolte Weed'),
(2, 1037.63, -3205.673, -38.983, 'Traitement Weed'),
(3, 1522.858, 6328.949, 23.606, 'Revente Weed'),
(4, 2645.97143554688, 2806.37280273438, 33.9922828674316, 'Récolte Cuivre'),
(5, 1218.978, 1841.154, 79.131, 'Traitement Cuivre'),
(6, -470.49160766602, -1716.6346435547, 18.689130783081, 'Revente Cuivre'),
(7, 3.814, 7405.39, 10.04, 'Récolte Coke'),
(8, 1100.226, -3194.542, -40, 'Traitement Coke'),
(9, -1291.756, -280.21, 37.689, 'Revente Coke'),
(10, 427.05981445313, 6470.2919921875, 28.790115356445, 'Ferme des Paysans'),
(11, 2887.9797363281, 4380.7573242188, 50.31233215332, 'Transformation Alimentaire'),
(12, 877.884765625, -1671.0543212891, 30.529088973999, 'Revente Produit alimentaire'),
(13, 654.81970214844, 3009.806640625, 43.290996551514, 'Récolte Petrole'),
(14, 1736.1260986328, -1669.7712402344, 112.5824508667, 'Traitement Pétrole'),
(15, 497.22463989258, -2172.123046875, 5.918270111084, 'Revente Pétrole'),
(16, -864.934, 4453.937, 18.488, 'Récolte Or'),
(17, 1059.929, -1976.225, 30.017, 'Traitement Or'),
(18, 539.716, -1945.359, 24.984, 'Revente Or'),
(19, 2415.461, 4993.219, 45.2323, 'Recolte Biere'),
(20, 838.816, -1925.372, 29.3146, 'Traitement biere'),
(21, -1855.985, -317.612, 49.145, 'Revente Biere'),
(22, 2242.261, 5152.147, 56.069, 'Récolte Légume'),
(23, 921.566, -2019.596, 29.399, 'Traitement Légume'),
(24, -1320.971, -1045.323, 6.461, 'Vente Légume'),
(25, -610.536, -1608.82, 25.896, 'récolte tattoo'),
(26, 1319.495, -1663.328, 50.236, 'traitement tattoo'),
(27, 1861.904, 3750.147, 32.031, 'vente tattoo'),
(28, 1902.533, 4920.182, 48.73, 'recolte bourbon'),
(29, 1955.099, 3844.041, 32.02, 'traitement bourbon'),
(30, 569.008, 2671.344, 42.0248, 'vente bourbon'),
(31, 2337.019, 4858.662, 41.808, 'recolte seigle'),
(32, 179.666, -2213.583, 5.9846, 'traitement whisky'),
(33, -1208.138, 332.158, 70.999, 'vente whisky');

-- --------------------------------------------------------

--
-- Structure de la table `craft`
--

CREATE TABLE `craft` (
  `autoinc_craft` int(11) NOT NULL,
  `identifier` varchar(60) NOT NULL,
  `illegal` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `items`
--

CREATE TABLE `items` (
  `id` int(11) UNSIGNED NOT NULL,
  `libelle` varchar(255) DEFAULT NULL,
  `poids` decimal(5,2) NOT NULL DEFAULT '0.00',
  `isIllegal` tinyint(1) NOT NULL DEFAULT '0',
  `food` varchar(255) NOT NULL DEFAULT '0',
  `water` varchar(255) NOT NULL DEFAULT '0',
  `needs` varchar(255) NOT NULL DEFAULT '0',
  `isArme` tinyint(1) NOT NULL DEFAULT '0',
  `VenteArme` int(11) NOT NULL DEFAULT '0',
  `hash` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `items`
--

INSERT INTO `items` (`id`, `libelle`, `poids`, `isIllegal`, `food`, `water`, `needs`, `isArme`, `VenteArme`, `hash`) VALUES
(1, 'Cuivre brut', '10.00', 0, '0', '0', '0', 0, 0, NULL),
(2, 'Chanvre', '1.00', 1, '0', '0', '0', 0, 0, NULL),
(3, 'Weed', '0.10', 1, '0', '0', '0', 0, 0, NULL),
(4, 'Cuivre traité', '8.00', 0, '0', '0', '0', 0, 0, NULL),
(5, 'Feuille de Coca', '1.00', 1, '0', '0', '0', 0, 0, NULL),
(6, 'Cocaïne', '0.10', 1, '0', '0', '0', 0, 0, NULL),
(7, 'Raisins et Viandes ', '5.00', 0, '0', '0', '0', 0, 0, NULL),
(8, 'Vins et Saucissons', '4.00', 0, '0', '0', '0', 0, 0, NULL),
(9, 'Or brut', '10.00', 0, '0', '0', '0', 0, 0, NULL),
(10, 'Or traité', '8.00', 0, '0', '0', '0', 0, 0, NULL),
(11, 'Baril brut', '131.00', 0, '0', '0', '0', 0, 0, NULL),
(12, 'Baril raffinée', '115.00', 0, '0', '0', '0', 0, 0, NULL),
(13, 'Viande fraiche', '2.00', 0, '0', '0', '0', 0, 0, NULL),
(14, 'Kit de réparation', '5.00', 0, '0', '0', '0', 0, 0, NULL),
(15, 'Cigarette', '0.01', 0, '0', '0', '0', 0, 0, NULL),
(16, 'Orge', '5.00', 0, '0', '0', '0', 0, 0, NULL),
(17, 'Bière', '0.66', 0, '0', '50', '0', 0, 0, 'prop_amb_beer_bottle'),
(18, 'Légumes en vrac', '5.00', 0, '0', '0', '0', 0, 0, NULL),
(19, 'Légumes cuisinés', '3.00', 0, '0', '0', '0', 0, 0, NULL),
(20, 'Taco', '0.25', 0, '55', '0', '0', 0, 0, 'prop_taco_01'),
(21, 'Burrito', '0.25', 0, '55', '0', '0', 0, 0, 'prop_taco_02'),
(22, 'Frites', '0.25', 0, '40', '0', '0', 0, 0, 'prop_food_chips'),
(23, 'Bière sans alcool', '0.66', 0, '0', '50', '0', 0, 0, 'prop_beer_pride'),
(24, 'Feuilles à dessin', '4.00', 0, '0', '0', '0', 0, 0, NULL),
(25, 'Tattoos flashs', '3.21', 0, '0', '0', '0', 0, 0, NULL),
(26, 'Hamburger', '0.40', 0, '100', '0', '0', 0, 0, 'prop_cs_burger_01'),
(30, 'Sandwich', '0.25', 0, '30', '0', '0', 0, 0, 'prop_sandwich_01'),
(31, 'Cola', '0.37', 0, '0', '30', '0', 0, 0, 'prop_ecola_can'),
(32, 'Téléphone', '0.16', 0, '0', '0', '0', 0, 0, NULL),
(33, 'Maïs', '5.00', 0, '0', '0', '0', 0, 0, NULL),
(34, 'Bourbon', '0.50', 0, '0', '50', '0', 0, 0, 'prop_drink_whisky'),
(35, 'Seigle', '5.00', 0, '0', '0', '0', 0, 0, NULL),
(36, 'Whisky', '0.50', 0, '0', '50', '0', 0, 0, 'prop_drink_whisky'),
(40, 'DeepSim', '0.01', 1, '0', '0', '0', 0, 0, NULL),
(41, 'Billet marqué', '0.00', 1, '0', '0', '0', 0, 0, NULL),
(42, 'Café', '0.24', 0, '0', '30', '0', 0, 0, 'ng_proc_coffee_01a'),
(43, 'Croissant', '0.05', 0, '30', '0', '0', 0, 0, NULL),
(46, 'Kit de crochetage', '0.10', 1, '0', '0', '0', 0, 0, NULL),
(47, 'Tenue de plongée', '16.00', 0, '0', '0', '0', 0, 0, NULL),
(48, 'Coffre', '100.00', 0, '0', '0', '0', 0, 0, NULL),
(101, 'Machette', '0.63', 0, '0', '0', '0', 1, 1, 'WEAPON_MACHETE'),
(102, 'Club de golf', '0.10', 0, '0', '0', '0', 1, 1, 'WEAPON_GOLFCLUB'),
(103, 'Poing américains', '0.32', 0, '0', '0', '0', 1, 1, 'WEAPON_KNUCKLE'),
(104, 'Couteau', '0.31', 0, '0', '0', '0', 1, 1, 'WEAPON_KNIFE'),
(105, 'Dague', '0.55', 0, '0', '0', '0', 1, 1, 'WEAPON_DAGGER'),
(106, 'Lampe torche', '0.16', 0, '0', '0', '0', 1, 1, 'WEAPON_FLASHLIGHT'),
(107, 'Pied de biche', '1.63', 0, '0', '0', '0', 1, 1, 'WEAPON_CROWBAR'),
(108, 'Batte de baseball', '0.42', 0, '0', '0', '0', 1, 1, 'WEAPON_BAT'),
(109, 'Bouteille', '0.21', 0, '0', '0', '0', 1, 1, 'WEAPON_BOTTLE'),
(110, 'Marteau', '0.65', 0, '0', '0', '0', 1, 1, 'WEAPON_HAMMER'),
(111, 'Pistolet', '0.59', 0, '0', '0', '0', 1, 1, 'WEAPON_Pistol'),
(112, 'Pistolet calibre 50', '1.87', 0, '0', '0', '0', 1, 1, 'WEAPON_PISTOL50'),
(113, 'SNS Pistolet', '0.34', 0, '0', '0', '0', 1, 1, 'WEAPON_SNSPistol'),
(114, 'Pistolet Lourd', '2.20', 0, '0', '0', '0', 1, 1, 'WEAPON_HeavyPistol'),
(115, 'Revolver', '1.31', 0, '0', '0', '0', 1, 1, 'WEAPON_Revolver'),
(116, 'Grenade Fumigène', '0.08', 0, '0', '0', '0', 1, 1, 'WEAPON_SmokeGrenade'),
(117, 'Fusil à pompe', '5.21', 0, '0', '0', '0', 1, 1, 'WEAPON_PumpShotgun'),
(118, 'Double-cannon', '2.61', 0, '0', '0', '0', 1, 1, 'weapon_dbshotgun'),
(119, 'MicroSMG', '3.18', 0, '0', '0', '0', 1, 1, 'WEAPON_MicroSMG'),
(120, 'SMG', '3.32', 0, '0', '0', '0', 1, 1, 'WEAPON_SMG'),
(121, 'Assault SMG', '3.21', 0, '0', '0', '0', 1, 1, 'WEAPON_AssaultSMG'),
(122, 'Fusil d\'assault', '5.31', 0, '0', '0', '0', 1, 1, 'WEAPON_AssaultRifle'),
(123, 'M16', '4.47', 0, '0', '0', '0', 1, 1, 'WEAPON_CarbineRifle'),
(124, 'Sniper', '15.21', 0, '0', '0', '0', 1, 1, 'WEAPON_SniperRifle'),
(125, 'Tazer', '0.20', 0, '0', '0', '0', 1, 1, 'WEAPON_STUNGUN'),
(126, 'T-batte', '0.66', 0, '0', '0', '0', 1, 1, 'WEAPON_NIGHTSTICK'),
(127, 'Lampe torche LSPD', '0.16', 0, '0', '0', '0', 1, 0, 'WEAPON_FLASHLIGHT'),
(128, 'Pistolet 50 LSPD', '1.87', 0, '0', '0', '0', 1, 0, 'WEAPON_PISTOL50'),
(129, 'Pistolet Lourd LSPD', '2.20', 0, '0', '0', '0', 1, 0, 'WEAPON_HeavyPistol'),
(130, 'Fusil à pompe LSPD', '5.21', 0, '0', '0', '0', 1, 0, 'WEAPON_PumpShotgun'),
(131, 'SMG LSPD', '3.32', 0, '0', '0', '0', 1, 0, 'WEAPON_SMG'),
(132, 'Assault SMG LSPD', '3.21', 0, '0', '0', '0', 1, 0, 'WEAPON_AssaultSMG'),
(133, 'Sniper LSPD', '15.21', 0, '0', '0', '0', 1, 0, 'WEAPON_SniperRifle'),
(134, 'Tazer LSPD', '0.20', 0, '0', '0', '0', 1, 0, 'WEAPON_STUNGUN'),
(135, 'T-batte LSPD', '0.66', 0, '0', '0', '0', 1, 0, 'WEAPON_NIGHTSTICK');

-- --------------------------------------------------------

--
-- Structure de la table `jobs`
--

CREATE TABLE `jobs` (
  `job_id` int(11) NOT NULL,
  `job_name` varchar(40) NOT NULL,
  `salary` int(11) NOT NULL DEFAULT '500'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `jobs`
--

INSERT INTO `jobs` (`job_id`, `job_name`, `salary`) VALUES
(1, 'Chomeur', 350),
(2, 'Policier', 350),
(3, 'Camionneur', 350),
(4, 'Mineur', 350),
(5, 'Chauffeur de taxi', 350),
(6, 'Dealer', 350),
(7, 'Agriculteur', 350),
(8, 'Dépanneur', 350),
(9, 'Procureur', 2300),
(10, 'Raffineur', 350),
(11, 'Ambulancier', 900),
(12, 'Médecin Chef', 1200),
(13, 'Chef Dépanneur', 350),
(14, 'Chef taxi', 500),
(15, 'Journaliste', 600),
(16, 'Président', 3000),
(17, 'Vice-Président', 2500),
(18, 'Agent du Gouvernement', 2100),
(19, 'PDG Tequi-la-la', 350),
(20, 'Barman', 350),
(21, 'Rédacteur en chef', 950),
(22, 'Journaliste', 650),
(23, 'Chasseur', 350),
(24, 'Inspecteur', 500),
(25, 'Inspecteur stagiaire', 350),
(26, 'Brasseur', 350),
(27, 'Maître Brasseur', 350),
(28, 'Vendeur chez AAT', 350),
(29, 'PDG AAT', 350),
(30, 'Tatoueur', 350),
(31, 'PDG Iris Tattoo', 350),
(32, 'PDG Voit\'Occas', 350),
(33, 'Vendeur Voit\'Occas', 350),
(34, 'Officier de Police', 1800),
(35, 'Barman Yellow Jack', 350),
(36, 'PDG Yellow Jack', 350);

-- --------------------------------------------------------

--
-- Structure de la table `modelmenu`
--

CREATE TABLE `modelmenu` (
  `autoinc_skin` int(11) NOT NULL,
  `identifier` varchar(50) DEFAULT NULL,
  `nom` varchar(60) NOT NULL DEFAULT 'Tenue',
  `Finish` int(11) NOT NULL DEFAULT '0',
  `Actif` int(11) NOT NULL DEFAULT '0',
  `Sexe` int(11) NOT NULL,
  `TetePere` int(11) DEFAULT NULL,
  `TeteMere` int(11) DEFAULT NULL,
  `RessPere` int(11) DEFAULT NULL,
  `Imp` int(11) DEFAULT NULL,
  `Ride` int(11) DEFAULT NULL,
  `Defaut` int(11) DEFAULT NULL,
  `Tache` int(11) DEFAULT NULL,
  `Barbe` int(11) DEFAULT NULL,
  `BarbeColor` int(11) DEFAULT NULL,
  `Lun` int(11) DEFAULT NULL,
  `LunColor` int(11) DEFAULT NULL,
  `Pier` int(11) DEFAULT NULL,
  `PierColor` int(11) DEFAULT NULL,
  `Veste` int(11) DEFAULT NULL,
  `VesteColor` int(11) DEFAULT NULL,
  `mask` int(11) NOT NULL DEFAULT '0',
  `mask_txt` int(11) NOT NULL DEFAULT '0',
  `Coupe` int(11) NOT NULL DEFAULT '0',
  `CoupeColor` int(11) NOT NULL DEFAULT '0',
  `CoupeSecColor` int(11) NOT NULL DEFAULT '1',
  `Chaus` int(11) NOT NULL DEFAULT '0',
  `ChausColor` int(11) NOT NULL DEFAULT '10',
  `Pant` int(11) NOT NULL DEFAULT '0',
  `PantColor` int(11) NOT NULL DEFAULT '0',
  `Haut` int(11) NOT NULL DEFAULT '2',
  `HautColor` int(11) NOT NULL DEFAULT '0',
  `Mains` int(11) DEFAULT NULL,
  `MainsColor` int(11) DEFAULT NULL,
  `Yeux` int(11) NOT NULL,
  `Blush` int(11) NOT NULL,
  `Soleil` int(11) NOT NULL,
  `Teint` int(11) NOT NULL,
  `TeintColor` int(11) NOT NULL,
  `Maqui` int(11) NOT NULL,
  `MaquiColor` int(11) NOT NULL,
  `Poit` int(11) NOT NULL,
  `PoitColor` int(11) NOT NULL,
  `Levre` int(11) NOT NULL,
  `LevreColor` int(11) NOT NULL,
  `Sourcil` int(11) NOT NULL,
  `SourcilColor` int(11) NOT NULL,
  `Chapeau` int(11) DEFAULT NULL,
  `ChapeauColor` int(11) DEFAULT NULL,
  `Coll` int(11) NOT NULL,
  `CollColor` int(11) NOT NULL,
  `Montre` smallint(6) NOT NULL DEFAULT '240',
  `MontreColor` smallint(6) NOT NULL DEFAULT '0',
  `Bracelet` smallint(6) NOT NULL DEFAULT '240',
  `BraceletColor` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `police`
--

CREATE TABLE `police` (
  `police_id` int(11) NOT NULL,
  `police_name` varchar(40) NOT NULL,
  `salary` int(11) NOT NULL DEFAULT '500'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `police`
--

INSERT INTO `police` (`police_id`, `police_name`, `salary`) VALUES
(1, 'Cadet', 750),
(2, 'Brigadier', 1000),
(3, 'Sergent', 1250),
(4, 'Lieutenant', 1500),
(5, 'Capitaine', 2000),
(6, 'Commandant', 2500),
(7, 'Colonel', 2500),
(8, 'Rien', 0);

-- --------------------------------------------------------

--
-- Structure de la table `presidentielle`
--

CREATE TABLE `presidentielle` (
  `id` int(11) NOT NULL,
  `identifier` varchar(50) NOT NULL,
  `vote` int(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `recolt`
--

CREATE TABLE `recolt` (
  `ID` int(11) UNSIGNED NOT NULL,
  `raw_id` int(11) UNSIGNED DEFAULT NULL,
  `treated_id` int(11) UNSIGNED DEFAULT NULL,
  `job_id` varchar(60) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `difference` int(11) NOT NULL DEFAULT '0',
  `field_id` int(10) UNSIGNED DEFAULT NULL,
  `treatment_id` int(10) UNSIGNED DEFAULT NULL,
  `seller_id` int(10) UNSIGNED DEFAULT NULL,
  `Name` text,
  `fluctuation` tinyint(1) NOT NULL DEFAULT '0',
  `nb_ventes` int(11) NOT NULL DEFAULT '0',
  `heure_update` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `JobEntreprise` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `recolt`
--

INSERT INTO `recolt` (`ID`, `raw_id`, `treated_id`, `job_id`, `price`, `difference`, `field_id`, `treatment_id`, `seller_id`, `Name`, `fluctuation`, `nb_ventes`, `heure_update`, `JobEntreprise`) VALUES
(1, 2, 3, '6,', 512, 0, 1, 2, 3, 'Weed', 0, 26717, '2017-12-09 17:54:09', 0),
(2, 1, 4, '4,', 83, -4, 4, 5, 6, 'Cuivre', 1, 94, '2017-11-01 00:00:00', 0),
(3, 5, 6, '6,', 612, 0, 7, 8, 9, 'Coke', 0, 12699, '2017-11-01 01:02:03', 0),
(4, 7, 8, '7,', 111, 3, 10, 11, 12, 'Paysan', 1, 0, '2017-11-02 00:00:00', 0),
(5, 11, 12, '10,', 129, -14, 13, 14, 15, 'Pétrole', 1, 0, '2017-11-02 00:00:00', 0),
(6, 9, 10, '4,', 133, 15, 16, 17, 18, 'Or', 1, 140, '2017-11-09 00:00:00', 0),
(7, 16, 17, '26,27,', 170, 0, 19, 20, 21, 'Biere', 0, 186136, '2017-11-01 00:00:00', 1),
(8, 18, 19, '28,29,', 178, 0, 22, 23, 24, 'Légumes', 0, 115421, '2017-12-09 00:00:00', 1),
(9, 24, 25, '30,31,', 155, 0, 25, 26, 27, 'Tattoos flashs', 0, 24388, '2017-12-29 00:00:00', 1),
(10, 33, 34, '35,36,', 140, 0, 28, 29, 30, 'bourbon', 0, 2095, '2018-06-02 15:32:13', 1),
(11, 35, 36, '19,20,', 180, 0, 31, 32, 33, 'Whisky', 0, 12386, '2018-06-10 15:13:12', 1);

-- --------------------------------------------------------

--
-- Structure de la table `topserveur`
--

CREATE TABLE `topserveur` (
  `autoinc_vote` int(11) NOT NULL,
  `steamid` varchar(255) NOT NULL,
  `heure` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `autoinc_user` int(11) NOT NULL,
  `identifier` varchar(255) CHARACTER SET utf8mb4 NOT NULL DEFAULT '',
  `player_state` int(255) NOT NULL DEFAULT '0',
  `group` varchar(50) NOT NULL DEFAULT 'user',
  `permission_level` int(11) NOT NULL DEFAULT '0',
  `LastInGameName` varchar(255) DEFAULT NULL,
  `LastConnexion` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `money` double NOT NULL DEFAULT '0',
  `personalvehicle` varchar(60) DEFAULT NULL,
  `bankbalance` int(32) DEFAULT '6000',
  `job` int(11) DEFAULT '1',
  `isFirstConnection` int(11) DEFAULT '1',
  `lastpos` varchar(255) DEFAULT '{-29.8749465942383, -1105.11791992188,  26.4223327636719, 153.204223632813}',
  `Commentaire` text,
  `enService` tinyint(1) NOT NULL DEFAULT '0',
  `food` double NOT NULL DEFAULT '100',
  `water` double NOT NULL DEFAULT '100',
  `needs` double DEFAULT '0',
  `nom` varchar(30) CHARACTER SET utf8mb4 NOT NULL DEFAULT 'RP',
  `prenom` varchar(30) CHARACTER SET utf8mb4 NOT NULL DEFAULT 'Fever',
  `police` int(11) NOT NULL DEFAULT '0',
  `telephone` varchar(30) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  `gunlicense` int(1) NOT NULL DEFAULT '0',
  `temps_jeu` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `user_appartement`
--

CREATE TABLE `user_appartement` (
  `autoinc_app` int(11) NOT NULL,
  `identifier` varchar(60) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `proprio` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user_inventory`
--

CREATE TABLE `user_inventory` (
  `user_id` varchar(50) NOT NULL,
  `inventaire` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user_vehicle`
--

CREATE TABLE `user_vehicle` (
  `autoinc_veh` int(10) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'terrestre',
  `garage` varchar(255) NOT NULL DEFAULT '1',
  `vehicle_name` varchar(60) DEFAULT NULL,
  `vehicle_model` varchar(60) DEFAULT NULL,
  `vehicle_price` int(60) DEFAULT NULL,
  `vehicle_prix_amelioration` int(60) NOT NULL DEFAULT '0',
  `vehicle_plate` varchar(60) DEFAULT NULL,
  `vehicle_state` varchar(60) DEFAULT NULL,
  `vehicle_hs` int(3) NOT NULL DEFAULT '0',
  `vendupar` varchar(255) DEFAULT '0',
  `vehicle_colorprimary` varchar(60) DEFAULT NULL,
  `vehicle_colorsecondary` varchar(60) DEFAULT NULL,
  `vehicle_pearlescentcolor` varchar(60) DEFAULT NULL,
  `vehicle_wheelcolor` varchar(60) DEFAULT NULL,
  `vehicle_mods` varchar(255) NOT NULL DEFAULT '-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,',
  `vehicle_vehiclecol` varchar(60) NOT NULL DEFAULT '1,1,',
  `vehicle_extracol` varchar(60) NOT NULL DEFAULT '1,1,',
  `vehicle_neoncolor` varchar(60) NOT NULL DEFAULT '0,255,0,255,0,',
  `vehicle_plateindex` varchar(60) NOT NULL DEFAULT '0',
  `vehicle_windowtint` int(32) NOT NULL DEFAULT '-1',
  `vehicle_wheeltype` int(32) NOT NULL DEFAULT '7',
  `vehicle_tyresmoke` varchar(60) NOT NULL DEFAULT '0,255,255,255,1,',
  `mise_en_fourriere` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `weapon_tint`
--

CREATE TABLE `weapon_tint` (
  `autoinc_tint` int(11) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `tint` int(2) NOT NULL DEFAULT '0',
  `date_abonnement` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `whitlist`
--

CREATE TABLE `whitlist` (
  `id` int(10) UNSIGNED NOT NULL,
  `steamid` varchar(50) NOT NULL,
  `NomRP` varchar(50) NOT NULL,
  `LastInGameName` text,
  `ID Discord` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `bans`
--
ALTER TABLE `bans`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `bracelet_elec`
--
ALTER TABLE `bracelet_elec`
  ADD UNIQUE KEY `identifier` (`identifier`);

--
-- Index pour la table `coordinates`
--
ALTER TABLE `coordinates`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `craft`
--
ALTER TABLE `craft`
  ADD PRIMARY KEY (`autoinc_craft`);

--
-- Index pour la table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`job_id`);

--
-- Index pour la table `modelmenu`
--
ALTER TABLE `modelmenu`
  ADD PRIMARY KEY (`autoinc_skin`);

--
-- Index pour la table `police`
--
ALTER TABLE `police`
  ADD PRIMARY KEY (`police_id`);

--
-- Index pour la table `presidentielle`
--
ALTER TABLE `presidentielle`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `recolt`
--
ALTER TABLE `recolt`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `raw_id` (`raw_id`),
  ADD KEY `treated_id` (`treated_id`),
  ADD KEY `job_id` (`job_id`),
  ADD KEY `field_id` (`field_id`),
  ADD KEY `treatment_id` (`treatment_id`),
  ADD KEY `seller_id` (`seller_id`);

--
-- Index pour la table `topserveur`
--
ALTER TABLE `topserveur`
  ADD PRIMARY KEY (`autoinc_vote`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`autoinc_user`),
  ADD UNIQUE KEY `autoinc_user` (`autoinc_user`),
  ADD UNIQUE KEY `identifier` (`identifier`);

--
-- Index pour la table `user_appartement`
--
ALTER TABLE `user_appartement`
  ADD PRIMARY KEY (`autoinc_app`);

--
-- Index pour la table `user_inventory`
--
ALTER TABLE `user_inventory`
  ADD PRIMARY KEY (`user_id`);

--
-- Index pour la table `user_vehicle`
--
ALTER TABLE `user_vehicle`
  ADD PRIMARY KEY (`autoinc_veh`),
  ADD UNIQUE KEY `vehicle_plate` (`vehicle_plate`);

--
-- Index pour la table `weapon_tint`
--
ALTER TABLE `weapon_tint`
  ADD PRIMARY KEY (`autoinc_tint`);

--
-- Index pour la table `whitlist`
--
ALTER TABLE `whitlist`
  ADD PRIMARY KEY (`id`,`steamid`,`NomRP`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `bans`
--
ALTER TABLE `bans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `coordinates`
--
ALTER TABLE `coordinates`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT pour la table `craft`
--
ALTER TABLE `craft`
  MODIFY `autoinc_craft` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT pour la table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `job_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT pour la table `modelmenu`
--
ALTER TABLE `modelmenu`
  MODIFY `autoinc_skin` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `police`
--
ALTER TABLE `police`
  MODIFY `police_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `presidentielle`
--
ALTER TABLE `presidentielle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `recolt`
--
ALTER TABLE `recolt`
  MODIFY `ID` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `topserveur`
--
ALTER TABLE `topserveur`
  MODIFY `autoinc_vote` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `autoinc_user` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `user_appartement`
--
ALTER TABLE `user_appartement`
  MODIFY `autoinc_app` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `user_vehicle`
--
ALTER TABLE `user_vehicle`
  MODIFY `autoinc_veh` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `weapon_tint`
--
ALTER TABLE `weapon_tint`
  MODIFY `autoinc_tint` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `whitlist`
--
ALTER TABLE `whitlist`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
