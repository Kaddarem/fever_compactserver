--uh1mash
data_file 'HANDLING_FILE' 'meta/uh1mash/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'meta/uh1mash/vehicles.meta'

files {
  'meta/uh1mash/handling.meta',
  'meta/uh1mash/vehicles.meta',
}

--flatbed
data_file 'VEHICLE_LAYOUTS_FILE' 'meta/flatbed/vehiclelayouts.meta'
data_file 'HANDLING_FILE' 'meta/flatbed/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'meta/flatbed/vehicles.meta'
data_file 'CARCOLS_FILE' 'meta/flatbed/carcols.meta'
data_file 'VEHICLE_VARIATION_FILE' 'meta/flatbed/carvariations.meta'
data_file 'DLC_ITYP_REQUEST' 'stream/flatbed/def_flatbed3_props.ytyp'

files {
  'meta/flatbed/vehiclelayouts.meta',
  'meta/flatbed/handling.meta',
  'meta/flatbed/vehicles.meta',
  'meta/flatbed/carcols.meta',
  'meta/flatbed/carvariations.meta',
}

client_script 'vehicle_names.lua'