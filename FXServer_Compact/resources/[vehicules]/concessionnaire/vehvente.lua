local vente_location = {-45.228, -1083.123, 25.816}
local VEHICLES = {}
local maxdamage = 950

Citizen.CreateThread(function()
	local loc = vente_location
	pos = vente_location
	local blip = AddBlipForCoord(pos[1],pos[2],pos[3])
	SetBlipSprite(blip,207)
	SetBlipColour(blip, 3)
	BeginTextCommandSetBlipName("STRING")
	AddTextComponentString("Vente")
	EndTextCommandSetBlipName(blip)
	SetBlipAsShortRange(blip,true)
	SetBlipAsMissionCreatorBlip(blip,true)
	checkgarage = 0
    local BoucleLent = 5000
    local BoucleCourt = 5
    local TempsBoucle = BoucleCourt
	while true do
		Wait(TempsBoucle)
        TempsBoucle = BoucleLent
        if GetDistanceBetweenCoords(vente_location[1],vente_location[2],vente_location[3],GetEntityCoords(GetPlayerPed(-1))) < 50 then
            TempsBoucle = BoucleCourt
            Marker(vente_location[1],vente_location[2],vente_location[3],3.0,0,155,255)
            if GetDistanceBetweenCoords(vente_location[1],vente_location[2],vente_location[3],GetEntityCoords(GetPlayerPed(-1))) < 5 and IsPedInAnyVehicle(GetPlayerPed(-1), true) == false then
                DisplayHelp("Appuyez sur ~INPUT_CONTEXT~ pour vendre le véhicule à ~r~50%~w~ du prix d\'achat")
                if IsControlJustPressed(1, 51) then
                    TriggerServerEvent("vehshop:CheckGarageForVeh")
                    VendreCar()
                end
            end
        end
	end
end)

--[[Events]]--

RegisterNetEvent("vehshop:getVehicles")
AddEventHandler("vehshop:getVehicles", function(THEVEHICLES)
    VEHICLES = {}
    VEHICLES = THEVEHICLES
end)

AddEventHandler("playerSpawned", function()
    TriggerServerEvent("vehshop:CheckGarageForVeh")
end)



function VendreCar()
    Citizen.CreateThread(function()		
		Citizen.Wait(1000)
		local caissei = GetVehiclePedIsIn(GetPlayerPed(-1), true)
        local model = GetEntityModel(caissei)
        local name = GetDisplayNameFromVehicleModel(model)	
		SetEntityAsMissionEntity(caissei, true, true)	
		local platecaissei = GetVehicleNumberPlateText(caissei)
        if tonumber(GetVehicleBodyHealth(caissei,false)) < maxdamage then
               TriggerEvent("hud:NotifColor","Véhicule trop endommagé",6)
        else                
            if DoesEntityExist(caissei) then
                local trouver = false
                for _,v in pairs(VEHICLES) do
                    if (v.vehicle_plate == platecaissei) and (v.vehicle_state == "Sorti") then
                        trouver = true
                        Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(caissei))
                        TriggerServerEvent('vehshop:SelVeh', platecaissei,name)
                        TriggerServerEvent("vehshop:CheckGarageForVeh")
                        break
                    end
                end
                if not trouver then
                    TriggerEvent("hud:NotifColor","Ce n'est pas votre véhicule",6)
                end
            else
                TriggerEvent("hud:NotifColor","Aucun véhicule présent",6)
            end     
        end
    end)
end


local firstspawn = 0
AddEventHandler('playerSpawned', function(spawn)
	if firstspawn == 0 then
		--RemoveIpl('v_carshowroom')
		--RemoveIpl('shutter_open')
		--RemoveIpl('shutter_closed')
		--RemoveIpl('shr_int')
		--RemoveIpl('csr_inMission')
		RequestIpl('v_carshowroom')
		RequestIpl('shr_int')
		RequestIpl('shutter_closed')
		firstspawn = 1
	end
end)
