function PlaqueValide(source)
    local player = getPlayerID(source)
    local plate1 = string.sub(player,-4)
    local plate2 = 1
    local plate_total = plate1.."8001"
    local valide = false
    while not valide do
        local result = MySQL.Sync.fetchAll('SELECT * FROM user_vehicle WHERE vehicle_plate = @plate',{["@plate"] = plate_total})
        if result[1] == nil then
            valide = true
        else
            plate2 = plate2 + 1
            if plate2 < 10 then
                plate_total = plate1.."800"..plate2
            elseif plate2 < 100 then
                plate_total = plate1.."80"..plate2
            else
                plate_total = plate1.."8"..plate2
            end
        end
    end
    return plate_total
end

RegisterServerEvent('CheckMoneyForVeh')
AddEventHandler('CheckMoneyForVeh', function(name, vehicle, price)
    local player = getPlayerID(source)
    local vehicle = vehicle
    local name = name
    local price = tonumber(price)
    local source = source
    local argent = MySQL.Sync.fetchScalar("SELECT bankbalance FROM users WHERE identifier = @name", {['@name'] = player})
    if (tonumber(argent) >= tonumber(price)) then
        TriggerClientEvent("banking:paywithbankclient",source, price)
        local plaque = PlaqueValide(source)
        TriggerClientEvent('FinishMoneyCheckForVeh', source, name, vehicle, price,plaque)
        TriggerClientEvent("es_freeroam:notify", source, "CHAR_SIMEON", 1, "Simeon", false, "Bonne route!\n")
    else
        TriggerClientEvent("es_freeroam:notify", source, "CHAR_SIMEON", 1, "Simeon", false, "Fonds insuffisants!\n")
    end
end)

RegisterServerEvent('BuyForVeh')
AddEventHandler('BuyForVeh', function(name, vehicle, price, plate, primarycolor, secondarycolor, pearlescentcolor, wheelcolor)

    local player = getPlayerID(source)
    local name = name
    local price = price
    local vehicle = vehicle
    local plate = plate
    local state = "Sorti"
    local primarycolor = primarycolor
    local secondarycolor = secondarycolor
    local pearlescentcolor = pearlescentcolor
    local wheelcolor = wheelcolor
    MySQL.Sync.execute("INSERT INTO user_vehicle (`identifier`, `vehicle_name`, `vehicle_model`, `vehicle_price`, `vehicle_plate`, `vehicle_state`, `vehicle_colorprimary`, `vehicle_colorsecondary`, `vehicle_pearlescentcolor`, `vehicle_wheelcolor`) VALUES ( @username ,  @name ,  @vehicle ,  @price ,  @plate ,  @state ,  @primarycolor ,  @secondarycolor ,  @pearlescentcolor ,  @wheelcolor )",
    {['@username'] = player, ['@name'] = name, ['@vehicle'] = vehicle, ['@price'] = price, ['@plate'] = plate, ['@state'] = state, ['@primarycolor'] = primarycolor, ['@secondarycolor'] = secondarycolor, ['@pearlescentcolor'] = pearlescentcolor, ['@wheelcolor'] = wheelcolor})
end)

RegisterServerEvent('vehshop:Saveinfovehicle')
AddEventHandler('vehshop:Saveinfovehicle', function(plate,vehiclecol,extracol,neoncolor,plateindex,windowtint,wheeltype,listmods,tyresmoke)
    local source = source
    local plate = plate
    --------Save info vehicle--------------------
    local listmods = listmods
    local listvehiclecol = table.concat(vehiclecol,",")..","
    local listextracol = table.concat(extracol,",")..","
    local listneoncolor ="0,"..table.concat(neoncolor,",")..",0,"
    local plateindex = plateindex
    local windowtint =tonumber(windowtint)
    local wheeltype = tonumber(wheeltype)
    MySQL.Async.insert("UPDATE user_vehicle SET `vehicle_mods`=@listmods, `vehicle_vehiclecol`=@listvehiclecol, `vehicle_extracol`=@listextracol, `vehicle_neoncolor`=@listneoncolor, `vehicle_plateindex`=@plateindex, `vehicle_windowtint`=@windowtint, `vehicle_wheeltype`=@wheeltype WHERE `vehicle_plate`=@plate",
    {['@listmods']=listmods,['@listvehiclecol']=listvehiclecol,['@listextracol']=listextracol,['@listneoncolor']=listneoncolor,['@plateindex']=plateindex,['@windowtint']=windowtint,['@wheeltype']=wheeltype,['@plate']=plate}, function()
        TriggerEvent('concess:initplate', source)
    end)
end)

RegisterServerEvent('vehshop:CheckGarageForVeh')
AddEventHandler('vehshop:CheckGarageForVeh', function()
    vehicles = {}
    local user = getPlayerID(source)
    local source = source
    local result = MySQL.Sync.fetchAll("SELECT * FROM user_vehicle WHERE identifier = @identifier  AND type = 'terrestre'",{['@identifier'] = user})
    for _, v in pairs(result) do
        table.insert(vehicles, v)
    end
    TriggerClientEvent('vehshop:getVehicles', source, vehicles)
end)

RegisterServerEvent('vehshop:SelVeh')
AddEventHandler('vehshop:SelVeh', function(plate,name)
    TriggerEvent('es:getPlayerFromId', source, function(user)
        local source = source
        local player = user.identifier
        local plate = plate
        local vehicle = name
        MySQL.Async.fetchScalar("SELECT vehicle_price FROM user_vehicle WHERE identifier = @username AND vehicle_plate =@plate",{['@username'] = player, ['@plate'] = plate}, function(price)    
            user.addMoney((price / 2))
            MySQL.Async.insert("DELETE from user_vehicle WHERE identifier = @username AND vehicle_plate = @plate ",{['@username'] = player,  ['@plate'] = plate})
            TriggerClientEvent("es_freeroam:notify", source, "CHAR_SIMEON", 1, "Simeon", false, "Véhicule vendu!\n")
        end)
    end)
end)

-- get's the player id without having to use bugged essentials
function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

-- gets the actual player id unique to the player,
-- independent of whether the player changes their screen name
function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end
