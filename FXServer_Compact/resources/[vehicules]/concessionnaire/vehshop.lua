--[[Register]]--

RegisterNetEvent('FinishMoneyCheckForVeh')
RegisterNetEvent('vehshop:spawnVehicle')



--[[Local/Global]]--

local vehshop = {
		["vehicles"] = {
			title = "Catégories",
			prev = "main",
			buttons = {
				{name = "Compacts", arg = "compacts"},
				{name = "Coupés", arg = 'coupes'},
				{name = "Berlines", arg = 'sedans'},
				{name = "Sportives", arg = 'sports'},
				{name = "Classics", arg = 'sportsclassics'},
				{name = "Super Sportives", arg = 'super'},
				{name = "Muscle", arg = 'muscle'},
				{name = "Off-Road", arg = 'offroad'},
				{name = "SUVs", arg = 'suvs'},
				{name = "Vans", arg = 'vans'},
			}
		},
		["compacts"] = {
			title = "Compacts",
			prev = "vehicles",
			buttons = {
                {name = "Blista", costs = 4000, arg = 'achat', model = "blista"},
                {name = "Blista Allongée", costs = 4000, arg = 'achat', model = "Blista2"},
                {name = "Brioso R/A", costs = 155000, arg = 'achat', model = "brioso"},
                {name = "Dilettante", costs = 25000, arg = 'achat', model = "Dilettante"},
                {name = "Issi", costs = 18000, arg = 'achat', model = "issi2"},
                {name = "Issi classic", costs = 360000, arg = 'achat', model = "issi3"},
                {name = "Panto", costs = 85000, arg = 'achat', model = "panto"},
                {name = "Prairie", costs = 30000, arg = 'achat', model = "prairie"},
                {name = "Rhapsody", costs = 120000, arg = 'achat', model = "rhapsody"},
			}
		},
		["coupes"] = {
			title = "Coupés",
			prev = "vehicles",
			buttons = {
				{name = "Exemplar", costs = 200000, arg = 'achat', model = "exemplar"},
				{name = "F620", costs = 80000, arg = 'achat', model = "f620"},
				{name = "Felon", costs = 90000, arg = 'achat', model = "felon"},
				{name = "Felon GT", costs = 95000, arg = 'achat', model = "felon2"},
				{name = "Jackal", costs = 60000, arg = 'achat', model = "jackal"},
				{name = "Oracle", costs = 80000, arg = 'achat', model = "oracle"},
				{name = "Oracle XS", costs = 82000, arg = 'achat', model = "oracle2"},
				{name = "Sentinel", costs = 90000, arg = 'achat', model = "sentinel"},
				{name = "Sentinel XS", costs = 60000, arg = 'achat', model = "sentinel2"},
				{name = "Windsor", costs = 800000, arg = 'achat', model = "windsor"},
				{name = "Windsor Drop", costs = 850000, arg = 'achat', model = "windsor2"},
				{name = "Zion", costs = 60000, arg = 'achat', model = "zion"},
				{name = "Zion Cabrio", costs = 65000, arg = 'achat', model = "zion2"},
			}
		},
		["sports"] = {
			title = "Sportive",
			prev = "vehicles",
			buttons = {
		        {name = "9F", costs = 120000, arg = 'achat', model = "ninef"},
                {name = "9F Cabrio", costs = 130000, arg = 'achat', model = "ninef2"},
                {name = "Alpha", costs = 150000, arg = 'achat', model = "alpha"},
                {name = "Banshee", costs = 105000, arg = 'achat', model = "banshee"},
                {name = "Bestia GTS", costs = 610000, arg = 'achat', model = "bestiagts"},
                {name = "Buffalo", costs = 35000, arg = 'achat', model = "buffalo"},
                {name = "Buffalo S", costs = 96000, arg = 'achat', model = "buffalo2"},
                {name = "Buffalo Custom", costs = 196000, arg = 'achat', model = "buffalo3"},
                {name = "Carbonizzare", costs = 195000, arg = 'achat', model = "carbonizzare"},
		        {name = "Coquette", costs = 138000, arg = 'achat', model = "coquette"},
                {name = "Comet", costs = 100000, arg = 'achat', model = "comet2"},
                {name = "CometGT", costs = 350000, arg = 'achat', model = "comet3"},
                {name = "Comet Safari", costs = 710000, arg = 'achat', model = "comet4"},
                {name = "Comet SR", costs = 1145000, arg = 'achat', model = "comet5"},
                {name = "Drift Tampa", costs = 995000, arg = 'achat', model = "tampa2"},
		        {name = "Elegy", costs = 375000, arg = 'achat', model = "Elegy"},
		        {name = "Elegy RH8", costs = 500000, arg = 'achat', model = "Elegy2"},
		        {name = "Feltzer", costs = 130000, arg = 'achat', model = "feltzer2"},
		        {name = "Flash GT", costs = 1675000, arg = 'achat', model = "flashgt"},
                {name = "Furore GT", costs = 448000, arg = 'achat', model = "furoregt"},
 		        {name = "Fusilade", costs = 36000, arg = 'achat', model = "fusilade"},
                {name = "Futo", costs = 37000, arg = 'achat', model = "Futo"},	
				{name = "GB 200", costs = 940000, arg = 'achat', model = "gb200"},	
                {name = "Hotring Sabre", costs = 830000, arg = 'achat', model = "hotring"},		
		        {name = "Jester", costs = 240000, arg = 'achat', model = "jester"},
                {name = "Jester(Racecar)", costs = 350000, arg = 'achat', model = "jester2"},
                {name = "Khamelion", costs = 100000, arg = 'achat', model = "khamelion"},
                {name = "Kuruma", costs = 95000, arg = 'achat', model = "kuruma"},
                {name = "Lynx", costs = 1735000, arg = 'achat', model = "lynx"},
                {name = "Massacro", costs = 275000, arg = 'achat', model = "massacro"},
                {name = "Massacro(Racecar)", costs = 385000, arg = 'achat', model = "massacro2"},
                {name = "Michelli GT", costs = 1225000, arg = 'achat', model = "michelli"},
                {name = "Neon", costs = 650000, arg = 'achat', model = "neon"},
                {name = "Omnis", costs = 701000, arg = 'achat', model = "omnis"},
                {name = "Penumbra", costs = 24000, arg = 'achat', model = "penumbra"},
                {name = "Pariah", costs = 200000, arg = 'achat', model = "pariah"},
                {name = "Raiden", costs = 550000, arg = 'achat', model = "raiden"},
                {name = "Rapid GT", costs = 140000, arg = 'achat', model = "rapidgt"},
                {name = "Rapid GT Convertible", costs = 150000, arg = 'achat', model = "rapidgt2"},
                {name = "Revolter", costs = 430000, arg = 'achat', model = "revolter"},
                {name = "Ruston", costs = 430000, arg = 'achat', model = "Ruston"},
                {name = "Schafter V12", costs = 140000, arg = 'achat', model = "schafter3"},
                {name = "Schafter LWB", costs = 208000, arg = 'achat', model = "schafter4"},
                {name = "Schwarzer", costs = 80000, arg = 'achat', model = "Schwarzer"},
                {name = "Sentinel Classic", costs = 650000, arg = 'achat', model = "sentinel3"},
		        {name = "Seven70", costs = 500000, arg = 'achat', model = "Seven70"},
		        {name = "Specter", costs = 590000, arg = 'achat', model = "Specter"},
		        {name = "Specter Custom", costs = 700000, arg = 'achat', model = "Specter2"},
                {name = "Sultan", costs = 12000, arg = 'achat', model = "sultan"},
                {name = "Surano", costs = 110000, arg = 'achat', model = "surano"},	
                {name = "Tropos", costs = 816000, arg = 'achat', model = "tropos"},
                {name = "Verkierer", costs = 695000, arg = 'achat', model = "verlierer2"},	
			}
		},
		["sportsclassics"] = {
			title = "Classics",
			prev = "vehicles",
			buttons = {
                --{name = "Ardent", costs = 225000, arg = 'achat', model = "ardent"},
                {name = "Casco", costs = 680000, arg = 'achat', model = "casco"},
                {name = "Cheburek", costs = 145000, arg = 'achat', model = "cheburek"},
				{name = "Cheetah Classic", costs = 865000, arg = 'achat', model = "Cheetah2"},
				{name = "Coquette Classic", costs = 665000, arg = 'achat', model = "coquette2"},
				{name = "Coquette BlackFin", costs = 695000, arg = 'achat', model = "coquette3"},
				{name = "Fagaloa", costs = 335000, arg = 'achat', model = "fagaloa"},
				{name = "Fränken Stange", costs = 550000, arg = 'achat', model = "btype2"},
				{name = "GB 200", costs = 940000, arg = 'achat', model = "gb200"},
				{name = "GT 500", costs = 150000, arg = 'achat', model = "gt500"},
                {name = "Infernus 2", costs = 625000, arg = 'achat', model = "infernus2"},
				{name = "JB 700", costs = 350000, arg = 'achat', model = "jb700"},
				{name = "Manana", costs = 90000, arg = 'achat', model = "Manana"},
				{name = "Mamba", costs = 995000, arg = 'achat', model = "mamba"},
				{name = "Monroe", costs = 490000, arg = 'achat', model = "monroe"},
               	{name = "Peyote", costs = 84000, arg = 'achat', model = "Peyote"},
				{name = "Pigalle", costs = 400000, arg = 'achat', model = "pigalle"},
                {name = "Rapid GT Classic", costs = 150000, arg = 'achat', model = "rapidgt3"},
				{name = "Radcar", costs = 25000, arg = 'achat', model = "tornado4"},
				{name = "Roosevelt", costs = 450000, arg = 'achat', model = "btype"},
				{name = "Roosevelt Valor", costs = 982000, arg = 'achat', model = "btype3"},
				{name = "Stinger", costs = 850000, arg = 'achat', model = "stinger"},
				{name = "Stinger GT", costs = 875000, arg = 'achat', model = "stingergt"},
				{name = "Stirling GT", costs = 975000, arg = 'achat', model = "feltzer3"},
				{name = "Torero", costs = 900000, arg = 'achat', model = "Torero"},
				{name = "Tornado", costs = 30000, arg = 'achat', model = "tornado"},
                {name = "Tornado Classic", costs = 40000, arg = 'achat', model = "tornado2"},
				{name = "Tornado Custom", costs = 375000, arg = 'achat', model = "tornado5"},
				{name = "Viseris", costs = 75000, arg = 'achat', model = "viseris"},
				{name = "Z-Type", costs = 950000, arg = 'achat', model = "ztype"},
				}
		},
		["super"] = {
			title = "Super Sportives",
			prev = "vehicles",
			buttons = {
                {name = "811", costs = 1135000, arg = 'achat', model = "pfister811"},
				{name = "Adder", costs = 1000000, arg = 'achat', model = "adder"},
				{name = "Autarch", costs = 700000, arg = 'achat', model = "autarch"},
				{name = "Banshee 900R", costs = 565000, arg = 'achat', model = "banshee2"},
				{name = "Bullet", costs = 155000, arg = 'achat', model = "bullet"},
				{name = "Cheetah", costs = 650000, arg = 'achat', model = "cheetah"},
				{name = "Cyclone", costs = 1550000, arg = 'achat', model = "cyclone"},
				{name = "Entity XF", costs = 795000, arg = 'achat', model = "entityxf"},
				{name = "Entity XXR", costs = 2305000, arg = 'achat', model = "entity2"},
				{name = "ETR1", costs = 1995000, arg = 'achat', model = "sheava"},
				{name = "FMJ", costs = 1750000, arg = 'achat', model = "fmj"},
				{name = "GP1", costs = 1260000, arg = 'achat', model = "GP1"},
				{name = "Infernus", costs = 440000, arg = 'achat', model = "infernus"},
				{name = "Itali GTB", costs = 1189000, arg = 'achat', model = "Italigtb"},
                {name = "Itali Custom", costs = 1285000, arg = 'achat', model = "Italigtb2"},
				{name = "Nero", costs = 1440000, arg = 'achat', model = "Nero"},
                {name = "Nero Custom", costs = 1768000, arg = 'achat', model = "Nero2"},
				{name = "Osiris", costs = 1950000, arg = 'achat', model = "osiris"},
				{name = "Penetrator", costs = 880000, arg = 'achat', model = "Penetrator"},
				{name = "RE-7B", costs = 2475000, arg = 'achat', model = "le7b"},
				{name = "Reaper", costs = 1595000, arg = 'achat', model = "reaper"},
				{name = "Sc1", costs = 950000, arg = 'achat', model = "sc1"},
				{name = "Sultan RS", costs = 795000, arg = 'achat', model = "sultanrs"},
				{name = "T20", costs = 2200000, arg = 'achat', model = "t20"},
				{name = "Taipan", costs = 1980000, arg = 'achat', model = "taipan"},
                {name = "Tempesta", costs = 975000, arg = 'achat', model = "Tempesta"},
                {name = "Tezeract", costs = 2825000, arg = 'achat', model = "Tezeract"},
				{name = "Turismo R", costs = 500000, arg = 'achat', model = "turismor"},
				{name = "TurismoC", costs = 1275000, arg = 'achat', model = "Turismo2"},
				{name = "Tyrant", costs = 2515000, arg = 'achat', model = "Tyrant"},
				{name = "Tyrus", costs = 2550000, arg = 'achat', model = "tyrus"},
				{name = "Vacca", costs = 240000, arg = 'achat', model = "vacca"},
                {name = "Vagner", costs = 1535000, arg = 'achat', model = "Vagner"},
                {name = "Visione", costs = 2000000, arg = 'achat', model = "visione"},
				{name = "Voltic", costs = 150000, arg = 'achat', model = "voltic"},
				{name = "X80 Proto", costs = 2700000, arg = 'achat', model = "prototipo"},
                {name = "XA21", costs = 2375000, arg = 'achat', model = "XA21"},
				{name = "Zentorno", costs = 725000, arg = 'achat', model = "zentorno"},
			}
		},
		["muscle"] = {
			title = "Muscles",
			prev = "vehicles",
			buttons = {
                {name = "190 Z", costs = 650000, arg = 'achat', model = "z190"},
		        {name = "Blade", costs = 160000, arg = 'achat', model = "blade"},
		        {name = "Buccaneer", costs = 29000, arg = 'achat', model = "buccaneer"},
                {name = "Buccaneer Custom", costs = 129000, arg = 'achat', model = "buccaneer2"},
		        {name = "Chino", costs = 225000, arg = 'achat', model = "chino"},
                {name = "Chino Custom", costs = 325000, arg = 'achat', model = "chino2"},
		        {name = "Dominator", costs = 35000, arg = 'achat', model = "dominator"},
                {name = "Dominator Custom", costs = 150000, arg = 'achat', model = "dominator2"},
                {name = "Dominator GTX", costs = 725000, arg = 'achat', model = "dominator3"},
		        {name = "Dukes", costs = 62000, arg = 'achat', model = "dukes"},
		        {name = "Ellie", costs = 565000, arg = 'achat', model = "ellie"},
		        {name = "Faction", costs = 36000, arg = 'achat', model = "faction"},
                {name = "Faction Custom", costs = 130000, arg = 'achat', model = "faction2"},
                {name = "FactionXXL", costs = 300000, arg = 'achat', model = "faction3"},
		        {name = "Gauntlet", costs = 32000, arg = 'achat', model = "gauntlet"},
                {name = "Gauntlet Custom", costs = 132000, arg = 'achat', model = "gauntlet2"},
		        {name = "Hermes", costs = 290000, arg = 'achat', model = "hermes"},
		        {name = "Hotknife", costs = 90000, arg = 'achat', model = "hotknife"},
		        {name = "Hustler", costs = 100000, arg = 'achat', model = "hustler"},
		        {name = "Lurcher", costs = 215000, arg = 'achat', model = "Lurcher"},
                {name = "Moonbeam", costs = 32000, arg = 'achat', model = "Moonbeam"},
                {name = "Moonbeam Custom", costs = 132000, arg = 'achat', model = "Moonbeam2"},
		        {name = "Nightshade", costs = 585000, arg = 'achat', model = "nightshade"},
                {name = "Phoenix", costs = 56000, arg = 'achat', model = "Phoenix"},
		        {name = "Picador", costs = 9000, arg = 'achat', model = "picador"},
                {name = "RatLoader", costs = 26000, arg = 'achat', model = "RatLoader"},
                {name = "RatLoader Custom", costs = 126000, arg = 'achat', model = "RatLoader2"},
                {name = "Ruiner", costs = 12000, arg = 'achat', model = "Ruiner"},
		        {name = "SabreGT", costs = 15000, arg = 'achat', model = "sabregt"},
                {name = "SabreTurbo", costs = 112000, arg = 'achat', model = "sabregt2"},
                {name = "SlamVan", costs = 75000, arg = 'achat', model = "SlamVan"},
                {name = "SlamVan Custom", costs = 135000, arg = 'achat', model = "SlamVan2"},
                {name = "SlamVanGT", costs = 117000, arg = 'achat', model = "SlamVan3"},
                {name = "Stalion", costs = 43000, arg = 'achat', model = "Stalion"},
                {name = "Stalion Custom", costs = 87000, arg = 'achat', model = "Stalion2"},
		        {name = "Tampa", costs = 375000, arg = 'achat', model = "tampa"},
		        {name = "Vigero", costs = 21000, arg = 'achat', model = "vigero"},
		        {name = "Virgo", costs = 195000, arg = 'achat', model = "virgo"},
                {name = "Virgo Custom", costs = 295000, arg = 'achat', model = "virgo2"},
                {name = "VirgoGT", costs = 156000, arg = 'achat', model = "virgo3"},
                {name = "Voodoo", costs = 56000, arg = 'achat', model = "Voodoo2"},
                {name = "Voodoo Custom", costs = 124000, arg = 'achat', model = "Voodoo"},
			}
		},
		["offroad"] = {
			title = "Off-road",
			prev = "vehicles",
			buttons = {
                {name = "BfInjection", costs = 16000, arg = 'achat', model = "BfInjection"},
				{name = "Blazer", costs = 75000, arg = 'achat', model = "blazer"},
				{name = "Bifta", costs = 75000, arg = 'achat', model = "bifta"},
                {name = "Bodhi2", costs = 12500, arg = 'achat', model = "Bodhi2"},
				{name = "Brawler", costs = 715000, arg = 'achat', model = "brawler"},
				--{name = "Caracara", costs = 1775000, arg = 'achat', model = "caracara"},
                {name = "DLoader", costs = 23500, arg = 'achat', model = "DLoader"},
				{name = "Dune Buggy", costs = 20000, arg = 'achat', model = "dune"},
                {name = "Hot Rod Blazer", costs = 34500, arg = 'achat', model = "blazer3"},
                {name = "Kalahari", costs = 34500, arg = 'achat', model = "Kalahari"},
                {name = "Kamacho", costs = 345000, arg = 'achat', model = "kamacho"},
                {name = "Marshall", costs = 600000, arg = 'achat', model = "Marshall"},
                {name = "Mesa", costs = 20000, arg = 'achat', model = "mesa"},
                {name = "Mesa Safari", costs = 75000, arg = 'achat', model = "Mesa3"},
                {name = "RancherXL", costs = 39000, arg = 'achat', model = "RancherXL"},
                {name = "Rebel", costs = 17500, arg = 'achat', model = "rebel"},
				{name = "Rebel Custom", costs = 22000, arg = 'achat', model = "rebel2"},
				{name = "Riata", costs = 220000, arg = 'achat', model = "riata"},
				{name = "Sandking", costs = 38000, arg = 'achat', model = "sandking"},
				{name = "Street Blazer", costs = 75000, arg = 'achat', model = "blazer4"},
                {name = "The Liberator", costs = 550000, arg = 'achat', model = "monster"},
				{name = "Trophy Truck", costs = 550000, arg = 'achat', model = "trophytruck"},
                {name = "Trophy Truck Custom", costs = 625000, arg = 'achat', model = "trophytruck2"},
			}
		},
		["suvs"] = {
			title = "SUVs",
			prev = "vehicles",
			buttons = {
                {name = "BJXL", costs = 58000, arg = 'achat', model = "BJXL"},
				{name = "Baller 2005", costs = 80000, arg = 'achat', model = "baller"},
				{name = "Baller 2013", costs = 90000, arg = 'achat', model = "baller2"},
				{name = "Baller LE", costs = 149000, arg = 'achat', model = "baller3"},
				{name = "Baller LE LWB", costs = 247000, arg = 'achat', model = "baller4"},
				{name = "Cavalcade 2005", costs = 60000, arg = 'achat', model = "cavalcade"},
				{name = "Cavalcade 2013", costs = 70000, arg = 'achat', model = "cavalcade2"},
                {name = "Contender", costs = 250000, arg = 'achat', model = "Contender"},
                {name = "Dubsta", costs = 155000, arg = 'achat', model = "Dubsta"},
                {name = "Dubsta 6x6", costs = 249000, arg = 'achat', model = "dubsta3"},
                {name = "Dubsta Custom", costs = 213000, arg = 'achat', model = "Dubsta2"},
                {name = "Fathom FQ2", costs = 45000, arg = 'achat', model = "FQ2"},
				{name = "Granger", costs = 35000, arg = 'achat', model = "granger"},
                {name = "Gresley", costs = 20000, arg = 'achat', model = "Gresley"},
                {name = "Habanero", costs = 75000, arg = 'achat', model = "Habanero"},
				{name = "Huntley S", costs = 195000, arg = 'achat', model = "huntley"},
				{name = "Landstalker", costs = 58000, arg = 'achat', model = "landstalker"},
                {name = "Patriot", costs = 53000, arg = 'achat', model = "Patriot"},
				{name = "Radius", costs = 32000, arg = 'achat', model = "radi"},
				{name = "Rocoto", costs = 85000, arg = 'achat', model = "rocoto"},
				{name = "Seminole", costs = 30000, arg = 'achat', model = "seminole"},
                {name = "Serrano", costs = 135000, arg = 'achat', model = "Serrano"},
				{name = "XLS", costs = 253000, arg = 'achat', model = "xls"},	
			}
		},
		["vans"] = {
			title = "Vans",
			prev = "vehicles",
			buttons = {
				{name = "Bison", costs = 30000, arg = 'achat', model = "bison"},
                {name = "Bison Safari", costs = 45000, arg = 'achat', model = "bison3"},
				{name = "Bobcat XL", costs = 23000, arg = 'achat', model = "bobcatxl"},
                {name = "Burrito", costs = 42000, arg = 'achat', model = "burrito3"},
                {name = "Camper", costs = 30000, arg = 'achat', model = "Camper"},
				{name = "Gang Burrito", costs = 65000, arg = 'achat', model = "gburrito"},
				{name = "Journey", costs = 15000, arg = 'achat', model = "journey"},
				{name = "Minivan", costs = 30000, arg = 'achat', model = "minivan"},
                {name = "Minivan Custom", costs = 130000, arg = 'achat', model = "minivan2"},
				{name = "Paradise", costs = 25000, arg = 'achat', model = "paradise"},
                {name = "Pony2", costs = 56000, arg = 'achat', model = "Pony2"},
				{name = "Rumpo", costs = 13000, arg = 'achat', model = "rumpo"},
                {name = "Rumpo Custom", costs = 34000, arg = 'achat', model = "rumpo3"},
                {name = "Speedo", costs = 35000, arg = 'achat', model = "Speedo"},
                {name = "Speedo Clown", costs = 86000, arg = 'achat', model = "Speedo2"},
				{name = "Surfer", costs = 11000, arg = 'achat', model = "surfer"},
				{name = "Youga", costs = 16000, arg = 'achat', model = "youga"},
				{name = "Youga Classic", costs = 195000, arg = 'achat', model = "youga2"},
			}
		},
		["sedans"] = {
			title = "Berlines",
			prev = "vehicles",
			buttons = {
				{name = "Asea", costs = 100000, arg = 'achat', model = "asea"},
				{name = "Asterope", costs = 100000, arg = 'achat', model = "asterope"},
                {name = "Cog55", costs = 154000, arg = 'achat', model = "Cog55"},
                {name = "Cognoscenti Cabrio", costs = 180000, arg = 'achat', model = "cogcabrio"},
                {name = "Emperor", costs = 56000, arg = 'achat', model = "Emperor"},
				{name = "Fugitive", costs = 24000, arg = 'achat', model = "fugitive"},
				{name = "Glendale", costs = 200000, arg = 'achat', model = "glendale"},
				{name = "Ingot", costs = 9000, arg = 'achat', model = "ingot"},
				{name = "Intruder", costs = 16000, arg = 'achat', model = "intruder"},
				{name = "Premier", costs = 10000, arg = 'achat', model = "premier"},
				{name = "Primo", costs = 9000, arg = 'achat', model = "primo"},
				{name = "Primo  Custom", costs = 9500, arg = 'achat', model = "primo2"},
				{name = "Regina", costs = 8000, arg = 'achat', model = "regina"},
				{name = "Schafter", costs = 65000, arg = 'achat', model = "schafter2"},
				{name = "Stanier", costs = 10000, arg = 'achat', model = "stanier"},
				{name = "Stratum", costs = 10000, arg = 'achat', model = "stratum"},
				{name = "Stretch", costs = 30000, arg = 'achat', model = "stretch"},
				{name = "Surge", costs = 38000, arg = 'achat', model = "surge"},
               	{name = "Super Diamond", costs = 250000, arg = 'achat', model = "superd"},
				{name = "Tailgater", costs = 55000, arg = 'achat', model = "tailgater"},
				{name = "Warrener", costs = 120000, arg = 'achat', model = "warrener"},
				{name = "Washington", costs = 15000, arg = 'achat', model = "washington"},
			}
		},
		["motorcycles"] = {
			title = "Motos",
			prev = "vehicles",
			buttons = {
				{name = "Akuma", costs = 18000, arg = 'achat', model = "AKUMA"},
				{name = "Avarus", costs = 250000, arg = 'achat', model = "Avarus"},
				{name = "Bagger", costs = 10000, arg = 'achat', model = "bagger"},
				{name = "Bati 801", costs = 30000, arg = 'achat', model = "bati"},
				{name = "Bati 801RR", costs = 30000, arg = 'achat', model = "bati2"},
				{name = "BF400", costs = 190000, arg = 'achat', model = "bf400"},
				{name = "Carbon RS", costs = 80000, arg = 'achat', model = "carbonrs"},
				{name = "Cliffhanger", costs = 450000, arg = 'achat', model = "cliffhanger"},
				{name = "Chimera", costs = 1000000, arg = 'achat', model = "chimera"},
                {name = "Daemon", costs = 10000, arg = 'achat', model = "daemon2"},
				{name = "Daemon Custom", costs = 10000, arg = 'achat', model = "daemon"},
                {name = "Defiler", costs = 70000, arg = 'achat', model = "Defiler"},
                {name = "Diabolus", costs = 170000, arg = 'achat', model = "diablous"},
                {name = "Diabolus Custom", costs = 180000, arg = 'achat', model = "diablous2"},
				{name = "Double T", costs = 24000, arg = 'achat', model = "double"},
				{name = "Enduro", costs = 96000, arg = 'achat', model = "enduro"},
                {name = "Esskey", costs = 205000, arg = 'achat', model = "Esskey"},
				{name = "Faggio", costs = 8000, arg = 'achat', model = "faggio2"},
                {name = "Faggio 102", costs = 10000, arg = 'achat', model = "faggio"},
                {name = "Faggio Custom", costs = 15000, arg = 'achat', model = "faggio3"},
                {name = "Fcr", costs = 135000, arg = 'achat', model = "Fcr"},
                {name = "Fcr Custom", costs = 135000, arg = 'achat', model = "Fcr2"},
				{name = "Gargoyle", costs = 240000, arg = 'achat', model = "gargoyle"},
				{name = "Hakuchou", costs = 164000, arg = 'achat', model = "hakuchou"},
                {name = "Hakuchou Drag", costs = 245000, arg = 'achat', model = "hakuchou2"},
				{name = "Hexer", costs = 30000, arg = 'achat', model = "hexer"},
				{name = "Innovation", costs = 180000, arg = 'achat', model = "innovation"},
				{name = "Lectro", costs = 1400000, arg = 'achat', model = "lectro"},
                {name = "Manchez", costs = 67000, arg = 'achat', model = "Manchez"},
				{name = "Nemesis", costs = 24000, arg = 'achat', model = "nemesis"},
                {name = "Nightblade", costs = 100000, arg = 'achat', model = "Nightblade"},
				{name = "PCJ-600", costs = 18000, arg = 'achat', model = "pcj"},
                {name = "Rat Bike", costs = 40000, arg = 'achat', model = "RatBike"},
				{name = "Ruffian", costs = 18000, arg = 'achat', model = "ruffian"},
                {name = "Sanchez", costs = 14000, arg = 'achat', model = "sanchez2"},
				{name = "Sanchez Custom", costs = 14000, arg = 'achat', model = "sanchez"},
				{name = "Sanctus", costs = 2000000, arg = 'achat', model = "sanctus"},
				{name = "Shotaro", costs = 2500000, arg = 'achat', model = "shotaro"},
				{name = "Sovereign", costs = 180000, arg = 'achat', model = "sovereign"},
				{name = "Thrust", costs = 150000, arg = 'achat', model = "thrust"},
				{name = "Vader", costs = 18000, arg = 'achat', model = "vader"},
				{name = "Vindicator", costs = 1200000, arg = 'achat', model = "vindicator"},
                {name = "Vortex", costs = 356000, arg = 'achat', model = "Vortex"},
				{name = "Wolfsbane", costs = 95000, arg = 'achat', model = "Wolfsbane"},
                {name = "Zombie", costs = 45000, arg = 'achat', model = "Zombiea"},
                {name = "ZombieChopper", costs = 220000, arg = 'achat', model = "Zombieb"},
			}
		},
        ["bicycles"] = {
			title = "Vélos",
			prev = "vehicles",
			buttons = {
            {name = "Bmx", costs = 1000, arg = 'achat', model = "Bmx"},
            {name = "Cruiser", costs = 1200, arg = 'achat', model = "Cruiser"},
            {name = "Endurex Race Bike", costs = 1200, arg = 'achat', model = "tribike2"},
            {name = "Fixter", costs = 2500, arg = 'achat', model = "Fixter"},
            {name = "Scorcher", costs = 3500, arg = 'achat', model = "Scorcher"},
            {name = "Tri-Cycle Race Bike", costs = 5000, arg = 'achat', model = "tribike3"},
            {name = "Whippet race bike", costs = 5000, arg = 'achat', model = "TriBike"},
            }
        },
	}

local fakecar = {model = '', car = nil}
local vehshop_locations = {{entering = {-33.803,-1102.322,25.422}, inside = {-46.56327,-1097.382,25.99875, 120.1953}, outside = {-31.849,-1090.648,25.998,322.345}}}
local vehshop_blips ={}
local inrangeofvehshop = false
local currentlocation = nil
local boughtcar = false
local vehicle_price = 0
local backlock = false
local firstspawn = 0

--[[Functions]]--

function LocalPed()
	return GetPlayerPed(-1)
end

function ShowVehshopBlips(bool)
	if bool and #vehshop_blips == 0 then
		for station,pos in pairs(vehshop_locations) do
			local loc = pos
			pos = pos.entering
			local blip = AddBlipForCoord(pos[1],pos[2],pos[3])
			-- 60 58 137
			SetBlipSprite(blip,326)
			BeginTextCommandSetBlipName("STRING")
			AddTextComponentString('Concessionnaire')
			EndTextCommandSetBlipName(blip)
			SetBlipAsShortRange(blip,true)
			SetBlipAsMissionCreatorBlip(blip,true)
			table.insert(vehshop_blips, {blip = blip, pos = loc})
		end
	elseif bool == false and #vehshop_blips > 0 then
		for i,b in ipairs(vehshop_blips) do
			if DoesBlipExist(b.blip) then
				SetBlipAsMissionCreatorBlip(b.blip,false)
				Citizen.InvokeNative(0x86A652570E5F25DD, Citizen.PointerValueIntInitialized(b.blip))
			end
		end
		vehshop_blips = {}
	end
end

function firstToUpper(str)
    return (string.gsub(str,"^%l", string.upper))
end

function OpenCreator()
    if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
	boughtcar = false
	local ped = LocalPed()
	local pos = currentlocation.pos.inside
	FreezeEntityPosition(ped,true)
	SetEntityVisible(ped,false)
	local g = Citizen.InvokeNative(0xC906A7DAB05C8D2B,pos[1],pos[2],pos[3],Citizen.PointerValueFloat(),0)
	SetEntityCoords(ped,pos[1],pos[2],g)
	SetEntityHeading(ped,pos[4])
    VMenu.curItem = 1
    MainMenu()
    VMenu.visible = true  -- Hide/Show the menu-- Hide/Show the menu
end

function MainMenu()
    VMenu.curItem = 1
    VMenu.ResetMenu()
    VMenu.EditHeader('Concessionnaire')
    VMenu.AddFunc("Véhicules", "menuveh", "vehicles","Accéder")
    VMenu.AddFunc("Motos", "menuveh", "motorcycles","Accéder")
    VMenu.AddFunc("Vélos", "menuveh", "bicycles","Accéder")
end   

function menuveh(info)
    UpdateSkin_back()
    VMenu.curItem = 1
    VMenu.ResetMenu()
    local table = vehshop[tostring(info)]
    VMenu.EditHeader(table.title)
    if table.buttons[1].costs ~= nil and info ~= "motorcycles" and info ~= "bicycles" then
        VMenu.AddPrev('menuveh',table.prev)
    else
        VMenu.AddPrev('MainMenu')
    end
    for a,v in pairs(table.buttons) do
        if v.arg ~= 'achat' then
            VMenu.AddFunc(v.name,"menuveh",v.arg,"Accéder")
        else
            VMenu.AddFunc(v.name,'achat',v,"Acheter",v.costs)
        end
    end
end

function CloseCreator(name, veh, price,plaque)
	Citizen.CreateThread(function()
		local ped = LocalPed()
		if not boughtcar then
			local pos = currentlocation.pos.entering
            VMenu.visible = false
            UpdateSkin_back()
			SetEntityCoords(ped,pos[1],pos[2],pos[3])
			FreezeEntityPosition(ped,false)
			SetEntityVisible(ped,true)
		else			
			local name = name	
			local vehicle = veh
			local price = price		
			local veh = GetVehiclePedIsUsing(ped)
			local model = GetEntityModel(veh)
			local colors = table.pack(GetVehicleColours(veh))
			local extra_colors = table.pack(GetVehicleExtraColours(veh))

			local mods = {}
			for i = 0,24 do
				mods[i] = GetVehicleMod(veh,i)
			end
			Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(veh))
			local pos = currentlocation.pos.outside

			FreezeEntityPosition(ped,false)
			RequestModel(model)
			while not HasModelLoaded(model) do
				Citizen.Wait(0)
			end
			personalvehicle = CreateVehicle(model,pos[1],pos[2],pos[3],pos[4],true,false)
			SetModelAsNoLongerNeeded(model)
			for i,mod in pairs(mods) do
				SetVehicleModKit(personalvehicle,0)
				SetVehicleMod(personalvehicle,i,mod)
			end
			SetVehicleOnGroundProperly(personalvehicle)
            SetVehicleNumberPlateText(personalvehicle, plaque)
			local plate = GetVehicleNumberPlateText(personalvehicle)
			SetVehicleHasBeenOwnedByPlayer(personalvehicle,true)
			local id = NetworkGetNetworkIdFromEntity(personalvehicle)
			SetNetworkIdCanMigrate(id, true)
			Citizen.InvokeNative(0x629BFA74418D6239,Citizen.PointerValueIntInitialized(personalvehicle))
			SetVehicleColours(personalvehicle,colors[1],colors[2])
			SetVehicleExtraColours(personalvehicle,extra_colors[1],extra_colors[2])
			TaskWarpPedIntoVehicle(GetPlayerPed(-1),personalvehicle,-1)
            SetVehicleDirtLevel(veh)
            SetVehicleTyresCanBurst(veh,true)
			SetEntityVisible(ped,true)			
			local primarycolor = colors[1]
			local secondarycolor = colors[2]	
			local pearlescentcolor = extra_colors[1]
			local wheelcolor = extra_colors[2]
			TriggerServerEvent('BuyForVeh', name, vehicle, price, plate, primarycolor, secondarycolor, pearlescentcolor, wheelcolor)
            ------------------------------------
            -- SAUVEGARDE DES INFOS VEHICULES --
            ------------------------------------
            local vehiclecol = {}
            local extracol = {}
            local wheeltype = nil
            local neoncolor = nil
            local plateindex = nil
            local windowtint = nil
            local mods = {
            [0] = { mod = nil },
            [1] = { mod = nil },
            [2] = { mod = nil },
            [3] = { mod = nil },
            [4] = { mod = nil },
            [5] = { mod = nil },
            [6] = { mod = nil },
            [7] = { mod = nil },
            [8] = { mod = nil },
            [9] = { mod = nil },
            [10] = { mod = nil },
            [11] = { mod = nil },
            [12] = { mod = nil },
            [13] = { mod = nil },
            [14] = { mod = nil },
            [15] = { mod = nil },
            [16] = { mod = nil },
            [23] = { mod = nil },
            [24] = { mod = nil },
            }
        -----------------------------------------
        -- Récupératuion des infos du véhicule --
        -----------------------------------------
        vehiclecol = table.pack(GetVehicleColours(personalvehicle))
		extracol = table.pack(GetVehicleExtraColours(personalvehicle))
		neoncolor = table.pack(GetVehicleNeonLightsColour(personalvehicle))
        tyresmoke = table.pack(GetVehicleTyreSmokeColor(personalvehicle))
		plateindex = GetVehicleNumberPlateTextIndex(personalvehicle)
		for i,t in pairs(mods) do 
		  t.mod = GetVehicleMod(personalvehicle,i)
		end
        local listmods =''
        for i=0,16 do
            if mods[i].mod ~= nil or mods[i].mod ~= false then
                listmods = listmods..mods[i].mod
                listmods = listmods..','
            else 
                listmods = listmods..'0'
                listmods = listmods..','
            end
        end
        for i=23,24 do
            if mods[i].mod ~= nil or mods[i].mod ~= false then
                listmods = listmods..mods[i].mod
                listmods = listmods..','
            else 
                listmods = listmods..'0'
                listmods = listmods..','
            end
        end
        windowtint = GetVehicleWindowTint(personalvehicle)
        wheeltype = GetVehicleWheelType(personalvehicle)
        TriggerServerEvent('vehshop:Saveinfovehicle',plate,vehiclecol,extracol,neoncolor,plateindex,windowtint,wheeltype,listmods,tyresmoke)
        ---------------------
        -- Fin Récupératon --
        ---------------------
		end
		VMenu.visible = false
	end)
end


function DoesPlayerHaveVehicle(model,button,y,selected)
		local t = false
		--TODO:check if player own car
		if t then
			drawMenuRight("OWNED",vehshop.menu.x,y,selected)
		else
			drawMenuRight(button.costs.."$",vehshop.menu.x,y,selected)
		end
end

function UpdateSkin_valid()
    local func = VMenu.items[VMenu.curItem].func
    local button = VMenu.items[VMenu.curItem].params
    if func == 'achat' then
        UpdateCar(button)
    end
end

function achat(button)
    TriggerServerEvent('CheckMoneyForVeh',button.name, button.model, button.costs)
end

function UpdateSkin_back()
    if DoesEntityExist(fakecar.car) then
        SetEntityVisible(fakecar.car, false, 0)
        SetEntityCoords(fakecar.car, 999999.0, 999999.0, 999999.0, false, false, false, true)
        FreezeEntityPosition(fakecar.car, true)
        SetEntityAsMissionEntity(fakecar.car, 1, 1)
        DeleteVehicle(fakecar.car)
    end
	fakecar = {model = '', car = nil}
end

function UpdateSkin_down()
    local func
    local arg
    if VMenu.items[VMenu.curItem] ~= nil then
        func = VMenu.items[VMenu.curItem].func
        arg = VMenu.items[VMenu.curItem].params
    else
        local i = 1
        func = VMenu.items[i].func
        arg = VMenu.items[i].params
        while func == nil or arg == nil do
            i = i + 1
            func = VMenu.items[i].func
            arg = VMenu.items[i].params
        end                
    end
    if func == 'achat' then
        UpdateCar(arg)
    end
end

function UpdateSkin_up()
    local func = VMenu.items[VMenu.curItem].func
    local arg = VMenu.items[VMenu.curItem].params
    if func == 'achat' then
        UpdateCar(arg)
    end
end

function UpdateCar(button)
    if fakecar.model ~= button.model then
	   if DoesEntityExist(fakecar.car) then
			Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(fakecar.car))
		end
		local pos = currentlocation.pos.inside
        local hash = GetHashKey(button.model)
		RequestModel(hash)
		while not HasModelLoaded(hash) do
			Citizen.Wait(0)
        end
        local veh = CreateVehicle(hash,pos[1],pos[2],pos[3],pos[4],false,false)
		while not DoesEntityExist(veh) do
			Citizen.Wait(0)
		end
        FreezeEntityPosition(veh,true)
		SetEntityInvincible(veh,true)
		SetVehicleDoorsLocked(veh,4)
		--SetEntityCollision(veh,false,false)
        SetVehicleDirtLevel(veh)
		TaskWarpPedIntoVehicle(LocalPed(),veh,-1)
		for i = 0,24 do
			SetVehicleModKit(veh,0)
			RemoveVehicleMod(veh,i)
		end
        fakecar = { model = button.model, car = veh}
    end
end
--[[Citizen]]--

Citizen.CreateThread(function()
	local inrange = false
    local BoucleLent = 5000
    local BoucleCourt = 5
    local TempsBoucle = BoucleCourt
	while true do
		Citizen.Wait(TempsBoucle)
        TempsBoucle = BoucleLent
        if inrange then
            if IsControlJustPressed(1,51) then
                if VMenu.visible then
                    CloseCreator()
                else
                    OpenCreator()
                end
            end
        end
        
        if VMenu.visible == false then
		    inrange = false
        end
		for i,b in ipairs(vehshop_blips) do
			if Vdist(GetEntityCoords(LocalPed()),b.pos.entering[1],b.pos.entering[2],b.pos.entering[3]) < 20.0 then		
                TempsBoucle = BoucleCourt
                Marker(b.pos.entering[1],b.pos.entering[2],b.pos.entering[3],1.5,0,155,255)
                if Vdist(GetEntityCoords(LocalPed()),b.pos.entering[1],b.pos.entering[2],b.pos.entering[3]) < 5.0 then		
                    DisplayHelp("Appuyer sur ~INPUT_CONTEXT~ pour accéder au concessionnaire")
                    currentlocation = b
                    inrange = true
			    end
            end
		end
        VMenu.Show()
	end
end)

AddEventHandler('FinishMoneyCheckForVeh', function(name, vehicle, price,plaque)	
	local name = name
	local vehicle = vehicle
	local price = price
	boughtcar = true
	CloseCreator(name, vehicle, price,plaque)
end)

AddEventHandler('onClientResourceStart', function(res)
    if res == "concessionnaire" then
	   FreezeEntityPosition(GetPlayerPed(-1),false)
	   SetEntityVisible(GetPlayerPed(-1),true)
       VMenu.AddMenu( 'Concessionnaire', 'concessionnaire',255,255,255)
       ShowVehshopBlips(true)
    end
end)


AddEventHandler('vehshop:spawnVehicle', function(v)
	local car = GetHashKey(v)
	local playerPed = GetPlayerPed(-1)
	if playerPed and playerPed ~= -1 then
		RequestModel(car)
		while not HasModelLoaded(car) do
				Citizen.Wait(0)
		end
		local playerCoords = GetEntityCoords(playerPed)
		veh = CreateVehicle(car, playerCoords, 0.0, true, false)
		TaskWarpPedIntoVehicle(playerPed, veh, -1)
		SetEntityInvincible(veh, true)
	end
end)

local firstspawn = 0
AddEventHandler('playerSpawned', function(spawn)
	if firstspawn == 0 then
		RequestIpl('v_carshowroom')
		RequestIpl('shr_int')
		RequestIpl('shutter_closed')
		firstspawn = 1
	end
end)
