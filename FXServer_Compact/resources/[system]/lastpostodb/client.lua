--Version 1.3
RegisterNetEvent('project:notify')
RegisterNetEvent("project:spawnlaspos")

local firstspawn = 0
local loaded = false

--Boucle Thread d'envoie de la position toutes les x secondes vers le serveur pour effectuer la sauvegarde
Citizen.CreateThread(function ()
	while true do
	--Durée entre chaque requêtes : 60000 = 60 secondes
        --Récupération de la position x, y, z du joueur
        LastPosX, LastPosY, LastPosZ = table.unpack(GetEntityCoords(GetPlayerPed(-1), true))
        --Récupération de l'azimut du joueur
        local LastPosH = GetEntityHeading(GetPlayerPed(-1))
        --Envoi des données vers le serveur
        TriggerServerEvent("project:savelastpos", LastPosX , LastPosY , LastPosZ, LastPosH)
        modelhashed = GetHashKey("mp_m_freemode_01")
        RequestModel(modelhashed)
        while not HasModelLoaded(modelhashed) do 
            RequestModel(modelhashed)
            Citizen.Wait(0)
        end
        SetPlayerModel(GetPlayerPed(-1), modelhashed)
        SetPedHeadBlendData(GetPlayerPed(-1), 0, 0, 0, 0, 0, 0,0/100,0/100, 0.0, false)
    SetPedHeadOverlay(GetPlayerPed(-1), 12, 0, 1.0)
    SetPedHeadOverlay(GetPlayerPed(-1), 3, 0, 1.0)
    SetPedHeadOverlay(GetPlayerPed(-1), 0, 0, 1.0)
    SetPedHeadOverlay(GetPlayerPed(-1), 9, 0, 1.0)
    SetPedHeadOverlay(GetPlayerPed(-1), 1, 0, 1.0)
    SetPedHeadOverlayColor(GetPlayerPed(-1), 1, 1, 0, 1)
    SetPedEyeColor(GetPlayerPed(-1),0)
    SetPedHeadOverlay(GetPlayerPed(-1), 7, 0, 1.0)
    SetPedComponentVariation(GetPlayerPed(-1), 2, 0, 0, 0)
    SetPedHairColor(GetPlayerPed(-1), 0, 0)
    SetPedHeadOverlay(GetPlayerPed(-1), 6, 0, 1.0)
    SetPedHeadOverlay(GetPlayerPed(-1), 5, 0, 1.0)
    SetPedHeadOverlayColor(GetPlayerPed(-1), 5, 1, 0, 1)
    SetPedHeadOverlay(GetPlayerPed(-1), 4, 0, 1.0)
    SetPedHeadOverlayColor(GetPlayerPed(-1), 4, 1, 0, 1)
    SetPedHeadOverlay(GetPlayerPed(-1), 10, 0, 1.0)
    SetPedHeadOverlayColor(GetPlayerPed(-1), 10, 1, 0, 1)
    SetPedHeadOverlay(GetPlayerPed(-1), 8, 0, 1.0)
    SetPedHeadOverlayColor(GetPlayerPed(-1), 8, 1, 0, 1)
    SetPedHeadOverlay(GetPlayerPed(-1), 2, 0, 1.0)
    SetPedHeadOverlayColor(GetPlayerPed(-1), 2, 1, 0, 1)
    --SetPedPropIndex(GetPlayerPed(-1),0, tonumber(Skin.Chapeau), tonumber(Skin.ChapeauColor), 0)  
    SetPedPropIndex(GetPlayerPed(-1),1, 0, 0, 0)  
    SetPedPropIndex(GetPlayerPed(-1),2, 0, 0, 0) 
    SetPedPropIndex(GetPlayerPed(-1),6, 0, 0, 0) 
    SetPedPropIndex(GetPlayerPed(-1),7, 0, 0, 0) 
    SetPedComponentVariation(GetPlayerPed(-1), 11, 0, 0, 0) 
    SetPedComponentVariation(GetPlayerPed(-1), 8, 0, 0, 0) 
    SetPedComponentVariation(GetPlayerPed(-1), 3, 0, 0, 0) 
    SetPedComponentVariation(GetPlayerPed(-1), 4, 0, 0, 0) 
    SetPedComponentVariation(GetPlayerPed(-1), 6, 0, 0, 0) 
    SetPedComponentVariation(GetPlayerPed(-1), 7, 0, 0, 0) 
    SetPedComponentVariation(GetPlayerPed(-1), 1, 0, 1, 0)
	Citizen.Wait(300000)
	end
end)

--Event pour le spawn du joueur vers la dernière position connue
AddEventHandler("project:spawnlaspos", function(PosX, PosY, PosZ)
	if not loaded then
		SetEntityCoords(GetPlayerPed(-1), PosX, PosY, PosZ, 1, 0, 0, 1)
        TriggerServerEvent('log:printname',"CONNEXION","viens de se connecter")
		loaded = true
        TriggerEvent('check:weed')
	end
end)

--Action lors du spawn du joueur
AddEventHandler('playerSpawned', function(spawn)
--On verifie que c'est bien le premier spawn du joueur
if firstspawn == 0 then
	TriggerServerEvent("project:SpawnPlayer")
	firstspawn = 1
end
end)