local managedDoor={}--init with armurie door

RegisterServerEvent('doormanager:toogle')
AddEventHandler('doormanager:toogle', function(coord, hashDoor)
    local idDoor=-1
    for i,v in pairs(managedDoor)do
        if v.pos.x==coord.x and v.pos.y==coord.y and v.pos.z==coord.z then
            idDoor=i
            break
        end
    end
    if idDoor ~= -1 then
        managedDoor[idDoor].toogle= not managedDoor[idDoor].toogle
    else
        table.insert(managedDoor,{pos=coord,toogle=true, hash=hashDoor})
    end
    TriggerClientEvent("doormanager:updateAll",-1,managedDoor)
end)

RegisterServerEvent('doormanager:getDoors')
AddEventHandler('doormanager:getDoors', function()
    TriggerClientEvent("doormanager:updateAll",source,managedDoor)
end)