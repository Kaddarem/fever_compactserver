
local distanceParam=2
local managedDoor={}

function getDoorInDirection(managedHash,coord)
  for _,hash in pairs(managedHash)do
    local closeDoor = GetClosestObjectOfType(coord.x, coord.y, coord.z, 1.0, GetHashKey(hash), false, false, false)
    if(closeDoor ~=0) then
      return closeDoor,hash
    end
  end
end

RegisterNetEvent('doormanager:toogleDoorStatus')
AddEventHandler('doormanager:toogleDoorStatus', function(managedHash)
  local targetDoor,hashDoor=getDoorInDirection(managedHash,GetEntityCoords(GetPlayerPed(-1), 1))
  if targetDoor ~=0 then
    local posDoor = GetEntityCoords(targetDoor,false)
    local pos= {x=posDoor.x,y=posDoor.y,z=posDoor.z}
    TriggerServerEvent('doormanager:toogle',pos, hashDoor)
  end
end)


RegisterNetEvent('doormanager:updateAll')
AddEventHandler('doormanager:updateAll', function(doors)
  managedDoor=doors
end)

Citizen.CreateThread(function()
  TriggerServerEvent("doormanager:getDoors")
  Citizen.Wait(1000)
  local BoucleLent = 5000
  local BoucleCourt = 100
  local TempsBoucle = BoucleCourt
  while true do
    Citizen.Wait(TempsBoucle)
    TempsBoucle = BoucleLent
    local playerPos = GetEntityCoords(GetPlayerPed(-1), true)
    for _,v in pairs(managedDoor) do
      if (Vdist(playerPos.x, playerPos.y, playerPos.z, v.pos.x, v.pos.y, v.pos.z) < 20.0) then
        TempsBoucle = BoucleCourt
        local door = GetClosestObjectOfType(v.pos.x, v.pos.y, v.pos.z, 1.0, GetHashKey(v.hash), false, false, false)
        if door ~= 0 then
          FreezeEntityPosition(door, v.toogle)
        end
      end
    end
  end
end)
