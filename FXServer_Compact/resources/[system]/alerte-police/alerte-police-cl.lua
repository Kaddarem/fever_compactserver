local colorNames = {
    ['0'] = "Noir métallisé",
    ['1'] = "Métallique Graphite Noir",
    ['2'] = "Vol noir métallique",
    ['3'] = "Argent foncé métallisé",
    ['4'] = "Argent métallique",
    ['5'] = "Argent bleu métallisé",
    ['6'] = "Métallique Gris acier",
    ['7'] = "Argent métallisé",
    ['8'] = "Pierre métallisée argent",
    ['9'] = "Argent métallique de minuit",
    ['10'] = "Métal Gun Metal",
    ['11'] = "Gris anthracite métallisé",
    ['12'] = "Noir mat",
    ['13'] = "Gris mat",
    ['14'] = "Matte Gris Clair",
    ['15'] = "Util Noir",
    ['16'] = "Util poly noir",
    ['17'] = "Util métal métal",
    ['18'] = "Util argent",
    ['19'] = "Util gun metal",
    ['20'] = "Util shadow silver",
    ['21'] = "Noir usé",
    ['22'] = "Graphite usé",
    ['23'] = "Gris argenté usé",
    ['24'] = "Argent usé",
    ['25'] = "Argent bleu usé",
    ['26'] = "Worn Shadow Silver",
    ['27'] = "Rouge métallisé",
    ['28'] = "Rouge Torino métallisé",
    ['29'] = "Formule métallique rouge",
    ['30'] = "Rouge métallisé Blaze",
    ['31'] = "Rouge gracieux métallisé",
    ['32'] = "Rouge grenat métallisé",
    ['33'] = "Metallic Desert red",
    ['34'] = "Rouge Cabernet Métallique",
    ['35'] = "Rouge Candy métallisé",
    ['36'] = "Metallic Sunrise Orange",
    ['37'] = "Metallic Classic gold",
    ['38'] = "Orange métallique",
    ['39'] = "Rouge mat",
    ['40'] = "Matte Rouge foncé",
    ['41'] = "Orange mat",
    ['42'] = "Jaune mat",
    ['43'] = "Util Red",
    ['44'] = "Util Rouge vif",
    ['45'] = "Util Garnet rouge",
    ['46'] = "Rouge usé",
    ['47'] = "Rouge d'or usé",
    ['48'] = "Rouge foncé usé",
    ['49'] = "Vert foncé métallisé",
    ['50'] = "Metallic Racing Green",
    ['51'] = "Mer verte métallisée",
    ['52'] = "Vert olive métallisé",
    ['53'] = "Vert métallique",
    ['54'] = "Essence métallisée Bleu Vert",
    ['55'] = "Vert mat citron vert",
    ['56'] = "Util Dark Green",
    ['57'] = "Util Vert",
    ['58'] = "Vert foncé usé",
    ['59'] = "Vert usé",
    ['60'] = "Lavage de mer usé",
    ['61'] = "Bleu de minuit métallisé",
    ['62'] = "Bleu foncé métallisé",
    ['63'] = "Bleu métallique de Saxe",
    ['64'] = "Bleu métallique",
    ['65'] = "Mariner métallisé bleu",
    ['66'] = "Port métallisé bleu",
    ['67'] = "Metallic Diamond blue",
    ['68'] = "Surf bleu métallisé",
    ['69'] = "Bleu nautique métallisé",
    ['70'] = "Bleu brillant métallisé",
    ['71'] = "Bleu violet métallisé",
    ['72'] = "Métallique Spinnaker bleu",
    ['73'] = "Métallique Ultra Bleu",
    ['74'] = "Bleu brillant métallisé",
    ['75'] = "Util Dark blue",
    ['76'] = "Util Midnight blue",
    ['77'] = "Util Bleu",
    ['78'] = "Util Sea Foam blue",
    ['79'] = "Uil Lightning bleu",
    ['80'] = "Util Maui blue Poly",
    ['81'] = "Util Bright blue",
    ['82'] = "Matte Bleu foncé",
    ['83'] = "Bleu mat",
    ['84'] = "Matte Bleu nuit",
    ['85'] = "Bleu foncé usé",
    ['86'] = "Bleu usé",
    ['87'] = "Porté bleu clair",
    ['88'] = "Taxi métallisé jaune",
    ['89'] = "Course métallique jaune",
    ['90'] = "Bronze métallique",
    ['91'] = "Oiseau jaune métallisé",
    ['92'] = "Chaux métallique",
    ['93'] = "Champagne métallisé",
    ['94'] = "Pueblo Beige métallisé",
    ['95'] = "Ivoire foncé métallique",
    ['96'] = "Choco brun métallisé",
    ['97'] = "Métallique doré",
    ['98'] = "Marron clair métallisé",
    ['99'] = "Beige métallisé",
    ['100'] = "Marron métallisé",
    ['101'] = "Biston marron métallisé",
    ['102'] = "Bois de hêtre métallisé",
    ['103'] = "Hêtre foncé métallisé",
    ['104'] = "Orange Choco métallisé",
    ['105'] = "Sable de plage métallique",
    ['106'] = "Sable métallisé au soleil",
    ['107'] = "Crème métallisée",
    ['108'] = "Util brun",
    ['109'] = "Util Brun moyen",
    ['110'] = "Util Brun clair",
    ['111'] = "Blanc métallisé",
    ['112'] = "Blanc de givre métallique",
    ['113'] = "Beige Miel Beige",
    ['114'] = "Brun usé",
    ['115'] = "Brun foncé usé",
    ['116'] = "Paille beige usée",
    ['117'] = "Acier brossé",
    ['118'] = "Acier noir brossé",
    ['119'] = "Aluminium brossé",
    ['120'] = "Chrome",
    ['121'] = "Porté blanc cassé",
    ['122'] = "Util Off blanc",
    ['123'] = "Orange usée",
    ['124'] = "Orange clair usé",
    ['125'] = "Vert métallique Securicor",
    ['126'] = "Taxi jaune usé",
    ['127'] = "voiture de police bleue",
    ['128'] = "Vert mat",
    ['129'] = "Brun mat",
    ['130'] = "Orange usée",
    ['131'] = "Blanc mat",
    ['132'] = "Blanc usé",
    ['133'] = "Vert olive de l'armée usée",
    ['134'] = "Blanc pur",
    ['135'] = "rose vif",
    ['136'] = "Saumon rose",
    ['137'] = "Vermillon rose métallisé",
    ['138'] = "Orange",
    ['139'] = "Vert",
    ['140'] = "Bleu",
    ['141'] = "Mettalic Black blue",
    ['142'] = "Metallic Black purple",
    ['143'] = "Rouge métallisé noir",
    ['144'] = "chasseur vert",
    ['145'] = "Metallic purple",
    ['146'] = "Métallique V Bleu foncé",
    ['147'] = "MODSHOP BLACK1",
    ['148'] = "Matte Purple",
    ['149'] = "Matte Dark Purple",
    ['150'] = "Rouge métallisé",
    ['151'] = "Matte Forêt Verte",
    ['152'] = "Matte Olive Drab",
    ['153'] = "Matte Desert Brown",
    ['154'] = "Matte Desert Tan",
    ['155'] = "Matte Foilage Vert",
    ['156'] = "COULEUR ALLIAGE PAR DEFAUT",
    ['157'] = "Epsilon Blue",
}

RegisterNetEvent('Alerte:vol_voiture')
AddEventHandler('Alerte:vol_voiture',function(veh)
    local ped = GetPlayerPed(-1)
    local plyPos = GetEntityCoords(ped,  true)
    local s1, s2 = Citizen.InvokeNative( 0x2EB41072B4C1E4C0, plyPos.x, plyPos.y, plyPos.z, Citizen.PointerValueInt(), Citizen.PointerValueInt() )
    local street1 = GetStreetNameFromHashKey(s1)
    local street2 = GetStreetNameFromHashKey(s2)    
    TriggerServerEvent('thiefInProgressPos', plyPos.x, plyPos.y, plyPos.z)
    local vehName = GetDisplayNameFromVehicleModel(GetEntityModel(veh))
    local vehName2 = GetLabelText(vehName)
    local primary, _ = GetVehicleColours(veh)
    local couleur = colorNames[tostring(primary)]
    local plaque = GetVehicleNumberPlateText(veh)
    TriggerServerEvent('CallService:Info',"police","~r~~h~Vol de vehicule :","Icon")
    Wait(100)
    TriggerServerEvent('CallService:Info','police',"~r~~h~"..vehName2.."~h~~w~ couleur ~r~~h~".. couleur.."~h~~n~~w~Plaque : ~h~~g~"..plaque)
    if s2 == 0 then
        TriggerServerEvent('CallService:Info','police',"Dans la rue ~r~~h~"..street1)
    elseif s2 ~= 0 then
        TriggerServerEvent('CallService:Info','police',"Entre ~r~~h~"..street1.."~h~~w~ et ~r~~h~"..street2)
    end
end)

Citizen.CreateThread(function()
    math.randomseed(GetNetworkTime())
    while true do
        Wait(5)
        local ped = GetPlayerPed(-1)
        if IsPedInMeleeCombat(ped) or IsPedShooting(ped) or IsPedJacking(ped) and GetInteriorFromEntity(GetPlayerPed(-1)) ~= 137729 then
            if math.random() < 0.8 then
                local plyPos = GetEntityCoords(ped,  true)
                local s1, s2 = Citizen.InvokeNative( 0x2EB41072B4C1E4C0, plyPos.x, plyPos.y, plyPos.z, Citizen.PointerValueInt(), Citizen.PointerValueInt() )
                local street1 = GetStreetNameFromHashKey(s1)
                local street2 = GetStreetNameFromHashKey(s2)
                --[[if IsPedInMeleeCombat(ped) then
                    TriggerServerEvent('thiefInProgressPos', plyPos.x, plyPos.y, plyPos.z)
                    TriggerServerEvent('CallService:Info',"police","~r~~h~Une bagarre a éclaté","Icon")
                    Wait(100)
                    if s2 == 0 then
                        TriggerServerEvent('CallService:Info','police',"Dans la rue ~r~~h~"..street1)
                    elseif s2 ~= 0 then
                        TriggerServerEvent('CallService:Info','police',"Entre ~r~~h~"..street1.."~h~~w~ et ~r~~h~"..street2)
                    end
                    Wait(3000)
                end]]
                if IsPedShooting(ped) and not IsPedCurrentWeaponSilenced(ped) and GetSelectedPedWeapon(ped) ~= GetHashKey('WEAPON_STUNGUN') then
                    TriggerServerEvent('thiefInProgressPos', plyPos.x, plyPos.y, plyPos.z)
                    TriggerServerEvent('CallService:Info',"police","~r~~h~Coup de feu entendu","Icon")
                    Wait(100)
                    if s2 == 0 then
                        TriggerServerEvent('CallService:Info','police',"Dans la rue ~r~~h~"..street1)
                    elseif s2 ~= 0 then
                        TriggerServerEvent('CallService:Info','police',"Entre ~r~~h~"..street1.."~h~~w~ et ~r~~h~"..street2)
                    end
                    Wait(3000)
                end
                if IsPedJacking(ped) then
                    local veh =  GetVehiclePedIsTryingToEnter(ped)
                    if veh == 0 then
                        veh = GetVehiclePedIsIn(ped,false)
                    end
                    if GetPedInVehicleSeat(veh,-1) ~= 0 then
                        Wait(2000)
                        TriggerServerEvent('thiefInProgressPos', plyPos.x, plyPos.y, plyPos.z)
                        local vehName = GetDisplayNameFromVehicleModel(GetEntityModel(veh))
                        local vehName2 = GetLabelText(vehName)
                        local primary, _ = GetVehicleColours(veh)
                        local couleur = colorNames[tostring(primary)]
                        local plaque = GetVehicleNumberPlateText(veh)
                        TriggerServerEvent('CallService:Info',"police","~r~~h~CarJacking de vehicule :","Icon")
                        Wait(100)
                        TriggerServerEvent('CallService:Info','police',"~r~~h~"..vehName2.."~h~~w~ couleur ~r~~h~".. couleur.."~h~~n~~w~Plaque : ~h~~g~"..plaque)
                        if s2 == 0 then
                            TriggerServerEvent('CallService:Info','police',"Dans la rue ~r~~h~"..street1)
                        elseif s2 ~= 0 then
                            TriggerServerEvent('CallService:Info','police',"Entre ~r~~h~"..street1.."~h~~w~ et ~r~~h~"..street2)
                        end
                    end
                end
            end
        end
    end
end)

RegisterNetEvent('thiefPlace')
AddEventHandler('thiefPlace', function(tx, ty, tz)
	local transT = 250
	local thiefBlip = AddBlipForCoord(tx, ty, tz)
	SetBlipSprite(thiefBlip,  10)
	SetBlipColour(thiefBlip,  1)
	SetBlipAlpha(thiefBlip,  transT)
	SetBlipAsShortRange(thiefBlip,  1)
	while transT ~= 0 do
	    Wait(20 * 4)
		transT = transT - 1
		SetBlipAlpha(thiefBlip,  transT)
		if transT == 0 then
			SetBlipSprite(thiefBlip,  2)
			return
		end
	end
end)
