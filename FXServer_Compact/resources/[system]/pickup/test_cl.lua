local parachutes = {
    {coord = {x=454.044,y= 5586.441,z= 781.189}, pickup = nil},
    {coord = {x = -1006.059, y = -3016.758, z = 13.945}, pickup = nil},
}

Citizen.CreateThread(function()
    RemoveWeaponFromPed(GetPlayerPed(-1), GetHashKey('gadget_parachute'))
    for int,info in pairs(parachutes) do
        parachutes[int].pickup = CreatePickupRotate(GetHashKey('PICKUP_PARACHUTE'),  info.coord.x, info.coord.y,info.coord.z,0.0,0.0,180.0,512,1,true,true,GetHashKey('gadget_parachute'))
    end
    while true do
        Wait(100)
        for int,info in pairs(parachutes) do
            local parachute = Citizen.InvokeNative(0x5099BC55630B25AE, Citizen.PointerValueIntInitialized(info.pickup))
            local stat = GetPedParachuteState(GetPlayerPed(-1))
            if stat == 0 then
                DisplayHelp("Appuyer sur ~INPUT_PARACHUTE_DEPLOY~ pour ouvrir le parachute")
            end
            if not DoesPickupObjectExist(parachute) then
                math.randomseed(GetNetworkTime())
                local tint = math.random(1,13)
                local model = math.random(1,4)
                local model2 = math.random(1,13)
                local red = math.random(0,255)
                local green = math.random(0,255)
                local blue = math.random(0,255)
                SetPedParachuteTintIndex(GetPlayerPed(-1),tint)
                SetPlayerParachuteSmokeTrailColor(GetPlayerIndex(),red,green,blue)
                SetPlayerParachutePackTintIndex(GetPlayerIndex(),model)
                SetPlayerReserveParachuteTintIndex(GetPlayerIndex(),model2)
                SetPlayerCanLeaveParachuteSmokeTrail(GetPlayerIndex(),true)
                Wait(3000)
                parachutes[int].pickup = CreatePickupRotate(GetHashKey('PICKUP_PARACHUTE'),  info.coord.x, info.coord.y,info.coord.z,0.0,0.0,180.0,512,1,true,true,GetHashKey('gadget_parachute'))
            end
        end
    end
end)