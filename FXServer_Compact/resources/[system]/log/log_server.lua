function getnameRP(joueur)
    local name = tostring(joueur)
    TriggerEvent('es:getPlayerFromId', joueur, function(user)
		if user ~= nil then
            name = user.prenom.." "..user.nom
        end
    end)
    return name
end
                

RegisterServerEvent('log:print')
AddEventHandler('log:print', function(message)
  appendNewLog(message)
end)

RegisterServerEvent('log:printname')
AddEventHandler('log:printname',function(info,text)
    local source = source
    appendNewLog('['..info..']'..' '..getnameRP(source)..' '..text)
end)
---------------------
----- CONNEXION -----
---------------------
RegisterServerEvent('log:connexion')
AddEventHandler('log:connexion', function()
    local name = GetPlayerName(source)
    appendNewLog("* Connexion de "..name.." ("..getPlayerID(source)..")")
end)

-----------------------
----- DECONNEXION -----
-----------------------
AddEventHandler('playerDropped', function()
	appendNewLog('* ' .. GetPlayerName(source) .. '(' .. getPlayerID(source) .. '/' .. GetPlayerEP(source) .. ') a quitté le serveur')
end)

--------------------
----- CHAT MSG -----
--------------------
--[[AddEventHandler('chatMessage', function(source, name, message)
	local displayMessage = string.gsub(message, '^%d', '')

	-- ignore zero-length messages
	if string.len(displayMessage) == 0 then
		return
	end
        
    if string.sub(displayMessage, 1, 1) == "/" then
		return
	end

	appendNewLog('[' .. tostring(GetPlayerName(source)) .. ']: ' .. displayMessage)
end)]]

----------------
----- KILL -----
----------------
RegisterServerEvent('baseevent:onPlayerKilled')
AddEventHandler('baseevent:onPlayerKilled', function(killedBy, data)
	local victim = getnameRP(source)
    if killedBy == -1 then
        tueur = "un PNJ"
    else
        tueur = getnameRP(killedBy)
    end
    if data.killerinvehicle then
        appendNewLog(victim.. " a été tué par "..tueur.." en carkill")
    else
        appendNewLog(victim.. " a été tué par "..tueur)
    end
end)

-------------------
----- SUICIDE -----
-------------------
RegisterServerEvent('baseevent:onPlayerDied')
AddEventHandler('baseevent:onPlayerDied', function(killedBy, pos)
	local victim = getnameRP(source)
    print(victim)
    appendNewLog(victim..' est mort(e) seul(e)')
end)

--------------------
----- BRAQUAGE -----
--------------------
local valeur = 0
RegisterServerEvent('log:braquagestart')
AddEventHandler('log:braquagestart', function(robb)
    local dureeminute=math.floor(robb.time/60)
    local dureeseconde=robb.time-(dureeminute*60)
    valeur = robb.reward
    local envoi = "[BRAQUAGE]"..getnameRP(source).." braque "..robb.nameofstore.. " pour une durée de "..dureeminute.."min et "..dureeseconde.."s pour $"..robb.reward
    appendNewLog(envoi)    
end)

RegisterServerEvent('log:braquagecomplete')
AddEventHandler('log:braquagecomplete', function()
    local envoi = "[BRAQUAGE]"..getnameRP(source).." a réussi son braquage pour $"..valeur
    valeur = 0
    appendNewLog(envoi)    
end)

RegisterServerEvent('es_holdup:toofar')
AddEventHandler('es_holdup:toofar', function(robb)
    local dureeminute=math.floor(robb.time/60)
    local dureeseconde=robb.time-(dureeminute*60)
    local date = os.date("*t", os.time())
    local datetxt = date.hour.."h"..date.min.."m"..date.sec.."s"
    local envoi = "[BRAQUAGE]"..getnameRP(source).." est sorti de "..robb.nameofstore.. " à "..datetxt
    appendNewLog(envoi)  
end)

--------------------
----- FONCTION -----
--------------------
function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end
-- PC (Windows) to Mac
local entities =
{
--	['&'] = "&amp;",
	['<'] = "&lt;",
	['>'] = "&gt;",
	-- French entities (the most common ones)
	['?'] = "&agrave;",
	['?'] = "&acirc;",
	['È'] = "&eacute;",
	['Ë'] = "&egrave;",
	['Í'] = "&ecirc;",
	['Î'] = "&euml;",
	['Ó'] = "&icirc;",
	['Ô'] = "&iuml;",
	['Ù'] = "&ocirc;",
	['?'] = "&ouml;",
	['<breve>'] = "&ugrave;",
	['?'] = "&ucirc;",
-- ... shortened
}

function appendNewLog(msg)
    date = os.date("*t", os.time())
    datetxt = date.day.."/"..date.month.." à "..date.hour.."h"..date.min.."m"..date.sec.."s".." : "
    print(datetxt..msg)
end