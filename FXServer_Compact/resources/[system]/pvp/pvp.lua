local crouched = false

AddEventHandler("playerSpawned", function(spawn)
	SetCanAttackFriendly(GetPlayerPed(-1), true, false)
	NetworkSetFriendlyFireOption(true)
    SetPlayerMeleeWeaponDamageModifier(PlayerId(),0.25)
    SetAiMeleeWeaponDamageModifier(0.25)   
end)

Citizen.CreateThread(function()
    Wait(0)
    SetPlayerMeleeWeaponDamageModifier(PlayerId(),0.25)
    SetAiMeleeWeaponDamageModifier(0.25)
    while true do
        Wait(0)
                SetVehicleDensityMultiplierThisFrame(0.5)
                SetPedDensityMultiplierThisFrame(1.0)
                SetRandomVehicleDensityMultiplierThisFrame(0.5)
                SetParkedVehicleDensityMultiplierThisFrame(0.5)
                SetScenarioPedDensityMultiplierThisFrame(1.0, 1.0)


                --SetGarbageTrucks(0)
                --SetRandomBoats(0)
        
        local ped = GetPlayerPed( -1 )

        if ( DoesEntityExist( ped ) and not IsEntityDead( ped ) ) then 
            DisableControlAction( 0, 36, true ) -- INPUT_DUCK  

            if ( not IsPauseMenuActive() ) then 
                if ( IsDisabledControlJustPressed( 0, 36 ) ) then 
                    RequestAnimSet( "move_ped_crouched" )
                    RequestAnimSet( "move_ped_bucket" )

                    while ( not HasAnimSetLoaded( "move_ped_crouched" ) ) do 
                        Citizen.Wait( 100 )
                    end 

                    if ( crouched == true ) then
                        SetPedMovementClipset( ped, "move_ped_bucket", 0.25 )
                        Wait(250)
                        ResetPedMovementClipset( ped, 0 )
                        crouched = false 
                    elseif ( crouched == false ) then
                        SetPedMovementClipset( ped, "move_ped_crouched", 0.25 )
                        crouched = true 
                    end 
                end
            end 
        end 
    end
end)