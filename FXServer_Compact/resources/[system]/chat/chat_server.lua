RegisterServerEvent('chatCommandEntered')
RegisterServerEvent('chatMessageEntered')

AddEventHandler('chatMessageEntered', function(name, color, message)
    if not name or not color or not message or #color ~= 3 then
        return
    end
    local source = source
    TriggerEvent('es:getPlayerFromId', source, function(user)
        name = user.prenom.." "..user.nom
        TriggerEvent('chatMessage', source, name, message)

        if not WasEventCanceled() then
            TriggerClientEvent('chatMessage', -1, name, color, message)
        end

        print(name .. ': ' .. message)
    end)
end)

-- player join messages
AddEventHandler('playerActivated', function()
    --TriggerClientEvent('chatMessage', -1, '', { 0, 0, 0 }, '^2* ' .. GetPlayerName(source) .. ' joined.')
end)

AddEventHandler('playerDropped', function(reason)
   -- TriggerClientEvent('chatMessage', -1, '', { 0, 0, 0 }, '^2* ' .. GetPlayerName(source) ..' left (' .. reason .. ')')
end)

-- say command handler
AddEventHandler('rconCommand', function(commandName, args)
    if commandName == "say" then
        local msg = table.concat(args, ' ')

        TriggerClientEvent('chatMessage', -1, 'console', { 255, 0, 0 }, "^1"..msg)
        RconPrint('console: ' .. msg .. "\n")

        CancelEvent()
    elseif commandName == 'status' then
        for netid, data in pairs(names) do
            local guid = GetPlayerIdentifiers(netid)

            if guid and guid[1] and data then
                local ping = GetPlayerPing(netid)

                RconPrint(netid .. ' ' .. guid[1] .. ' ' .. data.name .. ' ' .. GetPlayerEP(netid) .. ' ' .. ping .. "\n")
            end
        end

        CancelEvent()
    end
end)

-- tell command handler
AddEventHandler('rconCommand', function(commandName, args)
    if commandName == "tell" then
        local target = table.remove(args, 1)
        local msg = table.concat(args, ' ')

        TriggerClientEvent('chatMessage', tonumber(target), 'console', { 255, 0x99, 0 }, msg)
        RconPrint('console: ' .. msg .. "\n")

        CancelEvent()
    end
end)
