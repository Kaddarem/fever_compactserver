local InfoTemps = {}
local tempsPause = 60*1000


local function saveTempsJeu()
	SetTimeout(tempsPause, function()
        --On sauvegarde les joueurs connectés
        local EnJeu = {}
        local a_save = {}
		TriggerEvent("es:getPlayers", function(users)
			for _,v in pairs(users)do
                EnJeu[v.identifier] = true
                if not IndexSearch(InfoTemps, v.identifier) then
                    InfoTemps[v.identifier] = 1
                end
			end
            for iden,temps in pairs (InfoTemps) do
                if IndexSearch(EnJeu,iden) then
                    InfoTemps[iden] = temps + 1            
                else
                    InfoTemps[iden] = nil
                    a_save[iden] = temps
                end
            end
            for iden,temps in pairs (a_save) do
                MySQL.Async.insert("UPDATE users SET temps_jeu = temps_jeu + @temps WHERE identifier = @id",{['@temps'] = temps, ['@id'] = iden})                
            end
		end)

		saveTempsJeu()
	end)
end

saveTempsJeu()

function IndexSearch(table,id)
    if table ~= nil then
        for key,_ in pairs(table) do
            if (key == id) then
                return true
            end
        end
    end
    return false
end
