-- CONFIG --

-- AFK Kick Time Limit (in seconds)
secondsUntilKick = 600

local reset = false
-- CODE --

Citizen.CreateThread(function()
	while true do
		Wait(1000)
        
		playerPed = GetPlayerPed(-1)
		if playerPed then
			currentPos = GetEntityCoords(playerPed, true)
			if (Vdist(currentPos,prevPos) < 1.0) and not IsEntityDead(PlayerPedId()) then
                if reset then
                    time = secondsUntilKick    
                    reset = false
                end
				if time > 0 then
					if time == math.ceil(secondsUntilKick / 4) then
                        PlaySound(-1, "Menu_Accept", "Phone_SoundSet_Default", 0, 0, 1)
                        Citizen.Wait(300)
                        PlaySound(-1, "Menu_Accept", "Phone_SoundSet_Default", 0, 0, 1)
						TriggerEvent("hud:NotifColor","Attention, vous allez être kick pour afk",6)
					end

					time = time - 1
				else
					TriggerServerEvent("kickForBeingAnAFKDouchebag")
				end
			else
				time = secondsUntilKick
			end

			prevPos = currentPos
		end
	end
end)

RegisterNetEvent('AFKkick:reset')
AddEventHandler('AFKkick:reset',function()
    reset = true
end)