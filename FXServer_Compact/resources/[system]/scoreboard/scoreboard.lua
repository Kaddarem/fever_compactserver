local listOn = false
local group = "0"
local ptable = {}

AddEventHandler('onClientResourceStart', function(res)
    if res == "scoreboard" then
        TriggerServerEvent('scoreboard:ini')
    end
end)

RegisterNetEvent('scoreboard:group')
AddEventHandler('scoreboard:group',function(groupbdd)
    group = groupbdd 
end)

Citizen.CreateThread(function()
    listOn = false
    while true do
        Wait(0)
        
        if IsControlJustPressed(0, 27) and (group == "owner")--[[ INPUT_PHONE ]] then
            if not listOn then
                ptable = {}
                for i = 0,500 do
                    if NetworkIsPlayerConnected(i) then
                        if NetworkIsPlayerActive(i) and GetPlayerPed(i) ~= nil then
                           TriggerServerEvent('scoreboard:player',GetPlayerServerId(i))
                        end
                    end
                end
                TriggerServerEvent('scoreboard:recup')
                local players = {}
                while ptable[1] == nil do
                    Wait(10)
                end
                for a, i in ipairs(ptable) do
                    local name = i.nom
                    local num = i.id
                    r, g, b = GetPlayerRgbColour(a)
                    table.insert(players, 
                    '<tr style=\"color: rgb(' .. r .. ', ' .. g .. ', ' .. b .. ')\"><td>' ..num .. '</td><td>' .. name .. '</td></tr>'
                    )
                end
                
                SendNUIMessage({ text = table.concat(players) })

                listOn = true
                while listOn do
                    Wait(0)
                    if(IsControlPressed(0, 27) == false) then
                        listOn = false
                        SendNUIMessage({
                            meta = 'close'
                        })
                        break
                    end
                end
            end
        end
    end
end)

RegisterNetEvent('scoreboard:aff')
AddEventHandler('scoreboard:aff', function(_ptable)
    ptable = _ptable
end)

function GetPlayers()
    local players = {}

    for i = 0, 31 do
        if NetworkIsPlayerActive(i) then
            table.insert(players, i)
        end
    end

    return players
end