local temp = {}

RegisterServerEvent('scoreboard:ini')
AddEventHandler('scoreboard:ini', function()
    local source = source
    local player = getPlayerID(source)
    MySQL.Async.fetchScalar('SELECT `group` FROM `users` WHERE identifier = @id',{['@id']=player}, function(result)
        if result ~= nil then
            TriggerClientEvent('scoreboard:group',source,result)
        else
            TriggerClientEvent('scoreboard:group',source,"0")
        end
    end)
end)

RegisterServerEvent('scoreboard:player')
AddEventHandler('scoreboard:player', function(target)
    local source = source
    TriggerEvent('es:getPlayerFromId', target, function(user)
        if user ~= nil then
            local temp2 = {id=target,nom=user.prenom..' '..user.nom}
            table.insert(temp,temp2)
        end
    end)
end)
        
RegisterServerEvent('scoreboard:recup')
AddEventHandler('scoreboard:recup', function()
    local source = source   
    TriggerClientEvent('scoreboard:aff',source, temp)
    temp = {}
end)

function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end
