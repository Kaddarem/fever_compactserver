--Private Messages
-- /sms en minuscule
TriggerEvent('es:addGroupCommand', 'sms', "owner", function(source, args, user)
    if(GetPlayerName(tonumber(args[2])) or GetPlayerName(tonumber(args[3])))then
        local player = tonumber(args[2])
        table.remove(args, 2)
        table.remove(args, 1)

        TriggerEvent("es:getPlayerFromId", player, function(target)
            TriggerClientEvent('chatMessage', player, "SMS:", {214, 214, 214}, "^2"..GetPlayerName(source).. " | " .. source .. " | PM: ^7" ..table.concat(args, " "))
            TriggerClientEvent('chatMessage', source, "SMS:", {214, 214, 214}, "^3 Envoyé à : "..GetPlayerName(player).. ": ^7" ..table.concat(args, " "))
        end)
    else
    TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "ID du joueur incorrect!")
    end
end, function(source, args, user)
    TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Vous n'avez pas la permission!")
end)
-- /SMS en majuscule
TriggerEvent('es:addGroupCommand', 'SMS', "owner", function(source, args, user)
    if(GetPlayerName(tonumber(args[2])) or GetPlayerName(tonumber(args[3])))then
        local player = tonumber(args[2])
        table.remove(args, 2)
        table.remove(args, 1)

        TriggerEvent("es:getPlayerFromId", player, function(target)
            TriggerClientEvent('chatMessage', player, "SMS:", {214, 214, 214}, "^2"..GetPlayerName(source).. " | " .. source .. " | SMS: ^7" ..table.concat(args, " "))
            TriggerClientEvent('chatMessage', source, "SMS:", {214, 214, 214}, "^3 Envoyé à : "..GetPlayerName(player).. ": ^7" ..table.concat(args, " "))
        end)
    else
        TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "ID du joueur incorrect!")
    end
end, function(source, args, user)
    TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Vous n'avez pas la permission!")
end)