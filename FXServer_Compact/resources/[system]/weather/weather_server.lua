CurrentMeteo = nil

AvailableWeatherTypes = {
    'EXTRASUNNY', 
    'CLEAR', 
    'NEUTRAL', 
    'SMOG', 
    'FOGGY', 
    'OVERCAST', 
    'CLOUDS', 
    'CLEARING', 
    'RAIN', 
    'THUNDER', 
    'SNOW', 
    'BLIZZARD', 
    'SNOWLIGHT', 
    'XMAS', 
    'HALLOWEEN',
}

RegisterServerEvent('Meteo:Check')
AddEventHandler('Meteo:Check', function()
    local source = source
    TriggerClientEvent('Meteo:Update',source,CurrentMeteo)  
end)


AddEventHandler('rconCommand', function(commandName, args)
	if commandName == 'halloween' then
			CurrentMeteo = 'HALLOWEEN'
            TriggerClientEvent('Meteo:Update',-1,'HALLOWEEN')
            CancelEvent()
			return
    elseif commandName == 'Meteo' then
        if args[1] ~= nil then
			CurrentMeteo = args[1]
            TriggerClientEvent('Meteo:Update',-1,args[1])  
            CancelEvent()
            return      
        else
            TriggerEvent('printtable2',AvailableWeatherTypes)
            CancelEvent()
            return
        end
    elseif commandName == "MeteoReset" then
        TriggerClientEvent('Meteo:Update',-1)
        CancelEvent()
        return
    elseif commandName == "Cloudy" then
        TriggerClientEvent('MeteoSnow:cl',-1,"CLOUDY")
        CancelEvent()
        return
    end
end)

RegisterServerEvent('MeteoSnow')
AddEventHandler('MeteoSnow', function(value)
    TriggerClientEvent('MeteoSnow:cl',-1,value)
end)