TriggerEvent("es:addGroup", "admin", "user", function(group) end)

--Help Commands
--[[TriggerEvent('es:addCommand', 'help', function(source, args, user)
  TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "F6 pour passer de MPH à KM/H")
    TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "L pour faire le taxi (depuis le véhicule)")
    TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "X pour mettre les mains en l'air")
    TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "B pour pointer du doigts")
    TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "F7 pour ouvrir le menu personnel")
    TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "Flèche de droite pour renommer un contact")
    TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "/givecash id montant pour donner de l'argent à un joueur à moins de 5 mètres")
    TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "/discord pour afficher l'adresse du discord")
    TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "/help1 pour voir la suite des commandes")
end)

TriggerEvent('es:addCommand', 'help1', function(source, args, user)
  TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "U pour verrouiller/déverrouiller un véhicule")
    TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "/tow pour remorquer un véhicule avec le flatbed")
    TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "/wc pour remettre votre niveau WC à 0")  
end)]]

TriggerEvent('es:addCommand', 'police', function(source, args, user)
  TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "/copadd [ID] pour ajouter un policier")
  TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "/copprem [ID] pour supprimer un policier")
  TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "/coppromo [ID] pour promouvoir un policier")
  TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "/copretro [ID] pour retrograder un policier")
end)

TriggerEvent('es:addCommand', 'discord', function(source, args, user)
  TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "discord.gg/TPxC6h3")
	
end)

TriggerEvent('es:addCommand', 'group', function(source, args, user)
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Group: ^2" .. user.group.group)
end)

-- Kicking
TriggerEvent('es:addGroupCommand', 'kick', "admin", function(source, args, user)
		if(GetPlayerName(tonumber(args[2])))then
			local player = tonumber(args[2])

			-- User permission check
			TriggerEvent("es:getPlayerFromId", player, function(target)
				if(tonumber(target.permission_level) > tonumber(user.permission_level))then
					TriggerClientEvent("chatMessage", source, "SYSTEM", {255, 0, 0}, "You're not allowed to target this person!")
					return
				end

				local reason = args
				table.remove(reason, 1)
				table.remove(reason, 1)
				if(#reason == 0)then
					reason = "Kicked: You have been kicked from the server"
				else
					reason = "Kicked: " .. table.concat(reason, " ")
				end

				TriggerClientEvent('chatMessage', -1, "SYSTEM", {255, 0, 0}, "Player ^2" .. GetPlayerName(player) .. "^0 has been kicked(^2" .. reason .. "^0)")
				--DropPlayer(player, reason)
			end)
		else
			TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "ID de joueur incorrect!")
		end
end, function(source, args, user)
	TriggerClientEvent('chatMessage', source, "SYSTEM", {255, 0, 0}, "Permission insuffisante!")
end)


TriggerEvent('es:addCommand', 'rmwanted', function(source)
  TriggerEvent("es:getPlayerFromId", source, function(user)
    if(user.money > 100) then
			user.removeMoney((100))
			TriggerClientEvent('es_freeroam:wanted', source)
			TriggerClientEvent("es_freeroam:notify", source, "CHAR_LESTER", 1, "Lester", false, "Troubles in paradise are fixed")
		else
			TriggerClientEvent("es_freeroam:notify", source, "CHAR_LESTER", 1, "Lester", false, "Sorry but you need more cash before i can help you")
		end
	end)
end)
