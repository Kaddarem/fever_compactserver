resource_type 'gametype' { name = 'es_freeroam'}

description 'FiveM es_freeroam'

server_script '@mysql-async/lib/MySQL.lua'
-- Manifest


-- Requiring essentialmode
dependency 'essentialmode'

-- General
client_scripts {
  'client.lua',
  'player/map.lua',
  'stores/stripclub.lua',
}

server_scripts {
  'config.lua',
  'server.lua',
  'player/commands.lua',
}
