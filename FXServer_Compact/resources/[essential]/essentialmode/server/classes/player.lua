-- NO TOUCHY, IF SOMETHING IS WRONG CONTACT KANERSPS! --
-- NO TOUCHY, IF SOMETHING IS WRONG CONTACT KANERSPS! --
-- NO TOUCHY, IF SOMETHING IS WRONG CONTACT KANERSPS! --
-- NO TOUCHY, IF SOMETHING IS WRONG CONTACT KANERSPS! --

-- Constructor

-- Meta table for users
function Player(source, group,info)
		local self = {}

		self.source = source
		self.permission_level = info.permission_level
		self.money = info.money
		self.identifier = info.identifier
		self.group = group
		self.session = {}
		self.job = info.job
		self.police = info.police
		self.enService = 0
		self.nom = info.nom
		self.prenom = info.prenom
		self.telephone = info.telephone
		self.coords = {x = 0.0, y = 0.0, z = 0.0}
		self.vehicle = 0
        self.gunlicense = info.gunlicense

		self.getSource = function()
            return self.source
        end

        -- Getting permissions
        self.getPermissions= function()
            return self.permission_level
        end

        -- Setting them
        self.setPermissions= function(p)
            TriggerEvent("es:setPlayerData", self.source, "permission_level", p, function(response, success)
                self.permission_level = p
            end)
        end

        -- No need to ever call this (No, it doesn't teleport the player)
        self.setCoords= function(x, y, z)
            self.coords.x, self.coords.y, self.coords.z = x, y, z
        end

        self.getCoords= function()
            return { x = self.coords.x, y = self.coords.y, z = self.coords.z }
        end

        -- Kicks a player with specified reason
        self.kick= function(reason)
            --DropPlayer(self.source, reason)
        end

        self.getMoney= function()
            return self.money
        end

        -- Sets the player money (required to call this from now)
        self.setMoney= function(m)
            local prevMoney = self.money
            local newMoney = m *1.0

            self.money = m

            if((prevMoney - newMoney) < 0)then
                TriggerClientEvent("es:addedMoney", self.source, math.abs(prevMoney - newMoney))
            else
                TriggerClientEvent("es:removedMoney", self.source, math.abs(prevMoney - newMoney))
            end

            TriggerClientEvent('es:activateMoney', self.source , self.money)
        end

        -- Adds to player money (required to call this from now)
        self.addMoney= function(m)
            local newMoney = self.money + m *1.0

            self.money = newMoney

            TriggerClientEvent("es:addedMoney", self.source, m)
            TriggerClientEvent('es:activateMoney', self.source , self.money)
        end

        -- Removes from player money (required to call this from now)
        self.removeMoney= function(m)
            local newMoney = self.money - m*1.0

            self.money = newMoney

            TriggerClientEvent("es:removedMoney", self.source, m)
            TriggerClientEvent('es:activateMoney', self.source , self.money)
        end

        -- Player session variables
        self.setSessionVar= function(key, value)
            self.session[key] = value
        end

        self.getSessionVar= function(key)
            return self.session[key]
        end

        -- POLICE VARS
        self.setenService= function(param)
            self.enService = param
        end

        self.getenService= function()
            return self.enService
        end

        self.setPolice= function(param)
            self.police = param
        end

        self.getPolice= function()
            return self.police
        end

        self.setNom= function(param)
            self.nom = param
        end

        self.getNom= function()
            return self.nom
        end

        self.setPrenom= function(param)
            self.prenom = param
        end

        self.getPrenom= function()
            return self.prenom
        end

        self.setVehicle= function(param)
            self.vehicle = param
        end

        self.getVehicle= function()
            return self.vehicle
        end

        self.getTel= function()
            return self.telephone
        end

        self.getJob= function()
            return self.job
        end

        self.setJob= function(param)
            self.job = param
        end
    
        self.setGunlicense = function(param)
            self.gunlicense = tonumber(param)
        end
    
    return self
end
