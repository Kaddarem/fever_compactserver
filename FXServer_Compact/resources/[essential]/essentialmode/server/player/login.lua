-- NO TOUCHY, IF SOMETHING IS WRONG CONTACT KANERSPS! --
-- NO TOUCHY, IF SOMETHING IS WRONG CONTACT KANERSPS! --
-- NO TOUCHY, IF SOMETHING IS WRONG CONTACT KANERSPS! --
-- NO TOUCHY, IF SOMETHING IS WRONG CONTACT KANERSPS! --
function LoadJob(source)
    local jobname = {}
    local source = source
    local result = MySQL.Sync.fetchAll("SELECT * FROM jobs WHERE 1 ORDER BY job_id",{})
    if (result) then
        for a, v in pairs(result) do
            table.insert(jobname,v.job_name)
        end
    end
    TriggerClientEvent('job:recuptableaujob', source, jobname)
end

function LoadItems(source)
    local itemname = {}
    local tabletempon = {}
    local max = 0
    local source = source
    local result = MySQL.Sync.fetchAll("SELECT * FROM items WHERE 1 ORDER BY id",{})
    if (result) then
        for a, v in pairs(result) do
            itemname = {
                name = v.libelle,
                id = v.id,
                isillegal = v.isIllegal,
                food = v.food,
                needs = v.needs,
                water = v.water,
            }
            if v.id > max then
                max = v.id
            end
            TriggerClientEvent('item:recuptableitem', source, itemname)
        end
    end
end


function LoadUser(identifier, source)
    local source = source
    local identifier = identifier
    local result = MySQL.Sync.fetchAll("SELECT * FROM users WHERE identifier = @name", {['@name'] = identifier}) 
    local group = groups[result[1].group]
    Users[source] = Player(source, group,result[1])

    -- Chargement de la liste des jobs
    LoadJob(source)

    --LoadItems(source)

    TriggerEvent('es:playerLoaded', source, Users[source])
        
    TriggerClientEvent('es:setPlayerDecorator', source, 'rank', Users[source]:getPermissions())
        
end


function isIdentifierBanned(id)
	local result = MySQL.Sync.fetchAll("SELECT * FROM bans WHERE banned = @name", {['@name'] = id})
        if(result)then
            for k,v in ipairs(result)do
                if os.time() < (v.expires/1000) then
                    local tstamp = os.date("*t", v.expires/1000)
                    local msg = " "
                    local msg = msg.."Tu es ban jusqu'au "..tstamp.year .. "-" .. tstamp.month .. "-" .. tstamp.day .. " " .. tstamp.hour .. ":" .. tstamp.min .. ":" .. tstamp.sec ..". Raison : "..v.reason
                    return msg
                end
            end
        end
        return nil
end

AddEventHandler('es:getPlayers', function(cb)
	cb(Users)
end)

function hasAccount(identifier)
	local result = MySQL.Sync.fetchAll("SELECT * FROM users WHERE identifier = @name", {['@name'] = identifier})
        if(result[1] ~= nil) then
            return true
        end
        return false
end

function isLoggedIn(source)
	if(Users[GetPlayerName(source)] ~= nil)then
	if(Users[GetPlayerName(source)]['isLoggedIn'] == 1) then
		return true
	else
		return false
	end
	else
		return false
	end
end

function registerUser(identifier, source)
    local telephone = 0
    local identifier = identifier
    local source = source
	if not hasAccount(identifier) then
        math.randomseed(os.time())
		telephone = tostring(generateTel(identifier))
		-- Inserting Default User Account Stats
        local valide = false
        while not valide do
            local cherche = MySQL.Sync.fetchAll('SELECT * FROM users WHERE telephone = @tel',{['@tel']= telephone})
            if cherche[1] == nil then
                valide = true
            else
		        telephone = tostring(generateTel(identifier))
            end
        end
    
		MySQL.Async.insert("INSERT INTO users (`identifier`, `permission_level`, `money`, `group`, `nom`, `prenom`, `telephone`) VALUES (@username, '0', @money, 'user', 'Citizen', 'Citizen', @telephone)",{['@username'] = identifier, ['@telephone'] = telephone, ['@money'] = settings.defaultSettings.startingCash}, function()
            
            LoadUser(identifier, source)

            TriggerClientEvent("ccreation:menu", source)
        end)
        
	else
        local result = MySQL.Sync.fetchAll("SELECT * FROM users WHERE identifier = @name", {['@name'] = identifier})
        if result[1].telephone =="0" or result[1].telephone =='nil' then
            telephone = tostring(generateTel(identifier))
            MySQL.Sync.execute("UPDATE users SET `telephone`=@telephone WHERE identifier = @identifier",{['@identifier'] = identifier, ['@telephone'] = telephone})
            TriggerClientEvent("ccreation:menu", source)
        end
        if result[1].nom == "Citizen" then
            TriggerClientEvent("ccreation:menu", source)
        end
        
        LoadUser(identifier, source)
	end
end

AddEventHandler("es:setPlayerData", function(user, k, v, cb)
        local source = source
	if(Users[user])then
		if(Users[user][k])then

			if(k ~= "money") then
				Users[user][k] = v

				MySQL.Sync.execute("UPDATE users SET `@key`=@value WHERE identifier = @identifier",{['@key'] = k, ['@value'] = v, ['@identifier'] = Users[user]['identifier']})
			end

			if(k == "group")then
				Users[user].group = groups[v]
			end

			cb("Player data edited.", true)
		else
			cb("Column does not exist!", false)
		end
	else
		cb("User could not be found!", false)
	end
end)

AddEventHandler("es:setPlayerDataId", function(user, k, v, cb)
    local source = source
	MySQL.Sync.execute("UPDATE users SET @key=@value WHERE identifier = @identifier",
	{['@key'] = k, ['@value'] = v, ['@identifier'] = user})

	cb("Player data edited.", true)
end)

AddEventHandler("es:getPlayerFromId", function(user, cb)
	if(Users)then
		if(Users[user])then
			cb(Users[user])
		else
			cb(nil)
		end
	else
		cb(nil)
	end
end)

AddEventHandler("es:getPlayerFromIdentifier", function(identifier, cb)
	local result = MySQL.Sync.fetchAll("SELECT * FROM users WHERE identifier = @name", {['@name'] = identifier})
    if(result[1])then
        cb(result[1])
    else
        cb(nil)
    end
end)

AddEventHandler("es:getAllPlayers", function(cb)
    local source = source
	local result = MySQL.Sync.fetchAll("SELECT * FROM users", {})
    if(result)then
        cb(result)
    else
        cb(nil)
    end
end)

RegisterServerEvent('es:savemoney')
AddEventHandler('es:savemoney', function()
    local source = source
    TriggerEvent('es:getPlayerFromId', source, function(user)
		if user ~= nil then
           MySQL.Async.insert("UPDATE users SET `money`=@value WHERE identifier = @identifier",{['@value'] = user.money, ['@identifier'] = user.identifier})
		end
	end)
end)

-- Function to update player money every 60 seconds.
local function savePlayerMoney()
        local source = source
	SetTimeout(60000, function()
		TriggerEvent("es:getPlayers", function(users)
			for k,v in pairs(users)do
				MySQL.Async.insert("UPDATE users SET `money`=@value WHERE identifier = @identifier",{['@value'] = v.money, ['@identifier'] = v.identifier})
			end
		end)

		savePlayerMoney()
	end)
end

savePlayerMoney()

-- GENERATING A CELLPHONE NUMBER, WITH... SOCIAL CLUB ID OMGG AGAIN
function generateTel(identifier)
	local tel = ""
	for i = #identifier, 1, -1 do
	    local c = string.sub(identifier,i,i)
	    -- do something with c
	    if (#tel) < 3 then
	        c = tonumber(c)
	        if c ~= nil then
	            c = tostring(c)
	            tel = tel .. c
	        end
	    end
    end
    tel = tel .. "-"
    local numero = math.random(1,9999)
    local teltemp = tel
    if numero < 10 then
        teltemp = teltemp.."000".. tostring(numero)
    elseif numero < 100 then
        teltemp = teltemp.."00".. tostring(numero)
    elseif numero < 1000 then
        teltemp = teltemp.."0".. tostring(numero)
    else
        teltemp = teltemp.. tostring(numero)
    end
    tel = teltemp
	return tel
end

-- GET PLATE NUMBER
RegisterServerEvent("es:getVehPlate")
AddEventHandler("es:getVehPlate", function()
	TriggerEvent('es:getPlayerFromId', source, function(user)
		TriggerClientEvent("es:f_getVehPlate", source, user.getVehicle())
	end)
end)

-- THE FUKED UP CODE THAT IS NOT SUPPOSE TO BE THERE BUT IT IS.
function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end
