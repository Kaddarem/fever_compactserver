RegisterServerEvent('es:updateName')
AddEventHandler('es:updateName', function(prenom, nom)
    local source = source
	TriggerEvent('es:getPlayerFromId', source, function(user)
		user.setNom(nom)
        user.nom = nom
		user.setPrenom(prenom)
        user.prenom = prenom
    local player = user.identifier
		MySQL.Sync.execute("UPDATE users SET `nom`=@value WHERE identifier = @identifier", {['@value'] = nom, ['@identifier'] = player})
		MySQL.Sync.execute("UPDATE users SET `prenom`=@value WHERE identifier = @identifier", {['@value'] = prenom, ['@identifier'] = player})
  end)
end)

RegisterServerEvent('es:getName')
AddEventHandler('es:getName', function()
    local source = source
	TriggerEvent('es:getPlayerFromId', source, function(user)
		local fullname = user.getPrenom() .. " " .. user.getNom()
    TriggerClientEvent("es:f_getName", source, fullname)
  end)
end)
