-- NO TOUCHY, IF SOMETHING IS WRONG CONTACT KANERSPS! --
-- NO TOUCHY, IF SOMETHING IS WRONG CONTACT KANERSPS! --
-- NO TOUCHY, IF SOMETHING IS WRONG CONTACT KANERSPS! --
-- NO TOUCHY, IF SOMETHING IS WRONG CONTACT KANERSPS! --


-- Server
Users = {}
commands = {}
settings = {}
settings.defaultSettings = {
	['banReason'] = "You are currently banned.",
	['pvpEnabled'] = true,
	['permissionDenied'] = false,
	['debugInformation'] = false,
	['startingCash'] = 4000,
	['enableRankDecorators'] = false,
}
settings.sessionSettings = {}

function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

-- gets the actual player id unique to the player,
-- independent of whether the player changes their screen name
function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end

AddEventHandler('playerConnecting', function(name, setCallback)
	local identifier = getPlayerID(source)
        --debugMsg('Checking user ban: ' .. identifier .. " (" .. name .. ")")
        
	local banned = isIdentifierBanned(identifier)
	if(banned ~= nil)then
        setCallback(banned)
		CancelEvent()
	end
end)

AddEventHandler('playerDropped', function()
    local source = source
	if(Users[source])then
		MySQL.Sync.execute("UPDATE users SET `money`=@value WHERE identifier = @identifier",
		{['@value'] = Users[source].money, ['@identifier'] = Users[source].identifier})
        Users[source] = nil
	end
end)

local justJoined = {}

RegisterServerEvent('es:firstJoinProper')
AddEventHandler('es:firstJoinProper', function()
	local identifier = getPlayerID(source)
		if(Users[source] == nil)then
			--debugMsg("Essential | Loading user. " .. GetPlayerName(source))
			registerUser(identifier, source)

			TriggerEvent('es:initialized', source)
			justJoined[source] = true

			if(settings.defaultSettings.pvpEnabled)then
				--TriggerClientEvent("es:enablePvp", source)
			end
		end
end)

AddEventHandler('es:setSessionSetting', function(k, v)
	settings.sessionSettings[k] = v
end)

AddEventHandler('es:getSessionSetting', function(k, cb)
	cb(settings.sessionSettings[k])
end)

RegisterServerEvent('playerSpawn')
AddEventHandler('playerSpawn', function()
	if(justJoined[source])then
		TriggerEvent("es:firstSpawn", source)
		justJoined[source] = nil
	end
end)

AddEventHandler("es:setDefaultSettings", function(tbl)
	for k,v in pairs(tbl) do
		if(settings.defaultSettings[k] ~= nil)then
			settings.defaultSettings[k] = v
		end
	end

	--debugMsg("Default settings edited.")
end)

AddEventHandler('chatMessage', function(source, n, message)
	if(startswith(message, "/"))then
		local command_args = stringsplit(message, " ")

		command_args[1] = string.gsub(command_args[1], "/", "")

		local command = commands[command_args[1]]

		if(command)then
			CancelEvent()
			if(command.perm > 0)then
				if(Users[source].permission_level >= command.perm or Users[source].group:canTarget(command.group))then
					command.cmd(source, command_args, Users[source])
					TriggerEvent("es:adminCommandRan", source, command_args, Users[source])
				else
					command.callbackfailed(source, command_args, Users[source])
					TriggerEvent("es:adminCommandFailed", source, command_args, Users[source])

					if(type(settings.defaultSettings.permissionDenied) == "string" and not WasEventCanceled())then
						TriggerClientEvent('chatMessage', source, "", {0,0,0}, defaultSettings.permissionDenied)
					end

					--debugMsg("Non admin (" .. GetPlayerName(source) .. ") attempted to run admin command: " .. command_args[1])
				end
			else
				command.cmd(source, command_args, Users[source])
				TriggerEvent("es:userCommandRan", source, command_args, Users[source])
			end
			
			TriggerEvent("es:commandRan", source, command_args, Users[source])
		else
			TriggerEvent('es:invalidCommandHandler', source, command_args, Users[source])
			CancelEvent()
		end
	else
		TriggerEvent('es:chatMessage', source, message, Users[source])
	end
end)

AddEventHandler('es:addCommand', function(command, callback)
	commands[command] = {}
	commands[command].perm = 0
	commands[command].group = "user"
	commands[command].cmd = callback

	--debugMsg("Command added: " .. command)
end)

AddEventHandler('es:addAdminCommand', function(command, perm, callback, callbackfailed)
	commands[command] = {}
	commands[command].perm = perm
	commands[command].group = "superadmin"
	commands[command].cmd = callback
	commands[command].callbackfailed = callbackfailed

	--debugMsg("Admin command added: " .. command .. ", requires permission level: " .. perm)
end)

AddEventHandler('es:addGroupCommand', function(command, group, callback, callbackfailed)
	commands[command] = {}
	commands[command].perm = math.maxinteger
	commands[command].group = group
	commands[command].cmd = callback
	commands[command].callbackfailed = callbackfailed

	--debugMsg("Group command added: " .. command .. ", requires group: " .. group)
end)

RegisterServerEvent('es:updatePositions')
AddEventHandler('es:updatePositions', function(x, y, z)
	if(Users[source])then
		Users[source].setCoords(x, y, z)
	end
end)

-- Info command
commands['info'] = {}
commands['info'].perm = 0
commands['info'].cmd = function(source, args, user)
--	TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "^2[^3EssentialMode^2]^0 Version: ^22.0.0")
--	TriggerClientEvent('chatMessage', source, 'SYSTEM', {255, 0, 0}, "^2[^3EssentialMode^2]^0 Commands loaded: ^2" .. (returnIndexesInTable(commands) - 1))
end
