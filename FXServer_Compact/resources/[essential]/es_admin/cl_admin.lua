local states = {}
states.frozen = false
states.frozenPos = nil

RegisterNetEvent('es_admin:spawnVehicle')
AddEventHandler('es_admin:spawnVehicle', function(v)
	local carid = GetHashKey(v)
	local playerPed = GetPlayerPed(-1)
	if playerPed and playerPed ~= -1 then
		RequestModel(carid)
		while not HasModelLoaded(carid) do
				Citizen.Wait(0)
		end
		local playerCoords = GetEntityCoords(playerPed)
        local heading = GetEntityHeading(playerPed)
		veh = CreateVehicle(carid, playerCoords, heading, true, false)
		TaskWarpPedIntoVehicle(playerPed, veh, -1)
--		SetEntityInvincible(veh, true)
	end
end)


RegisterNetEvent('es_admin:freezePlayer')
AddEventHandler("es_admin:freezePlayer", function(state)
	local player = PlayerId()

	local ped = GetPlayerPed(-1)

	states.frozen = state
	states.frozenPos = GetEntityCoords(ped, false)

	if not state then
			if not IsEntityVisible(ped) then
					SetEntityVisible(ped, true)
			end

			if not IsPedInAnyVehicle(ped) then
					SetEntityCollision(ped, true)
			end

			FreezeEntityPosition(ped, false)
			--SetCharNeverTargetted(ped, false)
			SetPlayerInvincible(player, false)
	else

			SetEntityCollision(ped, false)
			FreezeEntityPosition(ped, true)
			--SetCharNeverTargetted(ped, true)
			SetPlayerInvincible(player, true)
			--RemovePtfxFromPed(ped)

			if not IsPedFatallyInjured(ped) then
					ClearPedTasksImmediately(ped)
			end
            BoucleFreeze()
	end
end)

function BoucleFreeze()
    Citizen.CreateThread(function()
        while states.frozen do
            Citizen.Wait(10)
            ClearPedTasksImmediately(GetPlayerPed(-1))
            SetEntityCoords(GetPlayerPed(-1), states.frozenPos)
        end
    end)
end

RegisterNetEvent('es_admin:showid')
AddEventHandler('es_admin:showid', function(t)
        for id = 0, 500 do
            if  NetworkIsPlayerActive( id ) and GetPlayerPed( id ) then
 
                ped = GetPlayerPed( id )
                blip = GetBlipFromEntity( ped )
 
                -- HEAD DISPLAY STUFF --
 
                -- Create head display (this is safe to be spammed)
                 headId = Citizen.InvokeNative( 0xBFEFE3321A3F5015, ped, "- "..GetPlayerServerId(id).." -", false, false, "", false )
 
            end
 
        end
end)


RegisterNetEvent('es_admin:teleportUser')
AddEventHandler('es_admin:teleportUser', function(user)
	local pos = GetEntityCoords(GetPlayerPed(GetPlayerFromServerId(tonumber(user))))
	RequestCollisionAtCoord(pos.x, pos.y, pos.z)
	while not HasCollisionLoadedAroundEntity(GetPlayerPed(-1))do
		RequestCollisionAtCoord(pos.x, pos.y, pos.z)
		Citizen.Wait(0)
	end
	SetEntityCoords(GetPlayerPed(-1), pos)
	states.frozenPos = pos
end)

RegisterNetEvent('es_admin:slap')
AddEventHandler('es_admin:slap', function()
	local ped = GetPlayerPed(-1)

	ApplyForceToEntity(ped, 1, 9500.0, 3.0, 7100.0, 1.0, 0.0, 0.0, 1, false, true, false, false)
end)

RegisterNetEvent('es_admin:givePosition')
AddEventHandler('es_admin:givePosition', function(nom)
	local pos = GetEntityCoords(GetPlayerPed(-1))
    local direction = GetEntityHeading(GetPlayerPed(-1))
    local name = ""
    if nom ~= nil then
        name = "name = "..nom
    end
	local string = "{"..name..", x = " .. math.floor(pos.x*1000)/1000 .. ", y = " .. math.floor(pos.y*1000)/1000 .. ", z = " .. math.floor(pos.z*1000)/1000 -1 .. ", a= ".. math.floor(direction*1000)/1000 .."},\n"
	TriggerServerEvent('es_admin:givePos', string)
	TriggerEvent('chatMessage', 'SYSTEM', {255, 0, 0}, 'Position sauvegardé dans le fichier positions.txt.')
end)

RegisterNetEvent('es_admin:kill')
AddEventHandler('es_admin:kill', function()
	SetEntityHealth(GetPlayerPed(-1), 0)
end)

RegisterNetEvent('es_admin:crash')
AddEventHandler('es_admin:crash', function()
	while true do
	end
end)

local noclip = false

RegisterNetEvent("es_admin:noclip")
AddEventHandler("es_admin:noclip", function(t)
	local msg = "disabled"
	if(noclip == false)then
		noclip_pos = GetEntityCoords(GetPlayerPed(-1), false)
	end

	noclip = not noclip

	if(noclip)then
		msg = "enabled"
        BoucleNoClip()
	end

	TriggerEvent("chatMessage", "SYSTEM", {255, 0, 0}, "Noclip has been ^2^*" .. msg)
end)

local heading = 0

function BoucleNoClip()
    Citizen.CreateThread(function()
        local coef = 1.0
        while noclip do
            Citizen.Wait(5)
            SetEntityCoordsNoOffset(GetPlayerPed(-1),  noclip_pos.x,  noclip_pos.y,  noclip_pos.z,  0, 0, 0)
            coef = 1.0
            if(IsControlPressed(1,  21))then
                coef = 5.0
            end
            if(IsControlPressed(1,  34))then
                heading = heading + 1.5
                if(heading > 360)then
                    heading = 0
                end
                SetEntityHeading(GetPlayerPed(-1),  heading)
            end
            if(IsControlPressed(1,  9))then
                heading = heading - 1.5
                if(heading < 0)then
                    heading = 360
                end
                SetEntityHeading(GetPlayerPed(-1),  heading)
            end
            if(IsControlPressed(1,  8))then
                noclip_pos = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, coef * 1.0, 0.0)
            end
            if(IsControlPressed(1,  32))then
                noclip_pos = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, coef * -1.0, 0.0)
            end
            if(IsControlPressed(1,  27))then
                noclip_pos = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, 0.0, coef * 0.5)
            end
            if(IsControlPressed(1,  173))then
                noclip_pos = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, 0.0, coef * -0.5)
            end
        end
    end)
end


local visible = true
RegisterNetEvent('es_admin:invisible')
AddEventHandler('es_admin:invisible', function()
    if visible then
	   SetEntityVisible(GetPlayerPed(-1),false)
	   SetEntityInvincible(GetPlayerPed(-1),false)
        SetPlayerInvincible(PlayerId(),false)
       visible = false
    else
       SetEntityVisible(GetPlayerPed(-1),true)
	   SetEntityInvincible(GetPlayerPed(-1),true)
        SetPlayerInvincible(PlayerId(),true)
       visible = true
    end
end)