RegisterServerEvent('paycheck:salary')
AddEventHandler('paycheck:salary', function()
    local source = source
	TriggerEvent('es:getPlayerFromId', source, function(user)
        local user_id = user.identifier
        if user.job == 2 then
            local salary_job = MySQL.Sync.fetchScalar("SELECT salary FROM users INNER JOIN police ON users.police = police.police_id WHERE identifier = @username",{['@username'] = user_id})
            user.addMoney(salary_job)
            TriggerClientEvent("es_freeroam:notify", source, "CHAR_BANK_MAZE", 1, "Maze Bank", false, "Salaire metier reçu: ~g~+ $"..salary_job)       
        else
            local salary_job = MySQL.Sync.fetchScalar("SELECT salary FROM users INNER JOIN jobs ON users.job = jobs.job_id WHERE identifier = @username",{['@username'] = user_id})
            if salary_job ~= nil then
                if salary_job > 0 then
                    user.addMoney(salary_job)
                    TriggerClientEvent("es_freeroam:notify", source, "CHAR_BANK_MAZE", 1, "Maze Bank", false, "Salaire metier reçu: ~g~+ $"..salary_job)
                end
            end
        end
	end)
end)
