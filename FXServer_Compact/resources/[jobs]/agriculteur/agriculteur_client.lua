--[[
##################
#    Oskarr      #
#    MysticRP    #
#   client.lua   #
#      2017      #
##################
--]]
local jobId=-1
local isInServiceTaxi = false -- don't edit
local taxiplatee = " PAYSAN " -- Plate for service vehicle
local taximodel = GetHashKey('rebel') -- Model for service car
local taxijob = 7 -- JobID for taxi
local useModelMenu = true -- set to true if you use https://forum.fivem.net/t/release-async-model-menu-v2-6-17-6/19999
local openMenuKey = 166 -- (G) Key for OPEN TAXI MENU 
local emplacement = {
    {name="Entreprise d'Agriculture", id=473, x=984.036, y=-1532.4, z=30.7861},
}
local zonerecolt = {
    {name="Ferme des paysans", id=76, x=427.059, y=6470.291, z=28.790},
    {name="Transformation alimentaire", id=76, x=2887.979, y=4380.757, z=50.312},
    {name="Revente produit alimentaire", id=76, x=877.884, y=-1671.054, z=30.529},
}
local BLIPS = {}

local BLIPSZONE = {}
RegisterNetEvent('recolt:updateJobs')
AddEventHandler('recolt:updateJobs', function(newjob)
    Citizen.CreateThread(function()
        jobId = newjob
        for k, existingBlip in ipairs(BLIPS) do
            RemoveBlip(existingBlip)
            BLIPS = {}
        end
        for k, existingBlip in ipairs(BLIPSZONE) do
            RemoveBlip(existingBlip)
            BLIPSZONE = {}
        end
        isInServiceTaxi = false
        if jobId == taxijob then
            -- Show blip
            for _, item in pairs(emplacement) do
                blip = AddBlipForCoord(item.x, item.y, item.z)
                SetBlipSprite(blip, tonumber(item.id))
                SetBlipAsShortRange(blip, true)
                BeginTextCommandSetBlipName("STRING")
                AddTextComponentString(item.name)
                EndTextCommandSetBlipName(blip)
                table.insert(BLIPS,blip)
            end
            BoucleInfini2()
        end
    
     end)
end)
---- THRE
---- THREADS ----

-- Service
function BoucleInfini2()
    Citizen.CreateThread(function()
        local x = 984.036
        local y = -1532.4
        local z = 30.7861
        local BoucleLent = 5000
        local BoucleCourt = 5
        local TempsBoucle = BoucleCourt
        while jobId == taxijob do
            Wait(TempsBoucle)
            local playerPos = GetEntityCoords(GetPlayerPed(-1), true)
            if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 50.0) then
                TempsBoucle = BoucleCourt
                Marker(x, y, z -1,1.5,89, 191, 31)
            if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 2.0) then
                if isInServiceTaxi then
                    DisplayHelp('Appuyez sur ~INPUT_CONTEXT~ pour ~r~stopper~s~ votre service') 
                else
                    DisplayHelp('Appuyez sur ~INPUT_CONTEXT~ pour ~g~prendre~s~ votre service')
                end
                if (IsControlJustReleased(1, 51)) then 
                    GetService()
                end
            end
            else
                TempsBoucle = BoucleLent  
            end
        end
    end)
end

Citizen.CreateThread(function()
    VMenu.AddMenu( 'Argiculteur', 'interim',248,157,45)  
end)
-- Service Car
function BoucleInfini()
    -- Gestion du menu
    Citizen.CreateThread(function()   
        while isInServiceTaxi do
            Citizen.Wait(5)
            if IsControlJustPressed(1, openMenuKey) then 
                if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                VMenu.curItem = 1
                GPSMenu()  
                VMenu.visible = not VMenu.visible
            end
            VMenu.Show()
        end
    end)
    
    Citizen.CreateThread(function()
        local x = 998.489
        local y = -1533.28
        local z = 30.0861
        local BoucleLent = 5000
        local BoucleCourt = 5
        local TempsBoucle = BoucleCourt
        local ply = GetPlayerPed(-1)
        while isInServiceTaxi do
            Citizen.Wait(TempsBoucle)
            local playerPos = GetEntityCoords(GetPlayerPed(-1), true)
            if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 50.0) then
                TempsBoucle = BoucleCourt
                Marker(x, y, z -1,3.0,89, 191, 31)
                if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 4.0) then
                    if IsPedInAnyVehicle(ply, true) then
                        DisplayHelp('Appuyez sur ~INPUT_CONTEXT~ pour ~r~ranger~s~ votre ~b~camion')
                        if (IsControlJustReleased(1, 51)) then 
                            local vehicle = GetVehiclePedIsIn(ply, true)
                            local isVehicleTaxi = IsVehicleModel(vehicle, taximodel)
                            local isTaxiPlate = GetVehicleNumberPlateText(vehicle)
                         if isVehicleTaxi then
                            DeleteTaxi()
                         else
                         TriggerEvent('hud:NotifColor',"Ce n'est pas un camion d'agriculteur !",6)
                         end
                        end
                    else						
                        DisplayHelp('Appuyez sur ~INPUT_CONTEXT~ pour ~b~sortir~s~ un ~b~camion')
                        if (IsControlJustReleased(1, 51)) then 
                            Taxi()
                        end
                    end
                end
            else
                TempsBoucle = BoucleLent    
            end
        end
    end)
end

---------------------------

function Taxi()
	Citizen.Wait(0)
	local ped = GetPlayerPed(-1)
	local player = PlayerId()
	local vehicle = taximodel

	RequestModel(vehicle)

	while not HasModelLoaded(vehicle) do
		Wait(1)
	end

	--local plate = math.random(300, 900)
	local coords = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0, 5.0, 0)
    local caisse = GetClosestVehicle(coords, 3.000, 0, 70)
    SetEntityAsMissionEntity(caisse, true, true)
    Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(caisse))
	local spawned_car = CreateVehicle(vehicle, 998.489, -1533.28, 30.0861, 90.0, true, false)
	SetVehicleOnGroundProperly(spawned_car)
	SetVehicleColours(spawned_car, 12, 131)
	SetVehicleExtraColours(spawned_car, 12, 12)
	SetPedIntoVehicle(ped, spawned_car, - 1)
    local id = NetworkGetNetworkIdFromEntity(spawned_car)
        SetNetworkIdCanMigrate(id, true)
        SetNetworkIdExistsOnAllMachines(id,true)
    goC()
    TriggerEvent('hud:Notif',"GPS accessible via ~g~F5")
end

function DeleteTaxi()
    local ply = GetPlayerPed(-1)
    local playerVeh = GetVehiclePedIsIn(ply, false)
    Citizen.Wait(1)
    ClearPedTasksImmediately(ply)
    SetEntityVisible(playerVeh, false, 0)
    SetEntityCoords(playerVeh, 999999.0, 999999.0, 999999.0, false, false, false, true)
    FreezeEntityPosition(playerVeh, true)
    SetEntityAsMissionEntity(playerVeh, 1, 1)
    DeleteVehicle(playerVeh)
end


-------
---------


function GPSMenu() -- FACTURE MENU
    VMenu.ResetMenu()
    VMenu.EditHeader('GPS')
    VMenu.AddFunc("Ferme des paysans", "goC", nil,'Mettre le GPS')
	VMenu.AddFunc("Transformation alimentaire", "goPO", nil,'Mettre le GPS')
    VMenu.AddFunc("Revente de produit alimentaire", "goPF", nil,'Mettre le GPS')
	VMenu.AddFunc("Fleeca Banque", "goFB", nil,'Mettre le GPS')	
end


--------------- GPS COORDS
function goC(x, y, z)
x = 427.163
y = 6469.97
z = 28.7881
BLIPP = AddBlipForCoord(x, y, z)
SetBlipSprite(BLIPP, 2)
SetNewWaypoint(x, y)
end
function goPO(x, y, z)
x = 2888.07
y = 4380.71
z = 50.3127
BLIPP = AddBlipForCoord(x, y, z)
SetBlipSprite(BLIPP, 2)
SetNewWaypoint(x, y)
end
function goPF(x, y, z)
x = 877.974
y = -1671.1
z = 30.5318
BLIPP = AddBlipForCoord(x, y, z)
SetBlipSprite(BLIPP, 2)
SetNewWaypoint(x, y)
end
function goFB(x, y, z)
x = 152.32469177246
y = -1030.0135498047
z = 29.185220718384
BLIPP = AddBlipForCoord(x, y, z)
SetBlipSprite(BLIPP, 2)
SetNewWaypoint(x, y)
end
--------------------------


function GetService()
    Citizen.CreateThread(function()
    if jobId ~= taxijob then
     TriggerEvent('hud:NotifColor',"Tu n'est pas mineur !",6) 
            return
    end
	if isInServiceTaxi then
		TriggerEvent('hud:Notif',"Vous avez ~r~fini~s~ votre service") 
		if (useModelMenu == true) then
		TriggerServerEvent("mm:spawn2") 
		end
        for k, existingBlip in ipairs(BLIPSZONE) do
                RemoveBlip(existingBlip)
                BLIPSZONE = {}
        end
	else 
		TriggerEvent('hud:NotifColor',"Vous êtes en service !",141) 
		TriggerEvent('hud:Notif',"Pensez à prendre votre véhicule !") 
        BoucleInfini()
            for k, existingBlip in ipairs(BLIPSZONE) do
                RemoveBlip(existingBlip)
                BLIPSZONE = {}
            end
            -- Show blip
            for _, item in pairs(zonerecolt) do
                blip = AddBlipForCoord(item.x, item.y, item.z)
                SetBlipSprite(blip, tonumber(item.id))
                SetBlipAsShortRange(blip, true)
                BeginTextCommandSetBlipName("STRING")
                AddTextComponentString(item.name)
                EndTextCommandSetBlipName(blip)
                table.insert(BLIPSZONE,blip)
            end
	end
	
	isInServiceTaxi = not isInServiceTaxi
-- Here for any clothes with SetPedComponentVariation ...
    SetPedPropIndex(GetPlayerPed(-1), 0, 0, 0, 0)--ear defenders
    if GetEntityModel(GetPlayerPed(-1)) == GetHashKey("mp_f_freemode_01") then
        SetPedComponentVariation(GetPlayerPed(-1), 8, 36, 0, 0)--Gilet Jaune Femme
        SetPedComponentVariation(GetPlayerPed(-1), 6, 63, 0, 0)--Chaussure
    else
        SetPedComponentVariation(GetPlayerPed(-1), 8, 59, 0, 0)--Gilet Jaune Homme
    SetPedComponentVariation(GetPlayerPed(-1), 6, 60, 0, 0)--Chaussure
    end
    end)
end

