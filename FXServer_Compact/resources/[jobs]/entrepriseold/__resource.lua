-- Manifest


-- Requiring essentialmode
dependency 'essentialmode'

server_script '@mysql-async/lib/MySQL.lua'
client_script 'entreprise_client.lua'
client_script 'entreprise_gui.lua'
server_script 'entreprise_server.lua'