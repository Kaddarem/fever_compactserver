RegisterServerEvent('entreprise:addmoney')
AddEventHandler('entreprise:addmoney', function (id_entreprise, monnaie)
    local source = source
    local result = MySQL.Sync.fetchAll("SELECT * FROM entreprise WHERE id = @entreprise", {['@entreprise'] = id_entreprise})
    if result then
        transaction = tonumber(result[1].transaction)
        caisse = tonumber(result[1].caisse)
        ca = tonumber(result[1].ca)
        pourcentage = tonumber(result[1].pourcentage)
        monnaie = tonumber(monnaie)
        transaction = transaction + 1
        ca = ca + monnaie
        monnaie = monnaie*pourcentage*0.01
        caisse = caisse + monnaie
        MySQL.Sync.execute("UPDATE entreprise SET caisse = @money, ca = @ca, transaction =@transaction WHERE id=@entreprise",{['@money']= caisse,['@ca']=ca,['@transaction']=transaction,['@entreprise']=id_entreprise})
    else
        local erreur = 'Erreur avec l\'entreprise '..id_entreprise
    end
end)

RegisterServerEvent('entreprise:valeur')
AddEventHandler('entreprise:valeur', function()
    local identifier = getPlayerID(source)
    local entreprise = {}
    local source = source
    local result = MySQL.Sync.fetchAll("SELECT * FROM entreprise WHERE proprietaire = @identifier",{['@identifier']=identifier})
    if result then
        for k,v in pairs (result) do
            tampon = {}
            tampon.proprio = v.proprietaire
            tampon.nom = v.nom
            tampon.id = v.id
            tampon.pourcentage = v.pourcentage
            tampon.caisse = v.caisse
            tampon.ca = v.ca
            tampon.transaction = v.transaction
            coord = {}
            str, sep = v.lieu, ","
            string.gsub(str,"([^,]*),", function(c)
                table.insert(coord, tonumber(c))
            end)
            tampon.x = coord[1]
            tampon.y = coord[2]
            tampon.z = coord[3] - 1
            table.insert(entreprise, tampon)
        end
    end
    TriggerClientEvent('entreprise:bureau',source, entreprise, identifier)
end)

RegisterServerEvent('entreprise:takemoney')
AddEventHandler('entreprise:takemoney', function(entreprise)
    local source = source
    TriggerEvent('es:getPlayerFromId', source, function(user)
    MySQL.Sync.execute("UPDATE entreprise SET caisse = 0 WHERE id=@entreprise",{['@entreprise']=entreprise.id})
    local player = user.identifier
    local amount = tonumber(entreprise.caisse)
    user.addMoney((amount))
    local new_balance = bankBalance(player)
    TriggerClientEvent("banking:updateBalance", source, new_balance)
    end)
end)

function bankBalance(player)
  local resultat = MySQL.Sync.fetchScalar("SELECT bankbalance FROM users WHERE identifier = @name", {['@name'] = player})
  return tonumber(resultat)
end

-- get's the player id without having to use bugged essentials
function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

-- gets the actual player id unique to the player,
-- independent of whether the player changes their screen name
function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end
