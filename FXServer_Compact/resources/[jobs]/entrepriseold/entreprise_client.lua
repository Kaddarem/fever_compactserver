local id_entreprise = 1 --LSCustom
local bureaux = {}
local proprio = false
local incircle = false
local options = {
    x = 0.1,
    y = 0.2,
    width = 0.2,
    height = 0.04,
    scale = 0.4,
    font = 0,
    menu_title = "Shop",
    menu_subtitle = "Rayons",
    menu_info = "Argent",
    color_r = 30,
    color_g = 144,
    color_b = 255,
}

--bureaux = {{name = "LSCuton", proprio = 'steam:steam:1100001047d45bd',x = -209.512, y=-1337.505, z=33.894}}

--------- Définition des lieux ----------------$
AddEventHandler('playerSpawned', function(spawn)
        TriggerServerEvent('entreprise:valeur')
end)

RegisterNetEvent('entreprise:bureau')
AddEventHandler('entreprise:bureau', function(entreprises)
    bureaux =  entreprises
    if bureaux[1] ~= nil then
        proprio = true
    end
end)
---------------- formatage argent ------------------
function comma_value(n)
	local left,num,right = string.match(n,'^([^%d]*%d)(%d*)(.-)$')
	return left..(num:reverse():gsub('(%d%d%d)','%1 '):reverse())..right
end

---------- Gestion du LSC --------------
RegisterNetEvent('lsc:buyAmelioration')
AddEventHandler('lsc:buyAmelioration', function(listbutton)
    TriggerServerEvent('entreprise:addmoney', id_entreprise, listbutton.costs)
end)

function Notify(text)
    SetNotificationTextEntry('STRING')
    AddTextComponentString(text)
    DrawNotification(false, false)
end

function Main(bureau)
    nom = tostring(bureau.nom)
    argent = tostring(comma_value(bureau.caisse))
    argent = 'Caisse '..argent..''
    options.menu_title = nom
    options.menu_subtitle = "GESTIONNAIRE ENTREPRISE"
    options.menu_info = argent
    ClearMenu()
	Menu.addButton("Retirer argent", "retirerargent", bureau)
end

function retirerargent(bureau)
    TriggerServerEvent('entreprise:takemoney',bureau)
    Menu.hidden = true
    TriggerServerEvent('entreprise:valeur')
end


Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
        if proprio then
            for _, bureau in pairs(bureaux) do
                DrawMarker(1, bureau.x, bureau.y, bureau.z, 0, 0, 0, 0, 0, 0, 1.0, 1.0, 0.5001, 0, 155, 255, 200, 0, 0, 0, 0)
                if GetDistanceBetweenCoords(bureau.x, bureau.y, bureau.z, GetEntityCoords(GetPlayerPed(-1))) < 1.5 then
                    if (incircle == false) then
                        DisplayHelpText("Appuyer sur ~INPUT_CONTEXT~ pour gérer l'entreprise")
                    end
                    incircle = true
                    bureauactif=bureau.id -- Draw menu on each tick if Menu.hidden = false
                else
                    incircle = false
                    Menu.hidden = true
                end
            end
            if incircle then
                if IsControlJustReleased(1, 51) then -- INPUT_CELLPHONE_DOWN
                    TriggerServerEvent('entreprise:valeur')
                    for _, bureau in pairs(bureaux) do
                        if bureau.id == bureauactif then
                            Main(bureau) -- Menu to draw
                            Menu.hidden = not Menu.hidden -- Hide sur Show the menu
                        end
                    end    
                end
                Menu.renderGUI(options)
            end
        end
	end
end)
function DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end