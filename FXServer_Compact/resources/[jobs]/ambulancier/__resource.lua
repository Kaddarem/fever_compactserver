-- Manifest


-- Requiring essentialmode
dependency 'essentialmode'

-- Emergency
client_script {
    '@hud-event/hudevent_aff.lua',
    '@appmenu/menu_cl.lua',
    'client/cl_patient.lua',
    'client/cl_healthplayer.lua',
    'client/cl_emergency.lua',
    'teleportation/teleportation_client.lua',
    '@entreprise/tattoo/tattoo_list.lua',
}

server_script { 
    '@mysql-async/lib/MySQL.lua',
    'server/sv_emergency.lua',
    'server/sv_patient.lua',
}
