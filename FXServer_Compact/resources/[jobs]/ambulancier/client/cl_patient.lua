local pedType={
    "A_C_BOAR",
    "A_C_CHICKENHAWK",
    "A_C_CHIMP",
    "A_C_CORMORANT",
    "A_C_COW",
    "A_C_COYOTE",
    "A_C_CROW",
    "A_C_DEER",
    "A_C_FISH",
    "A_C_HEN",
    "A_C_PIG",
    "A_C_PIGEON",
    "A_C_RAT",
    "A_C_RHESUS",
    "A_C_SEAGULL",
    "A_F_M_BEACH_01",
    "A_F_M_BEVHILLS_01",
    "A_F_M_BEVHILLS_02",
    "A_F_M_BODYBUILD_01",
    "A_F_M_BUSINESS_02",
    "A_F_M_DOWNTOWN_01",
    "A_F_M_EASTSA_01",
    "A_F_M_EASTSA_02",
    "A_F_M_FATBLA_01",
    "A_F_M_FATCULT_01",
    "A_F_M_FATWHITE_01",
    "A_F_M_KTOWN_01",
    "A_F_M_KTOWN_02",
    "A_F_M_PROLHOST_01",
    "A_F_M_SALTON_01",
    "A_F_M_SKIDROW_01",
    "A_F_M_SOUCENTMC_01",
    "A_F_M_SOUCENT_01",
    "A_F_M_SOUCENT_02",
    "A_F_M_TOURIST_01",
    "A_F_M_TRAMPBEAC_01",
    "A_F_M_TRAMP_01",
    "A_F_O_GENSTREET_01",
    "A_F_O_INDIAN_01",
    "A_F_O_KTOWN_01",
    "A_F_O_SALTON_01",
    "A_F_O_SOUCENT_01",
    "A_F_O_SOUCENT_02",
    "A_F_Y_BEACH_01",
    "A_F_Y_BEVHILLS_01",
    "A_F_Y_BEVHILLS_02",
    "A_F_Y_BEVHILLS_03",
    "A_F_Y_BEVHILLS_04",
    "A_F_Y_BUSINESS_01",
    "A_F_Y_BUSINESS_02",
    "A_F_Y_BUSINESS_03",
    "A_F_Y_BUSINESS_04",
    "A_F_Y_EASTSA_01",
    "A_F_Y_EASTSA_02",
    "A_F_Y_EASTSA_03",
    "A_F_Y_EPSILON_01",
    "A_F_Y_FITNESS_01",
    "A_F_Y_FITNESS_02",
    "A_F_Y_GENHOT_01",
    "A_F_Y_GOLFER_01",
    "A_F_Y_HIKER_01",
    "A_F_Y_HIPPIE_01",
    "A_F_Y_HIPSTER_01",
    "A_F_Y_HIPSTER_02",
    "A_F_Y_HIPSTER_03",
    "A_F_Y_HIPSTER_04",
    "A_F_Y_INDIAN_01",
    "A_F_Y_JUGGALO_01",
    "A_F_Y_RUNNER_01",
    "A_F_Y_RURMETH_01",
    "A_F_Y_SCDRESSY_01",
    "A_F_Y_SKATER_01",
    "A_F_Y_SOUCENT_01",
    "A_F_Y_SOUCENT_02",
    "A_F_Y_SOUCENT_03",
    "A_F_Y_TENNIS_01",
    "A_F_Y_TOPLESS_01",
    "A_F_Y_TOURIST_01",
    "A_F_Y_TOURIST_02",
    "A_F_Y_VINEWOOD_01",
    "A_F_Y_VINEWOOD_02",
    "A_F_Y_VINEWOOD_03",
    "A_F_Y_VINEWOOD_04",
    "A_F_Y_YOGA_01",
    "A_M_M_ACULT_01",
    "A_M_M_AFRIAMER_01",
    "A_M_M_BEACH_01",
    "A_M_M_BEACH_02",
    "A_M_M_BEVHILLS_01",
    "A_M_M_BEVHILLS_02",
    "A_M_M_BUSINESS_01",
    "A_M_M_EASTSA_01",
    "A_M_M_EASTSA_02",
    "A_M_M_FARMER_01",
    "A_M_M_FATLATIN_01",
    "A_M_M_GENFAT_01",
    "A_M_M_GENFAT_02",
    "A_M_M_GOLFER_01",
    "A_M_M_HASJEW_01",
    "A_M_M_HILLBILLY_01",
    "A_M_M_HILLBILLY_02",
    "A_M_M_INDIAN_01",
    "A_M_M_KTOWN_01",
    "A_M_M_MALIBU_01",
    "A_M_M_MEXCNTRY_01",
    "A_M_M_MEXLABOR_01",
    "A_M_M_OG_BOSS_01",
    "A_M_M_PAPARAZZI_01",
    "A_M_M_POLYNESIAN_01",
    "A_M_M_PROLHOST_01",
    "A_M_M_RURMETH_01",
    "A_M_M_SALTON_01",
    "A_M_M_SALTON_02",
    "A_M_M_SALTON_03",
    "A_M_M_SALTON_04",
    "A_M_M_SKATER_01",
    "A_M_M_SKIDROW_01",
    "A_M_M_SOCENLAT_01",
    "A_M_M_SOUCENT_01",
    "A_M_M_SOUCENT_02",
    "A_M_M_SOUCENT_03",
    "A_M_M_SOUCENT_04",
    "A_M_M_STLAT_02",
    "A_M_M_TENNIS_01",
    "A_M_M_TOURIST_01",
    "A_M_M_TRAMPBEAC_01",
    "A_M_M_TRAMP_01",
    "A_M_M_TRANVEST_01",
    "A_M_M_TRANVEST_02",
    "A_M_O_ACULT_01",
    "A_M_O_ACULT_02",
    "A_M_O_BEACH_01",
    "A_M_O_GENSTREET_01",
    "A_M_O_KTOWN_01",
    "A_M_O_SALTON_01",
    "A_M_O_SOUCENT_01",
    "A_M_O_SOUCENT_02",
    "A_M_O_SOUCENT_03",
    "A_M_O_TRAMP_01",
    "A_M_Y_ACULT_01",
    "A_M_Y_ACULT_02",
    "A_M_Y_BEACHVESP_01",
    "A_M_Y_BEACHVESP_02",
    "A_M_Y_BEACH_01",
    "A_M_Y_BEACH_02",
    "A_M_Y_BEACH_03",
    "A_M_Y_BEVHILLS_01",
    "A_M_Y_BEVHILLS_02",
    "A_M_Y_BREAKDANCE_01",
    "A_M_Y_BUSICAS_01",
    "A_M_Y_BUSINESS_01",
    "A_M_Y_BUSINESS_02",
    "A_M_Y_BUSINESS_03",
    "A_M_Y_CYCLIST_01",
    "A_M_Y_DHILL_01",
    "A_M_Y_DOWNTOWN_01",
    "A_M_Y_EASTSA_01",
    "A_M_Y_EASTSA_02",
    "A_M_Y_EPSILON_01",
    "A_M_Y_EPSILON_02",
    "A_M_Y_GAY_01",
    "A_M_Y_GAY_02",
    "A_M_Y_GENSTREET_01",
    "A_M_Y_GENSTREET_02",
    "A_M_Y_GOLFER_01",
    "A_M_Y_HASJEW_01",
    "A_M_Y_HIKER_01",
    "A_M_Y_HIPPY_01",
    "A_M_Y_HIPSTER_01",
    "A_M_Y_HIPSTER_02",
    "A_M_Y_HIPSTER_03",
    "A_M_Y_INDIAN_01",
    "A_M_Y_JETSKI_01",
    "A_M_Y_JUGGALO_01",
    "A_M_Y_KTOWN_01",
    "A_M_Y_KTOWN_02",
    "A_M_Y_LATINO_01",
    "A_M_Y_METHHEAD_01",
    "A_M_Y_MEXTHUG_01",
    "A_M_Y_MOTOX_01",
    "A_M_Y_MOTOX_02",
    "A_M_Y_MUSCLBEAC_01",
    "A_M_Y_MUSCLBEAC_02",
    "A_M_Y_POLYNESIAN_01",
    "A_M_Y_ROADCYC_01",
    "A_M_Y_RUNNER_01",
    "A_M_Y_RUNNER_02",
    "A_M_Y_SALTON_01",
    "A_M_Y_SKATER_01",
    "A_M_Y_SKATER_02",
    "A_M_Y_SOUCENT_01",
    "A_M_Y_SOUCENT_02",
    "A_M_Y_SOUCENT_03",
    "A_M_Y_SOUCENT_04",
    "A_M_Y_STBLA_01",
    "A_M_Y_STBLA_02",
    "A_M_Y_STLAT_01",
    "A_M_Y_STWHI_01",
    "A_M_Y_STWHI_02",
    "A_M_Y_SUNBATHE_01",
    "A_M_Y_SURFER_01",
    "A_M_Y_VINDOUCHE_01",
    "A_M_Y_VINEWOOD_01",
    "A_M_Y_VINEWOOD_02",
    "A_M_Y_VINEWOOD_03",
    "A_M_Y_VINEWOOD_04",
    "A_M_Y_YOGA_01",
    "COMP_PEDS_GENERIC",
    "COMP_PEDS_HELMETS_MOPED",
    "COMP_PEDS_HELMETS_MOTOX",
    "COMP_PEDS_HELMETS_SHORTY",
    "COMP_PEDS_HELMETS_SPORTS",
    "COMP_PEDS_MARINE",
    "CSB_ABIGAIL",
    "CSB_ANITA",
    "CSB_ANTON",
    "CSB_BALLASOG",
    "CSB_BRIDE",
    "CSB_BURGERDRUG",
    "CSB_CAR3GUY1",
    "CSB_CAR3GUY2",
    "CSB_CHEF",
    "CSB_CHIN_GOON",
    "CSB_CLETUS",
    "CSB_COP",
    "CSB_CUSTOMER",
    "CSB_DENISE_FRIEND",
    "CSB_FOS_REP",
    "CSB_G",
    "CSB_GROOM",
    "CSB_GROVE_STR_DLR",
    "CSB_HAO",
    "CSB_HUGH",
    "CSB_IMRAN",
    "CSB_JANITOR",
    "CSB_MAUDE",
    "CSB_MWEATHER",
    "CSB_ORTEGA",
    "CSB_OSCAR",
    "CSB_PORNDUDES",
    "CSB_PROLOGUEDRIVER",
    "CSB_PROLSEC",
    "CSB_RAMP_GANG",
    "CSB_RAMP_HIC",
    "CSB_RAMP_HIPSTER",
    "CSB_RAMP_MARINE",
    "CSB_RAMP_MEX",
    "CSB_REPORTER",
    "CSB_ROCCOPELOSI",
    "CSB_SCREEN_WRITER",
    "CSB_STRIPPER_01",
    "CSB_STRIPPER_02",
    "CSB_TONYA",
    "CSB_TRAFFICWARDEN",
    "G_F_Y_BALLAS_01",
    "G_F_Y_FAMILIES_01",
    "G_F_Y_LOST_01",
    "G_F_Y_VAGOS_01",
    "G_M_M_ARMBOSS_01",
    "G_M_M_ARMGOON_01",
    "G_M_M_ARMLIEUT_01",
    "G_M_M_CHEMWORK_01",
    "G_M_M_CHIBOSS_01",
    "G_M_M_CHICOLD_01",
    "G_M_M_CHIGOON_01",
    "G_M_M_CHIGOON_02",
    "G_M_M_KORBOSS_01",
    "G_M_M_MEXBOSS_01",
    "G_M_M_MEXBOSS_02",
    "G_M_Y_ARMGOON_02",
    "G_M_Y_AZTECA_01",
    "G_M_Y_BALLAEAST_01",
    "G_M_Y_BALLAORIG_01",
    "G_M_Y_BALLASOUT_01",
    "G_M_Y_FAMCA_01",
    "G_M_Y_FAMDNF_01",
    "G_M_Y_FAMFOR_01",
    "G_M_Y_KOREAN_01",
    "G_M_Y_KOREAN_02",
    "G_M_Y_KORLIEUT_01",
    "G_M_Y_LOST_01",
    "G_M_Y_LOST_02",
    "G_M_Y_LOST_03",
    "G_M_Y_MEXGANG_01",
    "G_M_Y_MEXGOON_01",
    "G_M_Y_MEXGOON_02",
    "G_M_Y_MEXGOON_03",
    "G_M_Y_POLOGOON_01",
    "G_M_Y_POLOGOON_02",
    "G_M_Y_SALVABOSS_01",
    "G_M_Y_SALVAGOON_01",
    "G_M_Y_SALVAGOON_02",
    "G_M_Y_SALVAGOON_03",
    "G_M_Y_STRPUNK_01",
    "G_M_Y_STRPUNK_02",
    "IG_ABIGAIL",
    "IG_ASHLEY",
    "IG_BANKMAN",
    "IG_BARRY",
    "IG_BESTMEN",
    "IG_BEVERLY",
    "IG_BRIDE",
    "IG_CAR3GUY1",
    "IG_CAR3GUY2",
    "IG_CASEY",
    "IG_CHEF",
    "IG_CHENGSR",
    "IG_CHRISFORMAGE",
    "IG_CLAY",
    "IG_CLAYPAIN",
    "IG_CLETUS",
    "IG_DALE",
    "IG_DREYFUSS",
    "IG_FBISUIT_01",
    "IG_GROOM",
    "IG_HAO",
    "IG_HUNTER",
    "IG_JANET",
    "IG_JEWELASS",
    "IG_JIMMYBOSTON",
    "IG_JOEMINUTEMAN",
    "IG_JOHNNYKLEBITZ",
    "IG_JOSEF",
    "IG_JOSH",
    "IG_KERRYMCINTOSH",
    "IG_LIFEINVAD_01",
    "IG_LIFEINVAD_02",
    "IG_MAGENTA",
    "IG_MANUEL",
    "IG_MARNIE",
    "IG_MARYANN",
    "IG_MAUDE",
    "IG_MICHELLE",
    "IG_MRSPHILLIPS",
    "IG_MRS_THORNHILL",
    "IG_NATALIA",
    "IG_NIGEL",
    "IG_OLD_MAN1A",
    "IG_OLD_MAN2",
    "IG_ONEIL",
    "IG_ORTEGA",
    "IG_PAPER",
    "IG_PRIEST",
    "IG_PROLSEC_02",
    "IG_RAMP_GANG",
    "IG_RAMP_HIC",
    "IG_RAMP_HIPSTER",
    "IG_RAMP_MEX",
    "IG_ROCCOPELOSI",
    "IG_RUSSIANDRUNK",
    "IG_SCREEN_WRITER",
    "IG_TALINA",
    "IG_TANISHA",
    "IG_TERRY",
    "IG_TOMEPSILON",
    "IG_TONYA",
    "IG_TRAFFICWARDEN",
    "IG_TYLERDIX",
    "IG_ZIMBOR",
    "MP_G_M_PROS_01",
    "MP_M_EXARMY_01",
    "SLOD_HUMAN",
    "SLOD_LARGE_QUADPED",
    "SLOD_SMALL_QUADPED",
    "S_F_M_FEMBARBER",
    "S_F_M_MAID_01",
    "S_F_M_SHOP_HIGH",
    "S_F_M_SWEATSHOP_01",
    "S_F_Y_AIRHOSTESS_01",
    "S_F_Y_BARTENDER_01",
    "S_F_Y_BAYWATCH_01",
    "S_F_Y_COP_01",
    "S_F_Y_FACTORY_01",
    "S_F_Y_HOOKER_01",
    "S_F_Y_HOOKER_02",
    "S_F_Y_HOOKER_03",
    "S_F_Y_MIGRANT_01",
    "S_F_Y_MOVPREM_01",
    "S_F_Y_RANGER_01",
    "S_F_Y_SCRUBS_01",
    "S_F_Y_SHERIFF_01",
    "S_F_Y_SHOP_LOW",
    "S_F_Y_SHOP_MID",
    "S_F_Y_STRIPPERLITE",
    "S_F_Y_STRIPPER_01",
    "S_F_Y_STRIPPER_02",
    "S_F_Y_SWEATSHOP_01",
    "S_M_M_AMMUCOUNTRY",
    "S_M_M_ARMOURED_01",
    "S_M_M_ARMOURED_02",
    "S_M_M_AUTOSHOP_01",
    "S_M_M_AUTOSHOP_02",
    "S_M_M_BOUNCER_01",
    "S_M_M_CHEMSEC_01",
    "S_M_M_CIASEC_01",
    "S_M_M_CNTRYBAR_01",
    "S_M_M_DOCKWORK_01",
    "S_M_M_DOCTOR_01",
    "S_M_M_FIBOFFICE_01",
    "S_M_M_FIBOFFICE_02",
    "S_M_M_GAFFER_01",
    "S_M_M_GARDENER_01",
    "S_M_M_GENTRANSPORT",
    "S_M_M_HAIRDRESS_01",
    "S_M_M_HIGHSEC_01",
    "S_M_M_HIGHSEC_02",
    "S_M_M_JANITOR",
    "S_M_M_LATHANDY_01",
    "S_M_M_LIFEINVAD_01",
    "S_M_M_LINECOOK",
    "S_M_M_LSMETRO_01",
    "S_M_M_MARIACHI_01",
    "S_M_M_MARINE_01",
    "S_M_M_MARINE_02",
    "S_M_M_MIGRANT_01",
    "S_M_M_MOVALIEN_01",
    "S_M_M_MOVPREM_01",
    "S_M_M_MOVSPACE_01",
    "S_M_M_PARAMEDIC_01",
    "S_M_M_PILOT_01",
    "S_M_M_PILOT_02",
    "S_M_M_POSTAL_01",
    "S_M_M_POSTAL_02",
    "S_M_M_PRISGUARD_01",
    "S_M_M_SCIENTIST_01",
    "S_M_M_SECURITY_01",
    "S_M_M_SNOWCOP_01",
    "S_M_M_STRPERF_01",
    "S_M_M_STRPREACH_01",
    "S_M_M_STRVEND_01",
    "S_M_M_TRUCKER_01",
    "S_M_M_UPS_01",
    "S_M_M_UPS_02",
    "S_M_O_BUSKER_01",
    "S_M_Y_AIRWORKER",
    "S_M_Y_AMMUCITY_01",
    "S_M_Y_ARMYMECH_01",
    "S_M_Y_AUTOPSY_01",
    "S_M_Y_BARMAN_01",
    "S_M_Y_BAYWATCH_01",
    "S_M_Y_BLACKOPS_01",
    "S_M_Y_BLACKOPS_02",
    "S_M_Y_BUSBOY_01",
    "S_M_Y_CHEF_01",
    "S_M_Y_CLOWN_01",
    "S_M_Y_CONSTRUCT_01",
    "S_M_Y_CONSTRUCT_02",
    "S_M_Y_COP_01",
    "S_M_Y_DEALER_01",
    "S_M_Y_DEVINSEC_01",
    "S_M_Y_DOCKWORK_01",
    "S_M_Y_DOORMAN_01",
    "S_M_Y_DWSERVICE_01",
    "S_M_Y_DWSERVICE_02",
    "S_M_Y_FACTORY_01",
    "S_M_Y_FIREMAN_01",
    "S_M_Y_GARBAGE",
    "S_M_Y_GRIP_01",
    "S_M_Y_HWAYCOP_01",
    "S_M_Y_MARINE_01",
    "S_M_Y_MARINE_02",
    "S_M_Y_MARINE_03",
    "S_M_Y_MIME",
    "S_M_Y_PESTCONT_01",
    "S_M_Y_PILOT_01",
    "S_M_Y_PRISMUSCL_01",
    "S_M_Y_PRISONER_01",
    "S_M_Y_RANGER_01",
    "S_M_Y_ROBBER_01",
    "S_M_Y_SHERIFF_01",
    "S_M_Y_SHOP_MASK",
    "S_M_Y_STRVEND_01",
    "S_M_Y_SWAT_01",
    "S_M_Y_USCG_01",
    "S_M_Y_VALET_01",
    "S_M_Y_WAITER_01",
    "S_M_Y_WINCLEAN_01",
    "S_M_Y_XMECH_01",
    "S_M_Y_XMECH_02",
    "U_F_M_CORPSE_01",
    "U_F_M_MIRANDA",
    "U_F_M_PROMOURN_01",
    "U_F_O_MOVIESTAR",
    "U_F_O_PROLHOST_01",
    "U_F_Y_BIKERCHIC",
    "U_F_Y_COMJANE",
    "U_F_Y_CORPSE_01",
    "U_F_Y_CORPSE_02",
    "U_F_Y_HOTPOSH_01",
    "U_F_Y_JEWELASS_01",
    "U_F_Y_MISTRESS",
    "U_F_Y_POPPYMICH",
    "U_F_Y_PRINCESS",
    "U_F_Y_SPYACTRESS",
    "U_M_M_ALDINAPOLI",
    "U_M_M_BANKMAN",
    "U_M_M_BIKEHIRE_01",
    "U_M_M_FIBARCHITECT",
    "U_M_M_FILMDIRECTOR",
    "U_M_M_GLENSTANK_01",
    "U_M_M_GRIFF_01",
    "U_M_M_JESUS_01",
    "U_M_M_JEWELSEC_01",
    "U_M_M_JEWELTHIEF",
    "U_M_M_MARKFOST",
    "U_M_M_PARTYTARGET",
    "U_M_M_PROLSEC_01",
    "U_M_M_PROMOURN_01",
    "U_M_M_RIVALPAP",
    "U_M_M_SPYACTOR",
    "U_M_M_WILLYFIST",
    "U_M_O_FINGURU_01",
    "U_M_O_TAPHILLBILLY",
    "U_M_O_TRAMP_01",
    "U_M_Y_ABNER",
    "U_M_Y_ANTONB",
    "U_M_Y_BABYD",
    "U_M_Y_BAYGOR",
    "U_M_Y_BURGERDRUG_01",
    "U_M_Y_CHIP",
    "U_M_Y_CYCLIST_01",
    "U_M_Y_FIBMUGGER_01",
    "U_M_Y_GUIDO_01",
    "U_M_Y_GUNVEND_01",
    "U_M_Y_HIPPIE_01",
    "U_M_Y_IMPORAGE",
    "U_M_Y_JUSTIN",
    "U_M_Y_MANI",
    "U_M_Y_MILITARYBUM",
    "U_M_Y_PAPARAZZI",
    "U_M_Y_PARTY_01",
    "U_M_Y_POGO_01",
    "U_M_Y_PRISONER_01",
    "U_M_Y_PROLDRIVER_01",
    "U_M_Y_RSRANGER_01",
    "U_M_Y_SBIKE",
    "U_M_Y_STAGGRM_01",
    "U_M_Y_TATTOO_01",
    "U_M_Y_ZOMBIE_01",
    "A_C_CHOP",
    "A_C_HUSKY",
    "A_C_MTLION",
    "A_C_RETRIEVER",
    "A_C_ROTTWEILER",
    "A_C_SHARKTIGER",
    "A_C_SHEPHERD",
    "HC_DRIVER",
    "HC_GUNMAN",
    "HC_HACKER",
    "IG_AMANDATOWNLEY",
    "IG_ANDREAS",
    "IG_BALLASOG",
    "IG_BRAD",
    "IG_DAVENORTON",
    "IG_DENISE",
    "IG_DEVIN",
    "IG_DOM",
    "IG_DRFRIEDLANDER",
    "IG_FABIEN",
    "IG_FLOYD",
    "IG_JAY_NORRIS",
    "IG_JIMMYDISANTO",
    "IG_LAMARDAVIS",
    "IG_LAZLOW",
    "IG_LESTERCREST",
    "IG_MILTON",
    "IG_MOLLY",
    "IG_MRK",
    "IG_NERVOUSRON",
    "IG_OMEGA",
    "IG_ORLEANS",
    "IG_PATRICIA",
    "IG_SIEMONYETARIAN",
    "IG_SOLOMON",
    "IG_STEVEHAINS",
    "IG_STRETCH",
    "IG_TAOCHENG",
    "IG_TAOSTRANSLATOR",
    "IG_TENNISCOACH",
    "IG_TRACYDISANTO",
    "IG_WADE",
    "MP_F_DEADHOOKER",
    "MP_F_FREEMODE_01",
    "MP_F_MISTY_01",
    "MP_F_STRIPPERLITE",
    "MP_HEADTARGETS",
    "MP_M_CLAUDE_01",
    "MP_M_FAMDD_01",
    "MP_M_FIBSEC_01",
    "MP_M_FREEMODE_01",
    "MP_M_MARSTON_01",
    "MP_M_NIKO_01",
    "MP_M_SHOPKEEP_01",
    "MP_S_M_ARMOURED_01",
    "PLAYER_ONE",
    "PLAYER_TWO",
    "PLAYER_ZERO",
}

local positions={
    {-614.58325195313, 677.51586914063,  149.79341125488, 326.32531738281},
    {216.11505126953, 757.92504882813,  204.66027832031, 39.032817840576},
    {-149.1544342041, 276.65975952148,  94.005661010742, 100.15242767334},
    {-640.93237304688, 293.76065063477,  81.585632324219, 190.76974487305},
    {-1873.2366943359, 194.9930267334,  84.294540405273, 133.61561584473},
    {-1005.3234863281, -992.01812744141,  2.1503100395203, 120.98846435547},
    {-1131.4493408203, -1585.4832763672,  4.387683391571, 162.99911499023},
    {10.100435256958, -1845.2132568359,  24.301013946533, 147.11282348633},
    {554.18249511719, -1776.5531005859,  29.168813705444, 338.15298461914},
    {1350.8253173828, -598.72009277344,  74.34578704834, 92.479797363281},
    {327.81225585938, -205.10710144043,  54.086318969727, 155.07333374023},
    {-238.9073638916, -985.02264404297,  29.288074493408, 21.339036941528},
    {-3017.0881347656, 87.39673614502,  11.608710289001, 337.60513305664},
    {-3201.0236816406, 1153.8282470703,  9.6543445587158, 262.00595092773},
    {1351.4102783203, -1574.0806884766,  54.048686981201, 235.39060974121},
    {-749.18902587891, -2288.9423828125,  13.057558059692, 39.924823760986},
    {-24.885778427124, -1437.4884033203,  30.653144836426, 195.10562133789},
}
function DrawMissionText(m_text, showtime)
    ClearPrints()
	SetTextEntry_2("STRING")
	AddTextComponentString(m_text)
	DrawSubtitleTimed(showtime, 1)
end

local pedIncharge
local blipPed
local pedCoordBlip
local positionPriseEnCharge={x=298.13983154297,y=-1445.6383056641,z=29.884799957275}
local PrixCentMettreAmb=50

function BoucleTakePatient()
    Citizen.CreateThread(function()
        DrawMissionText("Vas chercher ton patient",5000)
        local prendrePatient = false
        local xSpawn=295.04022216797
        local ySpawn=-1449.7999267578
        local zSpawn=29.966424942017
        local BoucleLent = 5000
        local BoucleCourt = 5
        local TempsBoucle = BoucleCourt
        while not prendrePatient do
            Wait(TempsBoucle)
            local playerPos = GetEntityCoords(GetPlayerPed(-1), true)
            if (Vdist(playerPos.x, playerPos.y, playerPos.z, positionPriseEnCharge.x, positionPriseEnCharge.y, positionPriseEnCharge.z) < 50.0) then
                TempsBoucle = BoucleCourt
                Marker(positionPriseEnCharge.x,positionPriseEnCharge.y,positionPriseEnCharge.z -1,3.5, 233, 255, 0)
                if (Vdist(playerPos.x, playerPos.y, playerPos.z, positionPriseEnCharge.x, positionPriseEnCharge.y, positionPriseEnCharge.z) < 4.0) then
                    local myCar = GetVehiclePedIsUsing(GetPlayerPed(-1))
                    if IsPedInAnyVehicle(GetPlayerPed(-1), true)then
                        if pedIncharge == nil then
                            DrawMissionText("Ton patient arrive",5000)
                            local type=math.floor(math.random(4,5))
                            local hashIndex=math.floor(math.random(1,#pedType))
                            RequestModel(GetHashKey(pedType[hashIndex]))
                            while not HasModelLoaded(GetHashKey(pedType[hashIndex])) do
                              Wait(1)
                            end
                            Citizen.Trace("Spawn Entity Amb: "..type.." "..hashIndex)
                            pedIncharge =  CreatePed(type, GetHashKey(pedType[hashIndex]), xSpawn, ySpawn, zSpawn, 123.98, true, true)
                            blipPed = AddBlipForEntity(pedIncharge)
                            SetBlipAsFriendly(blipPed, 1)
                            SetBlipColour(blipPed, 2)
                            SetBlipCategory(blipPed, 3)
                            TaskEnterVehicle(pedIncharge, myCar, -1, 2, 2.0001, 1)
                            prendrePatient=true
                        else
                            SetPedAsNoLongerNeeded(pedIncharge)
                            pedIncharge = nil
                            if pedCoordBlip ~= nil then
                            RemoveBlip(pedCoordBlip)
                            end
                        end 
                    else
                        DrawMissionText("Tu n'as pas de vehicule pour conduire le patient",5000)
                    end        
                end
            else
                TempsBoucle = BoucleLent  
            end
        end
        WaitPatient()
    end)
end

function WaitPatient()
     Citizen.CreateThread(function()
        local BoucleLent = 1000
        local BoucleCourt = 5
        local TempsBoucle = BoucleCourt
        local myCar = GetVehiclePedIsUsing(GetPlayerPed(-1))
        local echec=false
        while not IsPedSittingInVehicle(pedIncharge, myCar) and not echec do
            Wait(TempsBoucle)
            local playerPos = GetEntityCoords(GetPlayerPed(-1), true)
            local pedPos = GetEntityCoords(pedIncharge,true)
            DrawMissionText('Attendez que le patient monte dans votre ambulance',1000)
            if (Vdist(playerPos.x, playerPos.y, playerPos.z, pedPos.x, pedPos.y, pedPos.z) < 20.0) then
                TempsBoucle = BoucleCourt
            else
                TempsBoucle = BoucleLent  
            end
             if not DoesEntityExist(pedIncharge) or IsPedFatallyInjured(pedIncharge)then
                DrawMissionText('Votre mission est un ~r~échec',5000)
                echec=true
            end
        end
        if echec then
            SetPedAsNoLongerNeeded(pedIncharge)
            if(pedCoordBlip ~= nil)then
                RemoveBlip(pedCoordBlip)
            end
            if(blipPed ~=nil)then
                RemoveBlip(blipPed)
            end
            blipPed=nil
            pedIncharge=nil
            pedCoordBlip=nil
        else
            conduitPed()
        end
     end)
end

function conduitPed()
    Citizen.CreateThread(function()
        RemoveBlip(blipPed)
        local pos=positions[math.random(1,#positions)]
        local x = pos[1]
        local y = pos[2]
        local z = pos[3]
        pedCoordBlip = AddBlipForCoord(x,y,z)
        N_0x80ead8e2e1d5d52e(pedCoordBlip)
        SetBlipRoute(pedCoordBlip, 1)
        local BoucleLent = 5000
        local BoucleCourt = 5
        local TempsBoucle = BoucleCourt
        local myCar = GetVehiclePedIsUsing(GetPlayerPed(-1))
        local pedIsIn = DoesEntityExist(pedIncharge) and IsPedSittingInVehicle(pedIncharge, myCar) and not IsPedFatallyInjured(pedIncharge)
        while pedIsIn do
            Wait(TempsBoucle)
            if not (DoesEntityExist(pedIncharge) and IsPedSittingInVehicle(pedIncharge, myCar) and not IsPedFatallyInjured(pedIncharge))then
                DrawMissionText('Votre mission est un ~r~échec',5000)
                pedIsIn=false
                SetPedAsNoLongerNeeded(pedIncharge)
                if(pedCoordBlip ~= nil)then
                    RemoveBlip(pedCoordBlip)
                end
                if(blipPed ~=nil)then
                    RemoveBlip(blipPed)
                end
                blipPed=nil
                pedIncharge=nil
                pedCoordBlip=nil
            end
            local playerPos = GetEntityCoords(GetPlayerPed(-1), true)
            if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 100.0) then
                TempsBoucle = BoucleCourt
                Marker(x,y,z -1,3.5, 255, 0, 0)
                if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 4.0) then
                    TaskLeaveVehicle(pedIncharge, myCar, 0)
                    SetPedAsNoLongerNeeded(pedIncharge)
                    if(pedCoordBlip ~= nil)then
                        RemoveBlip(pedCoordBlip)
                    end
                    pedIncharge=nil
                    pedCoordBlip=nil
                    local dist = Vdist(positionPriseEnCharge.x,positionPriseEnCharge.y, positionPriseEnCharge.z, x, y, z)
                    local total = math.floor(dist/100*PrixCentMettreAmb)
                    --TriggerServerEvent('es_em:payMe', total)
                    TriggerEvent('Entreprise:AddVente', total)
                    DrawMissionText('~g~Mission terminé',5000)
                    pedIsIn=false
                    SetPedAsNoLongerNeeded(pedIncharge)
                    if(pedCoordBlip ~= nil)then
                        RemoveBlip(pedCoordBlip)
                    end
                    if(blipPed ~=nil)then
                        RemoveBlip(blipPed)
                    end
                    blipPed=nil
                    pedIncharge=nil
                    pedCoordBlip=nil
                end
            else
                TempsBoucle = BoucleLent  
            end
        end
    end)
end