--[[
################################################################
- Creator: Jyben
- Date: 30/04/2017
- Url: https://github.com/Jyben/emergency
- Licence: Apache 2.0
################################################################
--]]

local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local isDead = false
local isKO = false
local isRes = false
local emergencyComes = false
local decomptedeath = 60*10

--[[
################################
            THREADS
################################
--]]

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)
    --NetworkResurrectLocalPlayer(357.757, -597.202, 28.6314, true, true, false)
    local playerPed = GetPlayerPed(-1)
    local playerID = PlayerId()
    local currentPos = GetEntityCoords(playerPed, true)
    local previousPos

    isDead = IsEntityDead(playerPed)

    if isKO and previousPos ~= currentPos then
      isKO = false
    end

    if (GetEntityHealth(playerPed) < 120 and not isDead and not isKO) then
      if (IsPedInMeleeCombat(playerPed)) then
        SetPlayerKO(playerID, playerPed)
      end
    end

    previousPos = currentPos
  end
end)

Citizen.CreateThread(function()
    local initimer = 0
    local currenttime = 0
	while true do
    Wait(100)
  	if IsEntityDead(PlayerPedId()) then
			StartScreenEffect("DeathFailOut", 0, 0)
			ShakeGameplayCam("DEATH_FAIL_IN_EFFECT_SHAKE", 1.0)
            DisplayRadar(false)
            TriggerEvent('es:setFoodDisplay',0)
            TriggerEvent('es:setMoneyDisplay',0)
            TriggerEvent('es:setBankDisplay',0)
            initimer = GetNetworkTime()
            currenttime = GetNetworkTime()
			local scaleform = RequestScaleformMovie("MP_BIG_MESSAGE_FREEMODE")
			if HasScaleformMovieLoaded(scaleform) then
                    
                while IsEntityDead(PlayerPedId()) do
                    currenttime = GetNetworkTime()
                    DrawScaleformMovieFullscreen(scaleform, 255, 255, 255, 255)
                    PushScaleformMovieFunction(scaleform, "SHOW_SHARD_WASTED_MP_MESSAGE")
                    BeginTextComponent("STRING")
                    local diff = math.floor(GetTimeDifference(currenttime,initimer)/1000)
                    local decompte = decomptedeath - diff
                    local minute = math.floor(decompte/60)
                    local seconde = decompte - minute*60
                    if seconde < 10 then
                        seconde = "0"..seconde
                    end
                    if (minute >= 0 and tonumber(seconde) > 0) or diff == 0 then
                        if IsControlJustReleased(1, Keys['E']) then
                            break
                        end
                        AddTextComponentString(minute..':'..seconde..' avant amnésie')
                    else
                        AddTextComponentString(diff..'Amnésie totale')
                    end
                    EndTextComponent()
                    PopScaleformMovieFunctionVoid()
                    Citizen.Wait(5)
                    
                end
                       
                while IsEntityDead(PlayerPedId()) do
                    DrawScaleformMovieFullscreen(scaleform, 255, 255, 255, 255)
                    PushScaleformMovieFunction(scaleform, "SHOW_SHARD_WASTED_MP_MESSAGE")
                    BeginTextComponent("STRING")
                    AddTextComponentString('Amnésie partielle')
                    EndTextComponent()
                    PopScaleformMovieFunctionVoid()
                    Citizen.Wait(5)  
                end

		  	StopScreenEffect("DeathFailOut")
            DisplayRadar(true)
            TriggerEvent('es:setFoodDisplay',100)
            TriggerEvent('es:setMoneyDisplay',100)
            TriggerEvent('es:setBankDisplay',100)
			end
		end
	end
    
end)

--[[
################################
            EVENTS
################################
--]]

AddEventHandler("playerSpawned", function(spawn)
    exports.spawnmanager:setAutoSpawn(false)
end)

-- Triggered when player died by environment
AddEventHandler('baseevents:onPlayerDied',
  function(playerId, reasonID)
    local reason = 'Un accident s\'est produit'
		OnPlayerDied(playerId, reasonID, reason)
	end
)

-- Triggered when player died by an another player
AddEventHandler('baseevents:onPlayerKilled',
  function(playerId, playerKill, reasonID)
    local reason = 'Tentative de meurtre'
		OnPlayerDied(playerId, reasonID, reason)
	end
)

RegisterNetEvent('es_em:cl_sendMessageToPlayerInComa')
AddEventHandler('es_em:cl_sendMessageToPlayerInComa',
	function()
		emergencyComes = true
		SendNotification('Une ~b~ambulance~s~ est en route !')
	end
)

RegisterNetEvent('es_em:cl_resurectPlayer')
AddEventHandler('es_em:cl_resurectPlayer',
	function()
		local playerPed = GetPlayerPed(-1)
		isRes = true

		if IsEntityDead(playerPed) then
            TriggerEvent('besoin:resetbesoin')
			SendNotification('Vous avez été réanimé')

			ResurrectPed(playerPed)
			SetEntityHealth(playerPed, GetPedMaxHealth(playerPed)/2)
			ClearPedTasksImmediately(playerPed)
		end
	end
)

RegisterNetEvent('es_em:cl_respawn')
AddEventHandler('es_em:cl_respawn',
	function()
		ResPlayer()
	end
)

--[[
################################
        BUSINESS METHODS
################################
--]]

function SetPlayerKO(playerID, playerPed)
  isKO = true
  SendNotification('Vous êtes KO !')
  SetPedToRagdoll(playerPed, 6000, 6000, 0, 0, 0, 0)
end

function SendNotification(message)
  SetNotificationTextEntry('STRING')
  AddTextComponentString(message)
  DrawNotification(false, false)
end

function ResPlayer()
	isRes = true
	TriggerServerEvent('es_em:sv_removeMoney')
	TriggerServerEvent("item:reset")
    TriggerEvent('besoin:resetbesoin')
	NetworkResurrectLocalPlayer(357.757, -597.202, 28.6314, true, true, false)
end

function OnPlayerDied(playerId, reasonID, reason)
	local pos = GetEntityCoords(GetPlayerPed(-1))
	local isDocConnected = nil
	isRes = false
    
	TriggerServerEvent('es_em:sv_getDocConnected')

	Citizen.CreateThread(
		function()
			while isDocConnected == nil do
				Citizen.Wait(1)

				RegisterNetEvent('es_em:cl_getDocConnected')
				AddEventHandler('es_em:cl_getDocConnected',
					function(cb)
						isDocConnected = cb
						if isDocConnected then
							SendNotification('Appuyez sur ~g~E~s~ pour appeler une ambulance')
						end
					end
				)                                       
			end
		end
	)

	SendNotification('Appuyez sur ~r~X~s~ pour respawn')


	Citizen.CreateThread(
		function()
			local emergencyCalled = false
			local notifReceivedAt = nil

			while not isRes do
				Citizen.Wait(1)

				if (IsControlJustReleased(1, Keys['E'])) and not emergencyCalled then
					if not isDocConnected then
						SendNotification("Il n'y a pas de médecin disponible")
					else
						notifReceivedAt = GetGameTimer()
						SendNotification('Vous avez appelé une ~b~ambulance~s~')
                        local msg = "Réanimation nécessaire (appel automatique)"
						TriggerServerEvent("call:makeCall", "medic", {x=pos.x,y=pos.y,z=pos.z},msg)
					end

					emergencyCalled = true
				elseif (IsControlJustReleased(1, Keys['X'])) and emergencyComes == false and emergencyCalled == false then
					ResPlayer()
                    if isDocConnected then
                        TriggerServerEvent('log:printname',"Unité X"," a unité X en présence de médecin")
                    end
				end

				if (GetTimeDifference(GetGameTimer(), notifReceivedAt) > 15000) and not emergencyComes and emergencyCalled then
					SendNotification('Appuyez sur ~g~E~s~ pour appeler une ambulance')
					emergencyCalled = false
				end

			end

			isDocConnected = nil
			isRes = false
			emergencyComes = false
	end)
end

--[[
################################
        USEFUL METHODS
################################
--]]
