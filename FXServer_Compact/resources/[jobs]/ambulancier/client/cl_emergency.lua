--[[
################################################################
- Creator: Jyben
- Date: 01/05/2017
- Url: https://github.com/Jyben/emergency
- Licence: Apache 2.0
################################################################
--]]

local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local isInService = false
local jobId = -1
local notificationInProgress = false
local playerInComaIsADoc = false
local JobAmbu = {11,12}
--[[
################################
            THREADS
################################
--]]
function BoucleTakeService()
    Citizen.CreateThread(function()
        local x = 268.26
        local y = -1363.10
        local z = 24.53
        local BoucleLent = 5000
        local BoucleCourt = 5
        local TempsBoucle = BoucleCourt
        while TestBonJob() do
            Wait(TempsBoucle)
            local playerPos = GetEntityCoords(GetPlayerPed(-1), true)
            if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 50.0) then
                TempsBoucle = BoucleCourt
                Marker(x,y,z -1,1.5,255, 0, 122)
                if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 2.0) then
                    if isInService then
                        DisplayHelp('Pressez ~INPUT_CONTEXT~ pour terminer votre service')
                    else
                        DisplayHelp('Pressez ~INPUT_CONTEXT~ pour prendre votre service')
                    end
                    if (IsControlJustReleased(1, 51)) then
                        TriggerServerEvent('es_em:sv_getJobId')
                    end
                end
            else
                TempsBoucle = BoucleLent  
            end
        end
    end)
end

--[[
################################
            EVENTS
################################
--]]
RegisterNetEvent('recolt:updateJobs')
AddEventHandler('recolt:updateJobs', function(newjob)
    jobId = newjob
    for _,job in pairs(JobAmbu) do
        if jobId == job then
            BoucleTakeService()
            break
        end
    end
end)

function TestBonJob()
    for _,job in pairs(JobAmbu) do
        if jobId == job then
            return true
        end
    end
    return false
end

RegisterNetEvent('es_em:sendEmergencyToDocs')
AddEventHandler('es_em:sendEmergencyToDocs', function(reason, playerIDInComa, x, y, z, sourcePlayerInComa)
	local playerServerId = GetPlayerServerId(PlayerId())

	if playerIDInComa == playerServerId then playerInComaIsADoc = true else playerInComaIsADoc = false end

	Citizen.CreateThread(function()
		if isInService --[[and (jobId == 11 or jobId == 12)]] and not playerInComaIsADoc then
			local controlPressed = false

			while notificationInProgress do
				Citizen.Wait(0)
			end

			local notifReceivedAt = GetGameTimer()

			TriggerEvent('hud:NotifColor','URGENCE',6)
            TriggerEvent('hud:Notif','Raison~s~:' .. reason)
			TriggerEvent('hud:Notif','Appuyez sur ~g~Y~s~ pour prendre l\'appel')

			while not controlPressed do
				Citizen.Wait(0)
				notificationInProgress = true
            
                if (GetTimeDifference(GetGameTimer(), notifReceivedAt) > 15000) then
                    notificationInProgress = false
                    controlPressed = true
                    TriggerEvent('hud:NotifColor','URGENCE',6)
                    TriggerEvent('hud:Notif','Attention, l\'appel précèdent a expiré !')
                end

                if IsControlPressed(1, Keys["Y"]) then
                    controlPressed = true
                    TriggerServerEvent('es_em:getTheCall', GetPlayerName(PlayerId()), playerServerId, x, y, z, sourcePlayerInComa)
                end

                if controlPressed then
                    notificationInProgress = false
                end
            end
        end
    end)
end)

RegisterNetEvent('es_em:cl_setJobId')
AddEventHandler('es_em:cl_setJobId',
	function(p_jobId)
		jobId = p_jobId
		GetService()
	end
)

--[[
################################
        BUSINESS METHODS
################################
--]]
-- Get job form server
function GetService()
	local playerPed = GetPlayerPed(-1)

	if jobId ~= 11 and jobId ~= 12  then
		TriggerEvent('hud:Notif','Vous n\'êtes pas ambulancier')
		return
	end

	if isInService then
		TriggerEvent('hud:Notif','Vous n\'êtes plus en service')
        TriggerServerEvent("player:serviceOff", "medic")
		TriggerServerEvent("mm:spawn2")
		TriggerServerEvent('es_em:sv_setService', 0)
	else
		TriggerEvent('hud:Notif',"Début de service")
        TriggerServerEvent("player:serviceOn", "medic")
		TriggerServerEvent('es_em:sv_setService', 1)
        BoucleGarage()
        mainBoucle()
	end

	isInService = not isInService
     if GetEntityModel(GetPlayerPed(-1)) == GetHashKey("mp_f_freemode_01") then
        SetPedComponentVariation(playerPed, 11, 27, 0, 2)--Veste
        SetPedComponentVariation(playerPed, 8, 159, 0, 2)--mattraque
        SetPedComponentVariation(playerPed, 4, 11, 4, 2)--Jean
        SetPedComponentVariation(playerPed, 3, 98, 0, 2)--Gants
        SetPedComponentVariation(playerPed, 6, 25, 0, 2)--Chaussure
        SetPedComponentVariation(playerPed, 7, 97, 0, 2)--collier        
    else
        SetPedComponentVariation(playerPed, 11, 13, 3, 2)--Veste
        SetPedComponentVariation(playerPed, 8, 129, 0, 2)--mattraque
        SetPedComponentVariation(playerPed, 4, 25, 2, 2)--Jean
        SetPedComponentVariation(playerPed, 3, 92, 0, 2)--Gants
        SetPedComponentVariation(playerPed, 6, 25, 0, 2)--Chaussure
    end
end

--[[
################################
        USEFUL METHODS
################################
--]]
function GetClosestPlayer()
	local players = GetPlayers()
	local closestDistance = -1
	local closestPlayer = -1
	local ply = GetPlayerPed(-1)
	local plyCoords = GetEntityCoords(ply, 0)

	for index,value in ipairs(players) do
		local target = GetPlayerPed(value)
		if(target ~= ply) then
			local targetCoords = GetEntityCoords(GetPlayerPed(value), 0)
			local distance = GetDistanceBetweenCoords(targetCoords["x"], targetCoords["y"], targetCoords["z"], plyCoords["x"], plyCoords["y"], plyCoords["z"], true)
			if(closestDistance == -1 or closestDistance > distance) then
				closestPlayer = value
				closestDistance = distance
			end
		end
	end

	return closestPlayer, closestDistance
end

function GetPlayers()
    local players = {}

    for i = 0, 500 do
        if NetworkIsPlayerActive(i) then
            table.insert(players, i)
        end
    end

    return players
end

------------------------
------- MENU JOB -------
------------------------
-------
---------
local selectTattoo = false

function DepMenu() -- Tow Menu
    selectTattoo = false
    VMenu.ResetMenu()
    VMenu.EditHeader('Ambulancier')
    VMenu.AddFunc("Retour hôpitale", "gpsAmbu", nil) 
    if jobId == 12 then
        VMenu.AddFunc("Recruter un ambulancier", "recruter", nil) 
        VMenu.AddFunc("Licencier un ambulancier", "licencier", nil) 
    end
    VMenu.AddFunc("Soigner", "soigner", nil) -- Tow
    VMenu.AddFunc("Réanimer","reanimer",nil)
    VMenu.AddFunc("Facture", "facture", nil, "Émettre une facture") -- Tow
    VMenu.AddFunc("Effacer tatouage","deltattoo",nil)
	
end

function gpsAmbu()

    VMenu.visible = false
    BLIPP = AddBlipForCoord(307.494, -1433.791, 28.916)
    SetBlipSprite(BLIPP, 2)
    SetNewWaypoint(307.494, -1433.791)
end

function recruter()
    DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8S", "", "", "", "", "", 20)
    while (UpdateOnscreenKeyboard() == 0) do
	    DisableAllControlActions(0);
		Wait(0);
	end
	if (GetOnscreenKeyboardResult()) then
	    local res = tonumber(GetOnscreenKeyboardResult())
		TriggerServerEvent('commande_metier:recruter',res,11)
    end
end

function licencier()
    DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8S", "", "", "", "", "", 20)
    while (UpdateOnscreenKeyboard() == 0) do
	    DisableAllControlActions(0);
		Wait(0);
	end
	if (GetOnscreenKeyboardResult()) then
	    local res = tonumber(GetOnscreenKeyboardResult())
		TriggerServerEvent('commande_metier:licencier',res)
    end
end

function facture()
    VMenu.visible = false
    TriggerEvent('facture:Open') 
end

function soigner() -- Call server Function Tow
    local t, distance = GetClosestPlayer()
	if(distance ~= -1 and distance < 3) then
        TriggerServerEvent('ambulancier:freeze', GetPlayerServerId(t))
        TaskStartScenarioInPlace(GetPlayerPed(-1), "CODE_HUMAN_MEDIC_KNEEL", 0, 1)
        Citizen.Wait(5000)
        ClearPedTasks(GetPlayerPed(-1))
        Citizen.Wait(2000)
        TriggerServerEvent('ambulancier:soin',GetPlayerServerId(t))
        TriggerEvent('hud:NotifColor','Personne soignée',141)
    else
		TriggerEvent('hud:NotifColor','Personne à proximité.',6)
	end
end

local attenteRea = false

RegisterNetEvent('Client:Callurgence')
AddEventHandler('Client:Callurgence', function(target,pos)
    attenteRea = true
    Citizen.CreateThread(function()
        while attenteRea do
            local playerPos = GetEntityCoords(GetPlayerPed(-1), true)
            Wait(5)
            if(Vdist(playerPos.x, playerPos.y, playerPos.z, pos.x, pos.y, pos.z) < 10.0) then
                Marker(pos.x, pos.y, pos.z,1.5,177, 202, 223)
                if Vdist(playerPos.x, playerPos.y, playerPos.z, pos.x, pos.y, pos.z) < 1.5 then
                DisplayHelp("Pressez ~INPUT_CONTEXT~ pour réanimer une personne non visible")
                    if IsControlJustPressed(1,51) then
                        attenteRea = false
                        local distance = 1
                        local pos = GetEntityCoords(GetPlayerPed(-1))
                        local heading = GetEntityHeading(GetPlayerPed(-1))
                        local rad = math.pi/2+math.rad(heading)
                        local coord = {
                                x = pos.x+distance*math.cos(rad),
                                y = pos.y+distance*math.sin(rad),
                                z = pos.z -1,
                            }
                        TriggerServerEvent('ambulancier:debutrea',target,coord,heading+90)
                        TriggerServerEvent('es_em:sv_resurectPlayer', target)
                        TriggerEvent('vmenu:anim','mini@cpr@char_a@cpr_def','cpr_intro')
                        Wait(15000)
                        for i=1,6 do
                            TriggerEvent('vmenu:anim','mini@cpr@char_a@cpr_str','cpr_pumpchest')
                            Wait(900)
                        end 
                        TriggerEvent('vmenu:anim','mini@cpr@char_a@cpr_str','cpr_success')
                        Wait(27000)
                        ClearPedTasks(GetPlayerPed(-1))
                        ClearPedTasks(GetPlayerPed(target))
                        TriggerEvent('hud:NotifColor','Personne réanimée',141)
                    end
                end
            end
        end    
    end)
end)

function reanimer()
    local t, distance = GetClosestPlayer()
	if(distance ~= -1 and distance < 3) then
        attenteRea = false
        local distance = 1
		local pos = GetEntityCoords(GetPlayerPed(-1))
        local heading = GetEntityHeading(GetPlayerPed(-1))
        local rad = math.pi/2+math.rad(heading)
        local coord = {
                x = pos.x+distance*math.cos(rad),
                y = pos.y+distance*math.sin(rad),
                z = pos.z -1,
            }
        TriggerServerEvent('ambulancier:debutrea',GetPlayerServerId(t),coord,heading+90)
        TriggerEvent('vmenu:anim','mini@cpr@char_a@cpr_def','cpr_intro')
        Citizen.Wait(15000)
        for i=1,6 do
            TriggerEvent('vmenu:anim','mini@cpr@char_a@cpr_str','cpr_pumpchest')
            Citizen.Wait(900)
        end 
        TriggerEvent('vmenu:anim','mini@cpr@char_a@cpr_str','cpr_success')
        Citizen.Wait(27000)
        ClearPedTasks(GetPlayerPed(-1))
        ClearPedTasks(GetPlayerPed(t))
        TriggerServerEvent('es_em:sv_resurectPlayer', GetPlayerServerId(t))
        TriggerEvent('hud:NotifColor','Personne réanimée',141)
    else
		TriggerEvent('hud:NotifColor','Personne à proximité.',6)
	end
end
    
RegisterNetEvent('ambulancier:animrea')
AddEventHandler("ambulancier:animrea", function(coord,heading)
    ResurrectPed(GetPlayerPed(-1))
	SetEntityHealth(playerPed, GetPedMaxHealth(GetPlayerPed(-1))/2)
    ClearPedTasksImmediately(GetPlayerPed(-1))
    SetEntityCoords(GetPlayerPed(t), coord.x,coord.y, coord.z, 1, 0, 0, 1)
    SetEntityHeading(GetPlayerPed(t), heading )
	TriggerEvent('vmenu:anim','mini@cpr@char_b@cpr_def','cpr_intro')
    Citizen.Wait(15000)
    for i=1,6 do
        TriggerEvent('vmenu:anim','mini@cpr@char_b@cpr_str','cpr_pumpchest')
        Citizen.Wait(900)
    end 
    TriggerEvent('vmenu:anim','mini@cpr@char_b@cpr_str','cpr_success')
    Citizen.Wait(27000)
    ClearPedTasks(GetPlayerPed(-1))
end)

RegisterNetEvent('ambulancier:recupvie')
AddEventHandler('ambulancier:recupvie', function()
    FreezeEntityPosition(GetPlayerPed(-1), false)
    SetEntityHealth(GetPlayerPed(-1), GetPedMaxHealth(GetPlayerPed(-1))/2)
end)

RegisterNetEvent('ambulancier:stopmove')
AddEventHandler('ambulancier:stopmove', function()
    FreezeEntityPosition(GetPlayerPed(-1), true)
end)

function GarageMenu()
    VMenu.ResetMenu()
    VMenu.EditHeader('Garage')
    VMenu.AddFunc("Sortir Ambulance", "SpawnVeh", 'ambulance')
    VMenu.AddFunc("Sortir Quad", "SpawnVeh", "blazer2")
    VMenu.AddFunc("Sortir LifeGuard", "SpawnVeh", "lguard")
	VMenu.AddFunc("Rentrer véhicule", "rentrer", nil)	
end

function HelicoMenu()
    VMenu.ResetMenu()
    VMenu.EditHeader('Garage')
    VMenu.AddFunc("Sortir Hélico", "SpawnHelico", nil)
	VMenu.AddFunc("Rentrer véhicule", "rentrerHelico", nil)	
end

local spawn = {x = 403.563, y = -1423.021, z = 28.447, a= 230.877}
local portespawn = {x = 403.761, y = -1417.889, z = 28.436, a= 47.609}
local sortirHelico ={x = 318.274, y = -1459.09, z = 45.509, a= 143.807}
local spawnHelico = {x = 313.453, y = -1465.065, z = 45.509, a= 227.052}

function SpawnVeh(hash)
	Citizen.Wait(0)
	local myPed = GetPlayerPed(-1)
	local player = PlayerId()
	local vehicle = GetHashKey(hash)

	RequestModel(vehicle)

	while not HasModelLoaded(vehicle) do
		Wait(1)
	end

	local plate = math.random(100, 900)
	local coords = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0, 5.0, 0)
	local spawned_car = CreateVehicle(vehicle, spawn.x,spawn.y,spawn.z +1,spawn.a, true, false)

	SetVehicleOnGroundProperly(spawned_car)
    SetVehicleLivery(spawned_car, livery_lv1)
	SetVehicleEnginePowerMultiplier(spawned_car, 35.0)
	SetVehicleEngineTorqueMultiplier(spawned_car, 50.0)
    local id = NetworkGetNetworkIdFromEntity(spawned_car)
    SetNetworkIdCanMigrate(id, true)
    SetNetworkIdExistsOnAllMachines(id,true)
end

function SpawnHelico()
    rentrerHelico()
	local myPed = GetPlayerPed(-1)
	local player = PlayerId()
	local vehicle = GetHashKey('polmav')

	RequestModel(vehicle)

	while not HasModelLoaded(vehicle) do
		Wait(1)
	end

	local plate = math.random(100, 900)
	local coords = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0, 5.0, 0)
	local spawned_car = CreateVehicle(vehicle, spawnHelico.x,spawnHelico.y,spawnHelico.z +1,spawnHelico.a, true, false)

	SetVehicleOnGroundProperly(spawned_car)
    SetVehicleLivery(spawned_car, 1)
	SetVehicleEnginePowerMultiplier(spawned_car, 35.0)
	SetVehicleEngineTorqueMultiplier(spawned_car, 50.0)
    local id = NetworkGetNetworkIdFromEntity(spawned_car)
                SetNetworkIdCanMigrate(id, true)
                SetNetworkIdExistsOnAllMachines(id,true)
end

function rentrer()
    Citizen.CreateThread(function()
        Citizen.Wait(100)
        local caisse = GetClosestVehicle(spawn.x,spawn.y,spawn.z, 5.000, 0, 70)
        SetEntityAsMissionEntity(caisse, true, true)	
        Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(caisse))
        TriggerEvent('hud:NotifColor','Véhicule rentré',141)
    end)
end


-----------------------------------------------
-------------- SUPPRESSION TATTOO -------------
-----------------------------------------------
function deltattoo()
    selectTattoo = false
    local t, distance = GetClosestPlayer()
    if(distance ~= -1 and distance < 3) then
        VMenu.ResetMenu()
        VMenu.EditHeader('Ambulancier')
        VMenu.AddPrev("DepMenu")
        VMenu.AddSep("Lecture du dossier médical") -- Tow
        VMenu.AddSep("Veuillez patienter") -- Tow
        TriggerServerEvent("Ambulancier:TattooAutorisation",GetPlayerServerId(t))
    else
		TriggerEvent('hud:NotifColor','Personne à proximité.',6)
	end
end

RegisterNetEvent('Ambulancier:TattooDemande')
AddEventHandler('Ambulancier:TattooDemande',function(provenance)
    TriggerEvent('hud:Notif',"Un ambulancier veut étudier vos tatouages")
    TriggerEvent('hud:NotifKey',"~INPUT_MP_TEXT_CHAT_TEAM~", "~g~Accepter")
    Citizen.Wait(100)
    TriggerEvent('hud:NotifKey',"~INPUT_REPLAY_ENDPOINT~", "~r~Ignorer")
    local compte = 0
    while compte < 1000 do
        Wait(5)
        if IsControlJustReleased(1, 246) then
            TriggerServerEvent('Ambulancier:ReponseTattoo', provenance, true)
            if(GetEntityModel(GetPlayerPed(-1)) == -1667301416) then  -- GIRL SKIN
                SetPedComponentVariation(GetPlayerPed(-1), 8, 34,0, 2)
                SetPedComponentVariation(GetPlayerPed(-1), 3, 15,0, 2)
                SetPedComponentVariation(GetPlayerPed(-1), 11, 101,1, 2)
                SetPedComponentVariation(GetPlayerPed(-1), 4, 56,1, 0)
                SetPedComponentVariation(GetPlayerPed(-1), 6, 35, 0, 0)--Chaussure
                SetPedComponentVariation(GetPlayerPed(-1), 7, 0, 0, 0) 
            else 													  -- BOY SKIN
                SetPedComponentVariation(GetPlayerPed(-1), 8, 15,0, 2)
                SetPedComponentVariation(GetPlayerPed(-1), 3, 15,0, 2)
                SetPedComponentVariation(GetPlayerPed(-1), 11, 91,0, 2)
                SetPedComponentVariation(GetPlayerPed(-1), 4, 14,0, 2)
                SetPedComponentVariation(GetPlayerPed(-1), 7, 0, 0, 0) 
            end 
            break
        end
        if IsControlJustReleased(1, 306) then
            TriggerServerEvent('Ambulancier:ReponseTattoo', provenance, false)
            break
        end
        compte = compte + 1
    end
    TriggerEvent('hud:NotifDel')
end)

RegisterNetEvent('Ambulancier:ClignTattoo')
AddEventHandler('Ambulancier:ClignTattoo',function(list,curItm,aff) 
    local player = -1
    local list = list
    local aff = aff
    local curItm = curItm
     ClearPedDecorations(GetPlayerPed(player))
     for int,tattoo in pairs(list) do       
         if int == curItm then
             if aff then
                 ApplyPedOverlay(GetPlayerPed(player), GetHashKey(tattoo[2]), GetHashKey(tattoo[1]))
             end
         else
             ApplyPedOverlay(GetPlayerPed(player), GetHashKey(tattoo[2]), GetHashKey(tattoo[1]))
         end
     end 
end)
RegisterNetEvent('Amublancier:AffTattoo')
AddEventHandler('Amublancier:AffTattoo',function(list,t)
    VMenu.ResetMenu()
    VMenu.AddPrev("DepMenu")
    VMenu.curItem = 1
    local list = list
    local t = t
    selectTattoo = true  
    Citizen.CreateThread( function()
        while selectTattoo do
            Wait(1)
            DisableControlAction(1,166, true)
            if IsDisabledControlPressed(1,177) or IsControlJustPressed(1,177) then
                selectTattoo = false
                TriggerServerEvent('Ambulancier:TattooQuitter',t)
            end
        end
        DisableControlAction(1,166, false)
    end)   
    if list == nil then
        VMenu.AddSep("Le dossier médical est vide")
    else
        local count = 0
        for int,tattoo in pairs(list) do
            count = count +1
            VMenu.AddFunc("Tattoo n°"..count.. " ("..tattoosCategories[GetTattooZone(GetHashKey(tattoo[2]), GetHashKey(tattoo[1])) +1].name..")","suptatto",{list,int,t},"Effacer")
        end
        if count == 0 then
            VMenu.AddSep("Le dossier médical est vide")
        end
        Citizen.CreateThread(function()
            local aff = false
           while selectTattoo do
               Wait(1000)
                aff = not aff
                TriggerServerEvent('Ambulancier:EnvoiCurItm',t,list,VMenu.curItem,aff)
            end
        end)
    end    
end)

function suptatto(args)
    selectTattoo = false
    local list = args[1]
    local int = args[2]
    local t = args[3]
    table.remove(list,int)
    TriggerServerEvent('Ambulancier:SaveTattoo',t,list)
    TriggerEvent('hud:NotifColor',"Tatouage effacé",141)
    TriggerEvent('Amublancier:AffTattoo',list,t)
end

RegisterNetEvent('Ambulancier:TattooFin')
AddEventHandler('Ambulancier:TattooFin', function()
   TriggerServerEvent("mm:spawn2") 
end)

function rentrerHelico()
        local caisse = getVehicleInDirection(GetEntityCoords(GetPlayerPed(-1), true), spawnHelico)
        SetEntityAsMissionEntity(caisse, true, true)	
        Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(caisse))
        TriggerEvent('hud:NotifColor','Véhicule rentré',141)
end

function getVehicleInDirection(coordFrom, coordTo)
	local rayHandle = Citizen.InvokeNative(0x28579D1B8F8AAC80,coordFrom.x, coordFrom.y, coordFrom.z, coordTo.x, coordTo.y, coordTo.z, 20.0, 10, GetPlayerPed(-1), 0)
	local a, b, c, d, vehicle = GetRaycastResult(rayHandle)
	return vehicle
end   
----------------------------------------------------------------------------------------------------------------------
local garage = false
function BoucleGarage()
    Citizen.CreateThread(function()
        while isInService do
            Citizen.Wait(5)
            if IsControlJustPressed(1, 166) then -- Open menu (168 = F5)
               if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                VMenu.curItem = 1
                DepMenu()
                VMenu.visible = not VMenu.visible
            end
            local playerPos = GetEntityCoords(GetPlayerPed(-1), true)
            if((Vdist(playerPos.x, playerPos.y, playerPos.z, portespawn.x, portespawn.y, portespawn.z) < 100.0)) then
                Marker(portespawn.x, portespawn.y, portespawn.z,1.5,255, 0, 122)
                if(Vdist(playerPos.x, playerPos.y, playerPos.z,portespawn.x, portespawn.y, portespawn.z) < 2.0) then
                    DisplayHelp("Appuyez sur ~INPUT_CONTEXT~ pour ouvrir le ~b~garage")
                    if IsControlJustPressed(1, 51) then -- Open menu (168 = F5)
                       if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                        GarageMenu()
                        garage = true
                        VMenu.curItem = 1
                        VMenu.visible = not VMenu.visible
                    end
                elseif garage then
                    garage = false
                    VMenu.visible = false
                end
            end
            if((Vdist(playerPos.x, playerPos.y, playerPos.z, sortirHelico.x, sortirHelico.y, sortirHelico.z) < 100.0)) then
                Marker(sortirHelico.x, sortirHelico.y, sortirHelico.z,1.5,255, 0, 122)
                if(Vdist(playerPos.x, playerPos.y, playerPos.z,sortirHelico.x, sortirHelico.y, sortirHelico.z) < 2.0) then
                    DisplayHelp("Appuyez sur ~INPUT_CONTEXT~ pour ouvrir le ~b~garage")
                    if IsControlJustPressed(1, 51) then -- Open menu (168 = F5)
                       if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                        HelicoMenu()
                        garage2 = true
                        VMenu.curItem = 1
                        VMenu.curMenu = 11
                        VMenu.visible = not VMenu.visible
                    end
                elseif garage2 then
                    garage2 = false
                    VMenu.visible = false
                end
            end
            VMenu.Show()
        end
    end)
end

Citizen.CreateThread(function()
    VMenu.AddMenu( 'Ambulancier', 'ambulancier',34,105,171)   
end)

function mainBoucle()
    Citizen.CreateThread(function()
        local x = 266.46060180664
        local y = -1358.4072265625
        local z = 24.537809371948
        local BoucleLent = 5000
        local BoucleCourt = 5
        local TempsBoucle = BoucleCourt
        while isInService do
            Wait(TempsBoucle)
            local playerPos = GetEntityCoords(GetPlayerPed(-1), true)
            if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 50.0) then
                TempsBoucle = BoucleCourt
                Marker(x,y,z -1,1.5,0, 233, 255)
                if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 2.0) then
                       DisplayHelp('Pressez ~INPUT_CONTEXT~ pour prendre un patient en charge')
                    if (IsControlJustReleased(1, 51)) then
                        BoucleTakePatient()
                        Wait(10000)
                    end
                end
            else
                TempsBoucle = BoucleLent  
            end
        end
    end)
end