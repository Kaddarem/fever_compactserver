RegisterServerEvent('es_em:sendEmergency')
AddEventHandler('es_em:sendEmergency',function(reason, playerIDInComa, x, y, z)
    TriggerEvent("es:getPlayers", function(players)
        for i,v in pairs(players) do
            TriggerClientEvent('es_em:sendEmergencyToDocs', i, reason, playerIDInComa, x, y, z, source)
        end
    end)
end)

RegisterServerEvent('es_em:getTheCall')
AddEventHandler('es_em:getTheCall',function(playerName, playerID, x, y, z, sourcePlayerInComa)
    TriggerEvent("es:getPlayers", function(players)
        for i,v in pairs(players) do
            TriggerClientEvent('es_em:callTaken', i, playerName, playerID, x, y, z, sourcePlayerInComa)
        end
    end)
end)

RegisterServerEvent('es_em:sv_resurectPlayer')
AddEventHandler('es_em:sv_resurectPlayer',function(sourcePlayerInComa)
    TriggerClientEvent('es_em:cl_resurectPlayer', sourcePlayerInComa)
end)

RegisterServerEvent('es_em:sv_getJobId')
AddEventHandler('es_em:sv_getJobId',function()
    TriggerClientEvent('es_em:cl_setJobId', source, GetJobId(source))
end)

RegisterServerEvent('es_em:sv_getDocConnected')
AddEventHandler('es_em:sv_getDocConnected',function()
    TriggerEvent("es:getPlayers", function(players)
        local source = source
        local identifier
        local table = {}
        local isConnected = false

        for i,v in pairs(players) do
        identifier = GetPlayerIdentifiers(i)
        if (identifier ~= nil) then
            local result = MySQL.Sync.fetchAll("SELECT identifier, job_id, job_name FROM users LEFT JOIN jobs ON jobs.job_id = users.job WHERE users.identifier = @identifier AND (job_id = 11 OR job_id = 12) AND enService = 1", {['@identifier'] = identifier[1]})
            if (result[1] ~= nil) then
                isConnected = true
            end
        end
      end
      TriggerClientEvent('es_em:cl_getDocConnected', source, isConnected)
    end)
end)

RegisterServerEvent('es_em:sv_setService')
AddEventHandler('es_em:sv_setService',function(service)
    local player = getPlayerID(source)
        local source = source
    MySQL.Sync.execute("UPDATE users SET enService = @service WHERE users.identifier = @identifier", {['@identifier'] = player, ['@service'] = service})
end)

RegisterServerEvent('es_em:sv_removeMoney')
AddEventHandler('es_em:sv_removeMoney',function()
    TriggerEvent("es:getPlayerFromId", source,function(user)
        if(user)then
            if user.money > 0 then
                user.setMoney(0)
            end
        end
    end)
end)

RegisterServerEvent('es_em:sv_sendMessageToPlayerInComa')
AddEventHandler('es_em:sv_sendMessageToPlayerInComa',function(sourcePlayerInComa)
    TriggerClientEvent('es_em:cl_sendMessageToPlayerInComa', sourcePlayerInComa)
end)

AddEventHandler('playerDropped', function()
    local player = getPlayerID(source)
    local source = source
    MySQL.Sync.execute("UPDATE users SET enService = 0 WHERE users.identifier = @identifier", {['@identifier'] = player})
end)

TriggerEvent('es:addCommand', 'respawn', function(source, args, user)
    TriggerClientEvent('es_em:cl_respawn', source)
end)

function GetJobId(source)
    local jobId = -1
    local player = getPlayerID(source)
    local result = MySQL.Sync.fetchAll("SELECT identifier, job_id, job_name FROM users LEFT JOIN jobs ON jobs.job_id = users.job WHERE users.identifier = @identifier AND job_id IS NOT NULL", {['@identifier'] = player})

      if (result[1] ~= nil) then
        jobId = result[1].job_id
      end

  return jobId
end

RegisterServerEvent('ambulancier:finesGranted')
AddEventHandler('ambulancier:finesGranted', function(target, amount)
	TriggerClientEvent('ambulancier:payFines', target, amount, source)
	TriggerClientEvent("depanneur:notify", source, "CHAR_PLANESITE", 1, "Urgence", false, "Envoi d'une facture de $"..amount.." à "..GetPlayerName(target))
end)

RegisterServerEvent('ambulancier:finesETA')
AddEventHandler('ambulancier:finesETA', function(officer, code)
	if(code==1) then
		TriggerClientEvent("depanneur:notify", officer, "CHAR_PLANESITE", 1, "Urgence", false, GetPlayerName(source).." à déjà une demande de facture")
	elseif(code==2) then
		TriggerClientEvent("depanneur:notify", officer,"CHAR_PLANESITE", 1, "Urgence", false, GetPlayerName(source).." n'a pas répondu à la demande de facture")
	elseif(code==3) then
		TriggerClientEvent("depanneur:notify", officer, "CHAR_PLANESITE", 1, "Urgence", false, GetPlayerName(source).." à refusé de payer")
	elseif(code==0) then
		TriggerClientEvent("depanneur:notify", officer, "CHAR_PLANESITE", 1, "Urgence", false, GetPlayerName(source).." à payé sa facture")
	end
end)

RegisterServerEvent('ambulancier:freeze')
AddEventHandler('ambulancier:freeze',function(t)
   TriggerClientEvent('ambulancier:stopmove',t) 
end)

RegisterServerEvent('ambulancier:soin')
AddEventHandler('ambulancier:soin',function(t)
   TriggerClientEvent('ambulancier:recupvie',t) 
end)

RegisterServerEvent('ambulancier:debutrea')
AddEventHandler('ambulancier:debutrea',function(t,coord,head)
     TriggerClientEvent('ambulancier:animrea',t,coord,head)
end)

RegisterServerEvent('Ambulancier:TattooAutorisation')
AddEventHandler('Ambulancier:TattooAutorisation', function(t)
    local source = source
    TriggerClientEvent('Ambulancier:TattooDemande',t,source)
end)

RegisterServerEvent('Ambulancier:ReponseTattoo')
AddEventHandler('Ambulancier:ReponseTattoo', function(provenance,reponse)
    local source = source
    local target = getPlayerID(source)
    if reponse then
        MySQL.Async.fetchAll("Select * from user_tattoos WHERE identifier = @id",{['@id'] = target}, function(result)
            if result[1] ~= nil then
                TriggerClientEvent('Amublancier:AffTattoo',provenance,json.decode(result[1].tattoos),source)
                --TriggerClientEvent('Ambulancier:TattooTenue',t)
            else
                TriggerClientEvent('Amublancier:AffTattoo',provenance,{},source)        
            end
        end) 
    else
        TriggerClientEvent('hud:NotifColor',provenance,"Le patient refuse d'être examiné",6)
        TriggerClientEvent('Amublancier:AffTattoo',provenance,nil,source)
    end
end)

RegisterServerEvent('Ambulancier:SaveTattoo')
AddEventHandler('Ambulancier:SaveTattoo', function(target,list)
    local target = target
    local list = list
    local source = source
    local playerId = getPlayerID(target)
    MySQL.Async.insert("UPDATE user_tattoos SET tattoos = @tattoos WHERE identifier = @identifier", {['@tattoos'] = json.encode(list), ['@identifier'] = playerId})
    TriggerClientEvent('hud:NotifColor',target,"Tatouage effacé",141)
end)

RegisterServerEvent('Ambulancier:TattooQuitter')
AddEventHandler('Ambulancier:TattooQuitter',function(target)
   TriggerClientEvent('Ambulancier:TattooFin',target) 
end)

RegisterServerEvent('Ambulancier:EnvoiCurItm')
AddEventHandler('Ambulancier:EnvoiCurItm',function(cible,list,curitem,aff)
    TriggerClientEvent('Ambulancier:ClignTattoo',cible,list,curitem,aff)
end)
-- get's the player id without having to use bugged essentials
function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

-- gets the actual player id unique to the player,
-- independent of whether the player changes their screen name
function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end

