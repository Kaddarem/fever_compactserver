--Variables
local recoltDistance = 5
local timeForRecolt = 5000 --1000 for 1 second
--
local vehjob = {
    {job = 4,veh='rubble'},
    {job = 7,veh='rebel01'},
    {job = 10,veh='phantom'},
}

local inService = {
    ["police"] = {},
    ["medic"] = {},
    ["taxi"] = {},
    ["depanneur"] = {},
}

local pourcentage = 1

local near
local jobId = 6
JOBS = {}
-- point 1/2/3 en plus ds points cuivre/diamand/or
-- BLIPS = {}

RegisterNetEvent("jobs:getJobs")
RegisterNetEvent("cli:getJobs")
RegisterNetEvent("recolt:updateJobs")

AddEventHandler("recolt:updateJobs", function(id)
    jobId = id
	TriggerServerEvent("jobs:getJobs")
end)

AddEventHandler("playerSpawned", function()
    TriggerServerEvent("jobs:getJobs")
end)

-- Get the list of all jobs in the database and create the blip associated
AddEventHandler("cli:getJobs", function(listJobs)
    JOBS = listJobs
end)

function DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

-- Control if the player of is near of a place of job
function IsNear()
    local ply = GetPlayerPed(-1)
    local plyCoords = GetEntityCoords(ply, 0)
	if(IsPedInAnyVehicle(ply, true) == false) then
		for k, item in pairs(JOBS) do
            local bonjob = false
            for _,v in pairs(item.job_id) do
                if(v == jobId) then
                    bonjob = true
                    break
                end
            end
            if bonjob or item.isIllegal then
				local distance_field = GetDistanceBetweenCoords(item.fx, item.fy, item.fz, plyCoords["x"], plyCoords["y"], plyCoords["z"], true)
				local distance_treatment = GetDistanceBetweenCoords(item.tx, item.ty, item.tz, plyCoords["x"], plyCoords["y"], plyCoords["z"], true)
				local distance_seller = GetDistanceBetweenCoords(item.sx, item.sy, item.sz, plyCoords["x"], plyCoords["y"], plyCoords["z"], true)
				if (distance_field <= recoltDistance) then
					--jobId = k
					return 'field', item
				elseif (distance_treatment <= recoltDistance) then
					--jobId = k
					return 'treatment', item
				elseif (distance_seller <= recoltDistance) then
					--jobId = k
					return 'seller', item
				end
			end
		end
	end
end

local BlocageCoffre = false
function  BlockCoffre()
    Citizen.CreateThread(function()
        while BlocageCoffre do
            Wait(0)
            DisableControlAction(1,47,true)
        end
    end)
end
-- Display the message of recolting/treating/selling and trigger the associated event(s)
function recolt(text, item, rl)
    BlocageCoffre = true
    BlockCoffre()
    if (text == 'Récolte') then
        TriggerEvent("mt:missiontext", text .. ' en cours de ~g~' .. tostring(item.raw_item) .. '~s~...', timeForRecolt - 800)
        Citizen.Wait(timeForRecolt - 800)
        TriggerEvent("player:receiveItem", tonumber(item.raw_id), 1)
        TriggerEvent("mt:missiontext", rl .. ' ~g~' .. tostring(item.raw_item) .. '~s~...', 800)
    elseif (text == 'Traitement') then
        TriggerEvent("mt:missiontext", text .. ' en cours de ~g~' .. tostring(item.raw_item) .. '~s~...', timeForRecolt - 800)
        Citizen.Wait(timeForRecolt - 800)
        animation("mp_common","givetake1_b")
        TriggerEvent("player:looseItem", tonumber(item.raw_id), 1)
        TriggerEvent("player:receiveItem", tonumber(item.treat_id), 1)
        TriggerEvent("mt:missiontext", rl .. ' ~g~' .. tostring(item.treat_item) .. '~s~...', 800)
    elseif (text == 'Vente') then
        TriggerEvent("mt:missiontext", text .. ' en cours de ~g~' .. tostring(item.treat_item) .. '~s~...', timeForRecolt - 800)
        Citizen.Wait(timeForRecolt - 800)
        animation("mp_common","givetake1_b")
        --TriggerServerEvent('vdkrec:illegal',item.treat_id,item.price)
        local new_price = item.price
        if item.isIllegal then
            if #inService["police"] == 0 then
                new_price = math.floor(0.1 * new_price) 
            elseif #inService["police"] == 1 then
                new_price = math.floor(0.5 * new_price) 
            elseif #inService["police"] == 2 then
                new_price = math.floor(0.8 * new_price) 
            else
                new_price =  new_price 
            end
            new_price = math.floor(new_price * pourcentage)
        end
        if item.isIllegal then
            TriggerEvent('player:looseItem',item.treat_id,1)
            TriggerEvent('player:receiveItem',41,tonumber(new_price))
            TriggerEvent('hud:Notif',"~g~+ "..new_price.."~w~ billets marqués")
        elseif item.JobEntreprise == 1 then
            TriggerEvent('Entreprise:AddVente', tonumber(new_price))
            TriggerEvent('player:looseItem',item.treat_id,1)
        else
            TriggerEvent("player:sellItem", tonumber(item.treat_id), tonumber(new_price))
        end
        TriggerEvent("mt:missiontext", rl .. ' ~g~' .. tostring(item.treat_item) .. '~s~...', 800)
        TriggerServerEvent('Recolt:nbvente',item)
    end
    Citizen.Wait(800)
    BlocageCoffre = false
end

-- Constantly check the position of the player
local recoltactive = false

function PlayScenario(param1, param2, param3)
	Citizen.CreateThread(function()
		TaskStartScenarioInPlace(GetPlayerPed(-1), param1, 0, 1)
		PlayAmbientSpeech1(GetPlayerPed(-1), param2, param3)
		
	end)
end

function animation(dict,anim)
    Citizen.CreateThread(function()
		Wait(100)
		RequestAnimDict(dict)

		while not HasAnimDictLoaded(dict) do
			Citizen.Wait(0)
		end

		local myPed = PlayerPedId()
		local animation = anim
		local flags = 16 -- only play the animation on the upper body

		TaskPlayAnim(myPed, dict, animation, 8.0, -8, -1, flags, 0, 0, 0, 0)
	end)
end

RegisterNetEvent('Call:ReturnNumber')
AddEventHandler('Call:ReturnNumber',function(temp)
    inService = {
    ["police"] = {},
    ["medic"] = {},
    ["taxi"] = {},
    ["depanneur"] = {},
}
    inService = temp
end)

RegisterNetEvent('Recolt:UpdateIllegal')
AddEventHandler('Recolt:UpdateIllegal', function(newpourcentage)
   pourcentage = newpourcentage
end)

function bonveh(item)
    if item.JobEntreprise then
        return true
    end
    local car = GetVehiclePedIsIn(GetPlayerPed(-1), true)
    local model = string.lower(GetDisplayNameFromVehicleModel(GetEntityModel(car)))
    for a,b in pairs(vehjob) do
        if b.job == jobId then
            if b.veh == model then
                return true
            end
        end
    end
    return false
end

local near = nil
local item = nil
Citizen.CreateThread(function()
    local activationcoffre = true
    while true do
        Citizen.Wait(1)
        near, item = IsNear()
            if near == 'field' then
                if IsControlJustReleased(1, 51)  then -- IF INPUT_context Is pressed
                    if not item.isIllegal then
                        if bonveh(item) then
                        recoltactive = not recoltactive
                        if recoltactive then
                            PlayScenario("world_human_gardener_plant","GENERIC_CURSE_MED" ,"SPEECH_PARAMS_FORCE")
                        else
                            ClearPedTasks(GetPlayerPed(-1))
                        end
                    else
                        TriggerEvent('hud:NotifColor',"Interdit avec ce vehicule",6)
                    end
                    else
                        recoltactive = not recoltactive
                    end
                end
            elseif near == 'treatment' then
                if IsControlJustReleased(1, 51)  then
                    if not item.isIllegal then
                        if bonveh(item) then -- IF INPUT_context Is pressed
                            recoltactive = not recoltactive
                        else
                            TriggerEvent('hud:NotifColor',"Interdit avec ce vehicule",6)
                        end
                    else
                        recoltactive = not recoltactive
                    end
                end
            elseif near == 'seller' then
                if IsControlJustReleased(1, 51)  then
                    if not item.isIllegal then
                        if bonveh(item) then -- IF INPUT_context Is pressed
                            recoltactive = not recoltactive
                        else
                            TriggerEvent('hud:NotifColor',"Interdit avec ce vehicule",6)
                        end
                    else
                        --TriggerServerEvent('Recolt:PrixIllegal',item)
                        TriggerServerEvent('Call:GetNumberService')
                        recoltactive = not recoltactive
                    end
                end
            else
                recoltactive = false
            end
            if recoltactive then
                DisableControlAction(1, 182, true)
                if activationcoffre then
                    activationcoffre = false
                end
                DisplayHelpText("Appuyez sur ~INPUT_CONTEXT~ pour arrêter",0,1,0.5,0.8,0.6,255,255,255,255)
            elseif not activationcoffre then
                EnableControlAction(1,82,true)
                activationcoffre = true
            end
    end 
end)
Citizen.CreateThread(function()
    Citizen.Wait(5000)
    while true do
        Citizen.Wait(1)
        ------------ RECOLTE -------------
        if (near == 'field') then
            if not recoltactive then
                DisplayHelpText("Appuyez sur ~INPUT_CONTEXT~ pour récolter du ~b~"..item.raw_item,0,1,0.5,0.8,0.6,255,255,255,255)
            end
            if recoltactive then
                    recolt('Récolte', item, '+1')
            end
            -------------- TRAITEMENT -------------
        elseif (near == 'treatment' and exports.menu_personnel:getQuantity(item.raw_id) > 0) then
            if not recoltactive then
                DisplayHelpText("Appuyez sur ~INPUT_CONTEXT~ pour traiter du ~b~"..item.raw_item,0,1,0.5,0.8,0.6,255,255,255,255)
            end
            if recoltactive then
                    recolt('Traitement', item, '+1')  
            end
            -------------- REVENTE --------------
        elseif (near == 'seller' and exports.menu_personnel:getQuantity(item.treat_id) > 0) then
            if not recoltactive then
                DisplayHelpText("Appuyez sur ~INPUT_CONTEXT~ pour vendre du ~b~"..item.treat_item,0,1,0.5,0.8,0.6,255,255,255,255)
            end
            if recoltactive then
                    recolt('Vente', item, '-1')   
            end
        end
    end
end)

function Chat(debugg)
    TriggerEvent("chatMessage", '', { 0, 0x99, 255 }, tostring(debugg))
end

function Notify(text)
    text = tostring(text)
    SetNotificationTextEntry('STRING')
    AddTextComponentString(text)
	DrawNotification(0,1)
end

