local Decroissance = 20
local Duree = 10
local jobs = {}
local result = nil

RegisterServerEvent("jobs:getJobs")
AddEventHandler("jobs:getJobs", function ()
    local source = source
	jobs = {}
	if(not result) then
		local result = MySQL.Sync.fetchAll("SELECT price, i1.`id` AS raw_id, i1.`libelle` AS raw_item, i2.`id` AS treat_id, i2.`libelle` AS treat_item, p1.x AS fx, p1.y AS fy, p1.z AS fz, p2.x AS tx, p2.y AS ty, p2.z AS tz, p3.x AS sx, p3.y AS sy, p3.z AS sz, job_id, i1.`isIllegal`, recolt.JobEntreprise FROM recolt LEFT JOIN items i1 ON recolt.`raw_id`=i1.id LEFT JOIN items i2 ON recolt.`treated_id`=i2.id LEFT JOIN coordinates p1 ON recolt.`field_id`=p1.id LEFT JOIN coordinates p2 ON recolt.`treatment_id`=p2.id LEFT JOIN coordinates p3 ON recolt.`seller_id`=p3.id",{})
            if (result) then
                for _,v in pairs(result) do
                    local jobid = {}
                    str, sep = v.job_id, ","
                    string.gsub(str,"([^,]*),", function(c)
                        table.insert(jobid, tonumber(c))
                    end)
                    v.job_id = jobid
                end
                jobs = result
            end
	else
		jobs = result
    end
    TriggerClientEvent("cli:getJobs", source, jobs)
end)

RegisterServerEvent('Recolt:nbvente')
AddEventHandler('Recolt:nbvente',function(item)
    local id_item = item.treat_id
    MySQL.Async.insert("UPDATE recolt SET nb_ventes = nb_ventes + 1 WHERE treated_id = @id",{['@id']=id_item})
end)

RegisterServerEvent('Recolt:PrixIllegal')
AddEventHandler('Recolt:PrixIllegal', function(item)
    local source = source
    local id_item = item.treat_id
    MySQL.Async.fetchAll("SELECT * FROM recolt WHERE treated_id = @id",{['@id']=id_item}, function(info)
        local hupdate = info[1].heure_update
        local nb_ventes = info[1].nb_ventes
        local hactuel = os.time()*1000
        difference = math.abs(os.difftime(hactuel,hupdate))
        difference = difference/1000 --seconde
        difference = difference/60 -- minute
        if difference > Duree then
            while difference > Duree do
                nb_ventes = nb_ventes - Decroissance
                difference = difference - Duree
            end
            if nb_ventes < 0 then
                nb_ventes = 0
            end
            local tstamp = os.date("*t")
            local hnew = tstamp.year .. "-" .. tstamp.month .. "-" .. tstamp.day .. " " .. tstamp.hour .. ":" .. tstamp.min .. ":" .. tstamp.sec
            MySQL.Async.insert("UPDATE recolt SET nb_ventes = @nb, heure_update = @heure WHERE treated_id = @id",{['@nb']=nb_ventes,['@heure']=hnew,['@id']=id_item})
        end
        local pourcentage = 0
        if nb_ventes < 180 then
            pourcentage = 1
        elseif nb_ventes < 290 then
            pourcentage = 0.8
        elseif nb_ventes < 410 then
            pourcentage = 0.5
        elseif nb_ventes < 500 then
            pourcentage = 0.25
        else
            pourcentage = 0.0
        end
        TriggerClientEvent('Recolt:UpdateIllegal',source,pourcentage)
    end)
end)