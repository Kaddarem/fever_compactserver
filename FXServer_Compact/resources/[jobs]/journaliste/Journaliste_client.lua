--[[
##################
#    Oskarr      #
#    MysticRP    #
#   client.lua   #
#      2017      #
##################
--]]

local jobId = -1 -- don't edit
local isInServiceJournaliste = false -- don't edit
local taxiplatee = " CNNCNN " -- Plate for service vehicle
local taximodel = GetHashKey('rumpo') -- Model for service car
local taxijob = 22 -- JobID for taxi
local openMenuKey = 166 -- (G) Key for OPEN TAXI MENU 
local emplacement = {
{name="LIFEinvader", id=498, x=-1085.5495605469, y=-249.12701416016, z=37.763282775879},
}
local JobJour = {22,21}

RegisterNetEvent('recolt:updateJobs')
AddEventHandler('recolt:updateJobs', function(newjob)
    jobId = newjob
    for _,job in pairs(JobJour) do
        if jobId == job then
            BoucleTakeService()
            break
        end
    end
end)

local function TestBonJob()
    for _,job in pairs(JobJour) do
        if jobId == job then
            return true
        end
    end
    return false
end

---- THREADS ----

-- Service
function BoucleTakeService()
    Citizen.CreateThread(function()
        while TestBonJob() do
            Citizen.Wait(5)
            local x = -1049.6303710938
            local y = -241.89895629883
            local z = 44.021022796631
            local playerPos = GetEntityCoords(GetPlayerPed(-1), true)
            if jobId == 22 or jobId == 21 then
                if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 100.0) then
                    Marker(x, y, z - 1,1.5,255, 0, 0)
                    if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 2.0) then
                        if isInServiceJournaliste then
                            DisplayHelp('Appuyez sur ~INPUT_CONTEXT~ pour ~r~stopper~s~ votre service') 
                        else
                            DisplayHelp('Appuyez sur ~INPUT_CONTEXT~ pour ~g~prendre~s~ votre service')
                        end
                        if (IsControlJustReleased(1, 51)) then 
                            GetService()
                        end
                    end
                end
            end
            x = -1097.625
            y = -255.95649719238
            z = 37.683208465576
            if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 100.0) and isInServiceJournaliste then
                Marker(x, y, z - 1,3.0,255, 0, 0)
                if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 1.0) then
                    local ply = GetPlayerPed(-1)
                    if IsPedInAnyVehicle(ply, true) then
                        DisplayHelp('Appuyez sur ~INPUT_CONTEXT~ pour ~r~ranger~s~ votre ~b~Rumpo')
                        if (IsControlJustReleased(1, 51)) then 
                            local vehicle = GetVehiclePedIsIn(ply, true)
                            local isVehicleTaxi = IsVehicleModel(vehicle, taximodel)
                             if isVehicleTaxi then
                                    DeleteTaxi()
                                    caution = false
                             else
                                TriggerEvent('hud:NotifColor',"Ce n'est pas une camionnette de journaliste !",6)
                             end
                        end
                    else						
                        DisplayHelp('Appuyez sur ~INPUT_CONTEXT~ pour ~b~sortir~s~ une ~b~camionnette')
                          if (IsControlJustReleased(1, 51)) then 
                            Taxi()
                          end
                    end
                end
            end
            x = -1057.7474365234
            y = -242.9644317627
            z = 44.021064758301
            if jobId == 21 then
                if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 100.0) and isInServiceJournaliste then
                    Marker(x, y, z - 1,1.5,255, 0, 0)
                    if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 1.0) then
                        DisplayHelp("Pressez ~INPUT_CONTEXT~ pour ouvrir l'appli CNN") 
                        if (IsControlJustReleased(1, 51)) then 
                            CNN()
                        end
                    end
                end
            end
        end
    end)
end


function CNN()
    VMenu.curItem = 1
    VMenu.ResetMenu()
    VMenu.EditHeader('CNN')
    VMenu.AddFunc("Envoyer une annonce", "annonce", nil)
    VMenu.visible = not VMenu.visible
end

function DisplayInput()
    DisplayOnscreenKeyboard(1, "FMMC_MPM_TYP8", "", "", "", "", "", 120)
    while UpdateOnscreenKeyboard() == 0 do
        DisableAllControlActions(0)
        Wait(1)
    end
    if (GetOnscreenKeyboardResult()) then
        return GetOnscreenKeyboardResult()
    end
end

function annonce()
    VMenu.visible = not VMenu.visible
    local res = DisplayInput()
    if string.len(res) > 5 then
        TriggerServerEvent('hud:CNN',"LifeInvader",res)
    else
        TriggerEvent('hud:NotifColor',"Message trop court",6)
    end
end


function give(info)
    taketarget()
    if Target ~= -1 then
		VMenu.visible = false
        
        if (info.quantity - res >= 0) then
            TriggerServerEvent("player:giveItem", info.item_id, info.libelle, res, GetPlayerServerId(Target))
        end
    else
		TriggerEvent('hud:NotifColor', "Personne devant vous",6)
    end 
end

---------------------------

function Taxi()
	Citizen.Wait(0)
	local ped = GetPlayerPed(-1)
	local player = PlayerId()
	local vehicle = taximodel

	RequestModel(vehicle)

	while not HasModelLoaded(vehicle) do
		Wait(1)
	end

	--local plate = math.random(300, 900)
	local coords = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0, 5.0, 0)
	local spawned_car = CreateVehicle(vehicle, -1098.9517822266, -257.56072998047, 37.679504394531, 161.37840270996,true, false)
	SetVehicleOnGroundProperly(spawned_car)
	SetVehicleNumberPlateText(spawned_car, taxiplatee)
    SetVehicleExtra(spawned_car, 5, 1)
    SetVehicleLivery(spawned_car, rump_lv1)
	SetVehicleColours(spawned_car, 12, 131)
	SetPedIntoVehicle(ped, spawned_car, - 1)
    local id = NetworkGetNetworkIdFromEntity(spawned_car)
        SetNetworkIdCanMigrate(id, true)
        SetNetworkIdExistsOnAllMachines(id,true)
end

function DeleteTaxi()
    local ply = GetPlayerPed(-1)
    local playerVeh = GetVehiclePedIsIn(ply, false)
    Citizen.Wait(1)
    ClearPedTasksImmediately(ply)
    SetEntityVisible(playerVeh, false, 0)
    SetEntityCoords(playerVeh, 999999.0, 999999.0, 999999.0, false, false, false, true)
    FreezeEntityPosition(playerVeh, true)
    SetEntityAsMissionEntity(playerVeh, 1, 1)
    DeleteVehicle(playerVeh)
end

---------

function TaxiMenu() -- TAXI MENU
    VMenu.ResetMenu()
    VMenu.EditHeader('Journaliste')
    if jobId == 21 then
        VMenu.AddFunc("Recruter un Journaliste", "recruter", nil) 
        VMenu.AddFunc("Licencier un Journaliste", "licencier", nil) 
    end
    VMenu.AddFunc("Sortir Bloc-Note", "note", nil)
    VMenu.AddFunc("Prendre des photos", "photo", nil)
    local vehicle = GetVehiclePedIsIn(GetPlayerPed(-1), true)
    local isVehicleTaxi = IsVehicleModel(vehicle, taximodel)
    if isVehicleTaxi then
        VMenu.AddFunc("Envoyer une annonce CNN", "annonce", nil)
    end
end

function recruter()
    DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8S", "", "", "", "", "", 20)
    while (UpdateOnscreenKeyboard() == 0) do
	    DisableAllControlActions(0);
		Wait(0);
	end
	if (GetOnscreenKeyboardResult()) then
	    local res = tonumber(GetOnscreenKeyboardResult())
		TriggerServerEvent('commande_metier:recruter',res,22)
    end
end

function licencier()
    DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8S", "", "", "", "", "", 20)
    while (UpdateOnscreenKeyboard() == 0) do
	    DisableAllControlActions(0);
		Wait(0);
	end
	if (GetOnscreenKeyboardResult()) then
	    local res = tonumber(GetOnscreenKeyboardResult())
		TriggerServerEvent('commande_metier:licencier',res)
    end
end

function note()
    Citizen.CreateThread(function()
        TaskStartScenarioInPlace(GetPlayerPed(-1), 'WORLD_HUMAN_CLIPBOARD', 0, false)
    end) 
end

function photo()
    Citizen.CreateThread(function()
        TaskStartScenarioInPlace(GetPlayerPed(-1), 'WORLD_HUMAN_PAPARAZZI', 0, false)
    end) 
end


RegisterNetEvent('taxi:cl_setJobId')
AddEventHandler('taxi:cl_setJobId',
	function(p_jobId)
		jobId = p_jobId
		GetService()
	end
)


function GetService()
if jobId ~= taxijob and jobId ~= 21 then
 TriggerEvent('hud:NotifColor',"Tu n'est pas Journaliste !",6) 
		return
end
	if isInServiceJournaliste then
		TriggerEvent('hud:Notif',"Vous avez ~r~fini~s~ votre service") 
        TriggerServerEvent("player:serviceOff", "journaliste")
	else 
		TriggerEvent('hud:NotifColor',"Vous êtes en service !",141)
        TriggerServerEvent("player:serviceOn", "journaliste")
        BoucleMenuInf()
        mainBoucleJobJournaliste()
	end
	
	isInServiceJournaliste = not isInServiceJournaliste
-- Here for any clothes with SetPedComponentVariation ... 
end

---- MENU OPEN -----
function BoucleMenuInf()
    Citizen.CreateThread(function()
        VMenu.AddMenu( 'Journaliste', 'journaliste',255,255,255)   
        while isInServiceJournaliste do
            Citizen.Wait(5)
            if IsControlJustPressed(1, openMenuKey) then 
                if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                VMenu.curItem = 1
                TaxiMenu() 
                VMenu.visible = not VMenu.visible
            end
            VMenu.Show()
        end
    end)
end
--------------

-- Show blip
Citizen.CreateThread(function()
    for _, item in pairs(emplacement) do
      item.blip = AddBlipForCoord(item.x, item.y, item.z)
      SetBlipSprite(item.blip, item.id)
      SetBlipColour(item.blip, item.colour)
      SetBlipAsShortRange(item.blip, true)
      BeginTextCommandSetBlipName("STRING")
      AddTextComponentString(item.name)
      EndTextCommandSetBlipName(item.blip)
    end
end)



function mainBoucleJobJournaliste()
    Citizen.CreateThread(function()
        local x = -1044.9453125
        local y = -235.30989074707
        local z = 37.964939117432
        local BoucleLent = 5000
        local BoucleCourt = 5
        local TempsBoucle = BoucleCourt
        while isInServiceJournaliste do
            Wait(TempsBoucle)
            local playerPos = GetEntityCoords(GetPlayerPed(-1), true)
            if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 50.0) then
                TempsBoucle = BoucleCourt
                Marker(x,y,z -1,1.5,0, 233, 255)
                if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 2.0) then
                    DisplayHelp('Pressez ~INPUT_CONTEXT~ pour prendre une affiche')
                    if (IsControlJustReleased(1, 51)) then
                        DrawMissionText("Allez acrocher cette affiche",5000)
                        acrocherAffiche()
                        Wait(10000)
                    end
                end
            else
                TempsBoucle = BoucleLent  
            end
        end
    end)
end