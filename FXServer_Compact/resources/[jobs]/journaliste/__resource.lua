-- Manifest

-- Requiring essentialmode
dependency 'essentialmode'

client_script {
    '@hud-event/hudevent_aff.lua',
    '@appmenu/menu_cl.lua',
    'Journaliste_client.lua',
    "Journaliste_affiche.lua",
}

