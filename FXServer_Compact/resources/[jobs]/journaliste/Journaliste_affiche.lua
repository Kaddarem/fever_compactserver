local pedType={}

local positions={
    {386.90127563477, -1001.0274658203,  29.417947769165, 94.380851745605},--devant comico
    {-409.64016723633, -76.488204956055,  42.891620635986, 232.67533874512},--garage mecano
    {269.3125, -1431.4215087891,  29.364923477173, 138.49412536621},--hopitale
    {-1643.6293945313, -1024.6549072266,  13.151970863342, 53.484092712402}, --delperro
    {227.78109741211, 1161.8292236328,  225.78941345215, 281.94641113281}, --random1
    {-2299.56640625, 372.22940063477,  174.60162353516, 97.875205993652}, --random2
    {-1452.3491210938, -583.49438476563,  30.839727401733, 221.81828308105},--bahamas
    {-1053.4455566406, -2738.2058105469,  20.892917633057, 63.135326385498}, --aeroport
    {-560.50323486328, 274.53353881836,  83.019638061523, 352.17660522461}, --tequilala
    {-315.46768188477, 2823.6574707031,  58.592510223389, 246.71859741211}, --eglise
    {154.16137695313, -1038.1358642578,  29.316913604736, 162.70249938965}, -- banque centrale
    {304.70599365234, 183.4454498291,  103.94776153564, 353.53140258789},--vinewood
}
function DrawMissionText(m_text, showtime)
    ClearPrints()
	SetTextEntry_2("STRING")
	AddTextComponentString(m_text)
	DrawSubtitleTimed(showtime, 1)
end

local PrixCentMettreJournaliste=60
local missionCoordBlip
local pointDeDepart={x = -1044.9453125 ,y = -235.30989074707,z = 37.964939117432}


function acrocherAffiche()
    Citizen.CreateThread(function()
        local pos=positions[math.random(1,#positions)]
        local x = pos[1]
        local y = pos[2]
        local z = pos[3]
        missionCoordBlip = AddBlipForCoord(x,y,z)
        N_0x80ead8e2e1d5d52e(missionCoordBlip)
        SetBlipRoute(missionCoordBlip, 1)
        local BoucleLent = 5000
        local BoucleCourt = 5
        local TempsBoucle = BoucleCourt
        while true do
            Wait(TempsBoucle)
            TempsBoucle = BoucleLent  
            local playerPos = GetEntityCoords(GetPlayerPed(-1), true)
            if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 100.0) then
                TempsBoucle = BoucleCourt
                Marker(x,y,z-1,1.0, 255, 0, 0)
                if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 1.0) then
                    DisplayHelp('Pressez ~INPUT_CONTEXT~ pour poser l\'affiche')
                    if (IsControlJustReleased(1, 51)) then
                        if(missionCoordBlip ~= nil)then
                            RemoveBlip(missionCoordBlip)
                        end
                        missionCoordBlip=nil
                        PlayScenario("WORLD_HUMAN_HAMMERING","GENERIC_CURSE_MED" ,"SPEECH_PARAMS_FORCE")
                        Wait(2000)
                        ClearPedTasks(GetPlayerPed(-1))
                        local dist = Vdist(pointDeDepart.x,pointDeDepart.y, pointDeDepart.z, x, y, z)
                        local total = math.floor(dist/100*PrixCentMettreJournaliste)
                        TriggerServerEvent('es_em:payMe', total)
                        DrawMissionText('~g~Mission terminé',5000)
                        return
                    end
                end
            end
        end
    end)
end

function PlayScenario(param1, param2, param3)
	Citizen.CreateThread(function()
		TaskStartScenarioInPlace(GetPlayerPed(-1), param1, 0, 1)
		PlayAmbientSpeech1(GetPlayerPed(-1), param2, param3)
	end)
end