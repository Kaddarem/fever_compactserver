RegisterServerEvent("Tequilala:cocasee")
AddEventHandler("Tequilala:cocasee", function()
    local source = source
	TriggerEvent("es:getPlayerFromId", source, function(target)
	    if (tonumber(target.money) >= 25) then
            target.removeMoney(25)
            TriggerEvent('entreprise:addmoney',2,25)
            TriggerClientEvent("es_freeroam:notify", source, "CHAR_PROPERTY_BAR_TEQUILALA", 1, "Tequilala", false, "Coca ~g~+1 !\n")
            TriggerClientEvent("player:receiveItem",source, 31, 1)
		else
		  TriggerClientEvent("es_freeroam:notify", source, "CHAR_PROPERTY_BAR_TEQUILALA", 1, "Tequilala", false, "~r~Tu n'as pas suffisamment d'argent !\n")
		end
	end)
end)

RegisterServerEvent("Tequilala:Sandwichs")
AddEventHandler("Tequilala:Sandwichs", function()
    local source = source
	TriggerEvent("es:getPlayerFromId", source, function(target)
	    if (tonumber(target.money) >= 25) then
            target.removeMoney(25)
            TriggerEvent('entreprise:addmoney',2,25)
            TriggerClientEvent("es_freeroam:notify", source, "CHAR_PROPERTY_BAR_TEQUILALA", 1, "Tequi-la-la", false, "Sandwich ~g~+1 !\n")
            TriggerClientEvent("player:receiveItem",source, 30, 1)
		else
		TriggerClientEvent("es_freeroam:notify", source, "CHAR_PROPERTY_BAR_TEQUILALA", 1, "Tequi-la-la", false, "~r~Tu n'as pas suffisamment d'argent !\n")
		end
	end)
end)

RegisterServerEvent('Tequilala:payement')
AddEventHandler("Tequilala:payement", function(prix)
    local source = source
	TriggerEvent("es:getPlayerFromId", source, function(target)
	    if (tonumber(target.money) >= prix) then
            target.removeMoney(prix)
		else
		TriggerClientEvent("es_freeroam:notify", source, "CHAR_PROPERTY_BAR_TEQUILALA", 1, "Tequi-la-la", false, "~r~Tu n'as pas suffisamment d'argent !\n")
		end
	end)
end)