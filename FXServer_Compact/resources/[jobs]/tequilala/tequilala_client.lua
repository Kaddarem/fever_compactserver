local jobId=-1
local taxijob = 19 -- JobID for taxi
local openMenuKey = 166 -- (G) Key for OPEN TAXI MENU 


RegisterNetEvent('recolt:updateJobs')
AddEventHandler('recolt:updateJobs', function(newjob)
    Citizen.CreateThread(function()
        jobId = newjob
     end)
end)
------

function changemodel(model)
	
	local modelhashed = GetHashKey(model)

	RequestModel(modelhashed)
	while not HasModelLoaded(modelhashed) do 
	    RequestModel(modelhashed)
	    Citizen.Wait(0)
	end

	SetPlayerModel(PlayerId(), modelhashed)
	local a = "" -- nil doesnt work
	SetPedRandomComponentVariation(GetPlayerPed(-1), true)
	SetModelAsNoLongerNeeded(modelhashed)
end

function Notify(text)
    SetNotificationTextEntry('STRING')
    AddTextComponentString(text)
    DrawNotification(false, false)
end

function DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

function Main()
    VMenu.ResetMenu()
    VMenu.EditHeader('Tequi-La-La')
    VMenu.AddFunc("Sandwich (25$)", "Sandwich", nil)
	VMenu.AddFunc("Coca (25$)", "cocase", nil)
end

function MenuFonction() -- FACTURE MENU
    VMenu.ResetMenu()
    VMenu.EditHeader('Recrutement')
	VMenu.AddFunc("Embaucher salarié", "recruter", nil)
    VMenu.AddFunc("Licencier", "licencier", nil)
end


function recruter()
    DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8S", "", "", "", "", "", 20)
    while (UpdateOnscreenKeyboard() == 0) do
	    DisableAllControlActions(0);
		Wait(0);
	end
	if (GetOnscreenKeyboardResult()) then
	    local res = tonumber(GetOnscreenKeyboardResult())
		TriggerServerEvent('commande_metier:recruter',res,20)
    end
end

function licencier()
    DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8S", "", "", "", "", "", 20)
    while (UpdateOnscreenKeyboard() == 0) do
	    DisableAllControlActions(0);
		Wait(0);
	end
	if (GetOnscreenKeyboardResult()) then
	    local res = tonumber(GetOnscreenKeyboardResult())
		TriggerServerEvent('commande_metier:licencier',res)
    end
end
------------------------------
--FONCTIONS
-------------------------------
local twentyfourseven_shops = {
	{ ['x'] = -560.27874755859, ['y'] = 285.45355224609, ['z'] = 82.176376342773 },
}
Citizen.CreateThread(function()
	for k,v in ipairs(twentyfourseven_shops)do
		if k ~= 8 and k ~= 10 then
            local blip = AddBlipForCoord(v.x, v.y, v.z)
            SetBlipSprite(blip, 93)
            SetBlipScale(blip, 0.8)
            SetBlipAsShortRange(blip, true)
            BeginTextCommandSetBlipName("STRING")
            AddTextComponentString("Tequi-la-la")
            EndTextCommandSetBlipName(blip)
        end
	end
end)

function Sandwich()
    TriggerServerEvent("Tequilala:Sandwichs")
end

function cocase()
    TriggerServerEvent("Tequilala:cocasee")
end


-------------------------
---INVENTAIRE
-------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Press F2 to open menu
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Citizen.CreateThread(function()
    VMenu.AddMenu( 'Tequi-La-La', 'tequi',221,179,67)   
    local playerPed = GetPlayerPed(-1)
	while true do
		Citizen.Wait(0)
        if jobId == taxijob then
            if IsControlJustPressed(1, openMenuKey) then
                if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                VMenu.curItem = 1
                MenuFonction()
                --Notify("~s~Menu mineur ~g~activé~s~")   
                VMenu.visible = not VMenu.visible
            end
        end
		local pos = GetEntityCoords(GetPlayerPed(-1), false)
		for k,v in ipairs(twentyfourseven_shops) do
			if(Vdist(v.x, v.y, v.z, pos.x, pos.y, pos.z) < 20.0)then
                Marker( v.x, v.y, v.z - 1,1.0,0, 25, 165)
				if(Vdist(v.x, v.y, v.z, pos.x, pos.y, pos.z) < 1.0)then
					DisplayHelpText("Appuyer sur ~INPUT_CONTEXT~ pour ~g~acheter.")
					if IsControlJustPressed(1, 51) then
                        if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                        VMenu.curItem = 1
                        Main()
                        VMenu.visible = not VMenu.visible
				    end
                end
            end
		end
        VMenu.Show()
        Marker( -560.094, 288.417, 81.1763,1.0,0, 25, 165)
        playerCoords = GetEntityCoords(playerPed, 0)
        if(GetDistanceBetweenCoords(playerCoords, -560.09484863281, 288.41781616211, 82.176391601563) < 1.0) then
             DisplayHelpText("Appuyer sur ~INPUT_CONTEXT~ pour ~g~boire.")
             -- Start mission
            if(IsControlPressed(1, 51)) then
                TriggerServerEvent('entreprise:addmoney',2,50)
                TriggerServerEvent('Tequilala:payement',50)
                Toxicated()
                Citizen.Wait(120000)
                reality()
            end
        else
            showStartText = false
        end
	end
end)


function Toxicated()
  RequestAnimSet("move_m@drunk@verydrunk")
  while not HasAnimSetLoaded("move_m@drunk@verydrunk") do
    Citizen.Wait(0)
  end

  TaskStartScenarioInPlace(GetPlayerPed(-1), "WORLD_HUMAN_DRUG_DEALER", 0, 1)
  DoScreenFadeOut(1000)
  Citizen.Wait(1000)
  ClearPedTasksImmediately(GetPlayerPed(-1))
  SetTimecycleModifier("spectator5")
  SetPedMotionBlur(GetPlayerPed(-1), true)
  SetPedMovementClipset(GetPlayerPed(-1), "move_m@drunk@verydrunk", true)
  SetPedIsDrunk(GetPlayerPed(-1), true)
  DoScreenFadeIn(1000)
  end

  function reality()
    DoScreenFadeOut(1000)
    Citizen.Wait(1000)
    DoScreenFadeIn(1000)
    ClearTimecycleModifier()
    ResetScenarioTypesEnabled()
    ResetPedMovementClipset(GetPlayerPed(-1), 0)
    SetPedIsDrunk(GetPlayerPed(-1), false)
    SetPedMotionBlur(GetPlayerPed(-1), false)
    -- Stop the toxication
    Citizen.Trace("Going back to reality\n")
    end
