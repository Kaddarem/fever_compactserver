local Props = {
    'coke_cut_01',
    'coke_cut_02',
    'coke_cut_03',
    'coke_cut_04',
    'coke_cut_05',
    'coke_press_upgrade',
    'equipment_upgrade',
    'production_upgrade',
    'security_high',
    'set_up',
    'table_equipment',
    'table_equipment_upgrade',
}
local annexe = "Coke"

local Entree = {
    {name ='recolt', x = 2403.457, y = 3127.281, z = 47.152},
}

local Sortie = {
    ['recolt'] = {x = 1088.779, y = -3187.708, z = -39.993},
}

AddEventHandler('onClientResourceStart', function(res)
    if res == "interieur" then
        _G['LoinPorteEntree'..annexe]()
        FreezeEntityPosition(GetPlayerPed(-1),false)
        DoScreenFadeIn(0)
    end
end)

RegisterNetEvent('check:weed')
AddEventHandler('check:weed', function()
    for a,coord in pairs (Sortie) do
        if Vdist(coord.x, coord.y, coord.z, GetEntityCoords(GetPlayerPed(-1))) < 50.0  then
            SetEntityCoords(GetPlayerPed(-1), -780.48724365234, 306.00482177734,  85.701446533203)
        end
    end
end)

_G['LoinPorteEntree'..annexe] = function()
    local boucle = true
    Citizen.CreateThread(function() 
        while boucle do
            Citizen.Wait(1000)
            
            for key, coord in pairs(Entree) do
                if Vdist(coord.x, coord.y, coord.z, GetEntityCoords(GetPlayerPed(-1))) < 100.0  then
                    _G['ProchePorteEntree'..annexe](coord)
                    boucle = false
                    break
                end 
            end
        end
    end)
end

_G['ProchePorteEntree'..annexe] = function (coord)
    Citizen.CreateThread(function()
        local boucle = true
        local coord = coord
        while boucle do
            Wait(5)
            if Vdist(coord.x, coord.y, coord.z, GetEntityCoords(GetPlayerPed(-1))) < 100.0 then
                if Vdist(coord.x, coord.y, coord.z, GetEntityCoords(GetPlayerPed(-1))) < 2.0 then
                    DisplayHelp("Pressez ~INPUT_CONTEXT~ pour entrer")
                    if IsControlJustPressed(1, 51) then
                        _G['TeleportationEntree'..annexe](coord)
                        boucle = false
                        break
                    end
                end
            else
                boucle = false
                _G['LoinPorteEntree'..annexe]()
                break
            end
        end
    end)
end

_G['TeleportationEntree'..annexe] = function (coord)
    local boucle = true
    local coord = coord
    local coordInterne = Sortie[coord.name]
    Citizen.CreateThread(function()
        DoScreenFadeOut(0)
		while IsScreenFadingOut() do
			Citizen.Wait(0)
		end
            
            --------------------------------------------------------------
        ----------------------------- CREATION DU LOCAL ---------------------
            -------------------------------------------------------------- 
        RequestIpl('bkr_biker_interior_placement_interior_4_biker_dlc_int_ware03_milo')
        SetEntityCoords(GetPlayerPed(-1),coordInterne.x, coordInterne.y, coordInterne.z)
        FreezeEntityPosition(GetPlayerPed(-1),true)
        Wait(100)
        local ID = tonumber(GetInteriorAtCoords(coordInterne.x, coordInterne.y, coordInterne.z+1))
        TriggerServerEvent('print','1 '..ID)
        while not IsInteriorReady(ID) do
            Wait(10)
        end
        ---- TELEPORTATION
        ---- AJOUT DES PROPS
        RefreshInterior(ID)
        for a,b in pairs (Props) do
            Citizen.InvokeNative(0x55E86AF2712B36A1,ID,b)   
        end
        Citizen.InvokeNative(0x2CA429C029CCF247,ID)
        RefreshInterior(ID)
        FreezeEntityPosition(GetPlayerPed(-1),false)
        DoScreenFadeIn(0)
		while IsScreenFadingIn() do
			Citizen.Wait(0)
		end
            
            ----------------------------------------------------------
        ------------------------ BOUCLE DE SORTIE -----------------------
            ----------------------------------------------------------
        while boucle do
            Wait(5)
            if Vdist(coordInterne.x, coordInterne.y, coordInterne.z, GetEntityCoords(GetPlayerPed(-1))) < 2.0 then
                DisplayHelp("Pressez ~INPUT_CONTEXT~ pour sortir")
                if IsControlJustPressed(1, 51) then
                    _G['TeleportationSortie'..annexe](coord)
                    boucle = false
                    break
                end
            end
        end
    end)
end

_G['TeleportationSortie'..annexe] = function (coord)
    local boucle = true
    Citizen.CreateThread(function()
        DoScreenFadeOut(0)
		while IsScreenFadingOut() do
			Citizen.Wait(0)
		end
        local ID = tonumber(GetInteriorFromEntity(GetPlayerPed(-1)))
            --------------------------------------------------------------
        ----------------------------- SUPPRESSION DU LOCAL ---------------------
            -------------------------------------------------------------- 
        ---- SUPPRESSION DES PROPS
        RefreshInterior(ID)
        for a,b in pairs (Props) do
            Citizen.InvokeNative(0x420BD37289EEE162,ID,b)   
        end
        Citizen.InvokeNative(0x2CA429C029CCF247,ID)
        RefreshInterior(ID)
        
        ---- SUPPRESSION DES PORTES
        
            ----------------------------------------------------------
        ------------------------ BOUCLE DE SORTIE -----------------------
            ----------------------------------------------------------
        SetEntityCoords(GetPlayerPed(-1),coord.x, coord.y, coord.z)
        RemoveIpl('bkr_biker_interior_placement_interior_4_biker_dlc_int_ware03_milo')
        
        while IsInteriorReady(ID) do
            Wait(10)
        end
            
        DoScreenFadeIn(0)
		while IsScreenFadingIn() do
			Citizen.Wait(0)
		end
        _G['LoinPorteEntree'..annexe]()
        
    end)
end