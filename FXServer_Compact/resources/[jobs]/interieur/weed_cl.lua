local Props = {
'weed_drying',
'weed_standard_equip',
'weed_upgrade_equip',
'weed_growtha_stage2',
'weed_growthb_stage2',
'weed_growthc_stage3',
'weed_growthd_stage3',
'weed_growthe_stage2',
'weed_growthf_stage2',
'weed_growthg_stage2',
'weed_growthh_stage3',
'weed_growthi_stage1',
'weed_security_upgrade',
'weed_production',
'weed_set_up',
'weed_hosea',
'weed_hoseb',
'weed_hosec',
'weed_hosed',
'weed_hosee',
'weed_hosef',
'weed_hoseg',
'weed_hoseh',
'weed_hosei',
'light_growtha_stage23_standard',
'light_growthb_stage23_standard',
'light_growthc_stage23_standard',
'light_growthd_stage23_standard',
'light_growthe_stage23_standard',
'light_growthf_stage23_standard',
'light_growthg_stage23_standard',
'light_growthh_stage23_standard',
'light_growthi_stage23_standard',
'light_growtha_stage23_upgrade',
'light_growthb_stage23_upgrade',
'light_growthc_stage23_upgrade',
'light_growthc_stage23_upgrade',
'light_growthd_stage23_upgrade',
'light_growthe_stage23_upgrade',
'light_growthf_stage23_upgrade',
'light_growthg_stage23_upgrade',
'light_growthh_stage23_upgrade',
'light_growthi_stage23_upgrade',
'weed_low_security',
'weed_chairs',
}

local separation = 'prop_ss1_mpint_garage'

local ZoneInterdite = {
    {x = 1046.500, y = -3207.000, z = -38.591, a= 167.073},
    {x = 1046.500, y = -3204.000, z = -38.591, a= 167.073},
    {x = 1046.500, y = -3201.000, z = -38.591, a= 167.073},
}

local annexe = 'Weed'
local Entree = {
    {name ='recolt', x = -109.135, y = 2796.17, z = 52.304},
    {name ='traitement', x = -684.976, y = 5792.608, z = 16.33}
}

local Sortie = {
    ['recolt'] = {x = 1066.105, y = -3183.363, z = -40.00},
    ['traitement'] = {x = 1039.217, y = -3195.947, z = -39.169},
}

AddEventHandler('onClientResourceStart', function(res)
    if res == "interieur" then
        _G['LoinPorteEntree'..annexe]()
        FreezeEntityPosition(GetPlayerPed(-1),false)
        DoScreenFadeIn(0)
    end
end)

RegisterNetEvent('check:weed')
AddEventHandler('check:weed', function()
    for a,coord in pairs (Sortie) do
        if Vdist(coord.x, coord.y, coord.z, GetEntityCoords(GetPlayerPed(-1))) < 50.0  then
            SetEntityCoords(GetPlayerPed(-1), -780.48724365234, 306.00482177734,  85.701446533203)
        end
    end
end)

_G['LoinPorteEntree'..annexe] = function()
    local boucle = true
    Citizen.CreateThread(function() 
        while boucle do
            Citizen.Wait(1000)
            
            for key, coord in pairs(Entree) do
                if Vdist(coord.x, coord.y, coord.z, GetEntityCoords(GetPlayerPed(-1))) < 100.0  then
                    _G['ProchePorteEntree'..annexe](coord)
                    boucle = false
                    break
                end 
            end
        end
    end)
end

_G['ProchePorteEntree'..annexe] = function (coord)
    Citizen.CreateThread(function()
        local boucle = true
        local coord = coord
        while boucle do
            Wait(5)
            if Vdist(coord.x, coord.y, coord.z, GetEntityCoords(GetPlayerPed(-1))) < 100.0 then
                if Vdist(coord.x, coord.y, coord.z, GetEntityCoords(GetPlayerPed(-1))) < 2.0 then
                    DisplayHelp("Pressez ~INPUT_CONTEXT~ pour entrer")
                    if IsControlJustPressed(1, 51) then
                        _G['TeleportationEntree'..annexe](coord)
                        boucle = false
                        break
                    end
                end
            else
                boucle = false
                _G['LoinPorteEntree'..annexe]()
                break
            end
        end
    end)
end

local sep = nil
local sep2 = nil
local sep3 = nil
local sep4 = nil

_G['TeleportationEntree'..annexe] = function (coord)
    local boucle = true
    local coord = coord
    local coordInterne = Sortie[coord.name]
    Citizen.CreateThread(function()
        DoScreenFadeOut(0)
		while IsScreenFadingOut() do
			Citizen.Wait(0)
		end
            
            --------------------------------------------------------------
        ----------------------------- CREATION DU LOCAL ---------------------
            -------------------------------------------------------------- 
        RequestIpl('bkr_biker_interior_placement_interior_3_biker_dlc_int_ware02_milo')
        SetEntityCoords(GetPlayerPed(-1),coordInterne.x, coordInterne.y, coordInterne.z)
        FreezeEntityPosition(GetPlayerPed(-1),true)
        Wait(100)
        local ID = tonumber(247297)
        while not IsInteriorReady(ID) do
            Wait(10)
        end
        ---- TELEPORTATION
        ---- AJOUT DES PROPS
        RefreshInterior(ID)
        for a,b in pairs (Props) do
            Citizen.InvokeNative(0x55E86AF2712B36A1,ID,b)   
        end
        Citizen.InvokeNative(0x2CA429C029CCF247,ID)
        RefreshInterior(ID)
        
        ---- AJOUT DES PORTES
        sep = CreateObject(GetHashKey(separation),1048.330, -3204.541, -40.0, false,true,true)
        sep2 = CreateObject(GetHashKey(separation),1048.330, -3204.541, -36.1, false,true,true)
        sep3 = CreateObject(GetHashKey(separation),1045.021, -3204.077, -39.3, false,true,true)
        sep4 = CreateObject(GetHashKey(separation),1045.021, -3204.077, -35.4, false,true,true)
        SetEntityHeading(sep,90.000)
        SetEntityHeading(sep3,90.000)
        SetEntityRotation(sep4,0.0,180.0,90.0,1,1)
        SetEntityRotation(sep2,0.0,180.0,90.0,1,1)
        while (not DoesObjectOfTypeExistAtCoords(1048.330, -3204.541, -40.0,5.0,GetHashKey(separation),0)) do
            Wait(100)
        end
        while (not DoesObjectOfTypeExistAtCoords(1045.021, -3204.077, -39.3,5.0,GetHashKey(separation),0)) do
            Wait(100)
        end
        FreezeEntityPosition(sep,true)
        FreezeEntityPosition(sep2,true)
        FreezeEntityPosition(sep3,true)
        FreezeEntityPosition(sep4,true)
        FreezeEntityPosition(GetPlayerPed(-1),false)
        DoScreenFadeIn(0)
		while IsScreenFadingIn() do
			Citizen.Wait(0)
		end
            
            ----------------------------------------------------------
        ------------------------ BOUCLE DE SORTIE -----------------------
            ----------------------------------------------------------
        while boucle do
            Wait(5)
            local pos = GetEntityCoords(GetPlayerPed(-1), false)
            if GetEntityHealth(GetPlayerPed(-1)) > 0 then
                for _,coord in pairs(ZoneInterdite) do
                    if Vdist(coord.x, coord.y, coord.z, GetEntityCoords(GetPlayerPed(-1))) < 1.5 then
                        SetEntityHealth(GetPlayerPed(-1), 0)
                        TriggerEvent("banking:paywithbankclient", 100000)
                        TriggerServerEvent('log:printname','USEBUG', 'a voulu glitcher entre les zones de weed')
                    end
                end
            end
            if Vdist(coordInterne.x, coordInterne.y, coordInterne.z, GetEntityCoords(GetPlayerPed(-1))) < 2.0 then
                DisplayHelp("Pressez ~INPUT_CONTEXT~ pour sortir")
                if IsControlJustPressed(1, 51) then
                    _G['TeleportationSortie'..annexe](coord)
                    boucle = false
                    break
                end
            end
        end
    end)
end

_G['TeleportationSortie'..annexe] = function (coord)
    local boucle = true
    Citizen.CreateThread(function()
        DoScreenFadeOut(0)
		while IsScreenFadingOut() do
			Citizen.Wait(0)
		end
        local ID = tonumber(GetInteriorFromEntity(GetPlayerPed(-1)))
            --------------------------------------------------------------
        ----------------------------- SUPPRESSION DU LOCAL ---------------------
            -------------------------------------------------------------- 
        ---- SUPPRESSION DES PROPS
        RefreshInterior(ID)
        for a,b in pairs (Props) do
            Citizen.InvokeNative(0x420BD37289EEE162,ID,b)   
        end
        Citizen.InvokeNative(0x2CA429C029CCF247,ID)
        RefreshInterior(ID)
        
        ---- SUPPRESSION DES PORTES
        
        DeleteObject(sep)
        DeleteObject(sep2)
        DeleteObject(sep3)
        DeleteObject(sep4)
        
            ----------------------------------------------------------
        ------------------------ BOUCLE DE SORTIE -----------------------
            ----------------------------------------------------------
        SetEntityCoords(GetPlayerPed(-1),coord.x, coord.y, coord.z)
        RemoveIpl('bkr_biker_interior_placement_interior_3_biker_dlc_int_ware02_milo')
        
        while IsInteriorReady(ID) do
            Wait(10)
        end
            
        DoScreenFadeIn(0)
		while IsScreenFadingIn() do
			Citizen.Wait(0)
		end
        _G['LoinPorteEntree'..annexe]()
        
    end)
end