--[[
##################
#    Oskarr      #
#    MysticRP    #
#   client.lua   #
#      2017      #
##################
--]]

local options = {
    x = 0.1,
    y = 0.2,
    width = 0.2,
    height = 0.04,
    scale = 0.4,
    font = 0,
    menu_title = "Menu Raffineur",
    menu_subtitle = "Menu",
    color_r = 10, 
    color_g = 235,
    color_b = 10,
}
local jobId=-1
local isInServiceTaxi = false -- don't edit
local caution = false -- don't edit
local cautionprice = 0 -- caution price for service vehicle
local taxiplatee = " PETROL " -- Plate for service vehicle
local taximodel = GetHashKey('phantom') -- Model for service car
local taxijob = 10 -- JobID for taxi
local useModelMenu = true -- set to true if you use https://forum.fivem.net/t/release-async-model-menu-v2-6-17-6/19999
local openMenuKey = 166 -- (G) Key for OPEN TAXI MENU 
local emplacement = {
    {name="Entreprise de Raffinage", id=473, x=-57.1651, y=-2520.59, z=7.40117},
}
local zonerecolt = {
    {name="Forage de pétrole", id=267, x=654.819, y=3009.806, z=43.290},
    {name="Raffinerie du pétrole", id=267, x=1736.126, y=-1669.771, z=112.582},
    {name="Dépôt de pétrole", id=267, x=497.224, y=-2172.123, z=5.918},
}
local BLIPS = {}
local BLIPSZONE = {}
RegisterNetEvent('recolt:updateJobs')
AddEventHandler('recolt:updateJobs', function(newjob)
    Citizen.CreateThread(function()
        jobId = newjob
        for k, existingBlip in ipairs(BLIPS) do
            RemoveBlip(existingBlip)
            BLIPS = {}
        end
        for k, existingBlip in ipairs(BLIPSZONE) do
            RemoveBlip(existingBlip)
            BLIPSZONE = {}
        end
        isInServiceTaxi = false
        if jobId == taxijob then
            -- Show blip
                for _, item in pairs(emplacement) do
                    blip = AddBlipForCoord(item.x, item.y, item.z)
                    SetBlipSprite(blip, tonumber(item.id))
                    SetBlipAsShortRange(blip, true)
                    BeginTextCommandSetBlipName("STRING")
                    AddTextComponentString(item.name)
                    EndTextCommandSetBlipName(blip)
                    table.insert(BLIPS,blip)
                end
                BoucleJob()
        end
    
     end)
end)
---- THREADS ----

-- Service
function BoucleJob()
    Citizen.CreateThread(function()
        VMenu.AddMenu( 'Raffineur', 'interim',248,157,45)    
        local x = -57.1651
        local y = -2520.59
        local z = 7.40117
        while jobId == taxijob do
            Citizen.Wait(1)
            x = -57.1651
            y = -2520.59
            z = 7.40117
            local playerPos = GetEntityCoords(GetPlayerPed(-1), true)
            if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 100.0) then
                Marker(x, y, z - 1,1.5, 0, 0, 0)
                if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 2.0) then
                    if isInServiceTaxi then
                        DisplayHelpText('Appuyez sur ~INPUT_CONTEXT~ pour ~r~stopper~s~ votre service') 
                    else
                        DisplayHelpText('Appuyez sur ~INPUT_CONTEXT~ pour ~g~prendre~s~ votre service')
                    end
                    if (IsControlJustReleased(1, 51)) then 
                        GetService()
                    end
                end
            end
            x = -63.2153
		    y = -2528.12
		    z = 6.01
            if isInServiceTaxi then
                if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 100.0) then
                Marker(x, y, z - 1,3.0, 0, 0, 0)
                    if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 4.0) then
                        local ply = GetPlayerPed(-1)
                        if IsPedInAnyVehicle(ply, true) then
                            DisplayHelpText('Appuyez sur ~INPUT_CONTEXT~ pour ~r~ranger~s~ votre ~b~camion')
                            if (IsControlJustReleased(1, 51)) then 
                                local vehicle = GetVehiclePedIsIn(ply, true)
                                local isVehicleTaxi = IsVehicleModel(vehicle, taximodel)
                                local isTaxiPlate = GetVehicleNumberPlateText(vehicle)
                                if isVehicleTaxi then
                                    DeleteTaxi()
                                    caution = false
                                else
                                    TriggerEvent('hud:NotifColor',"Ce n'est pas un camion de la raffinerie !",6)
                                end
                            end
                        else						
                            DisplayHelpText('Appuyez sur ~INPUT_CONTEXT~ pour ~b~sortir~s~ un ~b~camion')
                            if (IsControlJustReleased(1, 51)) then 
                                local caisse = GetClosestVehicle(-63.2153, -2528.12, 6.01, 3.000, 0, 70)
                                SetEntityAsMissionEntity(caisse, true, true)
                                Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(caisse))
                                Taxi()
                                caution = true
                                --TriggerServerEvent("taxi:cautionOn", cautionprice)
                                --Notify("Vous avez laisser ~g~"..cautionprice.."$~s~ de caution pour le ~b~taxi")
                            end
                        end
                    end
                end
                x = -63.2153
                y = -2528.12
                z = 6.01
                if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 100.0) then
                Marker(x, y, z - 1,3.0, 0, 0, 0)
                    if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 4.0) then
                        local ply = GetPlayerPed(-1)
                        if IsPedInAnyVehicle(ply, true) then
                            DisplayHelpText('Appuyez sur ~INPUT_CONTEXT~ pour ~r~ranger~s~ votre ~b~camion')
                            if (IsControlJustReleased(1, 51)) then 
                                local vehicle = GetVehiclePedIsIn(ply, true)
                                local isVehicleTaxi = IsVehicleModel(vehicle, taximodel)
                                local isTaxiPlate = GetVehicleNumberPlateText(vehicle)
                                if isVehicleTaxi then
                                    DeleteTaxi()
                                    caution = false
                                else
                                    TriggerEvent('hud:NotifColor',"Ce n'est pas un camion de la raffinerie !",6)
                                end
                            end
                        else						
                            DisplayHelpText('Appuyez sur ~INPUT_CONTEXT~ pour ~b~sortir~s~ un ~b~camion')
                            if (IsControlJustReleased(1, 51)) then 
                                local caisse = GetClosestVehicle(-63.2153, -2528.12, 6.01, 3.000, 0, 70)
                                SetEntityAsMissionEntity(caisse, true, true)
                                Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(caisse))
                                Taxi()
                                caution = true
                                --TriggerServerEvent("taxi:cautionOn", cautionprice)
                                --Notify("Vous avez laisser ~g~"..cautionprice.."$~s~ de caution pour le ~b~taxi")
                            end
                        end
                    end
                end
                if IsControlJustPressed(1, openMenuKey) then 
                    if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                    VMenu.curItem = 1
                    GPSMenu()      
                    VMenu.visible = not VMenu.visible
                end
                VMenu.Show()
		    end
		end
    end)
end


---- FONCTIONS ----

function Notify(text)
	SetNotificationTextEntry('STRING')
	AddTextComponentString(text)
	DrawNotification(false, false)
end

function DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

---------------------------

function Taxi()
	Citizen.Wait(0)
	local ped = GetPlayerPed(-1)
	local player = PlayerId()
	local vehicle = taximodel

	RequestModel(vehicle)

	while not HasModelLoaded(vehicle) do
		Wait(1)
	end

	--local plate = math.random(300, 900)
	local spawned_car = CreateVehicle(vehicle, -63.2153, -2528.12, 6.01, 55.0, true, false)
	SetVehicleOnGroundProperly(spawned_car)
    Remorque()
	SetPedIntoVehicle(ped, spawned_car, - 1)
        local id = NetworkGetNetworkIdFromEntity(spawned_car)
                SetNetworkIdCanMigrate(id, true)
                SetNetworkIdExistsOnAllMachines(id,true)
end

function getVehicleInDirection(coordFrom, coordTo)
	local rayHandle = CastRayPointToPoint(coordFrom.x, coordFrom.y, coordFrom.z, coordTo.x, coordTo.y, coordTo.z, 10, GetPlayerPed(-1), 0)
	local a, b, c, d, vehicle = GetRaycastResult(rayHandle)
	return vehicle
end

function Remorque()
    local carid = GetHashKey("Tanker")
                RequestModel(carid)
                while not HasModelLoaded(carid) do
                    Citizen.Wait(0)
                end
                local playerped = GetPlayerPed(-1)
                SetEntityCoords(GetPlayerPed(-1), -95.341552734375,  -2461.541015625, 6.0203547477722, 1, 0, 0, 1)
                SetEntityHeading(GetPlayerPed(-1), 334.8069152832)
                local coordA = GetEntityCoords(playerped, 1)
                local coordB = GetOffsetFromEntityInWorldCoords(playerped, 0.0, 5.0, 0.0)
                local targetVehicle = getVehicleInDirection(coordA, coordB)
                if targetVehicle ~= 0 then
                    SetEntityAsMissionEntity(targetVehicle, true, true)
                    Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(targetVehicle))
                end
                lastvehpomp2 = CreateVehicle(carid, -97.623,-2457.68, 6.01931, 55.0, true, false)
                Notify("Allez chercher votre ~b~remorque~w~.")
                x = -97.623
                y = -2457.68
                z = 6.01931
                BLIPP = AddBlipForCoord(x, y, z)
                SetBlipSprite(BLIPP, 2)
                SetNewWaypoint(x, y)
end

function DeleteTaxi()
    local ply = GetPlayerPed(-1)
    local playerVeh = GetVehiclePedIsIn(ply, false)
    Citizen.Wait(1)
    ClearPedTasksImmediately(ply)
    SetEntityVisible(playerVeh, false, 0)
    SetEntityCoords(playerVeh, 999999.0, 999999.0, 999999.0, true, true, true, true)
    FreezeEntityPosition(playerVeh, true)
    SetEntityAsMissionEntity(playerVeh, 1, 1)
    DeleteVehicle(playerVeh)
end

-------
---------


function GPSMenu() -- FACTURE MENU
    VMenu.ResetMenu()
    VMenu.EditHeader('GPS')
	VMenu.AddFunc("Forage de pétrole", "goC", nil)
	VMenu.AddFunc("Raffinerie de pétrole", "goPO", nil)
    VMenu.AddFunc("Dépot de pétrole", "goPF", nil)
	VMenu.AddFunc("Fleeca Banque", "goFB", nil)	
end


--------------- GPS COORDS
function goC(x, y, z)
x = 654.734
y = 3009.87
z = 43.2908
BLIPP = AddBlipForCoord(x, y, z)
SetBlipSprite(BLIPP, 2)
SetNewWaypoint(x, y)
end
function goPO(x, y, z)
x = 1734.4
y = -1669.73
z = 112.582
BLIPP = AddBlipForCoord(x, y, z)
SetBlipSprite(BLIPP, 2)
SetNewWaypoint(x, y)
end
function goPF(x, y, z)
x = 497.338
y = -2172.37
z = 5.91827
BLIPP = AddBlipForCoord(x, y, z)
SetBlipSprite(BLIPP, 2)
SetNewWaypoint(x, y)
end
function goFB(x, y, z)
x = 152.32469177246
y = -1030.0135498047
z = 29.185220718384
BLIPP = AddBlipForCoord(x, y, z)
SetBlipSprite(BLIPP, 2)
SetNewWaypoint(x, y)
end
--------------------------


function GetService()
    Citizen.CreateThread(function()
    if jobId ~= taxijob then
     TriggerEvent('hud:NotifColor',"Tu n'est pas mineur !",6) 
            return
    end
	if isInServiceTaxi then
		Notify("Vous avez ~r~fini~s~ votre service") 
		if (useModelMenu == true) then
		TriggerServerEvent("mm:spawn2") 
		end
		TriggerServerEvent('taxi:sv_setService', 0) 
        for k, existingBlip in ipairs(BLIPSZONE) do
                RemoveBlip(existingBlip)
                BLIPSZONE = {}
        end
	else 
		TriggerEvent('hud:NotifColor',"Vous êtes en service !",141) 
		Notify("Pensez à prendre votre véhicule !") 
		TriggerServerEvent('taxi:sv_setService', 1) 
            for k, existingBlip in ipairs(BLIPSZONE) do
                RemoveBlip(existingBlip)
                BLIPSZONE = {}
            end
            -- Show blip
            for _, item in pairs(zonerecolt) do
                blip = AddBlipForCoord(item.x, item.y, item.z)
                SetBlipSprite(blip, tonumber(item.id))
                SetBlipAsShortRange(blip, true)
                BeginTextCommandSetBlipName("STRING")
                AddTextComponentString(item.name)
                EndTextCommandSetBlipName(blip)
                table.insert(BLIPSZONE,blip)
            end
	end
	
	isInServiceTaxi = not isInServiceTaxi
-- Here for any clothes with SetPedComponentVariation ...
    SetPedPropIndex(GetPlayerPed(-1), 0, 0, 0, 0)--ear defenders
    if GetEntityModel(GetPlayerPed(-1)) == GetHashKey("mp_f_freemode_01") then
        SetPedComponentVariation(GetPlayerPed(-1), 8, 36, 0, 0)--Gilet Jaune Femme
        SetPedComponentVariation(GetPlayerPed(-1), 6, 63, 0, 0)--Chaussure
    else
        SetPedComponentVariation(GetPlayerPed(-1), 8, 59, 0, 0)--Gilet Jaune Homme
    SetPedComponentVariation(GetPlayerPed(-1), 6, 60, 0, 0)--Chaussure
    end
    end)
end


--------------

-- Copy/paste from fs_freeroam (by FiveM-Script : https://forum.fivem.net/t/alpha-fs-freeroam-0-1-4-fivem-scripts/14097)
