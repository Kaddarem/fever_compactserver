-- Manifest

-- Requiring essentialmode
dependency 'essentialmode'

client_script {
    '@hud-event/hudevent_aff.lua',
    '@appmenu/menu_cl.lua',
    'raffineur_client.lua',
}

