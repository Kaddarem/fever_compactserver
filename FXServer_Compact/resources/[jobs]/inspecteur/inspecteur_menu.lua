--[[
##################
#    Oskarr      #
#    MysticRP    #
#   client.lua   #
#      2017      #
##################
--]]
local jobId=-1
local taxijob = 24 -- JobID for taxi
local openMenuKey = 166 -- (G) Key for OPEN TAXI MENU 
local garage = { x = -806.189, y = 162.767, z = 71.542, a = 110.0 }
local porte = {name="Garage Inspecteur", id=473, x = -811.598, y = 167.539, z = 71.2279, a = 110.0 }
local garageautorisation = {24,25}
local afficher = false
local taxiplatee = "SHERLOCK" -- Plate for service vehicle
---- FONCTIONS ----
local BLIPS = {}
RegisterNetEvent('recolt:updateJobs')
AddEventHandler('recolt:updateJobs', function(newjob)
    Citizen.CreateThread(function()
        jobId = newjob
        for k, existingBlip in ipairs(BLIPS) do
            RemoveBlip(existingBlip)
            BLIPS = {}
        end
        afficher = false
        for k, v in pairs(garageautorisation) do
            if jobId == v then
                afficher = true
                blip = AddBlipForCoord(porte.x, porte.y, porte.z)
                SetBlipSprite(blip, tonumber(porte.id))
                SetBlipAsShortRange(blip, true)
                BeginTextCommandSetBlipName("STRING")
                AddTextComponentString(porte.name)
                EndTextCommandSetBlipName(blip)
                table.insert(BLIPS,blip)
                BoucleTakeService()
                break
            end
        end
     end)
end)

function Notify(text)
	SetNotificationTextEntry('STRING')
	AddTextComponentString(text)
	DrawNotification(false, false)
end

function DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

---------------------------
function MenuFonction() -- FACTURE MENU
    VMenu.ResetMenu()
    VMenu.EditHeader('Recrutement')
    VMenu.AddFunc("Embaucher Inspecteur", "recruterinspec", nil)
    VMenu.AddFunc("Embaucher Stagiaire", "recruterstage", nil)
    VMenu.AddFunc("Licencier", "licencier", nil)
end


function recruterinspec()
    DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8S", "", "", "", "", "", 20)
    while (UpdateOnscreenKeyboard() == 0) do
	    DisableAllControlActions(0);
		Wait(0);
	end
	if (GetOnscreenKeyboardResult()) then
	    local res = tonumber(GetOnscreenKeyboardResult())
		TriggerServerEvent('commande_metier:recruter',res,24)
    end
end

function recruterstage()
    DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8S", "", "", "", "", "", 20)
    while (UpdateOnscreenKeyboard() == 0) do
	    DisableAllControlActions(0);
		Wait(0);
	end
	if (GetOnscreenKeyboardResult()) then
	    local res = tonumber(GetOnscreenKeyboardResult())
		TriggerServerEvent('commande_metier:recruter',res,25)
    end
end

function licencier()
    DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8S", "", "", "", "", "", 20)
    while (UpdateOnscreenKeyboard() == 0) do
	    DisableAllControlActions(0);
		Wait(0);
	end
	if (GetOnscreenKeyboardResult()) then
	    local res = tonumber(GetOnscreenKeyboardResult())
		TriggerServerEvent('commande_metier:licencier',res)
    end
end


--------------

-- Copy/paste from fs_freeroam (by FiveM-Script : https://forum.fivem.net/t/alpha-fs-freeroam-0-1-4-fivem-scripts/14097)
--[[
##################
#    Oskarr      #
#    MysticRP    #
#   client.lua   #
#      2017      #
##################
--]]
---- FONCTIONS ----




---------------------------
function MenuGarage() -- FACTURE MENU
    VMenu.ResetMenu()
    VMenu.EditHeader('Garage')
    VMenu.AddFunc("Oracle", "sortir", "oracle2","Sortir")
	VMenu.AddFunc("Baller", "sortir", "baller3","Sortir")
    VMenu.AddFunc("Rentrer le véhicule", "rentrer", nil,"Sortir")
end

function sortir(name)
    Citizen.Wait(100)
    local caisse = GetClosestVehicle(garage.x, garage.y, garage.z, 15.000, 0, 70)	
	SetEntityAsMissionEntity(caisse, true, true)	
    Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(caisse))
	Citizen.Wait(0)
	local myPed = GetPlayerPed(-1)
	local player = PlayerId()
	local vehicle = GetHashKey(name)
	RequestModel(vehicle)
	while not HasModelLoaded(vehicle) do
		Wait(1)
	end
	local plate = taxiplatee
	local spawned_car = CreateVehicle(vehicle, garage.x, garage.y, garage.z, garage.a, true, false)
	SetVehicleOnGroundProperly(spawned_car)
    SetVehicleHasBeenOwnedByPlayer(spawned_car,true)
    SetVehicleDirtLevel(spawned_car)
        local id = NetworkGetNetworkIdFromEntity(spawned_car)
                SetNetworkIdCanMigrate(id, true)
                SetNetworkIdExistsOnAllMachines(id,true)
end

function rentrer()
    Citizen.Wait(100)
    local caisse = GetClosestVehicle(garage.x, garage.y, garage.z, 15.000, 0, 70)	
	SetEntityAsMissionEntity(caisse, true, true)	
    Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(caisse))
end

local function TestBonJob()
    for _,job in pairs(garageautorisation) do
        if jobId == job then
            return true
        end
    end
    return false
end

local incircle = false
---- MENU OPEN -----
function BoucleTakeService()
    Citizen.CreateThread(function()
        VMenu.AddMenu( 'Inspecteur', 'inspecteur',255,255,255)
        while TestBonJob() do
            Citizen.Wait(0)
            if IsControlJustPressed(1, openMenuKey) then 
                if jobId == taxijob then
                    if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                    VMenu.curItem = 1
                    MenuFonction()
                    VMenu.visible = not VMenu.visible
                end
            end
            if afficher then
                local pos = GetEntityCoords(GetPlayerPed(-1), true)
                    if(Vdist(pos.x, pos.y, pos.z, porte.x, porte.y, porte.z) < 15.0)then
                        DrawMarker(1, porte.x, porte.y, porte.z , 0, 0, 0, 0, 0, 0, 1.0001, 1.0001, 0.5001, 1555, 0, 0,165, 0, 0, 0,0)
                        if(Vdist(pos.x, pos.y, pos.z, porte.x, porte.y, porte.z) < 2.0)then
                            if (incircle == false) then
                                DisplayHelpText("Appuyer sur ~INPUT_CONTEXT~ pour ouvrir le garage.")
                            end
                            incircle = true
                            if IsControlJustReleased(1, 51)then -- INPUT_CELLPHONE_DOWN
                                if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                                MenuGarage() -- Menu to draw
                                VMenu.curItem = 1
                                VMenu.visible = not VMenu.visible
                                incircle = true
                            end
                        end
                    end
            end
            VMenu.Show()
        end
    end)
end