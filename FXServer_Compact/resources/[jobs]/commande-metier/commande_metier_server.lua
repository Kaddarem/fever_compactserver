----------------
----- /job -----
----------------
TriggerEvent('es:addGroupCommand', 'job', "admin", function(source, args, user)
    TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "/removechef pour mettre au chomage un chef")
    TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "/chefdep pour promouvoir chef des dépanneurs")
    TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "/chefamb pour promouvoir chef des ambulanciers")
    TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "/cheftaxi pour promouvoir chef des taxis")
    TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "/president pour promouvoir président")
    TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "/cheftequi pour promouvoir chef Tequi-la-la")
    TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "/cheftredac pour promouvoir chef Journaliste")
    TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "/chefinsp pour promouvoir chef Journaliste")
    TriggerClientEvent("chatMessage", source, "^3SYSTEM", {255, 255, 255}, "/chomage pour mettre au chomage")
end, function(source, args, user) 
        TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Vous n'avez pas la permission de faire ça !")
end)
-----------------------
----- Taxi -----
-----------------------

TriggerEvent('es:addGroupCommand', 'cheftaxi', "admin", function(source, args, user)
	if(not args[2]) then
		TriggerClientEvent('chatMessage', "Gouvernement", {255, 0, 0}, "Utilisation : /cheftaxi [ID]")	
	else
		if(GetPlayerName(tonumber(args[2])) ~= nil)then
	        local player = tonumber(args[2])
            TriggerClientEvent('commande_metier:updatejob',player,14)
			TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Compris !")
        else
			TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Aucun joueur avec cet ID !")
		end
	end
end, function(source, args, user) 
		TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Vous n'avez pas la permission de faire ça !")
end)

-----------------------
----- Journal -----
-----------------------

TriggerEvent('es:addGroupCommand', 'chefredac', "admin", function(source, args, user)
	if(not args[2]) then
		TriggerClientEvent('chatMessage', "Gouvernement", {255, 0, 0}, "Utilisation : /chefredac [ID]")	
	else
		if(GetPlayerName(tonumber(args[2])) ~= nil)then
	        local player = tonumber(args[2])
            TriggerClientEvent('commande_metier:updatejob',player,21)
			TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Compris !")
        else
			TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Aucun joueur avec cet ID !")
		end
	end
end, function(source, args, user) 
		TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Vous n'avez pas la permission de faire ça !")
end)
-----------------------
----- inspecteur -----
-----------------------

TriggerEvent('es:addGroupCommand', 'chefinsp', "admin", function(source, args, user)
	if(not args[2]) then
		TriggerClientEvent('chatMessage', "Gouvernement", {255, 0, 0}, "Utilisation : /chefinsp [ID]")	
	else
		if(GetPlayerName(tonumber(args[2])) ~= nil)then
	        local player = tonumber(args[2])
            TriggerClientEvent('commande_metier:updatejob',player,24)
			TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Compris !")
        else
			TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Aucun joueur avec cet ID !")
		end
	end
end, function(source, args, user) 
		TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Vous n'avez pas la permission de faire ça !")
end)
-----------------------
----- Ambulancier -----
-----------------------

TriggerEvent('es:addGroupCommand', 'chefamb', "admin", function(source, args, user)
	if(not args[2]) then
		TriggerClientEvent('chatMessage', "Gouvernement", {255, 0, 0}, "Utilisation : /chafamb [ID]")	
	else
		if(GetPlayerName(tonumber(args[2])) ~= nil)then
	        local player = tonumber(args[2])
            TriggerClientEvent('commande_metier:updatejob',player,12)
			TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Compris !")
        else
			TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Aucun joueur avec cet ID !")
		end
	end
end, function(source, args, user) 
		TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Vous n'avez pas la permission de faire ça !")
end)
---------------------
----- Dépanneur -----
---------------------

TriggerEvent('es:addGroupCommand', 'chefdep', "admin", function(source, args, user)
	if(not args[2]) then
		TriggerClientEvent('chatMessage', "Gouvernement", {255, 0, 0}, "Utilisation : /chefdep [ID]")	
	else
		if(GetPlayerName(tonumber(args[2])) ~= nil)then
	        local player = tonumber(args[2])
            TriggerClientEvent('commande_metier:updatejob',player,13)
			TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Compris !")
        else
			TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Aucun joueur avec cet ID !")
		end
	end
end, function(source, args, user) 
		TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Vous n'avez pas la permission de faire ça !")
end)

---------------------
----- Président -----
---------------------
TriggerEvent('es:addGroupCommand', 'president', "admin", function(source, args, user)
	if(not args[2]) then
		TriggerClientEvent('chatMessage', "Gouvernement", {255, 0, 0}, "Utilisation : /president [ID]")	
	else
		if(GetPlayerName(tonumber(args[2])) ~= nil)then
	        local player = tonumber(args[2])
            TriggerClientEvent('commande_metier:updatejob',player,16)
			TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Compris !")
        else
			TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Aucun joueur avec cet ID !")
		end
	end
end, function(source, args, user) 
		TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Vous n'avez pas la permission de faire ça !")
end)

---------------------
----- Tequilala -----
---------------------
TriggerEvent('es:addGroupCommand', 'cheftequi', "admin", function(source, args, user)
	if(not args[2]) then
		TriggerClientEvent('chatMessage', "Gouvernement", {255, 0, 0}, "Utilisation : /cheftequi [ID]")	
	else
		if(GetPlayerName(tonumber(args[2])) ~= nil)then
	        local player = tonumber(args[2])
            TriggerClientEvent('commande_metier:updatejob',player,19)
			TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Compris !")
        else
			TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Aucun joueur avec cet ID !")
		end
	end
end, function(source, args, user) 
		TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Vous n'avez pas la permission de faire ça !")
end)

--------------------------
----- Supprimer chef -----
--------------------------
TriggerEvent('es:addGroupCommand', 'removechef', "admin", function(source, args, user) 
    if(not args[2]) then
		TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0},"Utilisation : /removechef [ID]")	
	else
		if(GetPlayerName(tonumber(args[2])) ~= nil)then
            local player = tonumber(args[2])
            TriggerClientEvent('commande_metier:updatejob',player,1)
            TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Compris !")
		else
		TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0},  "Aucun joueur avec cet ID !")
		end
	end
end, function(source, args, user) 
		TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Vous n'avez pas la permission de faire ça !")
end)

-----------------------------
----- Mettre au chomage -----
-----------------------------
TriggerEvent('es:addGroupCommand', 'chomage', "admin", function(source, args, user) 
    if(not args[2]) then
		TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0},"Utilisation : /chomage [ID]")	
	else
		if(GetPlayerName(tonumber(args[2])) ~= nil)then
            local player = tonumber(args[2])
            TriggerClientEvent('commande_metier:updatejob',player,1)
            TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Compris !")
		else
		TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0},  "Aucun joueur avec cet ID !")
		end
	end
end, function(source, args, user) 
		TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Vous n'avez pas la permission de faire ça !")
end)

--------------------------
----- Ajout via menu -----
--------------------------
RegisterServerEvent('commande_metier:recruter')
AddEventHandler('commande_metier:recruter', function (id,job)
    if(GetPlayerName(id) ~= nil)then
        local player = tonumber(id)
        TriggerClientEvent('commande_metier:updatejob',player,job)
        TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Compris !")
    else
		TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0},  "Aucun joueur avec cet ID !")
    end
end)
------------------------------
----- Licencier via menu -----
------------------------------
RegisterServerEvent('commande_metier:licencier')
AddEventHandler('commande_metier:licencier', function (id)
    if(GetPlayerName(id) ~= nil)then
        local player = tonumber(id)
        TriggerClientEvent('commande_metier:updatejob',player,1)
        TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0}, "Compris !")
    else
		TriggerClientEvent('chatMessage', source, "Gouvernement", {255, 0, 0},  "Aucun joueur avec cet ID !")
    end
end)

-- get's the player id without having to use bugged essentials
function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

-- gets the actual player id unique to the player,
-- independent of whether the player changes their screen name
function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end
