dependency 'essentialmode'
client_script{
    '@hud-event/hudevent_aff.lua',
    '@appmenu/menu_cl.lua',
    'pres_client.lua',
	'pres_menu.lua',
    'teleportation_client.lua',
}
server_script '@mysql-async/lib/MySQL.lua'
server_script 'pres_server.lua'
