--[[
##################
#    Oskarr      #
#    MysticRP    #
#   client.lua   #
#      2017      #
##################
--]]
local jobId=-1
local taxijob = 16 -- JobID for taxi
local openMenuKey = 166 -- (G) Key for OPEN TAXI MENU 
local garage = { x = -407.06958007813, y = 1062.8358154297, z = 322.8410949707, a = 314.67330932617 }
local porte = { x = -420.50210571289, y = 1065.3321533203, z = 322.84097290039, a = 344.87048339844 }
local garageautorisation = {9,16,17,18}
local afficher = false
local taxiplatee = "  GOUV  " -- Plate for service vehicle
---- FONCTIONS ----

RegisterNetEvent('recolt:updateJobs')
AddEventHandler('recolt:updateJobs', function(newjob)
    Citizen.CreateThread(function()
        jobId = newjob
        afficher = false
        Wait(5)
        for k, v in pairs(garageautorisation) do
            if jobId == v then
                afficher = true
                BoucleJob()
                break
            end
        end
     end)
end)

function Notify(text)
	SetNotificationTextEntry('STRING')
	AddTextComponentString(text)
	DrawNotification(false, false)
end

function DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

---------------------------
function MenuFonction() -- FACTURE MENU
    VMenu.ResetMenu()
    VMenu.EditHeader("Recrutement")
	VMenu.AddFunc("Embaucher Gouvernement", "recrutergouv", nil)
	VMenu.AddFunc("Embaucher Procureur", "recruterproc", nil)
    VMenu.AddFunc("Embaucher Agent", "recruteragent", nil)
    VMenu.AddFunc("Licencier", "licencier", nil)
end


function recrutergouv()
    DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8S", "", "", "", "", "", 20)
    while (UpdateOnscreenKeyboard() == 0) do
	    DisableAllControlActions(0);
		Wait(0);
	end
	if (GetOnscreenKeyboardResult()) then
	    local res = tonumber(GetOnscreenKeyboardResult())
		TriggerServerEvent('commande_metier:recruter',res,17)
    end
end

function recruterproc()
    DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8S", "", "", "", "", "", 20)
    while (UpdateOnscreenKeyboard() == 0) do
	    DisableAllControlActions(0);
		Wait(0);
	end
	if (GetOnscreenKeyboardResult()) then
	    local res = tonumber(GetOnscreenKeyboardResult())
		TriggerServerEvent('commande_metier:recruter',res,9)
    end
end

function recruteragent()
    DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8S", "", "", "", "", "", 20)
    while (UpdateOnscreenKeyboard() == 0) do
	    DisableAllControlActions(0);
		Wait(0);
	end
	if (GetOnscreenKeyboardResult()) then
	    local res = tonumber(GetOnscreenKeyboardResult())
		TriggerServerEvent('commande_metier:recruter',res,18)
    end
end

function licencier()
    DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8S", "", "", "", "", "", 20)
    while (UpdateOnscreenKeyboard() == 0) do
	    DisableAllControlActions(0);
		Wait(0);
	end
	if (GetOnscreenKeyboardResult()) then
	    local res = tonumber(GetOnscreenKeyboardResult())
		TriggerServerEvent('commande_metier:licencier',res)
    end
end


--------------

-- Copy/paste from fs_freeroam (by FiveM-Script : https://forum.fivem.net/t/alpha-fs-freeroam-0-1-4-fivem-scripts/14097)
--[[
##################
#    Oskarr      #
#    MysticRP    #
#   client.lua   #
#      2017      #
##################
--]]
---- FONCTIONS ----




---------------------------
function MenuGarage() -- FACTURE MENU
    VMenu.ResetMenu()
    VMenu.EditHeader("Garage")
	--VMenu.AddFunc(17,"Cheetah", "sortir", "cheetah")
	VMenu.AddFunc("Baller Blindé", "sortir", "baller6")
    VMenu.AddFunc("Schafter Blindé", "sortir", "schafter6")
    VMenu.AddFunc("Kuruma Blindé", "sortir", "kuruma2")
    --VMenu.AddFunc("T20", "sortir", "t20")
    --VMenu.AddFunc("Itali GTB", "sortir", "italigtb2")
    --VMenu.AddFunc("JB 700", "sortir", "jb700")
    VMenu.AddFunc("Rentrer le véhicule", "rentrer", nil)
end

function sortir(name)
    Citizen.Wait(100)
    local caisse = GetClosestVehicle(garage.x, garage.y, garage.z, 15.000, 0, 70)	
	SetEntityAsMissionEntity(caisse, true, true)	
    Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(caisse))
	Citizen.Wait(0)
	local myPed = GetPlayerPed(-1)
	local player = PlayerId()
	local vehicle = GetHashKey(name)
	RequestModel(vehicle)
	while not HasModelLoaded(vehicle) do
		Wait(1)
	end
	local plate = taxiplatee
	local spawned_car = CreateVehicle(vehicle, garage.x, garage.y, garage.z, garage.a, true, false)
	SetVehicleOnGroundProperly(spawned_car)
    SetVehicleHasBeenOwnedByPlayer(spawned_car,true)
    SetVehicleDirtLevel(spawned_car)
        local id = NetworkGetNetworkIdFromEntity(spawned_car)
                SetNetworkIdCanMigrate(id, true)
                SetNetworkIdExistsOnAllMachines(id,true)
end

function rentrer()
    Citizen.Wait(100)
    local caisse = GetClosestVehicle(garage.x, garage.y, garage.z, 15.000, 0, 70)	
	SetEntityAsMissionEntity(caisse, true, true)	
    Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(caisse))
end

local incircle = false
---- MENU OPEN -----
function BoucleJob()
    Citizen.CreateThread(function()
        VMenu.AddMenu('President', 'president',251,247,237)   
        while afficher do
            Citizen.Wait(0)
            if IsControlJustPressed(1, openMenuKey) and jobId == taxijob then 
                if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                VMenu.curItem = 1
                MenuFonction()
                VMenu.visible = not VMenu.visible
            end
            if afficher then
                local pos = GetEntityCoords(GetPlayerPed(-1), true)
                    if(Vdist(pos.x, pos.y, pos.z, porte.x, porte.y, porte.z) < 15.0)then
                    Marker(porte.x, porte.y, porte.z,1.5, 0, 0, 0)
                        if(Vdist(pos.x, pos.y, pos.z, porte.x, porte.y, porte.z) < 2.0)then
                            if (incircle == false) then
                                DisplayHelpText("Appuyer sur ~INPUT_CONTEXT~ pour ouvrir le garage.")
                            end
                            incircle = true
                            if IsControlJustReleased(1, 51)then -- INPUT_CELLPHONE_DOWN
                                if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                                VMenu.curItem = 1
                                MenuGarage() -- Menu to draw
                                VMenu.visible = not VMenu.visible
                            incircle = true
                        end
                    end
                end
            end
            VMenu.Show() -- Draw menu on each tick if Menu.hidden = false
        end
    end)
end