local periodeElection = false

disableKeys = { 32,33,34,35 }

function Notify(text)
    SetNotificationTextEntry('STRING')
    AddTextComponentString(text)
    DrawNotification(false, false)
end

function drawTxt(x,y ,width,height,scale, text, r,g,b,a, outline)
    SetTextFont(0)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    if(outline)then
        SetTextOutline()
    end
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width/2, y - height/2 + 0.005)
end

function DisplayHelpText(str)
    SetTextComponentFormat("STRING")
    AddTextComponentString(str)
    DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Customisation Menu
--Hair
--Male
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
local candidats = {
    "Jacques Boulon",
    "Sergeui Kovalevski",
    "Al Nour Sahak"
}
function Main()
    VMenu.ResetMenu()
    VMenu.EditHeader("choisis ton candidat")
    for int, nom in pairs (candidats) do
        VMenu.AddFunc(nom, "vote", int,"Voter")
    end
end
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Customisation Menu
--Hair
--Colour
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function vote(int)
    TriggerServerEvent('pres:vote',candidats,int)
    Main()
end
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Press E to open/close menu in the red marker
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

local emplacement = {
    {name="Bureau de vote", id=419, x = -266.37240600586, y = -2035.5620117188, z = 30.145580291748, a = 218.38635253906},
}
incircle = false

if periodeElection then
    Citizen.CreateThread(function()
    VMenu.AddMenu(16, 'Election', 'election',154,0,10)   
        
        for _, item in pairs(emplacement) do
          item.blip = AddBlipForCoord(item.x, item.y, item.z)
          SetBlipSprite(item.blip, item.id)
          SetBlipColour(item.blip, item.colour)
          SetBlipAsShortRange(item.blip, true)
          BeginTextCommandSetBlipName("STRING")
          AddTextComponentString(item.name)
          EndTextCommandSetBlipName(item.blip)
        end
        
        local BoucleLent = 5000
        local BoucleCourt = 5
        local TempsBoucle = BoucleCourt
        while true do
            Citizen.Wait(TempsBoucle)
            local pos = GetEntityCoords(GetPlayerPed(-1), true)
            for k,v in ipairs(emplacement) do
                if(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) < 50.0)then
                    TempsBoucle = BoucleCourt
                    Marker(v.x, v.y, v.z - 1,1.5,155, 0, 0)
                    if(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) < 1.0)then
                        if (incircle == false) then
                            DisplayHelpText("Appuyer sur ~INPUT_CONTEXT~ pour voter.")
                        end
                        incircle = true
                        if IsControlJustReleased(1, 51)then -- INPUT_CELLPHONE_DOWN
                            if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                            VMenu.curItem = 1
                            Main() -- Menu to draw
                            VMenu.visible = not VMenu.visible
                            incircle = true
                        end
                        VMenu.Show()
                    elseif(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) > 3.0)then
                        incircle = false
                        VMenu.visible = false
                    end
                else
                    TempsBoucle = BoucleLent  
                end
            end
        end
    end)
end
