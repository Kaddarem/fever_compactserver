----------------
RegisterServerEvent('pres:vote')
AddEventHandler('pres:vote', function(candidats,vote)
    local identifier = getPlayerID(source)
    local source = source
	local result = MySQL.Sync.fetchScalar("SELECT vote FROM presidentielle WHERE identifier = @name", {['@name'] = identifier})
    if result ~= nil then
            TriggerClientEvent("es_freeroam:notify", source, "CHAR_ANDREAS", 1, "Gouvernement", false, "Vous avez déjà voté pour ~b~"..candidats[result]..".")
    else
        MySQL.Async.insert("INSERT INTO `presidentielle` (`identifier`, `vote`) VALUES ( @name, @vote)", {['@name'] = identifier,['@vote']=vote})
        TriggerClientEvent("es_freeroam:notify", source, "CHAR_ANDREAS", 1, "Gouvernement", false, "Vous avez voté pour ~b~"..candidats[vote]..".")
        TriggerClientEvent('hud:Notif', source, "La ville vous remercie.")
    end
end)
-- get's the player id without having to use bugged essentials
function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

-- gets the actual player id unique to the player,
-- independent of whether the player changes their screen name
function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end
