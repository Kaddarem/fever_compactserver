local maxSpeed = 0
-- local minSpeed = 0
local info = ""
local isRadarPlaced = false -- bolean to get radar status
local Radar -- entity object
local RadarBlip -- blip
local RadarPos = {} -- pos
local RadarAng = 0 -- angle
local LastPlate = ""
local LastVehDesc = ""
local LastSpeed = 0
local LastInfo = ""
local canManageCop = false

-- ALTER TABLE `entreprise`	ADD COLUMN `templateFacture` VARCHAR(50) NULL DEFAULT 'facture' AFTER `offcommission`;
-- INSERT INTO `entreprise` (`id`, `nom`, `proprietaire`, `pourcentage`, `caisse`, `ca`, `lieu`, `transaction`, `blip`, `jobList`, `job_pdg`, `entrepot`, `capacite`, `offcapacite`, `offspeed`, `offcommission`, `templateFacture`) VALUES (11, 'LSPD', '0', 0, 500, 500, '0,0,0,', 0, -1, '2,34', '34,', '441.8403, -978.8469,  29.6896,', 100000, 0, 0, 0, 'Amende');
-- INSERT INTO `jobs` (`job_id`, `job_name`, `salary`) VALUES (34, 'Officier de Police', 1800);

-----------------------------------------------------------------------------------------------------------------
----------------------------------------------------POLICE MENU--------------------------------------------------
-----------------------------------------------------------------------------------------------------------------

function Main()
    VMenu.curItem = 1
    VMenu.ResetMenu()
    VMenu.EditHeader('Action')
    VMenu.AddFunc(txt[config.lang]["menu_animations_title"], "animations", nil)
    VMenu.AddFunc(txt[config.lang]["menu_citizens_title"], "citizens", nil)
    VMenu.AddFunc(txt[config.lang]["menu_vehicles_title"], "vehicles", nil)
    if canManageCop then
        VMenu.AddFunc("Gestion des policiers","manageCop",nil)
    end
end

function manageCop()
    VMenu.curItem = 1
    VMenu.ResetMenu()
    VMenu.EditHeader(txt[config.lang]["Gestion des policiers"])
    VMenu.AddPrev('Main')
    if canManageCop then
        VMenu.AddFunc("Recruter un policer", "recruter",nil)
        VMenu.AddFunc("Promouvoir un policer", "promo",nil)
        VMenu.AddFunc("retrogader un policer", "retro",nil)
        VMenu.AddFunc("Licencier un policer", "licencier",nil)
    end
end

function animations()
    VMenu.curItem = 1
    VMenu.ResetMenu()
    VMenu.EditHeader(txt[config.lang]["menu_animations_title"])
    VMenu.AddPrev('Main')
    VMenu.AddFunc("Poser une barrière", "Putbarriere",nil)
    VMenu.AddFunc("Ranger la barrière", "Removebarriere",nil)
    VMenu.AddFunc(txt[config.lang]["menu_anim_do_traffic_title"],"DoTraffic",nil)
	VMenu.AddFunc(txt[config.lang]["menu_anim_take_notes_title"],"Note",nil)
	VMenu.AddFunc(txt[config.lang]["menu_anim_standby_title"],"StandBy",nil)
	VMenu.AddFunc(txt[config.lang]["menu_anim_standby_2_title"],"StandBy2",nil)
end

function citizens()
    VMenu.curItem = 1
    VMenu.ResetMenu()
    VMenu.EditHeader(txt[config.lang]["menu_citizens_title"])
    VMenu.AddPrev('Main')
   -- VMenu.AddFunc(txt[config.lang]["menu_check_inventory_title"],"CheckInventory",nil)
    --VMenu.AddFunc("Saisir une arme","SaisirArme",nil)
    VMenu.AddFunc(txt[config.lang]["menu_toggle_cuff_title"],"ToggleCuff",nil)
    VMenu.AddFunc(txt[config.lang]["menu_force_player_get_in_car_title"],"PutInVehicle",nil)
    VMenu.AddFunc(txt[config.lang]["menu_force_player_get_out_car_title"],"UnseatVehicle",nil)
    VMenu.AddFunc(txt[config.lang]["menu_drag_player_title"],"DragPlayer",nil)
    VMenu.AddFunc("Bracelet électronique","PutBracelet")
    VMenu.AddFunc(txt[config.lang]["menu_fines_title"],"Fines",nil)
end

function vehicles()
    VMenu.curItem = 1
    VMenu.ResetMenu()
    VMenu.EditHeader(txt[config.lang]["menu_vehicles_title"])
    if(config.enableCheckPlate == true) then
	   VMenu.AddFunc(txt[config.lang]["menu_check_plate_title"],"CheckPlate",nil)
    end
    VMenu.AddFunc(txt[config.lang]["menu_crochet_veh_title"],"Crochet",nil)
    if isRadarPlaced then
        VMenu.AddFunc("Ranger le radar","POLICE_radar",nil)
    else
        VMenu.AddFunc("Poser le radar","POLICE_radar",nil)
    end
    if not SpikesSpawned then
        VMenu.AddFunc("Placer une herse","Addherse",nil)
    else
        VMenu.AddFunc("Ranger la herse","Delherse",nil)
    end
    VMenu.AddFunc(txt[config.lang]["menu_fourriere_veh_title"],"putInFourriere", nil)
end

function finesmenu()
    VMenu.curItem = 1
    VMenu.ResetMenu()
    VMenu.EditHeader("Amendes")
    VMenu.AddPrev("citizens")
    --[[VMenu.AddFunc("$250","Fines",250)
    VMenu.AddFunc("$500","Fines",500)
    VMenu.AddFunc("$1.000","Fines",1000)
    VMenu.AddFunc("$1.500","Fines",1500)
    VMenu.AddFunc("$2.000","Fines",2000)
    VMenu.AddFunc("$4.000","Fines",4000)
    VMenu.AddFunc("$6.000","Fines",6000)
    VMenu.AddFunc("$8.000","Fines",8000)
    VMenu.AddFunc("$10.000","Fines",10000)]]--
    VMenu.AddFunc(txt[config.lang]["menu_custom_amount_fine_title"],"Fines",-1)
end

-------------------------------------------------
---------------ANIMATIONS FUNCTIONS--------------
-------------------------------------------------
function Putbarriere()
    Citizen.CreateThread(function()
        local distance = 2
        local barriere = GetHashKey("prop_barrier_work05")
		local pos = GetEntityCoords(GetPlayerPed(-1))
        local heading = GetEntityHeading(GetPlayerPed(-1))
        local rad = math.pi/2+math.rad(heading)
        local coord = {
                x = pos.x+distance*math.cos(rad),
                y = pos.y+distance*math.sin(rad),
                z = pos.z -1,
            }
        local barr = CreateObject(barriere,coord.x,coord.y,coord.z,true,true,false)
        PlaceObjectOnGroundProperly(barr)
        SetEntityHeading(barr,heading)
        SetObjectTargettable(barr, true)
        FreezeEntityPosition(barr,true)
        TriggerEvent('hud:Notif',"Barrière ~g~placée")
    end)
end

function Removebarriere()
    local pos = GetEntityCoords(GetPlayerPed(-1))
    local barriere = GetHashKey("prop_barrier_work05")
	local targetObject = GetClosestObjectOfType(pos.x,pos.y,pos.z,2.0,barriere,false,true,true)
    if targetObject ~= nil then
        SetEntityAsMissionEntity(targetObject, true, true)
        DeleteObject(targetObject)
        TriggerEvent('hud:Notif','Barrière ~g~rangée')
    else
        TriggerEvent('hud:NotifColor','Aucune barrière devant vous',6)
    end
end

function DoTraffic()
	Citizen.CreateThread(function()
        TaskStartScenarioInPlace(GetPlayerPed(-1), "WORLD_HUMAN_CAR_PARK_ATTENDANT", 0, false)
        Citizen.Wait(60000)
        ClearPedTasksImmediately(GetPlayerPed(-1))
    end)
	drawNotification(txt[config.lang]["menu_doing_traffic_notification"])
end

function Note()
	Citizen.CreateThread(function()
        if GetEntityModel(GetPlayerPed(-1)) == GetHashKey("mp_m_freemode_01") then
            TaskStartScenarioInPlace(GetPlayerPed(-1), "WORLD_HUMAN_CLIPBOARD", 0, false)
        else
            TaskStartScenarioInPlace(GetPlayerPed(-1), "CODE_HUMAN_MEDIC_TIME_OF_DEATH", 0, false)
        end
        Citizen.Wait(20000)
        ClearPedTasksImmediately(GetPlayerPed(-1))
    end) 
	drawNotification(txt[config.lang]["menu_taking_notes_notification"])
end

function StandBy()
	Citizen.CreateThread(function()
        TaskStartScenarioInPlace(GetPlayerPed(-1), "WORLD_HUMAN_COP_IDLES", 0, true)
        Citizen.Wait(20000)
        ClearPedTasksImmediately(GetPlayerPed(-1))
    end)
	drawNotification(txt[config.lang]["menu_being_stand_by_notification"])
end

function StandBy2()
	Citizen.CreateThread(function()
        TaskStartScenarioInPlace(GetPlayerPed(-1), "WORLD_HUMAN_GUARD_STAND", 0, 1)
        Citizen.Wait(20000)
        ClearPedTasksImmediately(GetPlayerPed(-1))
    end)
	drawNotification(txt[config.lang]["menu_being_stand_by_notification"])
end

-------------------------------------------------
-----------------CITIZENS FUNCTIONS--------------
-------------------------------------------------

function CheckInventory()
	local t, distance = GetClosestPlayer()
	if(distance ~= -1 and distance < 3) then
		TriggerServerEvent("police:targetCheckInventory", GetPlayerServerId(t))
	else
		TriggerEvent('chatMessage', txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["no_player_near_ped"])
	end
end

function SaisirArme()
        ClearMenu()
    local t, distance = GetClosestPlayer()
	if(distance ~= -1 and distance < 3) then
        TriggerServerEvent('police:checkarme',GetPlayerServerId(t))
	else
		TriggerEvent('chatMessage', txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["no_player_near_ped"])
	end
end

function takeweapon(arme)
    TriggerServerEvent("police:takeweapon",arme)
    Citizen.Wait(100)
    drawNotification('Arme saisie')
    SaisirArme()
end

RegisterNetEvent('police:confirmarme')
AddEventHandler('police:confirmarme',function()
    local listweapon = {
        "WEAPON_KNIFE", "WEAPON_NIGHTSTICK", "WEAPON_HAMMER", "WEAPON_BAT", "WEAPON_GOLFCLUB",
        "WEAPON_CROWBAR", "WEAPON_PISTOL", "WEAPON_COMBATPISTOL", "WEAPON_APPISTOL", "WEAPON_PISTOL50",
        "WEAPON_MICROSMG", "WEAPON_SMG", "WEAPON_ASSAULTSMG", "WEAPON_ASSAULTRIFLE",
        "WEAPON_CARBINERIFLE", "WEAPON_ADVANCEDRIFLE", "WEAPON_MG", "WEAPON_COMBATMG", "WEAPON_PUMPSHOTGUN",
        "WEAPON_SAWNOFFSHOTGUN", "WEAPON_ASSAULTSHOTGUN", "WEAPON_BULLPUPSHOTGUN", "WEAPON_STUNGUN", "WEAPON_SNIPERRIFLE",
        "WEAPON_HEAVYSNIPER", "WEAPON_GRENADELAUNCHER", "WEAPON_GRENADELAUNCHER_SMOKE", "WEAPON_RPG", "WEAPON_MINIGUN",
        "WEAPON_GRENADE", "WEAPON_STICKYBOMB", "WEAPON_SMOKEGRENADE", "WEAPON_BZGAS", "WEAPON_MOLOTOV",
        "WEAPON_FIREEXTINGUISHER", "WEAPON_PETROLCAN", "WEAPON_FLARE", "WEAPON_SNSPISTOL", "WEAPON_SPECIALCARBINE",
        "WEAPON_HEAVYPISTOL", "WEAPON_BULLPUPRIFLE", "WEAPON_HOMINGLAUNCHER", "WEAPON_PROXMINE", "WEAPON_SNOWBALL",
        "WEAPON_VINTAGEPISTOL", "WEAPON_DAGGER", "WEAPON_FIREWORK", "WEAPON_MUSKET", "WEAPON_MARKSMANRIFLE",
        "WEAPON_HEAVYSHOTGUN", "WEAPON_GUSENBERG", "WEAPON_HATCHET", "WEAPON_RAILGUN", "WEAPON_COMBATPDW",
        "WEAPON_KNUCKLE", "WEAPON_MARKSMANPISTOL", "WEAPON_FLASHLIGHT", "WEAPON_MACHETE", "WEAPON_MACHINEPISTOL",
        "WEAPON_SWITCHBLADE", "WEAPON_REVOLVER", "WEAPON_COMPACTRIFLE", "WEAPON_DBSHOTGUN", "WEAPON_FLAREGUN",
        "WEAPON_AUTOSHOTGUN", "WEAPON_BATTLEAXE", "WEAPON_COMPACTLAUNCHER", "WEAPON_MINISMG", "WEAPON_PIPEBOMB",
        "WEAPON_POOLCUE", "WEAPON_SWEEPER", "WEAPON_WRENCH"
    }
    local possarme = {}
    for i=#listweapon,1,-1 do
        weaponhash = GetHashKey(listweapon[i])
        if HasPedGotWeapon(GetPlayerPed(-1),weaponhash,false) then
            table.insert(possarme,listweapon[i])
        end
    end
    TriggerServerEvent('police:listarme',possarme)
end)

RegisterNetEvent('police:boutonarme')
AddEventHandler('police:boutonarme',function(list)
    boutonarme(list)
end)

function boutonarme(list)
    if list[1] ~= nil then
        VMenu.curItem = 1
        VMenu.ResetMenu()
        VMenu.EditHeader("Armes")
        VMenu.AddPrev("citizens")
        for i=#list,1,-1 do
            arme = GetHashKey(list[i])
            VMenu.AddFunc(list[i],"takeweapon",arme)
        end
    else
        VMenu.AddFunc("Pas d'arme","SaisirArme",nil)
    end
end

RegisterNetEvent('police:dropweapon')
AddEventHandler('police:dropweapon', function(weapon)
        drawNotification('Arme prise')
        RemoveWeaponFromPed(GetPlayerPed(-1),weapon)
end)

function CheckId()
	local t , distance  = GetClosestPlayer()
    if(distance ~= -1 and distance < 3) then
		TriggerServerEvent('gc:copOpenIdentity', GetPlayerServerId(t))
    else
		TriggerEvent('chatMessage', txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["no_player_near_ped"])
	end
end

function ToggleCuff()
	local t, distance = GetClosestPlayer()
	if(distance ~= -1 and distance < 3) then
		TriggerServerEvent("police:cuffGranted", GetPlayerServerId(t))
	else
		TriggerEvent('chatMessage', txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["no_player_near_ped"])
	end
end

function PutInVehicle()
	local t, distance = GetClosestPlayer()
	if(distance ~= -1 and distance < 3) then
		local v = GetVehiclePedIsIn(GetPlayerPed(-1), true)
		TriggerServerEvent("police:forceEnterAsk", GetPlayerServerId(t), v)
	else
		TriggerEvent('chatMessage', txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["no_player_near_ped"])
	end
end

function UnseatVehicle()
	local t, distance = GetClosestPlayer()
	if(distance ~= -1 and distance < 3) then
		TriggerServerEvent("police:confirmUnseat", GetPlayerServerId(t))
	else
		TriggerEvent('chatMessage', txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["no_player_near_ped"])
	end
end

function DragPlayer()
	local t, distance = GetClosestPlayer()
	if(distance ~= -1 and distance < 3) then
		TriggerServerEvent("police:dragRequest", GetPlayerServerId(t))
	else
		TriggerEvent('chatMessage', txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["no_player_near_ped"])
	end
end

function Fines()
    VMenu.visible = false
    TriggerEvent('facture:Open') 
end
-------------------------------------------------
-----------------VEHICLES FUNCTIONS--------------
-------------------------------------------------

function Crochet()
	Citizen.CreateThread(function()
		local pos = GetEntityCoords(GetPlayerPed(-1))
		local entityWorld = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, 5.0, 0.0)
	   local rayHandle = Citizen.InvokeNative(0x28579D1B8F8AAC80,pos,entityWorld, 5/2, 10, GetPlayerPed(-1), 0)    
		local _, _, _, _, vehicleHandle = GetRaycastResult(rayHandle)
		if(DoesEntityExist(vehicleHandle)) then
			TaskStartScenarioInPlace(GetPlayerPed(-1), "WORLD_HUMAN_WELDING", 0, true)
			Citizen.Wait(20000)
			SetVehicleDoorsLocked(vehicleHandle, 1)
            SetVehicleDoorsLockedForAllPlayers(vehicleHandle, false)
			ClearPedTasksImmediately(GetPlayerPed(-1))
			drawNotification(txt[config.lang]["menu_veh_opened_notification"])
		else
			drawNotification(txt[config.lang]["no_veh_near_ped"])
		end
	end)
end

function CheckPlate()
	local pos = GetEntityCoords(GetPlayerPed(-1))
	local entityWorld = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, 20.0, 0.0)
	local rayHandle = CastRayPointToPoint(pos.x, pos.y, pos.z, entityWorld.x, entityWorld.y, entityWorld.z, 10, GetPlayerPed(-1), 0)
	local _, _, _, _, vehicleHandle = GetRaycastResult(rayHandle)
	if(DoesEntityExist(vehicleHandle)) then
		TriggerServerEvent("police:checkingPlate", GetVehicleNumberPlateText(vehicleHandle))
	else
		drawNotification(txt[config.lang]["no_veh_near_ped"])
	end
end

function putInFourriere()
    TriggerServerEvent('police:mayIPutInFourriere') 
end
RegisterNetEvent("police:putInFourriere")
AddEventHandler('police:putInFourriere', function(nbDep)
    if nbDep >0 then
        drawNotification(txt[config.lang]["call_mecano_err"])
    else
        TriggerEvent('fourriere:mettrefourriere')
    end
end)
-------------------------------------------------
------------------DRAW NOTIFY--------------------
-------------------------------------------------
function drawNotification(text)
	SetNotificationTextEntry("STRING")
	AddTextComponentString(text)
	DrawNotification(false, false)
end

Citizen.CreateThread(function()
    VMenu.AddMenu( 'Police', 'police',0,60,106)   
    while true do
        Citizen.Wait(0)
        if isInService then
            if IsControlJustPressed(1, 166) then -- Open menu (168 = F5)
               if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                VMenu.curItem = 1
                Main()
                VMenu.visible = not VMenu.visible
            end
            VMenu.Show()
        end
    end
end)

----------------------------------------------
----------------- RADAR ----------------------
----------------------------------------------


 
function radarSetSpeed(defaultText)
    DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8", "", defaultText or "", "", "", "", 5)
    while (UpdateOnscreenKeyboard() == 0) do
        DisableAllControlActions(0);
        Wait(0);
    end
    if (GetOnscreenKeyboardResult()) then
        local gettxt = tonumber(GetOnscreenKeyboardResult())
        if gettxt ~= nil then
            return gettxt
        else
            ClearPrints()
            SetTextEntry_2("STRING")
            AddTextComponentString("~r~Veuillez entrer un nombre correct !")
            DrawSubtitleTimed(3000, 1)
            return
        end
    end
    return
end
 
 
local function drawTxt(x,y ,width,height,scale, text, r,g,b,a)
    SetTextFont(0)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextOutline()
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width/2, y - height/2 + 0.005)
end
 
function POLICE_radar()
    VMenu.visible = false
    if isRadarPlaced then -- remove the previous radar if it exists, only one radar per cop
       
        if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), RadarPos.x, RadarPos.y, RadarPos.z, true) < 0.9 then -- if the player is close to his radar
       
            RequestAnimDict("anim@apt_trans@garage")
            while not HasAnimDictLoaded("anim@apt_trans@garage") do
               Wait(1)
            end
            TaskPlayAnim(GetPlayerPed(-1), "anim@apt_trans@garage", "gar_open_1_left", 1.0, -1.0, 5000, 0, 1, true, true, true) -- animation
       
            Citizen.Wait(2000) -- prevent spam radar + synchro spawn with animation time
       
            SetEntityAsMissionEntity(Radar, false, false)
           
            DeleteObject(Radar) -- remove the radar pole (otherwise it leaves from inside the ground)
            DeleteEntity(Radar) -- remove the radar pole (otherwise it leaves from inside the ground)
            Radar = nil
            RadarPos = {}
            RadarAng = 0
            isRadarPlaced = false
           
            RemoveBlip(RadarBlip)
            RadarBlip = nil
            LastPlate = ""
            LastVehDesc = ""
            LastSpeed = 0
            LastInfo = ""
           
        else
           
            ClearPrints()
            SetTextEntry_2("STRING")
            AddTextComponentString("~r~Vous n'êtes pas à coté de votre Radar !")
            DrawSubtitleTimed(3000, 1)
           
            Citizen.Wait(1500) -- prevent spam radar
       
        end
   
    else -- or place a new one
        maxSpeed = radarSetSpeed("70")
       
        Citizen.Wait(200) -- wait if the player was in moving
        RadarPos = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0, 1.5, 0)
        RadarAng = GetEntityRotation(GetPlayerPed(-1))
       
        if maxSpeed ~= nil then -- maxSpeed = nil only if the player hasn't entered a valid number
       
            RequestAnimDict("anim@apt_trans@garage")
            while not HasAnimDictLoaded("anim@apt_trans@garage") do
               Wait(1)
            end
            TaskPlayAnim(GetPlayerPed(-1), "anim@apt_trans@garage", "gar_open_1_left", 1.0, -1.0, 5000, 0, 1, true, true, true) -- animation
           
            Citizen.Wait(1500) -- prevent spam radar placement + synchro spawn with animation time
           
            RequestModel("prop_cctv_pole_01a")
            while not HasModelLoaded("prop_cctv_pole_01a") do
               Wait(1)
            end
           
            Radar = CreateObject(1927491455, RadarPos.x, RadarPos.y, RadarPos.z - 7, true, true, true) -- http://gtan.codeshock.hu/objects/index.php?page=1&search=prop_cctv_pole_01a
            SetEntityRotation(Radar, RadarAng.x, RadarAng.y, RadarAng.z - 115)
            -- SetEntityInvincible(Radar, true) -- doesn't work, radar still destroyable
            -- PlaceObjectOnGroundProperly(Radar) -- useless
            SetEntityAsMissionEntity(Radar, true, true)
           
            FreezeEntityPosition(Radar, true) -- set the radar invincible (yeah, SetEntityInvincible just not works, okay FiveM.)
 
            isRadarPlaced = true
           
            RadarBlip = AddBlipForCoord(RadarPos.x, RadarPos.y, RadarPos.z)
            SetBlipSprite(RadarBlip, 380) -- 184 = cam
            SetBlipColour(RadarBlip, 1) -- https://github.com/Konijima/WikiFive/wiki/Blip-Colors
            SetBlipAsShortRange(RadarBlip, true)
            BeginTextCommandSetBlipName("STRING")
            AddTextComponentString("Radar")
            EndTextCommandSetBlipName(RadarBlip)
       
        end
       
    end
end
 
Citizen.CreateThread(function()
    while true do
        Wait(0)
 
        if isRadarPlaced then
       
            if HasObjectBeenBroken(Radar) then -- check is the radar is still there
               
                SetEntityAsMissionEntity(Radar, false, false)
                SetEntityVisible(Radar, false)
                DeleteObject(Radar) -- remove the radar pole (otherwise it leaves from inside the ground)
                DeleteEntity(Radar) -- remove the radar pole (otherwise it leaves from inside the ground)
               
                Radar = nil
                RadarPos = {}
                RadarAng = 0
                isRadarPlaced = false
               
                RemoveBlip(RadarBlip)
                RadarBlip = nil
               
                LastPlate = ""
                LastVehDesc = ""
                LastSpeed = 0
                LastInfo = ""
               
            end
           
            if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), RadarPos.x, RadarPos.y, RadarPos.z, true) > 100 then -- if the player is too far from his radar
           
                SetEntityAsMissionEntity(Radar, false, false)
                SetEntityVisible(Radar, false)
                DeleteObject(Radar) -- remove the radar pole (otherwise it leaves from inside the ground)
                DeleteEntity(Radar) -- remove the radar pole (otherwise it leaves from inside the ground)
               
                Radar = nil
                RadarPos = {}
                RadarAng = 0
                isRadarPlaced = false
               
                RemoveBlip(RadarBlip)
                RadarBlip = nil
               
                LastPlate = ""
                LastVehDesc = ""
                LastSpeed = 0
                LastInfo = ""
               
                ClearPrints()
                SetTextEntry_2("STRING")
                AddTextComponentString("~r~Vous êtes parti trop loin de votre Radar !")
                DrawSubtitleTimed(3000, 1)
               
            end
           
        end
       
        if isRadarPlaced then
 
            local Radius = 10.0
            local DistanceInit = 15.0
            local Distance = 50.0
            local Zcentre = 6.0
            local posRadar = GetOffsetFromEntityInWorldCoords(Radar, -1*DistanceInit, -1*(DistanceInit*11/20), Zcentre) -- forwarding the camera angle, to increase or reduce the distance, just make a cross product like this one :  ( X * 11.0 ) / 20.0 = Y   gives  (Radar, X, Y, 0.0)
            
            local Offset = GetOffsetFromEntityInWorldCoords(Radar, -1*Distance, -1*(Distance*11/20), Zcentre)
            --forwarding the camera angle, to increase or reduce the distance, just make a cross product like this one :  ( X * 11.0 ) / 20.0 = Y   gives  (Radar, X, Y, 0.0)
           -- local veh, dist = GetClosestDrivingPlayerFromPos(Radius, posRadar,entityWorld) -- viewAngle
                
            local dist = radius or -1
            local veh = -1
            local rayHandle = Citizen.InvokeNative(0x28579D1B8F8AAC80, posRadar.x,posRadar.y, posRadar.z,Offset, Radius, 10, Radar, 0)   
            local _, _, _, _, vehicleHandle = GetRaycastResult(rayHandle)
            if(DoesEntityExist(vehicleHandle)) then
                local targetCoords = GetEntityCoords(vehicleHandle, 0)
                local distance = GetDistanceBetweenCoords(targetCoords["x"], targetCoords["y"], targetCoords["z"], posRadar.x,posRadar.y, posRadar.z,true)
                if(dist == -1 or dist > distance) then
                    veh = vehicleHandle
                    dist = distance
                end
            end
            
            --Marker(posRadar.x,posRadar.y,posRadar.z,Radius*2.0,0, 0, 200,1,0.5)
            --Marker(Offset.x,Offset.y,Offset.z,Radius*2.0,0, 0, 200,1,0.5)
            -- local debuginfo = string.format("%s ~n~%s ~n~%s ~n~", ply, veh, dist)
            -- drawTxt(0.27, 0.1, 0.185, 0.206, 0.40, debuginfo, 255, 255, 255, 255)
 
            if veh ~= nil then
           
                local vehPlate = GetVehicleNumberPlateText(veh) or ""
                local vehSpeedKm = GetEntitySpeed(veh)*3.6
                local vehDesc = GetDisplayNameFromVehicleModel(GetEntityModel(veh))--.." "..GetVehicleColor(veh)
                if vehDesc == "CARNOTFOUND" then vehDesc = "" end
               
                -- local vehSpeedMph= GetEntitySpeed(veh)*2.236936
                -- if vehSpeedKm > minSpeed then            
                     
                if vehSpeedKm < maxSpeed then
                    info = string.format("~b~Véhicule  ~w~ %s ~n~~b~Plaque    ~w~ %s ~n~~y~Km/h        ~g~%s", vehDesc, vehPlate, math.ceil(vehSpeedKm))
                else
                    info = string.format("~b~Véhicule  ~w~ %s ~n~~b~Plaque    ~w~ %s ~n~~y~Km/h        ~r~%s", vehDesc, vehPlate, math.ceil(vehSpeedKm))
                    if LastPlate ~= vehPlate then
                        LastSpeed = vehSpeedKm
                        LastVehDesc = vehDesc
                        LastPlate = vehPlate
                    elseif LastSpeed < vehSpeedKm and LastPlate == vehPlate then
                            LastSpeed = vehSpeedKm
                    end
                    LastInfo = string.format("~b~Véhicule  ~w~ %s ~n~~b~Plaque    ~w~ %s ~n~~y~Km/h        ~r~%s", LastVehDesc, LastPlate, math.ceil(LastSpeed))
                end
                   
                DrawRect(0.76, 0.0455, 0.18, 0.09, 0,10, 28, 210)
                drawTxt(0.77, 0.1, 0.185, 0.206, 0.40, info, 255, 255, 255, 255)
               
                DrawRect(0.76, 0.145, 0.18, 0.09, 0,10, 28, 210)
                drawTxt(0.77, 0.20, 0.185, 0.206, 0.40, LastInfo, 255, 255, 255, 255)
                 
                -- end
               
            end
           
        end
           
    end  
end)

function getIdToManage()
    DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8S", "", "", "", "", "", 20)
    while (UpdateOnscreenKeyboard() == 0) do
	    DisableAllControlActions(0);
		Wait(0);
	end
	if (GetOnscreenKeyboardResult()) then
        return tonumber(GetOnscreenKeyboardResult())
    else
        return -1
    end
end

function retro()
    local res = getIdToManage()
    if res ~= -1 then
        TriggerServerEvent('police:retrograder',res)
    end
end

function promo()
    local res = getIdToManage()
    if res ~= -1 then
        TriggerServerEvent('police:promouvoir',res)
    end
end    

function recruter()
    local res = getIdToManage()
    if res ~= -1 then
        TriggerServerEvent('police:recruter',res)
    end
end    

function licencier()
    local res = getIdToManage()
    if res ~= -1 then
        TriggerServerEvent('police:licensier',res)
    end
end

RegisterNetEvent("jobspolice:capitaine")
AddEventHandler('jobspolice:capitaine', function()
    canManageCop=true
	TriggerEvent("commande_metier:updatejob",config.job.chief_officer_on_duty_job_id)--acces Gester
end)
RegisterNetEvent("jobspolice:commandant")
AddEventHandler('jobspolice:commandant', function()
    canManageCop=true
    TriggerEvent("commande_metier:updatejob",config.job.chief_officer_on_duty_job_id)--acces Gester
end)
RegisterNetEvent("jobspolice:lieutenant")
AddEventHandler('jobspolice:lieutenant', function()
    canManageCop=false
    TriggerEvent("commande_metier:updatejob", config.job.officer_on_duty_job_id)
end)
RegisterNetEvent("jobspolice:sergent")
AddEventHandler('jobspolice:sergent', function()
    canManageCop=false
    TriggerEvent("commande_metier:updatejob", config.job.officer_on_duty_job_id)
end)
RegisterNetEvent("jobspolice:brigadier")
AddEventHandler('jobspolice:brigadier', function()
    canManageCop=false
    TriggerEvent("commande_metier:updatejob", config.job.officer_on_duty_job_id)
end)
RegisterNetEvent("jobspolice:cadet")
AddEventHandler('jobspolice:cadet', function()
    canManageCop=false
    TriggerEvent("commande_metier:updatejob", config.job.officer_on_duty_job_id)
end)

function taketarget()
    local X = GetEntityForwardX(GetPlayerPed(-1))
    local Y = GetEntityForwardY(GetPlayerPed(-1))
    for i = 0,64 do
        if NetworkIsPlayerConnected(i) then
            if NetworkIsPlayerActive(i) and GetPlayerPed(i) ~= nil then
                if GetPlayerServerId(i) ~= GetPlayerServerId(PlayerId()) then
                    if (GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)).x+X,GetEntityCoords(GetPlayerPed(-1)).y+Y,GetEntityCoords(GetPlayerPed(-1)).z,GetEntityCoords(GetPlayerPed(i)).x,GetEntityCoords(GetPlayerPed(i)).y,GetEntityCoords(GetPlayerPed(i)).z) < 1.0001) then
                        return GetPlayerServerId(i)
                    end
                end
            end
        end
    end
    return -1
end

function PutBracelet()
    local target = taketarget()
    if target == -1 then
        TriggerEvent('hud:NotifColor',"Personne devant vous.",6)
    else
        TriggerEvent('hud:NotifKey',"~INPUT_MP_TEXT_CHAT_TEAM~", "Poser un bracelet")
        Citizen.Wait(100)
        TriggerEvent('hud:NotifKey',"~INPUT_REPLAY_ENDPOINT~", "Retirer un bracelet")
        while true do
            Citizen.Wait(5)
            if IsControlJustReleased(1, 246) then
                --TriggerServerEvent('bank:withdrawAmende', amount)
                TriggerEvent('hud:NotifDel')
                TriggerServerEvent('Police:AddBracelet',target) 
                break
            end
            if IsControlJustReleased(1, 306) then
                TriggerEvent('hud:NotifDel')
                TriggerServerEvent('Police:RemoveBracelet',target) 
                break
            end
        end   
    end
end

local blipsBracelet = {}
local Bracelet = {}

function UpdateBlipBracelet()
    for _,blip in pairs (blipsBracelet) do
        RemoveBlip(blip)    
    end
    for _,joueur in pairs (Bracelet) do
        local coord = GetEntityCoords(GetPlayerPed(GetPlayerFromServerId(joueur.id)))
        blipsBracelet[joueur.id] = AddBlipForCoord(coord.x, coord.y, coord.z)
        SetBlipSprite(blipsBracelet[joueur.id],1)
        BeginTextCommandSetBlipName("STRING")
        AddTextComponentString("Bracelet électronique : "..joueur.nom)
        EndTextCommandSetBlipName(blipsBracelet[joueur.id])
    end
end

RegisterNetEvent('BraceletElec:UpdateBlip')
AddEventHandler('BraceletElec:UpdateBlip',function(ListBracelet)
    Bracelet = ListBracelet
    UpdateBlipBracelet()
end)

RegisterNetEvent('Police:DeleteBracelet')
AddEventHandler('Police:DeleteBracelet',function()
    for _,blip in pairs (blipsBracelet) do
        RemoveBlip(blip)    
    end
    blipsBracelet = {}
end)