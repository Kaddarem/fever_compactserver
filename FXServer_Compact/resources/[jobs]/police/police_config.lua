--Main config file, mmodify it as you want
config = {
	useModifiedEmergency = true, --require modified emergency script // not compatible with couchdb
	useModifiedBanking = true, --require Simple Banking // compatible with couchdb
	useVDKInventory = true, --require VDK Inventory script // not compatible with couchdb
	useGcIdentity = false, --require GCIdentity // not compatible with couchdb
	enableOutfits = true, --require Skin Customization // not compatible with couchdb
	useJobSystem = true, -- require job system // not compatible with couchdb
	useWeashop = true, -- require es_weashop // not compatible with couchdb
	
	useCopWhitelist = true, --require essentialmode + es_admin // compatible with couchdb
	enableCheckPlate = true, --require garages // not compatible with couchdb
	
	enableOtherCopsBlips = false,
	useNativePoliceGarage = true,
	enableNeverWanted = true,
	
	--Available languages : 'en', 'fr'
	lang = 'fr',
	
	--Use by job system
	job = {
		officer_on_duty_job_id = 2,
		officer_not_on_duty_job_id = 1,
		chief_officer_on_duty_job_id = 34,
	}
}

--[[
You can modify the script if you want it to work with a specific gamemode/script
Just share us your modification and maybe could I integrate your changes in the script (manageable with this config file)
Also feel free to send me translation into your own language and notice me if I done some mistakes in transaltions ;)
]]

txt = {
	fr = {
		title_notification = "Gouvernement",
		now_cuffed = "Vous êtes menotté !",
		now_uncuffed = "Liberté !",
		info_fine_request_before_amount = "Appuyez sur ~g~Y~s~ pour accepter l'amende de $",
		info_fine_request_after_amount = ", appuyez sur ~r~R~s~ pour la refuser !",
		request_fine_expired = "La demande de l'amende à ~r~expirée~s~ !",
		pay_fine_success_before_amount = "Vous avez payé l'amende de $",
		pay_fine_success_after_amount = ".",
		help_text_open_cloackroom = "Appuyez sur ~INPUT_CONTEXT~ pour ouvrir le ~b~vestiaire de Police",
		help_text_put_car_into_garage = "Appuyez sur ~INPUT_CONTEXT~ pour rentrer le ~b~véhicule de Police",
		help_text_get_car_out_garage = "Appuyez sur ~INPUT_CONTEXT~ pour sortir un ~b~véhicule de Police",
		help_text_put_heli_into_garage = "Appuyez sur ~INPUT_CONTEXT~ pour rentrer l'~b~hélicoptère de Police",
		help_text_get_heli_out_garage = "Appuyez sur ~INPUT_CONTEXT~ pour sortir un ~b~hélicoptère de Police",
		no_player_near_ped = "Aucun joueur à proximité",
		no_veh_near_ped = "Aucun véhicule à proximté",
		
		
		menu_id_card_title = "Carte d'identité",
		menu_check_inventory_title = "Fouiller",
		menu_toggle_cuff_title = "(De)Menotter",
		menu_force_player_get_in_car_title = "Mettre dans le véhicule",
		menu_force_player_get_out_car_title = "Faire sortir du véhicule",
		menu_drag_player_title = "Escorter le joueur",
		menu_fines_title = "Amendes",
		menu_check_plate_title = "Plaque d'immatriculation",
		menu_crochet_veh_title = "Crocheter le véhicule",
		menu_global_title = "Menu Police",
		menu_categories_title = "Categories",
		menu_animations_title = "Animations",
		menu_citizens_title = "Citoyens",
		menu_vehicles_title = "Véhicules",
		menu_close_menu_title = "Fermer le menu",
		menu_anim_do_traffic_title = "Faire la circulation",
		menu_anim_take_notes_title = "Prendre des notes",
		menu_anim_standby_title = "Repos",
		menu_anim_standby_2_title = "Repos 2",
		menu_custom_amount_fine_title = "Autre montant",
		menu_doing_traffic_notification = "~g~Vous faites la circulation.",
		menu_taking_notes_notification = "~g~Vous prenez des notes.",
		menu_being_stand_by_notification = "~g~Vous êtes en Stand By.",
		menu_veh_opened_notification = "Le véhicule est ~g~ouvert~w~.",
		menu_fourriere_veh_title="Mettre en fourrière",
		
		garage_global_title = "Garage de police",
		garage_loading = "~b~Chargement...",
		
		cloackroom_global_title = "Vestiaire Police",
		cloackroom_take_service_normal_title = "Prendre le service",
		cloackroom_take_service_hidden_title = "Prendre le service (BAC)",
		cloackroom_take_service_swat_title = "Prendre le service (intervention)",
		cloackroom_break_service_title = "Fin de service",
		cloackroom_add_bulletproof_vest_title = "Gilet par balle",
		cloackroom_remove_bulletproof_vest_title = "Enlever le Gilet par balle",
		cloackroom_add_yellow_vest_title = "Gilet jaune",
		cloackroom_remove_yellow_vest_title = "Enlever le Gilet jaune",
		now_in_service_notification = "Vous êtes maintenant ~g~En service",
		break_service_notification = "Vous avez ~r~terminé votre service",
		help_open_menu_notification = "Appuyer sur ~g~F5~w~ pour ouvrir le ~b~Menu Police",
		
		vehicle_checking_plate_part_1 = "Le véhicule #", -- before number plate
		vehicle_checking_plate_part_2 = " appartient à ", -- between number plate and player name when veh registered
		vehicle_checking_plate_part_3 = "", -- after player name when veh registered
		vehicle_checking_plate_not_registered = " n'est pas enregistré !", -- after player name
		unseat_sender_notification_part_1 = "", -- before player name
		unseat_sender_notification_part_2 = " est sortie !", -- after player name
		drag_sender_notification_part_1 = "Escorte de ", -- before player name
		drag_sender_notification_part_2 = "", -- after player name
		checking_inventory_part_1 = "Items de ",
		checking_inventory_part_2 = " : ",
		checking_weapons_part_1 = "Armes de ",
		checking_weapons_part_2 = " : ",
		send_fine_request_part_1 = "Envoi d'une requête d'amende de $",
		send_fine_request_part_2 = " à ",
		already_have_a_pendind_fine_request = " à déjà une demande d'amende",
		request_fine_timeout = " n'a pas répondu à la demande d'amende",
		request_fine_refused = " à refusé son amende",
		request_fine_accepted = " à payé son amende",
		toggle_cuff_player_part_1 = "Tentative de mettre les menottes à ",
		toggle_cuff_player_part_2 = "",
		force_player_get_in_vehicle_part_1 = "Tentative de faire rentrer ",
		force_player_get_in_vehicle_part_2 = " dans le véhicule",
		usage_command_copadd = "Utilisation : /copadd [ID]",
		usage_command_coprem = "Utilisation : /coprem [ID]",
		usage_command_coppromo = "Utilisation : /coppromo [ID]",
		usage_command_copretro = "Utilisation : /copretro [ID]",
		command_received = "Compris !",
		become_cop_success = "Félicitation, vous êtes désormais policier !",
        promo_cop_sucess = "Félicitation, vous avez été promu !",
        retro_cop_sucess = "Vous avez été rétrogradé !",
		remove_from_cops = "Vous n'êtes plus policier !",
		no_player_with_this_id = "Aucun joueur avec cet ID !",
		not_enough_permission = "Vous n'avez pas la permission de faire ça !",
		call_mecano_err = "Appel un mécano pour mettre en fourrière",
	}
}