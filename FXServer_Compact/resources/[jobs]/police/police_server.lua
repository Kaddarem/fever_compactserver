local inServiceCops = {}

function addCop(identifier)
	
    local result = "nil"
	local resultq = MySQL.Sync.fetchAll("SELECT * FROM users WHERE identifier = @identifier", { ['@identifier'] = identifier})
	if resultq[1].police == 0 then
		result = "nil"
	else
		result = resultq[1].police
	end
	if(result == "nil") then
		MySQL.Sync.execute("UPDATE `users` SET `job` = '2', `police` = '1' WHERE `identifier` = @identifier", { ['@identifier'] = identifier})
	end
end

function remCop(identifier)
    MySQL.Sync.execute("UPDATE `users` SET `job` = '0', `police` = '0' WHERE `identifier` = @identifier", { ['@identifier'] = identifier})
    
end

function promoCop(identifier)
    local resultq = MySQL.Sync.fetchAll("SELECT * FROM users WHERE identifier = @identifier", { ['@identifier'] = identifier})
	if resultq[1].police < 7 then
		job = config.job.officer_on_duty_job_id
		newrang = resultq[1].police + 1
		if newrang > 4 then
			job=config.job.chief_officer_on_duty_job_id
		end
		MySQL.Sync.execute("UPDATE `users` SET `job` = @job, `police` = @newrang WHERE `identifier` = @identifier", {['job']=job, ['@newrang']=newrang, ['@identifier'] = identifier})
    end
end

function retroCop(identifier)
    local resultq = MySQL.Sync.fetchAll("SELECT * FROM users WHERE identifier = @identifier", { ['@identifier'] = identifier})
	if resultq[1].police > 2 then
		job = config.job.officer_on_duty_job_id
		newrang = resultq[1].police - 1
		if newrang > 4 then
			job=config.job.chief_officer_on_duty_job_id
		end
        MySQL.Sync.execute("UPDATE `users` SET `job` = @job, `police` = @newrang WHERE `identifier` = @identifier", {['job']=job, ['@newrang']=newrang, ['@identifier'] = identifier})
    end
end

function getService(identifier)
	local resultq = MySQL.Sync.fetchAll("SELECT * FROM users WHERE identifier = @identifier", { ['@identifier'] = identifier})
	job = config.job.officer_on_duty_job_id
	rang = resultq[1].police 
	if rang > 4 then
		job=config.job.chief_officer_on_duty_job_id
	end
	MySQL.Sync.execute("UPDATE `users` SET `job` = @job WHERE `identifier` = @identifier", {['job']=job, ['@identifier'] = identifier})
end
    

function checkIsCop(identifier)
    local source = source
	local result = MySQL.Sync.fetchAll("SELECT * FROM users JOIN police ON users.police = police.police_id WHERE identifier = @identifier", { ['@identifier'] = identifier})
	if(not result[1]) then
		TriggerClientEvent('police:receiveIsCop', source, "unknown")
	else
		TriggerClientEvent('police:receiveIsCop', source, result[1].police_name)
	end
end

AddEventHandler('playerDropped', function()
	if(inServiceCops[source]) then
		inServiceCops[source] = nil
		--[[if(config.useJobSystem == true) then
			TriggerEvent("jobssystem:disconnectReset", source, config.job.officer_not_on_duty_job_id)
		end]]
		
		for i, c in pairs(inServiceCops) do
			TriggerClientEvent("police:resultAllCopsInService", i, inServiceCops)
		end
    end
end)

if(config.useCopWhitelist == true) then
	RegisterServerEvent('police:checkIsCop')
	AddEventHandler('police:checkIsCop', function()
		local identifier = getPlayerID(source)
		checkIsCop(identifier)
	end)
end
RegisterServerEvent('police:setJob')
AddEventHandler('police:setJob', function()
	TriggerEvent('es:getPlayerFromId', source, function(user)
		local identifier=user.identifier
	end)
end)
RegisterServerEvent('police:takeService')
AddEventHandler('police:takeService', function()
    local source = source
	if(not inServiceCops[source]) then
		inServiceCops[source] = GetPlayerNameRP(source)
		print(inServiceCops[source].." prend son service de policier")
		for i, c in pairs(inServiceCops) do
			TriggerClientEvent("police:resultAllCopsInService", i, inServiceCops)
		end
	end
end)

RegisterServerEvent('police:breakService')
AddEventHandler('police:breakService', function()

	if(inServiceCops[source]) then
        print(inServiceCops[source].." arrête son service de policier")
		inServiceCops[source] = nil
		
		for i, c in pairs(inServiceCops) do
			TriggerClientEvent("police:resultAllCopsInService", i, inServiceCops)
		end
	end
end)

RegisterServerEvent('police:getAllCopsInService')
AddEventHandler('police:getAllCopsInService', function()
	TriggerClientEvent("police:resultAllCopsInService", source, inServiceCops)
end)

RegisterServerEvent('police:checkingPlate')
AddEventHandler('police:checkingPlate', function(plate)
     local source = source
	 local result = MySQL.Sync.fetchAll("SELECT Nom, Prenom FROM user_vehicle JOIN users ON user_vehicle.identifier = users.identifier WHERE vehicle_plate = @plate", { ['@plate'] = plate })
     if (result[1]) then
          for _, v in ipairs(result) do
              TriggerClientEvent("police:notify", source, "CHAR_ANDREAS", 1, txt[config.lang]["title_notification"], false, txt[config.lang]["vehicle_checking_plate_part_1"]..plate..txt[config.lang]["vehicle_checking_plate_part_2"] .. v.Nom.." "..v.Prenom..txt[config.lang]["vehicle_checking_plate_part_3"])
          end
     else
          TriggerClientEvent("police:notify", source, "CHAR_ANDREAS", 1, txt[config.lang]["title_notification"], false, txt[config.lang]["vehicle_checking_plate_part_1"]..plate..txt[config.lang]["vehicle_checking_plate_not_registered"])
     end
end)

RegisterServerEvent('police:choisirtenue')
AddEventHandler('police:choisirtenue', function()
        local source = source
        local player = getPlayerID(source)
		local namePolice = namePolice(player) --SELECT
		local id_police = idPolice(player) --Donne le ID de la police du joueur pour lui donne les armes et le skin aproprier
        TriggerClientEvent("es_freeroam:notify", source, "CHAR_ANDREAS", 1, "Commissariat", false, "Vous êtes en service en tant que : ".. namePolice)
        ArmeSelonGrade(source, id_police)
end)


function idPolice(player)
    return MySQL.Sync.fetchScalar("SELECT police FROM users WHERE identifier = @identifier", {['@identifier'] = player})
end

function namePolice(player)
  local idPolice = idPolice(player)
  return MySQL.Sync.fetchScalar("SELECT police_name FROM police WHERE police_id = @respolice", {['@respolice'] = idPolice})
end

function ArmeSelonGrade(source,id_police) --Donne certaine Arme en fonction du grade de la police PAS ENCORE TERMINER
    local source = source
	if id_police == 1 then
		TriggerClientEvent("jobspolice:cadet", source , nil)
	elseif id_police == 2 then
		TriggerClientEvent("jobspolice:brigadier", source, nil)
	elseif id_police == 3 then
		TriggerClientEvent("jobspolice:sergent", source , nil)
	elseif id_police == 4 then
    		TriggerClientEvent("jobspolice:lieutenant", source , nil)
	elseif id_police == 5 then
		TriggerClientEvent("jobspolice:capitaine", source , nil)
	elseif id_police >= 6 then
        TriggerClientEvent("jobspolice:commandant", source , nil)
	end
end

RegisterServerEvent('police:confirmUnseat')
AddEventHandler('police:confirmUnseat', function(t)
	TriggerClientEvent("police:notify", source, "CHAR_ANDREAS", 1, txt[config.lang]["title_notification"], false, txt[config.lang]["unseat_sender_notification_part_1"] .. GetPlayerNameRP(t) .. txt[config.lang]["unseat_sender_notification_part_2"])
	TriggerClientEvent('police:unseatme', t)
end)

RegisterServerEvent('police:dragRequest')
AddEventHandler('police:dragRequest', function(t)
	TriggerClientEvent("police:notify", source, "CHAR_ANDREAS", 1, txt[config.lang]["title_notification"], false, txt[config.lang]["drag_sender_notification_part_1"] .. GetPlayerNameRP(t) .. txt[config.lang]["drag_sender_notification_part_2"])
	TriggerClientEvent('police:toggleDrag', t, source)
end)

RegisterServerEvent('police:targetCheckInventory')
AddEventHandler('police:targetCheckInventory', function(target)
	local identifier = getPlayerID(target)
    local source = source
	local strResult = txt[config.lang]["checking_inventory_part_1"] .. GetPlayerNameRP(target) .. txt[config.lang]["checking_inventory_part_2"]
	local result = MySQL.Sync.fetchAll("SELECT * FROM `user_inventory` JOIN items ON items.id = user_inventory.item_id WHERE user_id = @username", { ['@username'] = identifier })
    if (result) then
        for _, v in ipairs(result) do
	       if(v.quantity ~= 0) then
                strResult = strResult .. v.quantity .. "*" .. v.libelle .. ", "
            end
            --if(v.isIllegal == "1" or v.isIllegal == "True" or v.isIllegal == 1 or v.isIllegal == true) then
            --TriggerClientEvent('police:dropIllegalItem', target, v.item_id)
			--end
        end
    end
    TriggerClientEvent("police:notify", source, "CHAR_ANDREAS", 1, txt[config.lang]["title_notification"], false, strResult)
    --local strResult = txt[config.lang]["checking_weapons_part_1"] .. GetPlayerNameRP(target) .. txt[config.lang]["checking_weapons_part_2"]
	--local result2 = MySQL.Sync.fetchAll("SELECT * FROM gunshops WHERE identifier = @username", { ['@username'] = identifier })
	--if (result2) then
    --for _, v in ipairs(result2) do
    --strResult = strResult .. v.weapon_model .. ", "
    --    end
    --end
    --TriggerClientEvent("police:notify", source, "CHAR_ANDREAS", 1, txt[config.lang]["title_notification"], false, strResult)	
end)

RegisterServerEvent('police:finesGranted')
AddEventHandler('police:finesGranted', function(target, amount)
	TriggerClientEvent('police:payFines', target, amount, source)
	TriggerClientEvent("police:notify", source, "CHAR_ANDREAS", 1, txt[config.lang]["title_notification"], false, txt[config.lang]["send_fine_request_part_1"]..amount..txt[config.lang]["send_fine_request_part_2"]..GetPlayerNameRP(target))
end)

RegisterServerEvent('police:finesETA')
AddEventHandler('police:finesETA', function(officer, code)
	if(code==1) then
		TriggerClientEvent("police:notify", officer, "CHAR_ANDREAS", 1, txt[config.lang]["title_notification"], false, GetPlayerNameRP(source)..txt[config.lang]["already_have_a_pendind_fine_request"])
	elseif(code==2) then
		TriggerClientEvent("police:notify", officer, "CHAR_ANDREAS", 1, txt[config.lang]["title_notification"], false, GetPlayerNameRP(source)..txt[config.lang]["request_fine_timeout"])
	elseif(code==3) then
		TriggerClientEvent("police:notify", officer, "CHAR_ANDREAS", 1, txt[config.lang]["title_notification"], false, GetPlayerNameRP(source)..txt[config.lang]["request_fine_refused"])
	elseif(code==0) then
		TriggerClientEvent("police:notify", officer, "CHAR_ANDREAS", 1, txt[config.lang]["title_notification"], false, GetPlayerNameRP(source)..txt[config.lang]["request_fine_accepted"])
	end
end)

RegisterServerEvent('police:cuffGranted')
AddEventHandler('police:cuffGranted', function(t)
	TriggerClientEvent("police:notify", source, "CHAR_ANDREAS", 1, txt[config.lang]["title_notification"], false, txt[config.lang]["toggle_cuff_player_part_1"]..GetPlayerNameRP(t)..txt[config.lang]["toggle_cuff_player_part_2"])
	TriggerClientEvent('police:getArrested', t)
end)

RegisterServerEvent('police:forceEnterAsk')
AddEventHandler('police:forceEnterAsk', function(t, v)
	TriggerClientEvent("police:notify", source, "CHAR_ANDREAS", 1, txt[config.lang]["title_notification"], false, txt[config.lang]["force_player_get_in_vehicle_part_1"]..GetPlayerNameRP(t)..txt[config.lang]["force_player_get_in_vehicle_part_2"])
	TriggerClientEvent('police:forcedEnteringVeh', t, v)
end)

-----------------------------------------------------------------------
----------------------EVENT SPAWN POLICE VEH---------------------------
-----------------------------------------------------------------------
RegisterServerEvent('CheckPoliceVeh')
AddEventHandler('CheckPoliceVeh', function(vehicle)
	TriggerClientEvent('FinishPoliceCheckForVeh',source)
	TriggerClientEvent('policeveh:spawnVehicle', source, vehicle)
end)

RegisterServerEvent('police:mayIPutInFourriere')
AddEventHandler('police:mayIPutInFourriere',function()
	local source=source
	local dep = exports.gcphone:GetNumberServer("depanneur")
	TriggerClientEvent('police:putInFourriere',source,dep)
end)
-----------------------------------------------------------------------
----------------------EVENT WEAPON-------------------------------------
-----------------------------------------------------------------------
local agent = ""
local pla = ""
RegisterServerEvent('police:takeweapon')
AddEventHandler('police:takeweapon', function(arme)
    TriggerClientEvent('police:dropweapon',pla,arme)
end)

RegisterServerEvent('police:checkarme')
AddEventHandler('police:checkarme', function(cible)
    agent = source
    pla = cible
   TriggerClientEvent('police:confirmarme',pla) 
end)

RegisterServerEvent('police:listarme')
AddEventHandler('police:listarme',function(table)
    TriggerClientEvent('police:boutonarme',agent,table) 
end)
-----------------------------------------------------------------------
---------------------COMMANDE ADMIN AJOUT / SUPP COP-------------------
-----------------------------------------------------------------------
if(config.useCopWhitelist) then

	RegisterServerEvent('police:recruter')
	AddEventHandler('police:recruter',function(id)
		local mySource = source
		recruter(mySource,id)
	end)

	TriggerEvent('es:addGroupCommand', 'copadd', "admin", function(source, args, user)
		local mySource = source
		 if(not args[2]) then
			TriggerClientEvent('chatMessage', source, txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["usage_command_copadd"])	
		else
			recruter(mySource,tonumber(args[2]))
		end
	end, function(source, args, user) 
		TriggerClientEvent('chatMessage', source, txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["not_enough_permission"])
	end)

	RegisterServerEvent('police:licensier')
	AddEventHandler('police:licensier',function(id)
		local mySource = source
		licensier(mySource,id)
	end)

	TriggerEvent('es:addGroupCommand', 'coprem', "admin", function(source, args, user) 
		local mySource = source
		 if(not args[2]) then
			TriggerClientEvent('chatMessage', mySource, txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["usage_command_coprem"])	
		else
			licensier(mySource,tonumber(args[2]))
		end
	end, function(source, args, user) 
		TriggerClientEvent('chatMessage', source, txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["not_enough_permission"])
	end)

	RegisterServerEvent('police:promouvoir')
	AddEventHandler('police:promouvoir',function(id)
		local mySource = source
		promo(mySource,id)
	end)
    
	TriggerEvent('es:addGroupCommand', 'coppromo', "admin", function(source, args, user)
		local mySource = source
		 if(not args[2]) then
			TriggerClientEvent('chatMessage', source, txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["usage_command_coppromo"])	
		else
			promo(mySource,tonumber(args[2]))
		end
	end, function(source, args, user) 
		TriggerClientEvent('chatMessage', source, txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["not_enough_permission"])
	end)

	RegisterServerEvent('police:retrograder')
	AddEventHandler('police:retrograder',function(id)
		local mySource = source
		retro(mySource,id)
	end)
    
	TriggerEvent('es:addGroupCommand', 'copretro', "admin", function(source, args, user)
		local mySource = source
		if(not args[2]) then
			TriggerClientEvent('chatMessage', source, txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["usage_command_copretro"])	
		else
			retro(mySource,tonumber(args[2]))
		end
	end, function(source, args, user) 
		TriggerClientEvent('chatMessage', source, txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["not_enough_permission"])
	end)
	
end

function GetPlayerNameRP(target)
    local name = "Erreur"
    TriggerEvent('es:getPlayerFromId', target, function(user)
		if user ~= nil then
            name = user.prenom.." "..user.nom
        end
    end)
    return name
end
-- get's the player id without having to use bugged essentials
function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

-- gets the actual player id unique to the player,
-- independent of whether the player changes their screen name
function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end

function licensier(source,id)
	if(GetPlayerNameRP(id) ~= nil)then
		local player = id
		remCop(getPlayerID(player))
		TriggerClientEvent("police:notify", player, "CHAR_ANDREAS", 1, txt[config.lang]["title_notification"], false, txt[config.lang]["remove_from_cops"])
		TriggerClientEvent('chatMessage', source, txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["command_received"])
		TriggerClientEvent('police:noLongerCop', player)
	else
		TriggerClientEvent('chatMessage', source, txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["no_player_with_this_id"])
	end
end

function retro(source,id)
	if(GetPlayerNameRP(id) ~= nil)then
		local player = id
		retroCop(getPlayerID(player))
		TriggerClientEvent('chatMessage', source, txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["command_received"])
		TriggerClientEvent("police:notify", player, "CHAR_ANDREAS", 1, txt[config.lang]["title_notification"], false, txt[config.lang]["retro_cop_sucess"])
		TriggerClientEvent('police:nowCop', player)
	else
		TriggerClientEvent('chatMessage', source, txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["no_player_with_this_id"])
	end
end

function promo(source,id)
    if(GetPlayerNameRP(id) ~= nil)then
		local player = id
		promoCop(getPlayerID(player))
		TriggerClientEvent('chatMessage', source, txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["command_received"])
		TriggerClientEvent("police:notify", player, "CHAR_ANDREAS", 1, txt[config.lang]["title_notification"], false, txt[config.lang]["promo_cop_sucess"])
		TriggerClientEvent('police:nowCop', player)
	else
		TriggerClientEvent('chatMessage', source, txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["no_player_with_this_id"])
	end
end    

function recruter(source,id)
    if(GetPlayerNameRP(id) ~= nil)then
		local player = id
		addCop(getPlayerID(player))
		TriggerClientEvent('chatMessage', source, txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["command_received"])
		TriggerClientEvent("police:notify", player, "CHAR_ANDREAS", 1, txt[config.lang]["title_notification"], false, txt[config.lang]["become_cop_success"])
		TriggerClientEvent('police:nowCop', player)
	else
		TriggerClientEvent('chatMessage', source, txt[config.lang]["title_notification"], {255, 0, 0}, txt[config.lang]["no_player_with_this_id"])
	end
end

local ListBracelet = {}

local function UpdateBracelet()
	SetTimeout(5*60*1000, function()
        if exports.gcphone:GetNumberServer("police") > 0 then
            ListBracelet = {}
            MySQL.Async.fetchAll('SELECT * FROM bracelet_elec',{}, function(result)
                for _,bracelet in pairs (result) do
                    TriggerEvent('es:getPlayers', function(joueurs)
                        for _,joueur in pairs (joueurs) do
                            if joueur.identifier == bracelet.identifier then
                                table.insert(ListBracelet,{id = joueur.source,nom = joueur.prenom.." "..joueur.nom})  
                                break
                            end 
                        end
                    end)
                end
                local policiers = exports.gcphone:GetPlayerEnService("police")
                for _,policier in pairs (policiers) do
                    TriggerClientEvent('BraceletElec:UpdateBlip',policier,ListBracelet)        
                end
            end)    
        end
		UpdateBracelet()
    end)
end

UpdateBracelet()

RegisterServerEvent('Police:AddBracelet')
AddEventHandler('Police:AddBracelet',function(target,date)
    local source = source
    local player = getPlayerID(target)
    MySQL.Async.insert("REPLACE into bracelet_elec VALUES (@id)",{['@id'] = player})
    TriggerClientEvent('hud:NotifColor',source,"Bracelet posé !",141)
    TriggerClientEvent("hud:NotifIcon", source, "CHAR_CALL911", 1, "Police", false, "On vous à posé un bracelet électronique !")
    table.insert(ListBracelet,{id = target, nom = GetPlayerNameRP(target) })
    local policiers = exports.gcphone:GetPlayerEnService("police")
    for _,policier in pairs (policiers) do
        TriggerClientEvent('BraceletElec:UpdateBlip',policier,ListBracelet)          
    end
end)

RegisterServerEvent('Police:RemoveBracelet')
AddEventHandler('Police:RemoveBracelet',function(target)
    local source = source
    local player = getPlayerID(target)
    MySQL.Async.insert("DELETE FROM bracelet_elec WHERE identifier =@id",{['@id'] = player})
    TriggerClientEvent('hud:NotifColor',source,"Bracelet enlevé !",141)
    TriggerClientEvent("hud:NotifIcon", source, "CHAR_CALL911", 1, "Police", false, "On vous a retiré votre bracelet électronique !")
    for id,info in pairs (ListBracelet) do
        if id == target then
            table.remove(ListBracelet,tonumber(id))
        end
    end
    local policiers = exports.gcphone:GetPlayerEnService("police")
    for _,policier in pairs (policiers) do
        TriggerClientEvent('BraceletElec:UpdateBlip',policier,ListBracelet)          
    end
end)

RegisterServerEvent('Police:LoadBracelet')
AddEventHandler('Police:LoadBracelet',function()
    TriggerClientEvent('BraceletElec:UpdateBlip',source,ListBracelet)   
end)