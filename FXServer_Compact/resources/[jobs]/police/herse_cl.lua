local SpawnedSpikes = {}
SpikesSpawned = false
--[[ Looped Thread ]]--
function Addherse()
    local MaxSpikes = 6
    local amount = DisplayInput()
    if amount > 0 then
        if amount > MaxSpikes then
            TriggerEvent('hud:NotifColor',"La herse ne peut pas être si longue",6)
        else
           for b = 1, amount do
                local plyCoords = GetOffsetFromEntityInWorldCoords(GetPlayerPed(PlayerId()), 0.0, 3.0 * b + 0.5, 0.0)
                local plyHead = GetEntityHeading(GetPlayerPed(PlayerId()))
                local spike = CreateObject(GetHashKey("P_ld_stinger_s"), plyCoords.x, plyCoords.y, plyCoords.z, true, 1, true)
                local spikeHeight = GetEntityHeightAboveGround(spike)
                SetEntityCoords(spike, plyCoords.x, plyCoords.y, plyCoords.z - spikeHeight + 0.05, 0.0, 0.0, 0.0, 0.0)
                SetEntityHeading(spike, plyHead)
                SetEntityAsMissionEntity(spike, 1, 1)
                SetEntityCollision(spike, false, false)
				FreezeEntityPosition(spike, true)
                table.insert(SpawnedSpikes, spike)
                SpikesSpawned = true
                TriggerEvent('hud:NotifColor',"Herse en place",141)
           end
        end
    else
        TriggerEvent('hud:NotifColor',"Erreur de saisie",6)
    end
    vehicles()
end

function Delherse()
    if SpikesSpawned then
		for i = 1, #SpawnedSpikes do
			local netId = NetworkGetNetworkIdFromEntity(SpawnedSpikes[i])
			NetworkRequestControlOfNetworkId(netId)
			while not NetworkHasControlOfNetworkId(netId) do
				Citizen.Wait(0)
				NetworkRequestControlOfNetworkId(netId)
			end
			local entity = NetworkGetEntityFromNetworkId(netId)
			DeleteEntity(entity)
			SpawnedSpikes[i] = nil
			SpikesSpawned = false
			DeleteDeadSpikes = true
		end
	end
    vehicles()
end

function CheckDistanceToStrips()
	local vehicle = GetVehiclePedIsIn(GetPlayerPed(PlayerId()), false)
	FrontLeftTire(vehicle)
	FrontRightTire(vehicle)
	BackLeftTire(vehicle)
	BackRightTire(vehicle)
end

function FrontLeftTire(vehicle)
	local tirePosition = GetWorldPositionOfEntityBone(vehicle, GetEntityBoneIndexByName(vehicle, "wheel_lf"))
	local spikestrip = GetClosestObjectOfType(tirePosition.x, tirePosition.y, tirePosition.z, 15.0, GetHashKey("P_ld_stinger_s"), false, 1, 1)
	local spikeCoords = GetEntityCoords(spikestrip, false)
	local distance = Vdist(tirePosition.x, tirePosition.y, tirePosition.z, spikeCoords.x, spikeCoords.y, spikeCoords.z)
	
	if distance < 1.8 then
		if not IsVehicleTyreBurst(vehicle, 0, false) and not IsVehicleTyreBurst(vehicle, 0, true) then
			SetVehicleTyreBurst(vehicle, 0, false, 1000.0)
		end
	end
end

function FrontRightTire(vehicle)
	local tirePosition = GetWorldPositionOfEntityBone(vehicle, GetEntityBoneIndexByName(vehicle, "wheel_rf"))
	local spikestrip = GetClosestObjectOfType(tirePosition.x, tirePosition.y, tirePosition.z, 15.0, GetHashKey("P_ld_stinger_s"), false, 1, 1)
	local spikeCoords = GetEntityCoords(spikestrip, false)
	local distance = Vdist(tirePosition.x, tirePosition.y, tirePosition.z, spikeCoords.x, spikeCoords.y, spikeCoords.z)
	
	if distance < 1.8 then
		if not IsVehicleTyreBurst(vehicle, 1, false) and not IsVehicleTyreBurst(vehicle, 1, true) then
			SetVehicleTyreBurst(vehicle, 1, false, 1000.0)
		end
	end
end

function BackLeftTire(vehicle)
	local tirePosition = GetWorldPositionOfEntityBone(vehicle, GetEntityBoneIndexByName(vehicle, "wheel_lr"))
	local spikestrip = GetClosestObjectOfType(tirePosition.x, tirePosition.y, tirePosition.z, 15.0, GetHashKey("P_ld_stinger_s"), false, 1, 1)
	local spikeCoords = GetEntityCoords(spikestrip, false)
	local distance = Vdist(tirePosition.x, tirePosition.y, tirePosition.z, spikeCoords.x, spikeCoords.y, spikeCoords.z)
	
	if distance < 1.8 then
		if not IsVehicleTyreBurst(vehicle, 4, false) and not IsVehicleTyreBurst(vehicle, 4, true) then
			SetVehicleTyreBurst(vehicle, 4, false, 1000.0)
			SetVehicleTyreBurst(vehicle , 2, false, 1000.0)
		end
	end
end

function BackRightTire(vehicle)
	local tirePosition = GetWorldPositionOfEntityBone(vehicle, GetEntityBoneIndexByName(vehicle, "wheel_rr"))
	local spikestrip = GetClosestObjectOfType(tirePosition.x, tirePosition.y, tirePosition.z, 15.0, GetHashKey("P_ld_stinger_s"), false, 1, 1)
	local spikeCoords = GetEntityCoords(spikestrip, false)
	local distance = Vdist(tirePosition.x, tirePosition.y, tirePosition.z, spikeCoords.x, spikeCoords.y, spikeCoords.z)
	
	if distance < 1.8 then
		if not IsVehicleTyreBurst(vehicle, 5, false) and not IsVehicleTyreBurst(vehicle, 5, true) then
			SetVehicleTyreBurst(vehicle, 5, false, 1000.0)
			SetVehicleTyreBurst(vehicle , 3, false, 1000.0)
		end
	end
end

function DisplayInput()
    DisplayOnscreenKeyboard(1, "FMMC_MPM_TYP8", "", "", "", "", "", 30)
    while UpdateOnscreenKeyboard() == 0 do
        DisableAllControlActions(0)
        Wait(1)
    end
    if (GetOnscreenKeyboardResult()) then
        return tonumber(GetOnscreenKeyboardResult())
    end
    return 0
end

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		if SpikesSpawned and IsEntityDead(GetPlayerPed(PlayerId())) then
			DeleteDeadSpikes = false
			Delherse()
		end
        
        if IsPedInAnyVehicle(GetPlayerPed(PlayerId()), false) then
			local vehicle = GetVehiclePedIsIn(GetPlayerPed(PlayerId()), false)
			local vehiclePos = GetEntityCoords(vehicle, false)
			local spikes = GetClosestObjectOfType(vehiclePos.x, vehiclePos.y, vehiclePos.z, 75.0, GetHashKey("P_ld_stinger_s"), false, 1, 1)
			local spikesCoords = GetEntityCoords(spikes, false)
			local distance = Vdist(vehiclePos.x, vehiclePos.y, vehiclePos.z, spikesCoords.x, spikesCoords.y, spikesCoords.z)
			if distance <= 75.0 then
				CheckDistanceToStrips()
			end
		end
	end
end)