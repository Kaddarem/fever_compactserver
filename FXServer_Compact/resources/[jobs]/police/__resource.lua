-- Manifest


-- General
client_scripts {
    '@hud-event/hudevent_aff.lua',
    '@appmenu/menu_cl.lua',
    'police_config.lua',
    'herse_cl.lua',
    'police_gui.lua',
    'police_client.lua',
    'police_vestpolice.lua',
    'police_menupolice.lua',
    'police_policeveh.lua',
    'police_code2client.lua',
    'police_weapon.lua',
    'prisonnier_client.lua',
    'heli_client.lua'
}

server_scripts {
    '@mysql-async/lib/MySQL.lua',
    'police_config.lua',
    'police_server.lua',
    'police_code2server.lua',
    'heli_server.lua'
}

export 'getIsInService'
