Armurerie = {
    {x = 459.144, y = -979.429, z = 29.689},
    {x = -450.048, y = 6016.245,  z = 30.716}
}
local bonnepos1 = false


function LoinArmure()
    local boucle = true
    Citizen.CreateThread(function() 
        while boucle do
            Citizen.Wait(2000)
            if isInService then
                for i=1,#Armurerie do
                    if Vdist(Armurerie[i].x, Armurerie[i].y, Armurerie[i].z, GetEntityCoords(GetPlayerPed(-1))) < 50.0  then
                        ProcheArmure(Armurerie[i])
                        boucle = false
                        break
                    end 
                end
            end
        end
    end)
end

function ProcheArmure(lieux)
    Citizen.CreateThread(function()
        local boucle = true
        while boucle do
            Wait(5)
            if Vdist(lieux.x, lieux.y, lieux.z, GetEntityCoords(GetPlayerPed(-1))) < 50.0 then
                Marker(lieux.x, lieux.y, lieux.z,1.5,0, 0, 200)
                ----------------------- VESTIAIRE
                if Vdist(lieux.x, lieux.y, lieux.z, GetEntityCoords(GetPlayerPed(-1))) < 1.5 then
                    bonnepos1 = true
                    DisplayHelp("Pressez ~INPUT_CONTEXT~ pour ouvrir l'armurerie")
                    if IsControlJustPressed(1, 51) then
                       if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                        VMenu.curItem = 1
                        Menu_Armure()
                        VMenu.visible = not VMenu.visible
                    end
                    VMenu.Show()
                elseif bonnepos1 then
                    VMenu.visible = false
                    bonnepos1 = false
                end
            else
                boucle = false
                LoinArmure()
                break
            end
        end
    end)
end

LoinArmure()

function Menu_Armure()
    VMenu.curItem = 1
    VMenu.ResetMenu()
    VMenu.EditHeader('Armurerie')
    VMenu.AddFunc("Lampe torche", "EquiperArme", 127)
    VMenu.AddFunc("T-batte","EquiperArme", 135)
    VMenu.AddFunc("Tazer", "EquiperArme", 134)
    VMenu.AddFunc("Pistolet calibre 50", "EquiperArme", 128)
    VMenu.AddFunc("Pistolet Lourd", "EquiperArme", 129)
    VMenu.AddFunc("Fusil à pompe", "EquiperArme", 130)
    VMenu.AddFunc("Sniper", "EquiperArme", 133)
    VMenu.AddFunc("Assault SMG", "EquiperArme", 132)
    VMenu.AddFunc("SMG", "EquiperArme", 131)
end

function EquiperArme(id)
    TriggerEvent("player:receiveItem", id,1)
end