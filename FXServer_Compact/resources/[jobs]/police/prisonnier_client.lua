local isInTenue = false -- don't edit
local positions={
	{x=1661.5024414063, y=2566.8828125,  z=45.56485748291},
	{x=454.54629516602, y=-987.51879882813,  z=26.674240112305}
}
local WaitBoucle = 1000
---- THREADS ----

-- Service
Citizen.CreateThread(function()
    while true do
		Citizen.Wait(WaitBoucle)
		local playerPos = GetEntityCoords(GetPlayerPed(-1), true)
        local proche = false
		for _,v in pairs(positions)do
	       if Vdist(playerPos.x, playerPos.y, playerPos.z, v.x, v.y, v.z) < 40.0 then
                proche = true
                WaitBoucle = 5
                Marker(v.x, v.y, v.z -1.02,1.0001,232, 84, 25)
                if (Vdist(playerPos.x, playerPos.y, playerPos.z, v.x, v.y, v.z) < 1.0) then
                    if isInTenue then
                        DisplayHelp('Appuyez sur ~INPUT_CONTEXT~ pour reprendre votre tenue') 
                    else
                        DisplayHelp('Appuyez sur ~INPUT_CONTEXT~ pour prendre la tenue de prisonnier')
                    end
                    if (IsControlJustReleased(1, 51)) then 
                        GetService()
                    end
                end
            end
        end
        if not proche then
            WaitBoucle = 1000
        end
	end
end)



function GetService()
	if isInTenue then
		TriggerEvent('hud:NotifColor',"Vous avez fini votre peine de prison",141) 
		TriggerServerEvent("mm:spawn2") 
	else 
		TriggerEvent('hud:Notif',"~r~Vous êtes prisonnier !") 
        if GetEntityModel(GetPlayerPed(-1)) == GetHashKey("mp_f_freemode_01")then
			SetPedComponentVariation(GetPlayerPed(-1), 3, 12, 0, 0)--Gants
			SetPedComponentVariation(GetPlayerPed(-1), 4, 3, 15, 0)--Jean
			SetPedComponentVariation(GetPlayerPed(-1), 6, 4, 2, 0)--Chaussure
			SetPedComponentVariation(GetPlayerPed(-1), 11, 118, 0, 0)--Veste	
			SetPedComponentVariation(GetPlayerPed(-1), 8, 2, 0, 0)--haut
		else
			SetPedComponentVariation(GetPlayerPed(-1), 3, 1, 0, 0)--Gants
			SetPedComponentVariation(GetPlayerPed(-1), 4, 3, 7, 0)--Jean
			SetPedComponentVariation(GetPlayerPed(-1), 6, 31, 2, 0)--Chaussure
			SetPedComponentVariation(GetPlayerPed(-1), 11, 3, 7, 0)--Veste	
			SetPedComponentVariation(GetPlayerPed(-1), 8, 76, 7, 0)--haut
		end
	end
	
	isInTenue = not isInTenue
-- Here for any clothes with SetPedComponentVariation ... 
end
