--[[
##################
#    Oskarr      #
#    MysticRP    #
#   client.lua   #
#      2017      #
##################
--]]

local options = {
    x = 0.1,
    y = 0.2,
    width = 0.2,
    height = 0.04,
    scale = 0.4,
    font = 0,
    menu_title = "Menu Mineur",
    menu_subtitle = "Menu",
    color_r = 10, 
    color_g = 235,
    color_b = 10,
}
local jobId=-1
local isInServiceTaxi = false -- don't edit
local caution = false -- don't edit
local cautionprice = 0 -- caution price for service vehicle
local taxiplatee = " MINEUR " -- Plate for service vehicle
local taximodel = GetHashKey('rubble') -- Model for service car
local taxijob = 4 -- JobID for taxi
local useModelMenu = true -- set to true if you use https://forum.fivem.net/t/release-async-model-menu-v2-6-17-6/19999
local openMenuKey = 166 -- (G) Key for OPEN TAXI MENU 
local emplacement = {
    {name="Entreprise de Mineur", id=473, x=473.164, y=-1952.28, z=24.5773},
}
local zonerecolt = {
    {name="Mine de cuivre", id=89, x=2645.97143554688, y=2806.37280273438, z=33.9922828674316},
    {name="Traitement de cuivre", id=89, x=1218.978, y=1841.154, z=79.131},
    {name="Acheteur de cuivre", id=89, x=-470.49160766602, y=-1716.6346435547, z=18.689130783081},
    {name="Mine d'or", id=270, x=-864.934, y=4453.937, z=18.488},
    {name="Fonderie", id=270, x=1059.929, y=-1976.225, z=30.017},
    {name="Acheteur d'or", id=270, x=539.71685791016, y=-1945.3895263672, z=24.984786987305},
}
local BLIPS = {}
local BLIPSZONE = {}
RegisterNetEvent('recolt:updateJobs')
AddEventHandler('recolt:updateJobs', function(newjob)
    Citizen.CreateThread(function()
        jobId = newjob
        for k, existingBlip in ipairs(BLIPS) do
            RemoveBlip(existingBlip)
            BLIPS = {}
        end
        for k, existingBlip in ipairs(BLIPSZONE) do
            RemoveBlip(existingBlip)
            BLIPSZONE = {}
        end
        isInServiceTaxi = false
        if jobId == taxijob then
            -- Show blip
            for _, item in pairs(emplacement) do
                blip = AddBlipForCoord(item.x, item.y, item.z)
                SetBlipSprite(blip, tonumber(item.id))
                SetBlipAsShortRange(blip, true)
                BeginTextCommandSetBlipName("STRING")
                AddTextComponentString(item.name)
                EndTextCommandSetBlipName(blip)
                table.insert(BLIPS,blip)
            end
            BoucleJob()
        end
    
     end)
end)


---- THRE

---- THREADS ----

-- Service
function BoucleJob()
    Citizen.CreateThread(function()
        local x = 473.164
        local y = -1952.28
        local z = 24.5773
        VMenu.AddMenu( 'Mineur', 'interim',248,157,45) 
        while jobId == taxijob do   
            Citizen.Wait(5)
            x = 473.164
            y = -1952.28
            z = 24.5773
            local playerPos = GetEntityCoords(GetPlayerPed(-1), true)
            if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 100.0) then
                Marker(x, y, z - 1,1.5,128, 77, 0)
                if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 2.0) then
                    if isInServiceTaxi then
                        DisplayHelpText('Appuyez sur ~INPUT_CONTEXT~ pour ~r~stopper~s~ votre service') 
                    else
                        DisplayHelpText('Appuyez sur ~INPUT_CONTEXT~ pour ~g~prendre~s~ votre service')
                    end
                    if (IsControlJustReleased(1, 51)) then 
                        GetService()
                    end
                end
            end
            if isInServiceTaxi then
                x =497.247
                y = -1973.84
                z = 24.910
                if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 100.0) then
                    Marker(x, y, z - 1,3.0,128, 77, 0)
                    if (Vdist(playerPos.x, playerPos.y, playerPos.z, x, y, z) < 4.0) then
                        local ply = GetPlayerPed(-1)
                        if IsPedInAnyVehicle(ply, true) then
                            DisplayHelpText('Appuyez sur ~INPUT_CONTEXT~ pour ~r~ranger~s~ votre ~b~camion')
                            if (IsControlJustReleased(1, 51)) then 
                                local vehicle = GetVehiclePedIsIn(ply, true)
                                local isVehicleTaxi = IsVehicleModel(vehicle, taximodel)
                                local isTaxiPlate = GetVehicleNumberPlateText(vehicle)
                                if isVehicleTaxi then
                                    DeleteTaxi()
                                    caution = false
                                else
                                    TriggerEvent('hud:NotifColor',"Ce n'est pas un camion de mineur !",6)
                                end
                            end
                        else						
                            DisplayHelpText('Appuyez sur ~INPUT_CONTEXT~ pour ~b~sortir~s~ un ~b~camion')
                            if (IsControlJustReleased(1, 51)) then 
                                Taxi()
                                caution = true
                                --TriggerServerEvent("taxi:cautionOn", cautionprice)
                                --Notify("Vous avez laisser ~g~"..cautionprice.."$~s~ de caution pour le ~b~taxi")
                            end
                        end
                    end
                end
                if IsControlJustPressed(1, openMenuKey) then 
                    if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                    VMenu.curItem = 1
                     GPSMenu()
                    VMenu.selection = 1
                    --Notify("~s~Menu mineur ~g~activé~s~")        
                    VMenu.visible = not VMenu.visible
                end
                VMenu.Show()
            end
        end
    end)
end

---- FONCTIONS ----

function Notify(text)
	SetNotificationTextEntry('STRING')
	AddTextComponentString(text)
	DrawNotification(false, false)
end

function DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

---------------------------

function Taxi()
	Citizen.Wait(0)
	local ped = GetPlayerPed(-1)
	local player = PlayerId()
	local vehicle = taximodel

	RequestModel(vehicle)

	while not HasModelLoaded(vehicle) do
		Wait(1)
	end

	--local plate = math.random(300, 900)
    local coords = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0, 5.0, 0)
	local caisse = GetClosestVehicle(497.24746704102, -1973.8449707031, 24.91063117981, 5.000, 0, 70)
    SetEntityAsMissionEntity(caisse, true, true)
    Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(caisse))
    caisseo = GetClosestVehicle(497.24746704102, -1973.8449707031, 24.91063117981, 5.000, 0, 70)
    if DoesEntityExist(caisseo) then
        TriggerEvent('hud:NotifColor','La zone est encombrée',6)
    else
        local spawned_car = CreateVehicle(vehicle, 497.24746704102, -1973.8449707031, 24.91063117981, 122.0, true, false)
        SetVehicleOnGroundProperly(spawned_car)
        SetVehicleColours(spawned_car, 12, 131)
        SetPedIntoVehicle(ped, spawned_car, - 1)
        local id = NetworkGetNetworkIdFromEntity(spawned_car)
                SetNetworkIdCanMigrate(id, true)
                SetNetworkIdExistsOnAllMachines(id,true)
    end
    caisseo = nil
    caisse = nil
    Notify("GPS accessible via ~g~F5")
end

function DeleteTaxi()
    local ply = GetPlayerPed(-1)
    local playerVeh = GetVehiclePedIsIn(ply, false)
    Citizen.Wait(1)
    ClearPedTasksImmediately(ply)
    SetEntityVisible(playerVeh, false, 0)
    SetEntityCoords(playerVeh, 999999.0, 999999.0, 999999.0, false, false, false, true)
    FreezeEntityPosition(playerVeh, true)
    SetEntityAsMissionEntity(playerVeh, 1, 1)
    DeleteVehicle(playerVeh)
end


-------
---------


function GPSMenu()-- FACTURE MENU
    VMenu.ResetMenu()
    VMenu.EditHeader('GPS')
    VMenu.AddFunc("Mine de cuivre", "goC", nil)
	VMenu.AddFunc("Traitement de cuivre", "goPO", nil)
    VMenu.AddFunc("Acheteur de cuivre", "goPF", nil)
    VMenu.AddFunc("Mine d'or", "goA", nil)
	VMenu.AddFunc("Fonderie d'or", "goZ", nil)
    VMenu.AddFunc("Acheteur d'or", "goE", nil)
	VMenu.AddFunc("Fleeca Banque", "goFB", nil)	
end

--------------- GPS COORDS
function goC(x, y, z)
x = 2645.9
y = 2806.3
z = 33.9899
BLIPP = AddBlipForCoord(x, y, z)
SetBlipSprite(BLIPP, 2)
SetNewWaypoint(x, y)
end
function goPO(x, y, z)
x = 1218.9
y = 1841.09
z = 79.1202
BLIPP = AddBlipForCoord(x, y, z)
SetBlipSprite(BLIPP, 2)
SetNewWaypoint(x, y)
end
function goPF(x, y, z)
x = -470.567
y = -1716.7
z = 18.6891
BLIPP = AddBlipForCoord(x, y, z)
SetBlipSprite(BLIPP, 2)
SetNewWaypoint(x, y)
end
function goA(x, y, z)
x = -864.934
y = 4453.937
z = 18.488
BLIPP = AddBlipForCoord(x, y, z)
SetBlipSprite(BLIPP, 2)
SetNewWaypoint(x, y)
end
function goZ(x, y, z)
x = 1059.929
y = -1976.225
z = 30.017
BLIPP = AddBlipForCoord(x, y, z)
SetBlipSprite(BLIPP, 2)
SetNewWaypoint(x, y)
end
function goE(x, y, z)
x=539.71685791016
y=-1945.3895263672
z=24.984786987305
BLIPP = AddBlipForCoord(x, y, z)
SetBlipSprite(BLIPP, 2)
SetNewWaypoint(x, y)
end
function goFB(x, y, z)
x = 152.32469177246
y = -1030.0135498047
z = 29.185220718384
BLIPP = AddBlipForCoord(x, y, z)
SetBlipSprite(BLIPP, 2)
SetNewWaypoint(x, y)
end
--------------------------


function GetService()
    Citizen.CreateThread(function()
    if jobId ~= taxijob then
     TriggerEvent('hud:NotifColor',"Tu n'est pas mineur !",6) 
            return
    end
	if isInServiceTaxi then
		Notify("Vous avez ~r~fini~s~ votre service") 
		if (useModelMenu == true) then
		TriggerServerEvent("mm:spawn2") 
		end
		TriggerServerEvent('taxi:sv_setService', 0) 
        for k, existingBlip in ipairs(BLIPSZONE) do
                RemoveBlip(existingBlip)
                BLIPSZONE = {}
        end
	else 
		TriggerEvent('hud:NotifColor',"Vous êtes en service !",141) 
		Notify("Pensez à prendre votre véhicule !") 
		TriggerServerEvent('taxi:sv_setService', 1) 
            for k, existingBlip in ipairs(BLIPSZONE) do
                RemoveBlip(existingBlip)
                BLIPSZONE = {}
            end
            -- Show blip
            for _, item in pairs(zonerecolt) do
                blip = AddBlipForCoord(item.x, item.y, item.z)
                SetBlipSprite(blip, tonumber(item.id))
                SetBlipAsShortRange(blip, true)
                BeginTextCommandSetBlipName("STRING")
                AddTextComponentString(item.name)
                EndTextCommandSetBlipName(blip)
                table.insert(BLIPSZONE,blip)
            end
	end
	
	isInServiceTaxi = not isInServiceTaxi
-- Here for any clothes with SetPedComponentVariation ...
    SetPedPropIndex(GetPlayerPed(-1), 0, 0, 0, 0)--ear defenders
    if GetEntityModel(GetPlayerPed(-1)) == GetHashKey("mp_f_freemode_01") then
        SetPedComponentVariation(GetPlayerPed(-1), 8, 36, 0, 0)--Gilet Jaune Femme
        SetPedComponentVariation(GetPlayerPed(-1), 6, 63, 0, 0)--Chaussure
    else
        SetPedComponentVariation(GetPlayerPed(-1), 8, 59, 0, 0)--Gilet Jaune Homme
    SetPedComponentVariation(GetPlayerPed(-1), 6, 60, 0, 0)--Chaussure
    end
    end)
end

-- Copy/paste from fs_freeroam (by FiveM-Script : https://forum.fivem.net/t/alpha-fs-freeroam-0-1-4-fivem-scripts/14097)


