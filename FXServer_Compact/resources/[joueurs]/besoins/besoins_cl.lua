local food = 100
local water = 100

faim = food
soif = water

local malusfood =  5
local maluswater = 5
local bonusfood = 100 -- MAX FOOD
local bonuswater = 100 -- MAX WATER
-- INDIVIDUAL TIMERS
local basecompteur = 60000 --1 minutes
 -- temps total des besoin
local foodtotal = 90
local watertotal = 80
-- palier
local foodtimer = math.floor(foodtotal*malusfood/100)
local watertimer = math.floor(watertotal*maluswater/100)

local fooddecompte = 0
local waterdecompte = 0

RegisterNetEvent('playerSpawned')
AddEventHandler("playerSpawned", function()
    TriggerServerEvent("besoin:loadbesoin")
end)

RegisterNetEvent('besoin:resetbesoin')
AddEventHandler('besoin:resetbesoin',function()
    food = 100
    water = 100
    fooddecompte = 0
    waterdecompte = 0
    TriggerEvent('besoin:checkvaleur')
end)

RegisterNetEvent('besoin:checkvaleur')
AddEventHandler('besoin:checkvaleur', function()
    if food <= 0 then
        food = 0
    end
    if food >= 100 then
        food = 100
    end
    if water <= 0 then
        water = 0
    end
    if water >= 100 then
        water = 100
    end
    TriggerEvent('besoin:updatehud')
    if water <= 20 or food <= 20 then
        applyDemarche('move_m@gangster@var_f')
    elseif demarche then
        removeDemarche()
    end
    if water <= 0 or food <= 0 then
        TriggerEvent('besoin:ko')
    end
end)

function applyDemarche(demarche)
    RequestAnimSet(demarche)
	while not HasAnimSetLoaded(demarche) do 
      Citizen.Wait(0)
    end
    SetPedMovementClipset(GetPlayerPed(-1),demarche,1.0)
end

function removeDemarche()
    ResetPedMovementClipset(GetPlayerPed(-1), 0)
end

RegisterNetEvent('besoin:setbesoin')
AddEventHandler('besoin:setbesoin', function(new_food,new_water)
    food = tonumber(new_food)
    water = tonumber(new_water)
    TriggerEvent('besoin:updatehud')
end)

RegisterNetEvent('besoin:updatehud')
AddEventHandler('besoin:updatehud',function()
    faim = food
    soif = water
    TriggerServerEvent('besoin:updateserver',faim,soif)
end)

RegisterNetEvent('besoin:addbesoin')
AddEventHandler('besoin:addbesoin', function(add_food,add_water)
    if (add_food ~= nil) and (add_food ~= 0) then
        fooddecompte = 0
        food = food + add_food
    end
    if (add_water ~= nil) and (add_water ~= 0) then
        waterdecompte = 0
        water = water + add_water
    end
    TriggerEvent('besoin:checkvaleur')
end)

RegisterNetEvent('besoin:deletebesoin')
AddEventHandler('besoin:deletebesoin', function(supp_food,supp_water)
    if add_food ~= nil then
        food = food - add_food
    end
    if add_water ~= nil then
        water = water - add_water
    end
    TriggerEvent('besoin:checkvaleur')
end)

RegisterNetEvent('besoin:ko')
AddEventHandler('besoin:ko', function()
	SetEntityHealth(GetPlayerPed(-1), 0)
end)

-- EMOTES
RegisterNetEvent('besoin:drink')
AddEventHandler('besoin:drink', function()
	ped = GetPlayerPed(-1)
	if ped then
		Citizen.CreateThread(function()
			RequestAnimDict('amb@world_human_drinking_fat@beer@male@idle_a')
		    local pedid = PlayerPedId()
			TaskPlayAnim(pedid, 'amb@world_human_drinking_fat@beer@male@idle_a', 'idle_a', 8.0, -8, -1, 16, 0, 0, 0, 0)
			Citizen.Wait(5000)
			ClearPedTasks(ped)
		end)
	end
end)

RegisterNetEvent('besoin:eat')
AddEventHandler('besoin:eat', function()
	ped = GetPlayerPed(-1)
	if ped then
		Citizen.CreateThread(function()
			RequestAnimDict('amb@code_human_wander_eating_donut@male@idle_a')
		    local pedid = PlayerPedId()
			TaskPlayAnim(pedid, 'amb@code_human_wander_eating_donut@male@idle_a', 'idle_c', 8.0, -8, -1, 16, 0, 0, 0, 0)
			Citizen.Wait(5000)
			ClearPedTasks(ped)
		end)
	end
end)

RegisterNetEvent('besoin:pee')
AddEventHandler('besoin:pee', function()
	ped = GetPlayerPed(-1)
	if ped then
		Citizen.CreateThread(function()
			RequestAnimDict('misscarsteal2peeing')
		    local pedid = PlayerPedId()
			TaskPlayAnim(pedid, 'misscarsteal2peeing', 'peeing_intro', 8.0, -8, -1, 16, 0, 0, 0, 0)
			Citizen.Wait(2000)
			TaskPlayAnim(pedid, 'misscarsteal2peeing', 'peeing_loop', 8.0, -8, -1, 16, 0, 0, 0, 0)
			Citizen.Wait(2000)
			TaskPlayAnim(pedid, 'misscarsteal2peeing', 'peeing_outro', 8.0, -8, -1, 16, 0, 0, 0, 0)
			ClearPedTasks(ped)
		end)
	end
end)


------------- Consommation food
Citizen.CreateThread(function()
    while true do
    Wait(tonumber(basecompteur))
            fooddecompte = fooddecompte + 1
            waterdecompte = waterdecompte + 1
            if fooddecompte == foodtimer then
                food = food - malusfood
                fooddecompte = 0
                TriggerEvent('besoin:checkvaleur')
            end
            if waterdecompte == watertimer then
                waterdecompte = 0
                water = water - maluswater
                TriggerEvent('besoin:checkvaleur')
            end
    end
end)

RegisterNetEvent('DogStopParam')
AddEventHandler('DogStopParam', function()
    malusfood =  0
    maluswater = 0
end)

Citizen.CreateThread(function()
    local loinTemps=2500
    local procheTemps=5
    local wait=procheTemps
    while true do
        Wait(wait)
        wait=loinTemps
        local pos = GetEntityCoords(GetPlayerPed(-1), false)
        local eau = GetHashKey('prop_watercooler')
        local fountain = GetClosestObjectOfType(pos.x, pos.y, pos.z, 20.0, eau, false, false, false)
        if fountain ~=0 then
            wait=procheTemps
            fountain = GetClosestObjectOfType(pos.x, pos.y, pos.z, 1.0, eau, false, false, false)
            if fountain ~= 0 then
                DisplayHelp("Appuyer sur ~INPUT_CONTEXT~ pour boire de l'eau")
                if IsControlJustPressed(1, 51) then
                    TriggerEvent('besoin:addbesoin',0,10)
                    TriggerEvent('besoin:drink')
                    Wait(5000)
                end
            end
        end
    end
end)