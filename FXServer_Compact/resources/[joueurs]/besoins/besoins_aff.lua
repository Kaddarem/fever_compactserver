
local posW = 0.165
local posH = 0.895
local width = 0.01
local height = 0.176
local HudHidden = false

---------------- Position variable ----------------
RegisterNetEvent("es:setFoodDisplay")
AddEventHandler("es:setFoodDisplay", function(val)
    if val == 0 then
        HudHidden = true
    else
        HudHidden = false
    end
end)

RegisterNetEvent("es:setFoodMode")
AddEventHandler("es:setFoodMode",function(mode)
   if mode == "cinema" then
        posW = 0.0
        posH = 0.8
    elseif mode == "simple" then
        posW = 0.0
        posH = 0.895
    else
        posW = 0.165
        posH = 0.895
    end
end)

local coeflarg = 0.65
local espace = 0.011

local faimnormal = { red=238,green=158,blue=0}
local faimfin = {red=238, green = 258, blue = 100}
local faimcolor = {red=faimnormal.red,green=faimnormal.green,blue=faimnormal.blue}
local faimdirection = true
local faimvitesse = 3
local faimtempH = 100

local soifnormal = { red=0,green=127,blue=255}
local soiffin = {red=100, green =227, blue = 255}
local soifcolor = {red=soifnormal.red,green=soifnormal.green,blue=soifnormal.blue}
local soifdirection = true
local soifvitesse =  3
local soiftempH = 100

local soiftrans = {red = false,blue = false,green = false}
local faimtrans = {red = false,blue = false,green = false}

local H
local pos
Citizen.CreateThread(function()
   while true do
        Wait(1)
        if IsRadarHidden() then
            posW = 0.01
        elseif posW ~= 0.165 then
            posW = 0.165
        end
        if not HudHidden then
           if faimtempH < faim then
                faimtempH = faimtempH + 0.1
            elseif faimtempH > faim then
                faimtempH = faimtempH - 0.05
            end
            
            if soiftempH < soif then
                soiftempH = soiftempH + 0.1
            elseif soiftempH > soif then
                soiftempH = soiftempH - 0.05
            end
            ---------- FAIM -----------
            H = faimtempH/100 * height
            pos = posH + ((height/2) * ((100-faimtempH)/100))
            DrawRect(posW, posH, width, height, 0, 0, 0, 100)
            DrawRect(posW, posH, width * coeflarg, height, faimnormal.red, faimnormal.green, faimnormal.blue, 50)
            if faimtempH > 20 then
                faimcolor.red =faimnormal.red
                faimcolor.green=faimnormal.green
                faimcolor.blue=faimnormal.blue
            else
                if faimdirection then
                    faimcolor.blue = faimcolor.blue + faimvitesse
                    faimcolor.red = faimcolor.red + faimvitesse
                    faimcolor.green = faimcolor.green + faimvitesse
                    faimtrans.red = false
                    faimtrans.blue = false
                    faimtrans.green = false
                    if faimcolor.blue > faimfin.blue then
                        faimcolor.blue = faimfin.blue
                        faimtrans.blue = true
                    end
                    if faimcolor.green > faimfin.green then
                        faimcolor.green = faimfin.green
                        faimtrans.green = true
                    end
                    if faimcolor.red > faimfin.red then
                        faimcolor.red = faimfin.red
                        faimtrans.red = true
                    end
                else
                    faimcolor.blue = faimcolor.blue - faimvitesse
                    faimcolor.red = faimcolor.red - faimvitesse
                    faimcolor.green = faimcolor.green - faimvitesse
                    faimtrans.red = false
                    faimtrans.blue = false
                    faimtrans.green = false
                    if faimcolor.blue < faimnormal.blue then
                        faimcolor.blue = faimnormal.blue
                        faimtrans.blue = true
                    end
                    if faimcolor.green < faimnormal.green then
                        faimcolor.green = faimnormal.green
                        faimtrans.green = true
                    end
                    if faimcolor.red < faimnormal.red then
                        faimcolor.red = faimnormal.red
                        faimtrans.red = true
                    end
                end
                if faimtrans.blue and faimtrans.red and faimtrans.green then
                    faimdirection = not faimdirection
                end
            end
            DrawRect(posW, pos, width * coeflarg, H, faimcolor.red, faimcolor.green, faimcolor.blue, 150)
            
            ----------- SOIF -----------
            H = soiftempH/100 * height
            pos = posH + ((height/2) * ((100-soiftempH)/100))
            DrawRect(posW + espace, posH, width, height, 0, 0, 0, 100)
            DrawRect(posW + espace, posH, width * coeflarg, height, soifnormal.red, soifnormal.green, soifnormal.blue, 50)
            if soiftempH > 20 then
               soifcolor.red=soifnormal.red
                soifcolor.green=soifnormal.green
                soifcolor.blue=soifnormal.blue
            else
                if soifdirection then
                    soifcolor.blue = soifcolor.blue + soifvitesse
                    soifcolor.red = soifcolor.red + soifvitesse
                    soifcolor.green = soifcolor.green + soifvitesse
                    soiftrans.red = false
                    soiftrans.blue = false
                    soiftrans.green = false
                    if soifcolor.blue > soiffin.blue then
                        soifcolor.blue = soiffin.blue
                        soiftrans.blue = true
                    end
                    if soifcolor.green > soiffin.green then
                        soifcolor.green = soiffin.green
                        soiftrans.green = true
                    end
                    if soifcolor.red > soiffin.red then
                        soifcolor.red = soiffin.red
                        soiftrans.red = true
                    end
                else
                    soifcolor.blue = soifcolor.blue - soifvitesse
                    soifcolor.red = soifcolor.red - soifvitesse
                    soifcolor.green = soifcolor.green - soifvitesse
                    soiftrans.red = false
                    soiftrans.blue = false
                    soiftrans.green = false
                    if soifcolor.blue < soifnormal.blue then
                        soifcolor.blue = soifnormal.blue
                        soiftrans.blue = true
                    end
                    if soifcolor.green < soifnormal.green then
                        soifcolor.green = soifnormal.green
                        soiftrans.green = true
                    end
                    if soifcolor.red < soifnormal.red then
                        soifcolor.red = soifnormal.red
                        soiftrans.red = true
                    end
                end
                if soiftrans.blue and soiftrans.red and soiftrans.green then
                    soifdirection = not soifdirection
                end
            end
           DrawRect(posW + espace, pos, width * coeflarg, H, soifcolor.red, soifcolor.green, soifcolor.blue, 150)
        end
    end
end)
    