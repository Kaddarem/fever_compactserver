function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end


RegisterServerEvent('besoin:loadbesoin')
AddEventHandler('besoin:loadbesoin', function()
    local player = getPlayerID(source)
    local source = source
    local result = MySQL.Sync.fetchAll("SELECT * FROM users WHERE identifier = @name", {['@name'] = player})
    local food = result[1].food
    local water = result[1].water
    TriggerClientEvent("besoin:setbesoin", source, tonumber(food), tonumber(water))
end)

RegisterServerEvent('besoin:updateserver')
AddEventHandler('besoin:updateserver',function(hud_food,hud_water)
    local player = getPlayerID(source)
    MySQL.Sync.execute("UPDATE users SET `food`=@foodvalue, `water`=@watervalue WHERE identifier = @identifier", {['@foodvalue'] = hud_food, ['@watervalue'] = hud_water, ['@identifier'] = player})
end)