local max_number_weapons = 20 --maximum number of weapons that the player can buy. Weapons given at spawn doesn't count.
local cost_ratio = 100 --Ratio for withdrawing the weapons. This is price/cost_ratio = cost.

RegisterServerEvent('CheckMoneyForWea')
AddEventHandler('CheckMoneyForWea', function(weapon,price)
        local source = source
	TriggerEvent('es:getPlayerFromId', source, function(user)
		if (tonumber(user.money) >= tonumber(price)) then
			local player = user.identifier
			local nb_weapon = 0
			local result = MySQL.Sync.fetchAll("SELECT * FROM user_weapons WHERE identifier = @username",{['@username'] = player})
            if result then
                for k,v in ipairs(result) do
                    nb_weapon = nb_weapon + 1
                end
            end
            if (tonumber(max_number_weapons) > tonumber(nb_weapon)) then
                -- Pay the shop (price)
                user.removeMoney((price))
                MySQL.Sync.execute("INSERT INTO user_weapons (identifier,weapon_model,withdraw_cost) VALUES (@username,@weapon,@cost)",{['@username'] = player, ['@weapon'] = weapon, ['@cost'] = 0})
                -- Trigger some client stuff
                TriggerClientEvent('FinishMoneyCheckForWea',source)
                TriggerClientEvent("es_freeroam:notify", source, "CHAR_MP_ROBERTO", 1, "Roberto", false, "MURDER TIME. FUN TIME!\n")
            else
                TriggerClientEvent('ToManyWeapons',source)
                TriggerClientEvent("es_freeroam:notify", source, "CHAR_MP_ROBERTO", 1, "Roberto", false, "Tu as atteint la limite d'armes ! (max: "..max_number_weapons..")\n")
            end
		else
			-- Inform the player that he needs more money
			TriggerClientEvent("es_freeroam:notify", source, "CHAR_MP_ROBERTO", 1, "Roberto", false, "Tu n'as pas assez d'argent !\n")
		end
	end)
end)

RegisterServerEvent("weaponshop:playerSpawned")
AddEventHandler("weaponshop:playerSpawned", function(spawn)
        local source = source
	TriggerEvent('es:getPlayerFromId', source, function(user)
		TriggerEvent('weaponshop:GiveWeaponsToPlayer', source,user)
	end)
end)

RegisterServerEvent("weaponshop:GiveWeaponsToPlayer")
AddEventHandler("weaponshop:GiveWeaponsToPlayer", function(player,user)
        local user = user
        local player = player
		local playerID = user.identifier
		local delay = nil
		local result = MySQL.Sync.fetchAll("SELECT * FROM user_weapons WHERE identifier = @username",{['@username'] = playerID})
        delay = 2000
        if(result)then
            for k,v in ipairs(result) do
                if (tonumber(user.money) >= tonumber(v.withdraw_cost)) then
                    TriggerClientEvent("giveWeapon", player, v.weapon_model, delay)
                    user.removeMoney((v.withdraw_cost))
	                   TriggerClientEvent("hud:NotifIcon", player, "CHAR_AMMUNATION", 1, "Douane", false, "Voici vos armes")
                else
                    TriggerClientEvent("es_freeroam:notify", player, "CHAR_AMMUNATION", 1, "Douane", false, "Tu n'as pas assez d'argent pour tes armes !\n")
                    return
                end
            end
        end
end)

RegisterServerEvent("weaponshop:GiveWeaponsToPlayerWithoutMoney")
AddEventHandler("weaponshop:GiveWeaponsToPlayerWithoutMoney", function()
        local source = source
	TriggerEvent('es:getPlayerFromId', source, function(user)
		local playerID = user.identifier
		local delay = nil
			
		local result = MySQL.Sync.fetchAll("SELECT * FROM user_weapons WHERE identifier = @username",{['@username'] = playerID})
        delay = 2000
        if(result)then
            for k,v in ipairs(result) do
                    TriggerClientEvent("giveWeapon", player, v.weapon_model, delay)
            end
        end
	end)
end)
