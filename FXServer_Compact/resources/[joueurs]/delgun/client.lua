-- Credits to @Havoc , @Flatracer , @Briglair , and @WolfKnight	on forum.fivem.net for helping me create this
-- Thanks to Ethan Rubinacci and Mark Curry
-- Lots of thanks to @Wolfknight for help with the admin feature
-- Special thanks to @Flatracer

-- Created by Murtaza. If you need help, msg me. The comments are there for people to learn.

-- CLIENTSIDED
-- Register a network event
RegisterNetEvent('ObjectDeleteGunOn') -- Registers the event on the net so that it can be called on a server_script
local toggle = false
local actif = false
AddEventHandler('ObjectDeleteGunOn', function() -- adds an event handler so it can be registered
	if toggle == false then -- checks if toggle is false
		TriggerEvent('hud:Notif',"~g~Delete Gun Activé!") -- activates function drawNotification() with message in parentheses
		GiveWeaponToPed(GetPlayerPed(-1), GetHashKey("WEAPON_STUNGUN"),true, true)
		toggle = true -- sets toggle to true
        boucletoggle()
	else -- if not
		TriggerEvent('hud:Notif','~b~Delete Gun désactivé!')
        RemoveWeaponFromPed(GetPlayerPed(-1), GetHashKey("WEAPON_STUNGUN"))
		toggle = false 
	end
end)

function boucletoggle()
    Citizen.CreateThread(function() -- Creates thread
        while toggle do -- infinite loop
            Citizen.Wait(0) -- wait so it doesnt crash
            if IsPlayerFreeAiming(PlayerId()) then -- checks if player is aiming around
                local entity = getEntity(PlayerId()) -- gets the entity
                if IsPedShooting(GetPlayerPed(-1)) then -- checks if ped is shooting
                    local temp = GetEntityModel(entity)
                    SetEntityAsMissionEntity(entity, true, true) -- sets the entity as mission entity so it can be despawned
                    DeleteEntity(entity) -- deletes the entity
                end
            end
        end
    end)
end

function getEntity(player) --Function To Get Entity Player Is Aiming At
	local result, entity = GetEntityPlayerIsFreeAimingAt(player)
	return entity
end
