-- Manifest

-- Requiring essentialmode
dependency 'essentialmode'

client_scripts {
    '@hud-event/hudevent_aff.lua',
    "@appmenu/menu_cl.lua",
    'douane_client.lua',
}
server_scripts {
    '@mysql-async/lib/MySQL.lua',
    'douane_sv.lua',
}

