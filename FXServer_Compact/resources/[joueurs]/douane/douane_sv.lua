local pattern = "(%d+)/(%d+)/(%d+)"
local max_par_jour = 15000
local argent_par_vote = 1200

RegisterServerEvent('douane:vote')
AddEventHandler('douane:vote', function()
    local source = source
    local player = getPlayerID(source)
    local save = {}
    local total = 0
    MySQL.Async.fetchAll('SELECT * FROM topserveur WHERE steamid = @id', {['@id'] = player}, function(result)
        local Total = 0
        for _,vote in pairs (result) do
            local day,month,_ = string.match(os.date("%x",vote.heure/1000),pattern)
            if save[month] == nil then
                save[month] = {}
            end
            if save[month][day] == nil then
                save[month][day] = 0
            end
            save[month][day] = save[month][day] + argent_par_vote
            if save[month][day] <= max_par_jour then
                total = total + argent_par_vote
            end
         end
        TriggerClientEvent('douane:Retour',source,total)   
    end)
end)

RegisterServerEvent('douane:givemoney')
AddEventHandler('douane:givemoney', function(valeur)
    local source = source
    local player = getPlayerID(source)
    local valeur = valeur
    MySQL.Async.insert("DELETE FROM topserveur WHERE steamid = @id", {['@id'] = player}, function()
        TriggerEvent("es:getPlayerFromId", source, function(user)
            user.addMoney(valeur)
        end)
        TriggerClientEvent('hud:NotifColor',source,"Voici vos $"..tostring(valeur)..". Merci pour vos votes.",141)
    end)
end)
-- get's the player id without having to use bugged essentials
function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

-- gets the actual player id unique to the player,
-- independent of whether the player changes their screen name
function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end