local interiors = {
    [1] = { ["price"] = 1200000,  blip = true,["xe"] = 1394.482, ["ye"] = 1141.740, ["ze"] = 114.606, ["he"] = 266.000, ["xo"] = 1396.1, ["yo"] = 1141.783, ["zo"] =114.8, ["ho"] = 90.92, marker = false,["name"] = 'Ranch Main'},
    
    [3] = { ["price"] = 400000, blip = true, ["xe"] = -1910.1, ["ye"] = -576.7, ["ze"] = 19.29, ["he"] = 318.00, ["xo"] = -1909.7, ["yo"] = -576.27, ["zo"] = 19.29, ["ho"] = 137.00,marker = false, ["name"] = 'Bureau sur Plage'},
    
    [4] = { ["price"] = 1300000, blip = true, ["xe"] = -271.3, ["ye"] = -957.9, ["ze"] = 31.2, ["he"] = 117.00, ["xo"] = -270.53, ["yo"] = -940.73, ["zo"] = 92.51, ["ho"] = 250.00,marker = false, ["name"] = 'Condo de Luxe 1'},
    
    [5] = { ["price"] = 1300000, blip = true, ["xe"] = -44.45, ["ye"] = -587.36, ["ze"] = 38.16, ["he"] = 248.00, ["xo"] = -31.2, ["yo"] = -595.1, ["zo"] = 80.03, ["ho"] = 71.00,marker = false, ["name"] = 'Condo de Luxe 2'},
    
    [6] = { ["price"] = 1200000, blip = false, ["xe"] = -43.4, ["ye"] = -584.69, ["ze"] = 38.16, ["he"] = 248.00, ["xo"] = -18.10, ["yo"] = -590.62, ["zo"] = 90.11, ["ho"] = 162.00,marker = false, ["name"] = 'Condo de Luxe 3'},
    
    [7] = { ["price"] = 1400000, blip = true, ["xe"] = -480.128, ["ye"] = -688.3, ["ze"] = 33.21, ["he"] = 270.00, ["xo"] = -466.6, ["yo"] = -708.7, ["zo"] = 77.1, ["ho"] = 85.00,marker = false, ["name"] = 'Condo de Luxe 4'},
    
    [8] = { ["price"] = 1300000, blip = true, ["xe"] = -796.28, ["ye"] = 336.18, ["ze"] = 85.85, ["he"] = 0.00, ["xo"] = -785.1, ["yo"] = 323.6, ["zo"] = 212.0, ["ho"] = 89.00,marker = true, ["name"] = 'Condo de Luxe 5'},
    
    [13] = { ["price"] = 700000, blip = true, ["xe"] = 119.4, ["ye"] = 564.0, ["ze"] = 183.96, ["he"] = 185.0, ["xo"] = 117.2, ["yo"] = 560.1, ["zo"] = 184.5, ["ho"] = 6.00,marker = false, ["name"] = 'House Premium 1'},
    
    [14] = { ["price"] = 800000, blip = true, ["xe"] = 374.3, ["ye"] = 427.7, ["ze"] = 145.8, ["he"] = 258.0, ["xo"] = 373.7, ["yo"] = 424.2, ["zo"] = 146.0, ["ho"] = 346.00,marker = false, ["name"] = 'House Premium 2'},
    
    [15] = { ["price"] = 900000, blip = true, ["xe"] = -174.2, ["ye"] = 502.65, ["ze"] = 137.5, ["he"] = 101.9, ["xo"] = -174.342, ["yo"] = 498.0, ["zo"] = 137.8, ["ho"] =10.00,marker = false, ["name"] = 'House Premium 3'},
    
    [16] = { ["price"] = 900000, blip = true, ["xe"] = 346.3, ["ye"] = 440.5, ["ze"] = 148.2, ["he"] = 117.00, ["xo"] = 342.3, ["yo"] = 437.9, ["zo"] = 149.5, ["ho"] = 294.00,marker = false, ["name"] = 'House Premium 4'},
    
    [18] = { ["price"] = 1500000, blip = true, ["xe"] = -635.6, ["ye"] = 44.3, ["ze"] = 42.8, ["he"] = 269.00, ["xo"] = -602.9, ["yo"] = 59.0, ["zo"] = 98.3, ["ho"] = 269.00,marker = false, ["name"] = 'Condo de Luxe 6'},
    
    [19] = { ["price"] = 2000000, blip = false, ["xe"] = -777.0, ["ye"] = 320.2, ["ze"] = 85.66, ["he"] =356.00, ["xo"] = -781.9, ["yo"] = 325.3, ["zo"] = 176.8, ["ho"] = 0.00,marker = false, ["name"] = 'Condo de Luxe 7'},
    
    [21] = { ["price"] = 1000000, blip = true, ["xe"] = -1443.8, ["ye"] = -536.0, ["ze"] = 34.7, ["he"] = 306.00, ["xo"] = -1452.1, ["yo"] = -540.7, ["zo"] = 74.0, ["ho"] = 213.00,marker = true, ["name"] = 'Condo de Luxe 8'},
    
    [23] = { ["price"] = 1000000, blip = false, ["xe"] = -1450.7, ["ye"] = -541.0, ["ze"] = 34.7, ["he"] = 120.00, ["xo"] = -1450.5, ["yo"] = -525.1, ["zo"] = 69.5, ["ho"] = 215.00,marker = true, ["name"] = 'Condo de Luxe 9'},
    
    [25] = { ["price"] = 1000000, blip = false, ["xe"] = -1447.65, ["ye"] = -537.3, ["ze"] = 34.7, ["he"] = 29.0, ["xo"] = -1450.4, ["yo"] = -525.0, ["zo"] = 56.9, ["ho"] = 216.00,marker = false, ["name"] = 'Condo de Luxe 10'},
    
    [27] = { ["price"] = 1200000, blip = true, ["xe"] = -889.44, ["ye"] = -333.08, ["ze"] = 34.68, ["he"] = 0.00, ["xo"] = -912.413, ["yo"] = -365.062, ["zo"] = 114.27, ["ho"] = 296.00,marker = true, ["name"] = 'Condo de Luxe 15'},
    
    [29] = { ["price"] = 1200000, blip = false, ["xe"] = -901.75, ["ye"] = -339.16, ["ze"] = 34.68, ["he"] = 48.00, ["xo"] = -907.41, ["yo"] = -371.88, ["zo"] = 109.44, ["ho"] = 207.15,marker = true, ["name"] = 'Condo de Luxe 11'},
    
    [30] = { ["price"] = 1200000, blip = false, ["xe"] = -894.84, ["ye"] = -353.67, ["ze"] = 34.67, ["he"] = 166.00, ["xo"] = -922.113, ["yo"] = -380.273, ["zo"] = 85.48, ["ho"] = 23.00,marker = true, ["name"] = 'Condo de Luxe 12'},
    
    [31] = { ["price"] = 1100000, blip = false, ["xe"] =-844.54, ["ye"] = -391.21, ["ze"] = 31.46, ["he"] = 71.00, ["xo"] = -907.26, ["yo"] = -454.27, ["zo"] = 126.53, ["ho"] = 27.00,marker = true, ["name"] = 'Condo de Luxe 13'},
    
    [32] = { ["price"] = 1200000, blip = true, ["xe"] = -837.70, ["ye"] = -405.88, ["ze"] = 31.47, ["he"] = 161.00, ["xo"] = -890.07, ["yo"] = -452.32, ["zo"] = 95.46, ["ho"] = 113.00,marker = true, ["name"] = 'Condo de Luxe 14'},
    
   -- [33] = { ["price"] = 500000, blip = true, ["xe"] = -3093.06884765625, ["ye"] = 349.211853027344, ["ze"] = 7.53054094314575, ["he"] = 0.00, ["xo"] = -3094.15478515625, ["yo"] = 339.901702880859, ["zo"] = 10.8038291931152, ["ho"] = 0.00, ["name"] = 'Appartement plage 1er'},
    --[34] = { ["price"] = 500000, blip = false, ["xe"] = -3100.38256835938, ["ye"] = 360.864776611328, ["ze"] = 7.59101963043213, ["he"] = 0.00, ["xo"] = -3094.47314453125, ["yo"] = 340.733428955078, ["zo"] = 14.4392118453979, ["ho"] = 0.00, ["name"] = 'Appartement plage 2e'},
}
local entrer = false
local isBuy = 0
local Buyer = nil
local proprio = false
local interior = nil
local Colocs = nil
local ped

---------------------------------------
---------------------------------------
---------------- TEMPORAIRE -----------
---------------------------------------
---------------------------------------

local job = 0

RegisterNetEvent('recolt:updateJobs')
AddEventHandler('recolt:updateJobs', function(newjob)
    job = newjob
end)


--------------------------------------
--------------------------------------
--------------------------------------
local distance = 50.0 -- distance to draw

RegisterNetEvent("apart:Statut")
AddEventHandler("apart:Statut", function(_interior,_isBusy,_Buyer,proprietaire,listColoc)
    isBuy = _isBusy
    Buyer = _Buyer
    proprio = proprietaire
    Colocs = listColoc
    interior = _interior
    VMenu.ResetMenu(interior.name)
    if isBuy == 1 then
        VMenu.AddFunc('Sonner à la porte',"Sonner",nil,"Se présenter")
    elseif isBuy == 2 then
        VMenu.AddFunc('Rentrer chez moi',"Visiter",1)
        if proprietaire then
            VMenu.AddFunc("Gérer colocataire","MenuColoc",nil)
            VMenu.AddFunc("Vendre l'appartement","Vendre",nil)
        end
    else
        VMenu.AddFunc("Acheter".. " pour "..  comma_value(interior.price).."$","Acheter",nil)
        VMenu.AddFunc("Visiter l'appartement","Visiter",0)
    end
end)

function comma_value(n)
	local left,num,right = string.match(n,'^([^%d]*%d)(%d*)(.-)$')
	return left..(num:reverse():gsub('(%d%d%d)','%1 '):reverse())..right
end

function DrawAdvancedText(x,y ,w,h,sc, text, r,g,b,a,font,jus)
    SetTextFont(font)
    SetTextProportional(0)
    SetTextScale(sc, sc)
        N_0x4e096588b13ffeca(jus)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextOutline()
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - 0.1+w, y - 0.02+h)
end


function MenuInsideAppartement()
    VMenu.curItem = 1
    VMenu.ResetMenu()
    VMenu.EditHeader('Gestion')
    if proprio then
        VMenu.AddFunc("Gérer colocataire","MenuColoc",nil)
    end
    VMenu.AddFunc("Sortir","Exit",nil)
end

function Exit()
    DoScreenFadeOut(1000)
    while IsScreenFadingOut() do Citizen.Wait(0) end
    NetworkFadeOutEntity(ped, true, false)
    Wait(1000)
    SetEntityCoords(ped, interior.xe,interior.ye,interior.ze)
    angle = interior.he + 180
	if angle > 360 then angle = angle - 360 end
    SetEntityHeading(ped, angle)
    NetworkFadeInEntity(ped, 0)
	Wait(1000)
	SimulatePlayerInputGait(PlayerId(), 1.0, 100, 1.0, 1, 0)
	DoScreenFadeIn(1000)
	VMenu.visible = false
    interior = nil
    proprio = false
	while IsScreenFadingIn() do Citizen.Wait(0) end
end
 
function MenuAppartement()
    VMenu.curItem = 1
    VMenu.ResetMenu()
    VMenu.EditHeader('Entree')
    for i,k in pairs(interiors) do
        if Vdist(GetEntityCoords(ped), interiors[i].xe,interiors[i].ye,interiors[i].ze) < 1.599 then
            TriggerServerEvent("apart:getAppart", interiors[i])
            VMenu.AddSep('Veuillez Patienter')
        end
    end
end
 
function CloseMenu()
    VMenu.visible = false  
end

local attente = false

function Sonner()
    DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP8S", "", "", "", "", "", 20)
    while (UpdateOnscreenKeyboard() == 0) do
	    DisableAllControlActions(0)
		Wait(0)
	end
	if (GetOnscreenKeyboardResult()) then
	    local nom = tostring(GetOnscreenKeyboardResult())
        if string.len(nom) > 0 then
	        TriggerEvent('InteractSound_CL:PlayOnOne', 'sonnette', 0.3)
            if Buyer ~= nil then
		      TriggerServerEvent('apart:Sonner',interior.name,Buyer,nom)
            end
            TriggerEvent("hud:NotifIcon", "CHAR_MARNIE", 1, "DYNASTY8", false, "~g~Propriétaire prévenu")
            attente = true
            Citizen.CreateThread(function()
                local compteur = 0
                while compteur < 1000 do
                    Wait(5)
                    compteur = compteur +1
                end
                if attente then
                    attente = false
                    TriggerEvent('hud:NotifColor',"Pas de réponse",6)
                end
            end)
        else
            TriggerEvent('hud:NotifColor',"Merci de vous présenter",6)
        end
    end  
end

RegisterNetEvent('apart:ReceiveSonner')
AddEventHandler('apart:ReceiveSonner', function(appartement,cible,nom)
    local Name = ""
    if nom ~= nil then
        Name = nom
    end
	TriggerEvent('InteractSound_CL:PlayOnOne', 'sonnette', 0.3)
    TriggerEvent('hud:Notif',"~g~~h~"..Name.."~s~ sonne à ~g~~h~"..appartement)
    TriggerEvent('hud:NotifKey',"~INPUT_MP_TEXT_CHAT_TEAM~", "~g~Faire entrer")
    Citizen.Wait(100)
    TriggerEvent('hud:NotifKey',"~INPUT_REPLAY_ENDPOINT~", "~r~Ignorer")
    local compte = 0
    while compte < 1000 do
        Wait(5)
        if IsControlJustReleased(1, 246) then
            TriggerServerEvent('apart:ReponseSonner', cible, 1)
            break
        end
        if IsControlJustReleased(1, 306) then
            TriggerServerEvent('apart:ReponseSonner', cible, 0)
            break
        end
        compte = compte + 1
    end
    TriggerEvent('hud:NotifDel')
end)

RegisterNetEvent('apart:EntrerSonner')
AddEventHandler('apart:EntrerSonner', function(reponse)
    if reponse == 1 then
        attente = false
        TriggerEvent('hud:NotifColor',"Vous pouvez entrer",141)
        Citizen.Wait(1000)
        Visiter()
    end     
end)

function Vendre()
    TriggerEvent('hud:NotifKey',"~INPUT_MP_TEXT_CHAT_TEAM~", "~g~Confirmer la vente")
    Wait(100)
    TriggerEvent('hud:NotifKey',"~INPUT_REPLAY_ENDPOINT~", "~r~Annuler la vente")
    while true do
        Wait(5)
		if IsControlJustReleased(1, 246) then
			--TriggerServerEvent('bank:withdrawAmende', amount)
            TriggerEvent('hud:NotifDel')
            TriggerServerEvent("apart:sellAppart", interior.name, interior.price)
            CloseMenu()
            break
        end
		if IsControlJustReleased(1, 306) then
            TriggerEvent('hud:NotifDel')
			break
        end
    end
end

function Acheter()
    -- TriggerEvent('hud:Nofif','Cette fonctionnalite arrivera tres vite')
    TriggerEvent('hud:NotifKey',"~INPUT_MP_TEXT_CHAT_TEAM~", "~g~Confirmer l'achat")
    Wait(100)
    TriggerEvent('hud:NotifKey',"~INPUT_REPLAY_ENDPOINT~", "~r~Annuler l'achat")
    while true do
        Wait(5)
		if IsControlJustReleased(1, 246) then
			--TriggerServerEvent('bank:withdrawAmende', amount)
            TriggerEvent('hud:NotifDel')
            TriggerServerEvent("apart:buyAppart", interior.name, interior.price)
            CloseMenu()
            break
        end
		if IsControlJustReleased(1, 306) then
            TriggerEvent('hud:NotifDel')
			break
        end
    end
    
end
 
function Visiter()
    DoScreenFadeOut(1000)
    while IsScreenFadingOut() do Citizen.Wait(0) end
    NetworkFadeOutEntity(ped, true, false)
    Wait(1000)
    SetEntityCoords(ped, interior.xo,interior.yo,interior.zo)
    local angle = interior.ho + 180
    if angle > 360 then angle = angle - 360 end
    SetEntityHeading(ped, angle)
    NetworkFadeInEntity(ped, 0)
    Wait(1000)
    SimulatePlayerInputGait(PlayerId(), 1.0, 100, 1.0, 1, 0)
    DoScreenFadeIn(1000)
    VMenu.visible = false
    while IsScreenFadingIn() do Citizen.Wait(0) end
end

function MenuColoc()
    VMenu.ResetMenu()
    VMenu.ResetCursor()
    VMenu.AddFunc("Ajouter un colocataire","AddColoc",nil)
    VMenu.AddSep("Colocataires actuels")
    for int,coloc in pairs(Colocs) do
        if int > 1 then
            VMenu.AddFunc(coloc.nom,"delColoc",coloc.identifier,"Récupérer les clés")
        end
    end
end
 
function AddColoc()
    TriggerEvent('DisplayInputHTML',"Entrez le numéro de compte du colocataire","number",3, function(id_coloc)
        local id_coloc = tonumber(id_coloc)
        if id_coloc > 0 then
            TriggerServerEvent("Appart:AddColoc",id_coloc,interior)
        else
            TriggerEvent('hud:NotifColor',"Erreur de saisie",6)
        end
    end)
    VMenu.visible = false
end

function delColoc(id)
    TriggerServerEvent('appart:DelColoc',id,interior)
    VMenu.visible = false
end

Citizen.CreateThread(function()
    while GetEntityModel(GetPlayerPed(-1)) ~= GetHashKey("mp_f_freemode_01") and GetEntityModel(GetPlayerPed(-1)) ~= GetHashKey("mp_m_freemode_01") do
        Wait(500)
    end
    AffBlip()
    VMenu.AddMenu( 'Appartement', 'appartement',48,115,76)
    local MenuEntree = {}
    local MenuSortie = {}
    local BoucleLent = 5000
    local BoucleCourt = 5
    local TempsBoucle = BoucleCourt
    local pos = nil
    ped = GetPlayerPed(-1)
    while true do
        Citizen.Wait(TempsBoucle)
        TempsBoucle = BoucleLent
        pos = GetEntityCoords(ped)
        for i,k in pairs(interiors) do
            if not IsEntityDead(ped) then
                if Vdist(pos, k.xe,k.ye,k.ze) < distance then
                    TempsBoucle = BoucleCourt
                    DrawIcon({x=k.xe,y=k.ye,z=k.ze},k.he,0.3,Vdist(pos, k.xe,k.ye,k.ze))
                    if k.marker then
                         Marker(k.xe,k.ye,k.ze-1.0001,1.5,212, 189, 0)    
                    end
                    if Vdist(pos, k.xe,k.ye,k.ze) < 1.599 then
                        DisplayHelp("Pressez ~INPUT_CONTEXT~ pour entrer")
                        if IsControlJustPressed(1, 51) then
                            if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                            VMenu.curItem = 1
                            MenuAppartement()
                            VMenu.visible = not VMenu.visible
                            MenuEntree[i] = true
                        end
                    elseif MenuEntree[i] then
                        VMenu.visible = false
                        MenuEntree[i] = false
                    end
                end
                if Vdist(pos, k.xo,k.yo,k.zo) < distance then
                    TempsBoucle = BoucleCourt
                    DrawIcon({x=k.xo,y=k.yo,z=k.zo},k.ho,0.3)
                    if Vdist(pos, k.xo,k.yo,k.zo, true) < 1.599 then
                        interior = interiors[i]
                        DisplayHelp("Pressez ~INPUT_CONTEXT~ pour sortir")
                        if IsControlJustPressed(1, 51) then
                            if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                            VMenu.curItem = 1
                            Exit()
                            --VMenu.visible = not VMenu.visible
                            --MenuSortie[i] = true
                        end
                    elseif MenuSortie[i] then
                        VMenu.visible = false
                        MenuSortie[i] = false
                    end
                end
            end
        end
    end
end)

local BlipSurMap = false
local Blips = {}

function AffBlip()
    for _,blip in pairs(Blips) do
        RemoveBlip(blip)
    end
    for _, item in pairs(interiors) do
        if item.blip then
            local blip = AddBlipForCoord(item.xe, item.ye, item.ze)
            SetBlipSprite(blip, 375)
            SetBlipAsShortRange(blip, true)
            if not BlipSurMap then
                SetBlipDisplay(blip,9)   
            end
            BeginTextCommandSetBlipName("STRING")
            SetBlipColour(blip, 32)
            --SetBlipScale(item.blip, 1)
            AddTextComponentString("Appartement")
            EndTextCommandSetBlipName(blip)
            table.insert(Blips,blip)
        end
    end
    BlipSurMap = not BlipSurMap
end

RegisterNetEvent('Appartement:Blips')
AddEventHandler('Appartement:Blips',function()
    AffBlip()
end)

function DrawIcon(coord,angle,size,distance)
    local fichier = "iconhud"
    if not HasStreamedTextureDictLoaded(fichier) then
        RequestStreamedTextureDict(fichier, true)
        while not HasStreamedTextureDictLoaded(fichier) do
            Wait(10)
        end
    end
    local opacite = 100
    if distance ~= nil then
        opacite = 100*(1- (distance -3 ) /15.0)
        if opacite < 0 then opacite = 0 end
        if opacite > 100 then opacite = 100 end
    end
    DrawMarker(8, coord.x , coord.y,coord.z, 0.0, 0.0, 0.0, 90.0,angle, 180.0, size,size,size,255,255,255, math.floor(opacite), false, false, false,false,fichier,"porte",false)
end