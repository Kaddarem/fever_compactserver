local isBuy = 0

RegisterServerEvent("apart:getAppart")
AddEventHandler('apart:getAppart', function(interior)
    local source = source
    local player = getPlayerID(source)
    local name = interior.name
    local proprio = false
    MySQL.Async.fetchAll("SELECT user_appartement.* , CONCAT(users.prenom,' ',users.nom) AS nom FROM user_appartement LEFT JOIN users ON user_appartement.identifier = users.identifier WHERE name = @nom ORDER BY autoinc_app", {['@nom'] = tostring(name)}, function (result)
        if (result) then
            if result[1] ~= nil then
                for _,list in pairs (result) do
                    if (list.identifier == player) then
                        proprio = list.proprio
                        TriggerClientEvent('apart:Statut', source,interior,2,nil,proprio,result)
                        return
                    end
                end
                TriggerEvent('es:getPlayers', function(Users)
                    local find = false
                    for _,user in pairs (Users) do
                        if getPlayerID(user.source) == result[1].identifier then
                            find = true
                            TriggerClientEvent('apart:Statut',source,interior,1,user.source,proprio,result)
                        end
                    end
                    if not find then
                        TriggerClientEvent('apart:Statut', source,interior,1,nil,proprio,result)
                    end
                end)
            else
                TriggerClientEvent('apart:Statut', source,interior,0,nil,proprio,result)
            end
        end
    end)
end)


RegisterServerEvent("apart:buyAppart")
AddEventHandler('apart:buyAppart', function(name, price)
    local source = source
    local player = getPlayerID(source)
    local name = name
    local price = price
    local argent = MySQL.Sync.fetchScalar("SELECT bankbalance FROM users WHERE identifier = @name", {['@name'] = player})
    if (tonumber(argent) >= tonumber(price)) then
        TriggerClientEvent("banking:paywithbankclient",source, price)
        MySQL.Sync.execute("INSERT INTO user_appartement (`identifier`, `name`, `price`, proprio) VALUES (@username, @name, @price,1)", {['@username'] = player, ['@name'] = name, ['@price'] = price})
    	TriggerClientEvent("hud:NotifIcon", source, "CHAR_MARNIE", 1, "DYNASTY8", false, "Bienvenue dans votre appartement")
    	TriggerClientEvent('apart:isMine', source)
    else
    	TriggerClientEvent("hud:NotifIcon", source, "CHAR_MARNIE", 1, "DYNASTY8", false, 'Vous n\'avez pas assez d\'argent!') --WITH LALIFE SCRIPTS
    end
end)

RegisterServerEvent("apart:sellAppart")
AddEventHandler('apart:sellAppart', function(name, price)
    local source = source
    TriggerEvent('es:getPlayerFromId', source, function(user)
        local player = user.identifier
        local name = name
        local price = price/2
        user.addMoney((price))
        MySQL.Sync.execute("DELETE from user_appartement WHERE name = @name", {['@name'] = name})
        TriggerClientEvent("hud:NotifIcon", source, "CHAR_MARNIE", 1, "DYNASTY8", false, 'Appartement vendu') --WITH LALIFE SCRIPTS
        TriggerClientEvent('apart:isNotBuy', source)
    end)
end)

RegisterServerEvent('apart:Sonner')
AddEventHandler('apart:Sonner', function(appartement,Buyer,nom)
    TriggerClientEvent('apart:ReceiveSonner',Buyer,appartement,source,nom)
end)

RegisterServerEvent('apart:ReponseSonner')
AddEventHandler('apart:ReponseSonner',function(cible,reponse)
    if reponse == 1 then
        TriggerClientEvent('apart:EntrerSonner',cible,1)
    end
end)

RegisterServerEvent('Appart:AddColoc')
AddEventHandler('Appart:AddColoc', function(id_coloc,interior)
    local source = source
    local name = interior.name
    if getPlayerID(id_coloc) ~= nil then
        local player = getPlayerID(id_coloc)
        TriggerClientEvent("hud:NotifIcon", id_coloc, "CHAR_MARNIE", 1, "DYNASTY8", false, "Voici le double des clés de "..name)
        TriggerClientEvent("hud:NotifIcon", source, "CHAR_MARNIE", 1, "DYNASTY8", false, "Double des clés envoyé")
        MySQL.Async.insert("INSERT INTO user_appartement (`identifier`, `name`, `price`, proprio) VALUES (@username, @name, 0,0)", {['@username'] = player, ['@name'] = name})
    else
        TriggerClientEvent('hud:NotifColor',source,"Numéro de compte invalide",6)
    end
end)

RegisterServerEvent('appart:DelColoc')
AddEventHandler('appart:DelColoc', function(identifier,interior)
    local identifier = identifier
    local interior = interior
    local source = source
    MySQL.Async.insert('DELETE FROM user_appartement WHERE identifier = @id AND name = @name',{['@id'] = identifier,['@name']= interior.name}, function()
        TriggerClientEvent("hud:NotifIcon", source, "CHAR_MARNIE", 1, "DYNASTY8", false, "Double des clés récupéré")    
    end)
end)

-- get's the player id without having to use bugged essentials
function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

-- gets the actual player id unique to the player,
-- independent of whether the player changes their screen name
function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end
