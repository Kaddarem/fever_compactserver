local dressing = {
    {x = -899.767, y = -433.925, z = 88.264},
    {x = -889.205, y = -444.598, z = 119.337},
    {x = 1398.912, y = 1164.402, z = 113.333},
    {x = -1902.297, y = -572.577, z = 18.097},
    {x = -278.103, y = -960.413, z = 85.314},
    {x = -37.252, y = -584.092, z = 82.918},
    {x = -37.856, y = -588.698, z = 77.83},
    {x = -467.014, y = -688.824, z = 69.89},
    {x = -761.499, y = 325.247, z = 169.607},
    {x = 121.808, y = 548.267, z = 179.497},
    {x = 374.472, y = 411.445, z = 141.1},
    {x = -167.24, y = 487.127, z = 132.84},
    {x = -594.584, y = 55.258, z = 95.999},
    {x = -1466.767, y = -536.726, z = 49.732},
    {x = -1450.24, y = -549.502, z = 71.843},
    {x = -1467.659, y = -537.563, z = 62.36},
    {x = -903.535, y = -364.842, z = 112.074},
    {x = -925.82, y = -381.255, z = 102.243},
    {x = -904.403, y = -369.857, z = 78.283},
    {x = -793.47, y = 327.18, z = 210.79},
    {x = 429.736, y = -811.318, z = 28.491}
}

local NbreTenueMax = 3
local SkinActif = {}

AddEventHandler('onClientResourceStart', function(res)
    if res == GetCurrentResourceName() then
    end
end)

Citizen.CreateThread(function()
    VMenu.AddMenu( "Dressing", "dressing",255,255,255) 
    local BoucleLent = 5000
    local BoucleCourt = 5
    local TempsBoucle = BoucleCourt
    local Bouttons = {
        {'~INPUT_CELLPHONE_RIGHT~',"Renommer la tenue"},
    }
    while true do
        Wait(TempsBoucle)
        TempsBoucle = BoucleLent
        for key, coord in pairs(dressing) do
            if Vdist(coord.x, coord.y, coord.z, GetEntityCoords(GetPlayerPed(-1))) < 50.0  then
                TempsBoucle = BoucleCourt
                if Vdist(coord.x, coord.y, coord.z, GetEntityCoords(GetPlayerPed(-1))) < 5.0 then
                    if Vdist(coord.x, coord.y, coord.z, GetEntityCoords(GetPlayerPed(-1))) < 2.0 then
                        DisplayHelp("Pressez ~INPUT_CONTEXT~ pour vous changer")
                        if IsControlJustPressed(1, 51) and not VMenu.visible then
                            TriggerServerEvent('dressing:Load')
                            if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                            VMenu.visible = not VMenu.visible
                            VMenu.curItem = 1
                        elseif IsControlJustPressed(1,51) and VMenu.visible then
                            VMenu.visible = false
                            UpdateAll(SkinActif)
                        end
                        if IsControlJustPressed(1,175) and VMenu.visible then
                            renommerTenue()
                        end
                        if VMenu.visible then
                            TriggerEvent('hud:SetButton',Bouttons, 0)
                            TriggerEvent('hud:RenderButton')     
                        end
                        VMenu.Show()
                    elseif VMenu.visible then
                        VMenu.visible = false
                        UpdateAll(SkinActif)
                    end
                end
            end 
        end
    end     
end)

RegisterNetEvent('dressing:OpenMenu')
AddEventHandler('dressing:OpenMenu',function(skins)
    UpdateAll(skins[VMenu.curItem])
    VMenu.ResetMenu()
    VMenu.EditHeader("Dressing")
    for int,skin in pairs (skins) do
        if skin.Actif == 1 then
            SkinActif = skin
            if skin.nom == "Tenue" then
                VMenu.AddFunc("~g~Tenue "..int,"SelectSkin",{skins,int},"S'habiller")
            else
                VMenu.AddFunc("~g~"..skin.nom,"SelectSkin",{skins,int},"S'habiller")   
            end
        else
            if skin.nom == "Tenue" then
                VMenu.AddFunc("Tenue "..int,"SelectSkin",{skins,int},"S'habiller")
            else
                VMenu.AddFunc(skin.nom,"SelectSkin",{skins,int},"S'habiller")   
            end
        end
    end
    VMenu.AddFunc("~g~Ajouter une tenue","AddSkin",tonumber(#skins))
    
end)

function AddSkin(NbSkins)
    if NbSkins >= NbreTenueMax then
        TriggerEvent('hud:NotifColor',"Acheter une tenue supplémentaire pour ~g~~h~1.99€~h~ ~s~sur ~h~~g~www.fever-rp.fr",15)
    else
        TriggerServerEvent('dressing:AddSlot')
    end
end

function SelectSkin(arg)
    local Skins = arg[1]
    for int,skin in pairs(Skins) do
        Skins[int].Actif = 0
    end
    Skins[arg[2]].Actif = 1
    local skin = Skins[arg[2]]
    TriggerServerEvent('dressing:Update',skin.autoinc_skin) 
    TriggerEvent('ch:update',skin)
    TriggerEvent('dressing:OpenMenu',Skins)
    TriggerEvent('hud:NotifColor',"Bon choix !",141)
end

function renommerTenue()
    TriggerEvent('DisplayInputHTML',"Entrez le nom de la tenue","text",60, function(res)
        if res ~= 0 and res ~= nil then
            local arg = VMenu.items[VMenu.curItem].params
            local Skins = arg[1]
            local skin = arg[1][arg[2]]
            Skins[arg[2]].nom = res
            TriggerEvent('hud:NotifColor','Tenue renommée',141)
            TriggerEvent('dressing:OpenMenu',Skins)
            TriggerServerEvent('Dressing:UpdateNom',skin.autoinc_skin,res)    
        else
            TriggerEvent('hud:NotifColor','Erreur de saisie',6)
        end
    end)
end
---------------------------
-------- APERCU ------------
---------------------------
function UpdateSkin_down()
    local func = VMenu.items[VMenu.curItem].func
    local arg = VMenu.items[VMenu.curItem].params 
    if func == "SelectSkin" then
        local skin = arg[1][arg[2]]
        UpdateAll(skin)
    end
end

function UpdateSkin_up()
    local func = VMenu.items[VMenu.curItem].func
    local arg = VMenu.items[VMenu.curItem].params
    
    if func == "SelectSkin" then
        local skin = arg[1][arg[2]]
        UpdateAll(skin)
    end
end

function UpdateAll(Skin)
    Citizen.CreateThread(function()
        SetPedHeadBlendData(GetPlayerPed(-1), tonumber(Skin.TetePere), tonumber(Skin.TeteMere), 0, tonumber(Skin.TetePere), tonumber(Skin.TeteMere), 0,Skin.RessPere/100,Skin.RessPere/100, 0.0, false)
        SetPedHeadOverlay(GetPlayerPed(-1), 12, tonumber(Skin.Imp), 1.0)
        SetPedHeadOverlay(GetPlayerPed(-1), 3, tonumber(Skin.Ride), 1.0)
        SetPedHeadOverlay(GetPlayerPed(-1), 0, tonumber(Skin.Defaut), 1.0)
        SetPedHeadOverlay(GetPlayerPed(-1), 9, tonumber(Skin.Tache), 1.0)
        SetPedHeadOverlay(GetPlayerPed(-1), 1, tonumber(Skin.Barbe), 1.0)
        SetPedHeadOverlayColor(GetPlayerPed(-1), 1, 1, tonumber(Skin.BarbeColor), 1)
        SetPedEyeColor(GetPlayerPed(-1),tonumber(Skin.Yeux))
        SetPedHeadOverlay(GetPlayerPed(-1), 7, tonumber(Skin.Blush), 1.0)
        SetPedComponentVariation(GetPlayerPed(-1), 2, tonumber(Skin.Coupe), 0, 0)
        SetPedHairColor(GetPlayerPed(-1), tonumber(Skin.CoupeColor), tonumber(Skin.CoupeSecColor))
        SetPedHeadOverlay(GetPlayerPed(-1), 6, tonumber(Skin.Soleil), 1.0)
        SetPedHeadOverlay(GetPlayerPed(-1), 5, tonumber(Skin.Teint), 1.0)
        SetPedHeadOverlayColor(GetPlayerPed(-1), 5, 1, tonumber(Skin.TeintColor), 1)
        SetPedHeadOverlay(GetPlayerPed(-1), 4, tonumber(Skin.Maqui), 1.0)
        SetPedHeadOverlayColor(GetPlayerPed(-1), 4, 1, tonumber(Skin.MaquiColor), 1)
        SetPedHeadOverlay(GetPlayerPed(-1), 10, tonumber(Skin.Poit), 1.0)
        SetPedHeadOverlayColor(GetPlayerPed(-1), 10, 1, tonumber(Skin.PoitColor), 1)
        SetPedHeadOverlay(GetPlayerPed(-1), 8, tonumber(Skin.Levre), 1.0)
        SetPedHeadOverlayColor(GetPlayerPed(-1), 8, 1, tonumber(Skin.LevreColor), 1)
        SetPedHeadOverlay(GetPlayerPed(-1), 2, tonumber(Skin.Sourcil), 1.0)
        SetPedHeadOverlayColor(GetPlayerPed(-1), 2, 1, tonumber(Skin.SourcilColor), 1)
        SetPedPropIndex(GetPlayerPed(-1),0, tonumber(Skin.Chapeau), tonumber(Skin.ChapeauColor), 0)  
        SetPedPropIndex(GetPlayerPed(-1),1, tonumber(Skin.Lun), tonumber(Skin.LunColor), 0)  
        SetPedPropIndex(GetPlayerPed(-1),2, tonumber(Skin.Pier), tonumber(Skin.PierColor), 0) 
        SetPedPropIndex(GetPlayerPed(-1),6, tonumber(Skin.Montre), tonumber(Skin.MontreColor), 0) 
        SetPedPropIndex(GetPlayerPed(-1),7, tonumber(Skin.Bracelet), tonumber(Skin.BraceletColor), 0) 
        SetPedComponentVariation(GetPlayerPed(-1), 11, tonumber(Skin.Veste), tonumber(Skin.VesteColor), 0) 
        SetPedComponentVariation(GetPlayerPed(-1), 8, tonumber(Skin.Haut), tonumber(Skin.HautColor), 0) 
        SetPedComponentVariation(GetPlayerPed(-1), 3, tonumber(Skin.Mains), tonumber(Skin.MainsColor), 0) 
        SetPedComponentVariation(GetPlayerPed(-1), 4, tonumber(Skin.Pant), tonumber(Skin.PantColor), 0) 
        SetPedComponentVariation(GetPlayerPed(-1), 6, tonumber(Skin.Chaus), tonumber(Skin.ChausColor), 0) 
        SetPedComponentVariation(GetPlayerPed(-1), 7, tonumber(Skin.Coll), tonumber(Skin.CollColor), 0) 
        SetPedComponentVariation(GetPlayerPed(-1), 1, 0, 1, 0)
    end)
end


