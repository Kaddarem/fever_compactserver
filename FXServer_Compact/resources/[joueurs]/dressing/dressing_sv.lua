RegisterServerEvent('dressing:Update')
AddEventHandler('dressing:Update', function(ID)
    local source = source
    local ID = ID
    local player = getPlayerID(source)
    MySQL.Async.insert("UPDATE modelmenu SET Actif = 0 WHERE identifier = @id and Actif = 1",{['@id']=player}, function()
    MySQL.Async.insert("UPDATE modelmenu SET Actif = 1 WHERE identifier = @id and autoinc_skin = @auto",{['@id']=player,['@auto']=ID})
                
    end)
end)

RegisterServerEvent('dressing:Load')
AddEventHandler('dressing:Load', function()
    local source = source
    local player = getPlayerID(source)
    local skin = MySQL.Sync.fetchAll('Select * FROM modelmenu WHERE identifier = @id ORDER BY autoinc_skin ASC', {['@id']=player})
    TriggerClientEvent('dressing:OpenMenu',source,skin)
end)

RegisterServerEvent('dressing:AddSlot')
AddEventHandler('dressing:AddSlot', function()
    local source = source
    local player = getPlayerID(source)
    MySQL.Sync.execute('INSERT INTO modelmenu(`identifier`, `Actif`, `Finish`, `Sexe`, `TetePere`, `TeteMere`, `RessPere`, `Imp`, `Ride`, `Defaut`, `Tache`, `Barbe`, `BarbeColor`, `Lun`, `LunColor`, `Pier`, `PierColor`, `Veste`, `VesteColor`, `mask`, `mask_txt`, `Coupe`, `CoupeColor`, `Chaus`, `ChausColor`, `Pant`, `PantColor`, `Haut`, `HautColor`, `Mains`, `MainsColor`, `Yeux`, `Blush`, `Soleil`, `Teint`, `TeintColor`, `Maqui`, `MaquiColor`, `Poit`, `PoitColor`, `Levre`, `LevreColor`, `Sourcil`, `SourcilColor`, `Chapeau`, `ChapeauColor`, `Coll`, `CollColor`) SELECT `identifier`, REPLACE(`Actif`,1,0), `Finish`, `Sexe`, `TetePere`, `TeteMere`, `RessPere`, `Imp`, `Ride`, `Defaut`, `Tache`, `Barbe`, `BarbeColor`, `Lun`, `LunColor`, `Pier`, `PierColor`, `Veste`, `VesteColor`, `mask`, `mask_txt`, `Coupe`, `CoupeColor`, `Chaus`, `ChausColor`, `Pant`, `PantColor`, `Haut`, `HautColor`, `Mains`, `MainsColor`, `Yeux`, `Blush`, `Soleil`, `Teint`, `TeintColor`, `Maqui`, `MaquiColor`, `Poit`, `PoitColor`, `Levre`, `LevreColor`, `Sourcil`, `SourcilColor`, `Chapeau`, `ChapeauColor`, `Coll`, `CollColor` FROM modelmenu WHERE identifier = @id and Actif = 1',{['@id']=player}, function()
        local skin = MySQL.Sync.fetchAll('Select * FROM modelmenu WHERE identifier = @id', {['@id']=player})
        TriggerClientEvent('dressing:OpenMenu',source,skin)
    end)

end)

RegisterServerEvent('Dressing:UpdateNom')
AddEventHandler('Dressing:UpdateNom', function(autoinc_skin,nom)
    MySQL.Async.insert('UPDATE modelmenu set nom = @nom WHERE autoinc_skin = @auto',{['@nom'] = nom,['@auto'] = autoinc_skin})
end)

-- get's the player id without having to use bugged essentials
function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

-- gets the actual player id unique to the player,
-- independent of whether the player changes their screen name
function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end
