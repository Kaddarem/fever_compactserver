ui_page 'html/ui.html'

files ({
	'html/ui.html',
	'html/SourceSansPro-Black.ttf',
	'html/SourceSansPro-Light.ttf',
	'html/logo.png',
	'html/wallpaper.png',
	'html/wallpaper_help.png',
	'html/script.js',
	'html/styles.css'
})

client_script "bienvenue_client.lua"

server_script "bienvenue_server.lua"

export 'HelpOuvert'
