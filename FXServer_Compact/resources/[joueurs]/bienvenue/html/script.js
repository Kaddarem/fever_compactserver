function NextPage() {
    document.getElementById('info').style.display = "none";
    document.getElementById('info2').style.display = "block";
}

function PrevPage() {
    document.getElementById('info2').style.display = "none";
    document.getElementById('info').style.display = "block";
}

function Muclose(e) {
    if (e.keyCode == 69) {
        $.post('http://bienvenue/closehelp', JSON.stringify({})); 
    }   
}

$(function() {
    function RemplirSteamId(steamid) {
        document.getElementById('topserveur').contentWindow.location.reload();
        var myIframe = document.getElementById('topserveur');
        myIframe.addEventListener("load", function() {
            if (this.contentWindow.document.getElementById('playername') != null) {
                this.contentWindow.document.getElementById('playername').value = steamid;
            }
        }); 
    }
    function Close() {
            $("#helppage").css("display", "none");
            $("#contenu").css("background-color", "rgba(0, 0, 0, 0.0)");
            $("#accueil").css("display", "none");
    }
    window.addEventListener('message', function(event) {
        // Open & Close main bank window
        if(event.data.help === true) {
            $("#helppage").css("display", "block");
            $("#contenu").css("background-color", "rgba(0, 0, 0, 0.7)");
            RemplirSteamId(event.data.steamid);
        }
        if(event.data.accueil === true) {
            $("#accueil").css("display", "block");
            $("#contenu").css("background-color", "rgba(0, 0, 0, 0.7)");
            RemplirSteamId(event.data.steamid);
        }
        if( (event.data.accueil === false) | (event.data.help === false) ) {
            Close();
        }
    });
}

);
