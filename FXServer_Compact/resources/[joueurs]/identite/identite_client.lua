options = {
}

jobsname = {}

AddEventHandler('onClientResourceStart', function(res)
    --ClearPedTasksImmediately(GetPlayerPed(-1))
    DoScreenFadeIn(300)
    if res == "identite" then
        TriggerServerEvent('identite:getjob')
    end
end)


RegisterNetEvent('identite:sendjob')
AddEventHandler('identite:sendjob', function (job)
    jobsname = job
end)

function DrawIdCarte(info)
   options = info
end

RegisterNetEvent('identite:draw')
AddEventHandler('identite:draw', function(info)
    DrawIdCarte(info)
    Menu.hidden = false
    RendreID()
end)

local rendre = false
local proprio = 0
RegisterNetEvent('identite:drawOther')
AddEventHandler('identite:drawOther', function(source,info)
    DrawIdCarte(info)
    rendre = true
    proprio = source
    Menu.hidden = false
    TriggerServerEvent('print','Rendre2')
    RendreID()
end)

RegisterNetEvent('identite:ranger')
AddEventHandler('identite:ranger', function()
    Menu.hidden = true
end)


function animation(dict,anim)
    Citizen.CreateThread(function()
		Wait(100)
		RequestAnimDict(dict)

		while not HasAnimDictLoaded(dict) do
			Citizen.Wait(0)
		end

		local myPed = PlayerPedId()
		local animation = anim
		local flags = 16 -- only play the animation on the upper body

		TaskPlayAnim(myPed, dict, animation, 8.0, -8, -1, flags, 0, 0, 0, 0)
	end)
end

local Prop = nil

function newObject(Model)
	local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1), true))
	RequestModel(Model)
	while not HasModelLoaded(Model) do
		Citizen.Wait(1)
	end
	Citizen.Wait(157)
	Prop = CreateObject(Model, 1.0, 1.0, 1.0, 1, 1, 0)
    SetEntityCollision(Prop,false,false)
	local bone = GetPedBoneIndex(GetPlayerPed(-1), 28422)
    AttachEntityToEntity(Prop, GetPlayerPed(-1), bone, 0.03, 0.03, 0.03, 180.0, 180.0, 0.0, 1, 1, 1, 0, 2, 1)
end

function RendreID()
    Citizen.CreateThread(function()
        while not Menu.hidden do
            Wait(5)
            Menu.renderGUI(options) -- Draw menu on each tick if Menu.hidden = false
            DisplayHelp( "Appuyez sur ~INPUT_CONTEXT~ pour rendre la carte")
            if IsControlJustReleased(1, 51) then -- Menu to draw
                Menu.hidden = true -- Hide/Show the menu
                rendre = false
                TriggerServerEvent('identite:rendrecarte',proprio)
                animation("mp_common","givetake2_a")
                newObject("prop_cs_swipe_card")
                Citizen.Wait(1600)
                DeleteEntity(Prop) 
            end
        end
    end)
end
