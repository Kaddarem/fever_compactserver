Menu = {}
Menu.hidden = true

-- when the user up or down in the menu we move the current value min and max to scroll in the list

function Menu.renderGUI(options)
    if not Menu.hidden then
        Menu.renderButtons(options)
    end
end

function Menu.DrawText(Text, X, Y, ScX, ScY, Font, Outline, Shadow, Center, RightJustify, R, G, B, A)
	SetTextFont(Font)
	SetTextScale(ScX, ScY)
	SetTextColour(R, G, B, A)
	if Outline then
		SetTextOutline()
	end
	if Shadow then
		SetTextDropShadow()
	end
	SetTextCentre(Center)
    SetTextRightJustify(RightJustify)
	SetTextEntry("STRING")
	AddTextComponentString(Text)
	DrawText(X, Y)
end

function Menu:setTitle(options)
    local td = "fivemenu"
	if not HasStreamedTextureDictLoaded(td) then
		RequestStreamedTextureDict(td, true)
		while not HasStreamedTextureDictLoaded(td) do
	       Wait(10)
		end
	end
    DrawSprite(td, "idcard", 0.85, 0.5-0.15, 0.250, 0.300, 0.0, 255, 255, 255, 255)
    Menu.DrawText(""..options.prenom.." "..string.upper(options.nom), 0.83, 0.295, 0.5, 0.7, 6, false, false, false, false, 0, 0, 0, 255)
    if options.nom == "Darem" then
        Menu.DrawText("Hacker", 0.83, 0.33, 0.5, 0.5, 6, false, false, false, false, 0, 0, 0, 255)
    else
      Menu.DrawText(jobsname[options.job], 0.83, 0.33, 0.5, 0.5, 6, false, false, false, false, 0, 0, 0, 255)
    end
    DrawRect(0.88,  0.365,0.1,0.001, 0,0,0,255)
    Menu.DrawText('Téléphone :', 0.83, 0.37, 0.4, 0.4, 6, false, false, false, false, 0, 0, 0, 255)
    Menu.DrawText('N° de compte :', 0.83, 0.395, 0.4, 0.4, 6, false, false, false, false, 0, 0, 0, 255)
    Menu.DrawText('License arme :', 0.83, 0.42, 0.4, 0.4, 6, false, false, false, false, 0, 0, 0, 255)
    Menu.DrawText(options.telephone, 0.89, 0.37, 0.4, 0.4, 6, false, false, true, false, 0, 0, 0, 255)
    Menu.DrawText(options.compte, 0.89, 0.395, 0.4, 0.4, 6, false, false, true, false, 0, 0, 0, 255)
    Menu.DrawText(tostring(options.gunlicense), 0.89, 0.42, 0.4, 0.4, 6, false, false, true, false, 0, 0, 0, 255)
    Menu.DrawText(options.numero, 0.74, 0.45, 1.0, 0.6, 1, false, false, false, false, 0, 0, 0,50)
    Menu.DrawText("Validité : 01/2021", 0.9, 0.255, 0.5, 0.4, 6, false, false, false, false, 255, 255, 255, 255)
end

function Menu.renderButtons(options)
        Menu:setTitle(options)
end