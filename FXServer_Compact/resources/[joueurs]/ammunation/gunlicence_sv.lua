RegisterServerEvent("license:checkMoney")
AddEventHandler("license:checkMoney", function(item)
	local sourcePlayer = tonumber(source)
    TriggerEvent('es:getPlayerFromId', sourcePlayer, function(user)
    	local identifier = getPlayerID(source)
	    local price = item.price
	    if (tonumber(user.money) >= tonumber(price)) then
	    	local license = user.gunlicense
	    	if license == 0 and item.level == 1 then
	    		user.removeMoney((price))
                user.setGunlicense((item.level))
	    		MySQL.Sync.execute("UPDATE users SET gunlicense=@license WHERE identifier=@identifier", {['@identifier'] = identifier, ['@license'] = item.level})
	    		TriggerClientEvent("hud:NotifColor",sourcePlayer,"Permis acheté",141)
	    	elseif license == 1 and item.level == 2 then
	    		user.removeMoney((price))
                user.setGunlicense((item.level))
	    		MySQL.Sync.execute("UPDATE users SET gunlicense=@license WHERE identifier=@identifier", {['@identifier'] = identifier, ['@license'] = item.level})
	    		TriggerClientEvent("hud:NotifColor",sourcePlayer,"Permis acheté",141)	 
	    	elseif license == 2 and item.level == 3 then
	    		user.removeMoney((price))
                user.setGunlicense((item.level))
	    		MySQL.Sync.execute("UPDATE users SET gunlicense=@license WHERE identifier=@identifier", {['@identifier'] = identifier, ['@license'] = item.level})
	    		TriggerClientEvent("hud:NotifColor",sourcePlayer,"Permis acheté",141)
	    	elseif license == 3 and item.level == 4 then
	    		user.removeMoney((price))
                user.setGunlicense((item.level))
	    		MySQL.Sync.execute("UPDATE users SET gunlicense=@license WHERE identifier=@identifier", {['@identifier'] = identifier, ['@license'] = item.level})
	    		TriggerClientEvent("hud:NotifColor",sourcePlayer,"Permis acheté",141)
	    	elseif license == item.level then
	    		TriggerClientEvent("hud:NotifColor", sourcePlayer, "Vous avez déjà ce permis",6)   
            elseif license > item.level then
                TriggerClientEvent('hud:NotifColor', sourcePlayer,"Vous avez une permis supérieur")
	    		TriggerClientEvent("hud:NotifColor", sourcePlayer, "Vous n'avez pas le permis inférieur",6)
	    	end	    	
	    else
	     	TriggerClientEvent("hud:NotifColor", sourcePlayer, "Fond Insuffisant",6)
	    end
	end)
end)

-- get's the player id without having to use bugged essentials
function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

-- gets the actual player id unique to the player,
-- independent of whether the player changes their screen name
function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end