
client_script {
    '@hud-event/hudevent_aff.lua',
    '@appmenu/menu_cl.lua',
    'gunlicence_cl.lua',
    'gunacc_cl.lua',
    'gunshop_cl.lua',
}
server_script {
    '@mysql-async/lib/MySQL.lua',
    'gunlicence_sv.lua',
    'gunshop_sv.lua',
}
