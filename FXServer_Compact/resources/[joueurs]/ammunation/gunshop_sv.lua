local required = false
RegisterServerEvent("ws:checkMoney")
AddEventHandler("ws:checkMoney", function(item)
	local sourcePlayer = tonumber(source)
    TriggerEvent('es:getPlayerFromId', source, function(user)
    	local identifier = getPlayerID(sourcePlayer)
	    local price = item.price
	    local hprice = item.price/2
	    if (tonumber(user.money) >= tonumber(price)) then
			local license = user.gunlicense
			required = false
			if license >= item.license then
                user.removeMoney((price))
		    	Messages(5,item.name,sourcePlayer)
                TriggerClientEvent("player:receiveItem", sourcePlayer, item.id,1)
                TriggerEvent('log:print',"[Ammunation] "..getnameRP(source)..' a acheté '..item.name..' pour $'..price)
            else
		      Messages(2,nil,sourcePlayer)
		    end
	    else
			Messages(3,nil,sourcePlayer)
	    end
	end)
end)

function Messages(value, item, sourcePlayer)
	if value == 1 then
		TriggerClientEvent('hud:NotifColor',sourcePlayer,"Vous n'avez pas le permis adéquate!",6)
	elseif value == 2 then
		TriggerClientEvent('hud:NotifColor',sourcePlayer,"Vous devais améliorer votre permis!",6)
	elseif value == 3 then
		TriggerClientEvent('hud:NotifColor',sourcePlayer,"Fonds Insuffisants!",6)
	elseif value == 4 then
		TriggerClientEvent('hud:NotifColor',sourcePlayer,"Vous avez atteint la limite d'armement!",6)
	elseif value == 5 then
		TriggerClientEvent('hud:NotifColor',sourcePlayer,item.." acheté!",141)
	elseif value == 6 then
		TriggerClientEvent('hud:NotifColor',sourcePlayer,"Armes vendues!",141)
	elseif value == 7 then
		TriggerClientEvent('hud:NotifColor',sourcePlayer,"Impossible de vendre une arme non enregistrée",6)
	end
end

RegisterServerEvent('Ammu:GetTeintArme')
AddEventHandler('Ammu:GetTeintArme', function()
    local source = source
    local PlayerId = getPlayerID(source)
	MySQL.Async.fetchAll('Select * FROM weapon_tint WHERE identifier = @id',{['@id'] = PlayerId}, function(result)
        if result[1] ~= nil then
            TriggerClientEvent('Ammu:SetWeaponTint',source,true,result[1].tint)
        else
            TriggerClientEvent('Ammu:SetWeaponTint',source,false,0)    
        end
    end)
end)

RegisterServerEvent('Ammu:ChangeWeaponTint')
AddEventHandler('Ammu:ChangeWeaponTint', function(tint)
    local source = source
    local PlayerId = getPlayerID(source)
    local tint = tint
    MySQL.Async.insert('UPDATE weapon_tint SET tint = @tint WHERE identifier = @id',{['@tint'] = tint,['@id'] = PlayerId})
    TriggerClientEvent('Ammu:SetWeaponTint',source,true,tint)
end)

RegisterServerEvent('ammunation:buyacc')
AddEventHandler('ammunation:buyacc',function(info)
    local source = source
    local value = info.value
    TriggerEvent('es:getPlayerFromId', source, function(user)
    	local identifier = getPlayerID(source)
	    local price = value[1].prix
	    if (tonumber(user.money) >= tonumber(price)) then
            user.removeMoney((price))
            TriggerClientEvent('player:addweapacc',source,info.id,info.ligne,value[1].hash)
			TriggerClientEvent("ammunation:EquipAcc",source,value,true)
	    else
			TriggerClientEvent("ammunation:EquipAcc",source,value,false)
	    end
	end)
end)

function getnameRP(joueur)
    local name = tostring(joueur)
    TriggerEvent('es:getPlayerFromId', joueur, function(user)
		if user ~= nil then
            name = user.prenom.." "..user.nom
        end
    end)
    return name
end

-- get's the player id without having to use bugged essentials
function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

-- gets the actual player id unique to the player,
-- independent of whether the player changes their screen name
function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end