local MenuTint = false

local ListeArme = {
    {name="Machette", price=4500,license=0,model="WEAPON_MACHETE",id=101},
    {name="Club de golf", price=1000,license=0,model="WEAPON_GOLFCLUB",id=102},
    {name="Poing américains", price=1000,license=0,model="WEAPON_KNUCKLE",id=103},
    {name="Couteau", price=2000,license=0,model="WEAPON_KNIFE",id=104},
    {name="Dague", price=4000,license=0,model="WEAPON_DAGGER",id=105},
    {name="Lampe torche", price=1500,license=0,model="WEAPON_FLASHLIGHT",id=106},
    {name="Pied de biche", price=3000,license=0,model="WEAPON_CROWBAR",id=107},
    {name="Batte de baseball", price=2500,license=0,model="WEAPON_BAT",id=108},
    {name="Bouteille", price=150,license=0,model="WEAPON_BOTTLE",id=109},
    {name="Marteau", price=3000,license=0,model="WEAPON_HAMMER",id=110},
    
    {name="Pistolet",price=30000,license=1,model="WEAPON_Pistol",id=111},
    {name="Pistolet calibre 50",price=38000,license=1,model="WEAPON_PISTOL50",id=112},
    {name="SNS Pistolet",price=37000,license=1,model="WEAPON_SNSPistol",id=113},
    {name="Pistolet Lourd",price=40000,license=1,model="WEAPON_HeavyPistol",id=114},
    {name="Revolver",price=45000,license=1,model="WEAPON_Revolver",id=115},
    
    --{name="Grenade Fumigène",price=1000,license=2,model="WEAPON_SmokeGrenade"},nil,1000)
    {name="Fusil à pompe",price=150000,license=2,model="WEAPON_PumpShotgun",id=117},
    {name="Double-cannon",price=175000,license=2,model="weapon_dbshotgun",id=118},
    
    {name="MicroSMG",price=200000,license=3,model="WEAPON_MicroSMG",id=119},
    {name="SMG",price=300000,license=3,model="WEAPON_SMG",id=120},
    {name="Assault SMG",price=450000,license=3,model="WEAPON_AssaultSMG",id=121},
    
    {name="Fusil d'assault",price=500000,license=4,model="WEAPON_AssaultRifle",id=122},
    {name="M16",price=600000,license=4,model="WEAPON_CarbineRifle",id=123},
    {name="Sniper",price=1000000, license=4,model="WEAPON_SniperRifle",id=124},
    
}

function Main()
    VMenu.ResetMenu()
    VMenu.EditHeader("Magasin d'armes")
    VMenu.curItem = 1
    VMenu.AddFunc("Sans permis", "level0",nil)
    VMenu.AddFunc("Catégorie 1", "level1",nil)
    VMenu.AddFunc("Catégorie 2", "level2",nil)
    VMenu.AddFunc("Catégorie 3", "level3",nil)
    VMenu.AddFunc("Catégorie 4", "level4",nil)
    VMenu.AddSep("")
    if MenuTint then
        VMenu.AddFunc("Teinte des armes","teinteArme",nil)
    else
       VMenu.AddFunc("Teinte des armes","refusTeinte",nil) 
    end
    VMenu.AddFunc("Accessoires","accessoires",nil)
end

function level0()
    VMenu.ResetMenu()
    VMenu.EditHeader("Sans permis")
    VMenu.curItem = 1
    VMenu.AddPrev("Main")
    for _,v in pairs (ListeArme) do
        if v.license == 0 then
            VMenu.AddFunc(v.name,"buyweapon",v,nil,v.price)
        end
    end
end

function level1()
    VMenu.ResetMenu()
    VMenu.EditHeader("Catégorie 1")
    VMenu.curItem = 1
    VMenu.AddPrev("Main")
    for _,v in pairs (ListeArme) do
        if v.license == 1 then
            VMenu.AddFunc(v.name,"buyweapon",v,nil,v.price)
        end
    end
    
end

function level2()
    VMenu.ResetMenu()
    VMenu.EditHeader("Catégorie 2")
    VMenu.curItem = 1
    VMenu.AddPrev("Main") 
    for _,v in pairs (ListeArme) do
        if v.license == 2 then
            VMenu.AddFunc(v.name,"buyweapon",v,nil,v.price)
        end
    end 
   
end

function level3()
    VMenu.ResetMenu()
    VMenu.EditHeader("Catégorie 3")
    VMenu.curItem = 1
    VMenu.AddPrev("Main")
    for _,v in pairs (ListeArme) do
        if v.license == 3 then
            VMenu.AddFunc(v.name,"buyweapon",v,nil,v.price)
        end
    end
    
end 

function level4()
    VMenu.ResetMenu()
    VMenu.EditHeader("Catégorie 4")
    VMenu.curItem = 1
    VMenu.AddPrev("Main")
    for _,v in pairs (ListeArme) do
        if v.license == 4 then
            VMenu.AddFunc(v.name,"buyweapon",v,nil,v.price)
        end
    end
    
end


function buyweapon(item)
    TriggerServerEvent("ws:checkMoney", item)
end

local Teinture = {
    {0,0,0}, --0
    {73,71,33}, --1
	{209, 154, 25}, --2
	{239, 78, 148}, --3
	{138, 116, 69}, --4
	{30, 48, 62}, --5
	{179, 70, 39},--6
	{73, 69, 60},--7
}

local TeintBdd = 0
RegisterNetEvent('Ammu:SetWeaponTint')
AddEventHandler('Ammu:SetWeaponTint', function(droitTint,tint)
    MenuTint = droitTint
    TeintBdd = tint
end)

function refusTeinte()
    TriggerEvent('hud:NotifColor',"La teinte des armes est disponible sur www.fever-rp.fr",6)
end

function teinteArme()
    VMenu.ResetMenu()
    --VMenu.ResetCursor()
    VMenu.curItem = 1
    VMenu.AddPrev("Main")
    VMenu.AddColor("Teinte globale","saveTeinture",nil,0,"Teinte",Teinture)
    Citizen.CreateThread(function()
       while VMenu.items[VMenu.curItem].func == "saveTeinture" do
            Wait(0)
            if IsControlJustPressed(1,177) then
                UpdateTint(TeintBdd)
            end
        end
    end)
end

function saveTeinture()
    TriggerServerEvent('Ammu:ChangeWeaponTint',VMenu.curColor -1)
    TriggerServerEvent('Weapon:EditTintWeapon',VMenu.curColor -1)
    for _,arme in pairs(ListeArme) do
        SetPedWeaponTintIndex(GetPlayerPed(-1), GetHashKey(arme.model),VMenu.curColor -1)    
    end
    TriggerEvent('hud:NotifColor','Très bon choix !',141)
end

function UpdateSkin_left()
    if VMenu.items[VMenu.curItem].func == "saveTeinture" then
        UpdateTint(VMenu.curColor -1)
    end
end

function UpdateSkin_right()
    if VMenu.items[VMenu.curItem].func == "saveTeinture" then
        UpdateTint(VMenu.curColor -1)
    end
end

function UpdateTint(Teint)
    local Weapons = exports.porter_arme:GetWeaponsObj()
	local playerPed = GetPlayerPed(-1)
    for _,arme in pairs (Weapons[playerPed]) do
        SetWeaponObjectTintIndex(arme,Teint)
    end
    SetPedWeaponTintIndex(GetPlayerPed(-1), GetSelectedPedWeapon(GetPlayerPed(-1)),Teint)
end

function accessoires()
    local weapon = GetSelectedPedWeapon(GetPlayerPed(-1))
    if weapon == 0 then
        TriggerEvent('hud:NotifColor',"Merci d'équiper l'arme à améliorer",6)
        VMenu.visible = false
    else
        VMenu.ResetMenu()
        VMenu.EditHeader("Accessoires")
        VMenu.curItem = 1
        VMenu.AddPrev("Main")
        local count = 0
        for _,v in pairs (ListAccessoire) do
            if DoesWeaponTakeWeaponComponent(weapon,GetHashKey(v.hash)) then
                count = count +1
                if HasPedGotWeaponComponent(GetPlayerPed(-1),weapon,GetHashKey(v.hash)) then
                    VMenu.AddFunc(v.name,"accessoires",nil,"~g~Déjà équipé")
                else
                    VMenu.AddFunc(v.name,"buyacc",{v,weapon},"Acheter",v.prix)
                end
            end
        end 
        if count == 0 then
            VMenu.AddFunc("~r~Aucun accessoire pour cette arme","Main",nil,"Retour")
        end
    end
end
    
function buyacc(value)
    local ITEMS = exports.menu_personnel:getItemsList()
    local INV = exports.menu_personnel:getINV()
    VMenu.ResetMenu()
    VMenu.EditHeader("Accessoires")
    VMenu.AddPrev("accessoires")
    VMenu.AddSep("Sélectionner l'arme à améliorer")
    for id,_items in pairs(INV) do
        if ITEMS[id].isArme then
            if GetHashKey(ITEMS[id].hash) == GetSelectedPedWeapon(GetPlayerPed(-1)) then
                for ligne, item in pairs(_items) do
                    local presence = false
                    for _,acc in pairs (item.extra) do
                       if acc == value.hash then
                            presence = true
                            break
                        end
                    end
                    if not presence then
                        VMenu.AddWeap(ITEMS[id].libelle,"ChoisirArmeAcc",{['id'] = id, ['ligne'] = ligne, ['value'] = value},ITEMS[id].poids,ITEMS[id].hash,item.extra)
                    end
                end
            end
        end
    end
    VMenu.ResetCursor(2)
end

function ChoisirArmeAcc(info) 
    TriggerServerEvent("ammunation:buyacc",info)
end

RegisterNetEvent('ammunation:EquipAcc')
AddEventHandler('ammunation:EquipAcc',function(value,autorisation)
    if not autorisation then
        TriggerEvent('hud:NotifColor',"Vous n'avez pas assez d'argent sur vous",6)
    else
        GiveWeaponComponentToPed(GetPlayerPed(-1),value[2],GetHashKey(value[1].hash))   
        TriggerEvent('hud:NotifColor',"Voici votre "..value[1].name,141) 
        
    end
    accessoires()
end)
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Press E to open/close menu in the red marker
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
emplacement = {
    {name="Ammunation", id=110, x= -1117.81, y= 2698.16, z= 18.55},
    {name="Ammunation", id=110, x= 1693.93, y= 3759.73, z= 34.7},
    {name="Ammunation", id=110, x= 251.85, y= -49.87, z= 69.94},
    {name="Ammunation", id=110, x= -1306.17, y= -394.16, z= 36.69},
    {name="Ammunation", id=110, x= -662.22, y= -935.71, z= 21.82},
    {name="Ammunation", id=110, x= -330.00, y= 6083.41, z= 31.45},
    {name="Ammunation", id=110, x= 2567.91, y= 294.74, z= 108.73},
    {name="Ammunation", id=110, x= -3171.67, y= 1087.66, z= 20.83},
    {name="Ammunation", id=110, x= 842.40, y= -1033.12, z= 28.19},
    {name="Ammunation Principal", id=110, x= 21.70, y= -1107.41, z= 29.79},
    {name="Ammunation", id=110, x= 810.15, y= -2156.88, z= 29.61},
}
emplacement_grove = {
    {name="Grove St. Gun Shop", id=110, x=7.7238087654114, y=-1894.3465576172, z=23.162649154663},
    {name="Grove St. Gun Shop", id=110, x=-1000.4946899414, y=4848.3916015625, z=274.89318847656},
}

sellweapons = {
    {name="Vente d'arme", id=434, x=19.142578125, y=-1104.2302246094, z=29.79700088501},
    {name="Vente d'arme", id=434, x=846.16723632813, y=-1034.9733886719, z=28.245889663696}, 
    {name="Vente d'arme", id=434, x=813.94311523438, y=-2158.876953125, z=29.619020462036},
    {name="Vente d'arme", id=434, x=-666.05834960938, y=-933.73907470703, z=21.829214096069},
    {name="Vente d'arme", id=434, x=-1303.5306396484, y=-390.9504699707, z=36.695751190186},
    {name="Vente d'arme", id=434, x=254.81108093262, y=-46.906475067139, z=69.941078186035},
    {name="Vente d'arme", id=434, x=-3174.9465332031, y=1084.7476806641, z=20.838752746582},
    {name="Vente d'arme", id=434, x=-1121.7216796875, y=2697.1447753906, z=18.55415725708},
    {name="Vente d'arme", id=434, x=1689.8410644531, y=3758.0588378906, z=34.705303192139},
    {name="Vente d'arme", id=434, x=-334.26705932617, y=6082.1689453125, z=31.45475769043},
    {name="Vente d'arme", id=434, x=2571.7854003906, y=292.71542358398, z=108.73484802246},
}
incircle = false
Citizen.CreateThread(function()
    for _, item in pairs(emplacement) do
      item.blip = AddBlipForCoord(item.x, item.y, item.z)
      SetBlipSprite(item.blip, item.id)
      SetBlipColour(item.blip, item.colour)
      SetBlipAsShortRange(item.blip, true)
      BeginTextCommandSetBlipName("STRING")
      AddTextComponentString(item.name)
      EndTextCommandSetBlipName(item.blip)
    end
    LoinAmmu()
    LoinVente()
    TriggerServerEvent('Ammu:GetTeintArme')
end)

function LoinAmmu()
    Citizen.CreateThread(function()
    local boucle = true
    
    while boucle do
        Citizen.Wait(1000)
        local pos = GetEntityCoords(GetPlayerPed(-1), true)
        for k,v in ipairs(emplacement) do
            if(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) < 50.0)then
                ProcheAmmu(v)
                boucle = false
                break
            end
        end
    end
    end)
end

function LoinVente()
    Citizen.CreateThread(function()
    local boucle = true
    
    while boucle do
        Citizen.Wait(1000)
        local pos = GetEntityCoords(GetPlayerPed(-1), true)
        for k,v in ipairs(sellweapons) do
            if(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) < 50.0)then
                ProcheVente(v)
                boucle = false
                break
            end
        end
    end
    end)
end

RegisterNetEvent('ammunation:vente')
AddEventHandler('ammunation:vente', function(item)
   local arme = 0
   local prix = 0
   local name = ""
   for _,v in pairs (ListeArme) do
        if tonumber(item.id) == v.id then
            arme = v.id
            name = v.name
            prix = math.floor(0.5*v.price)
            break
        end
    end
    if arme == 0 then
        TriggerEvent('hud:NotifColor',"Aucune arme en main",6)
    elseif exports.menu_personnel:getQuantity(arme) > 0 then  
        TriggerEvent("player:sellItem",arme,prix,item.ligne)
        TriggerEvent('hud:NotifColor',name.." vendu pour ~h~~r~$"..prix,141)
        TriggerServerEvent('log:printname',"Ammunation",' a vendu '..name..' pour $'..prix)
    else
        TriggerEvent("hud:NotifColor","Ce n'est pas votre arme",6)
    end
end)

function ProcheAmmu(b)
    Citizen.CreateThread(function()
        local boucle = true
        local bonnepos1 = false

        while boucle do
            Wait(5)
            local pos = GetEntityCoords(GetPlayerPed(-1), true)
            if(Vdist(pos.x, pos.y, pos.z, b.x, b.y, b.z) < 50.0)then
                if(Vdist(pos.x, pos.y, pos.z, b.x, b.y, b.z) < 15.0)then
                    Marker(b.x, b.y, b.z-1,1.5,255, 90, 10)
                    if(Vdist(pos.x, pos.y, pos.z, b.x, b.y, b.z) < 1.0)then
                        if (incircle == false) then
                            DisplayHelp("Appuie sur ~INPUT_CONTEXT~ pour acheter une arme !")
                        end
                        bonnepos1 = true
                        if IsControlJustReleased(1, 51) then -- INPUT_CELLPHONE_DOWN*
                           if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                            Main() -- Menu to draw
                            VMenu.curItem = 1
                            VMenu.visible = not VMenu.visible -- Hide/Show the menu
                        end
                        VMenu.Show() -- Draw menu on each tick if Menu.hidden = false
                    elseif(Vdist(pos.x, pos.y, pos.z, b.x, b.y, b.z) > 1.0) and bonnepos1 then
                        bonnepos1 = false
                        VMenu.visible = false
                    end
                end
            end
        end
    end)
end

function ProcheVente(replacement)
    Citizen.CreateThread(function()
        local boucle = true
        local bonnepos2 = false

        while boucle do
            Wait(5)
            local pos = GetEntityCoords(GetPlayerPed(-1), true)
            if(Vdist(pos.x, pos.y, pos.z, replacement.x, replacement.y, replacement.z) < 50.0)then
                Marker(replacement.x, replacement.y, replacement.z - 1,1.0001,255, 10, 100)
                if Vdist(pos.x, pos.y, pos.z, replacement.x, replacement.y, replacement.z) < 0.5 then
                    DisplayHelp("Ouvrez votre inventaire pour vendre à 50%")
                    if not bonnepos2 then
                        bonnepos2 = true
                        TriggerEvent('ammunation:emplacementvente',true)
                    end
                elseif bonnepos2 then
                    bonnepos2 = false
                    TriggerEvent('ammunation:emplacementvente',false)
                end
            else
                boucle = false
                LoinVente()
                break
            end
        end
    end)
end
