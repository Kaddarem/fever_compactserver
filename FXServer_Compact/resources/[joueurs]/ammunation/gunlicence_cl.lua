function license()
    VMenu.ResetMenu()
    VMenu.EditHeader("Acheter Permis port d'armes")
    VMenu.AddFunc("Catégorie 1","buylicense",{name="Level 1", price=10000,level=1},"Arme de poing",10000)
    VMenu.AddFunc("Catégorie 2","buylicense",{name="Level 2", price=50000,level=2},"Fusil à pompe",50000)
    VMenu.AddFunc("Catégorie 3","buylicense",{name="Level 3", price=300000,level=3},"Arme automatique",300000)
    VMenu.AddFunc("Catégorie 4","buylicense",{name="Level 4", price=800000,level=4},"Fusil d'assaut et Sniper",800000)
end

function buylicense(item)
   -- TriggerEvent('hud:NotifColor',"Guichet fermé",6)
    TriggerServerEvent("license:checkMoney", item)
end
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Press E to open/close menu in the red marker
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
emplacement_license = {
    {name="Permis port d'armes", id=434, x=12.699568748474, y=-1105.0811767578, z=29.797033309937},
}
incircle = false

Citizen.CreateThread(function()
    --[[for _, item in pairs(emplacement_license) do
      item.blip = AddBlipForCoord(item.x, item.y, item.z)
      SetBlipSprite(item.blip, item.id)
      SetBlipAsShortRange(item.blip, true)
      BeginTextCommandSetBlipName("STRING")
      AddTextComponentString(item.name)
      EndTextCommandSetBlipName(item.blip)
    end]]
    VMenu.AddMenu( "Ammunation", 'ammunation',156,45,33)
    LoinLicense()
end)
    
function LoinLicense()
    Citizen.CreateThread(function()
        local boucle = true
        while boucle do
            Citizen.Wait(1000)
            local pos = GetEntityCoords(GetPlayerPed(-1), true)
            for k,v in ipairs(emplacement_license) do
                if(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) < 50.0)then
                    ProcheLicense(v)
                    boucle = false
                    break
                end
            end
        end
    end)
end
    
function ProcheLicense(v)
    Citizen.CreateThread(function()
        local boucle = true
        local bonnepos1 = false
        while boucle do
            Citizen.Wait(5)
            local pos = GetEntityCoords(GetPlayerPed(-1), true)
            if(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) < 50.0)then
                    
                Marker(v.x, v.y, v.z -1,1.5,177, 202, 223)
                if(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) < 1.0) and not IsEntityDead(GetPlayerPed(-1))then
                    if (incircle == false) then
                        DisplayHelp("Appuie sur ~INPUT_CONTEXT~ pour acheter un permis port d'armes !")
                    end
                    bonnepos1 = true
                    if IsControlJustReleased(1, 51) then -- INPUT_CELLPHONE_DOWN
                       if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                        license() -- Menu to draw
                        VMenu.curItem = 1
                        VMenu.visible = not VMenu.visible -- Hide/Show the menu
                    end
                    VMenu.Show() -- Draw menu on each tick if Menu.hidden = false
                elseif(Vdist(pos.x, pos.y, pos.z, v.x, v.y, v.z) > 1.0) and bonnepos1 then
                    bonnepos1 = false
                    VMenu.visible = false
                end
            else
                LoinLicense()
                boucle = false
                break
            end
        end
    end)
end
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Help messages
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------