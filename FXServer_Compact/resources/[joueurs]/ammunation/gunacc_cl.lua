ListAccessoire = {
    --Poing Américain
    {name = "Poing Américain Standard", hash = "COMPONENT_KNUCKLE_VARMOD_PLAYER", prix = 1000},
    {name = "Poing Américain des Amants", hash = "COMPONENT_KNUCKLE_VARMOD_LOVE", prix = 1000},
    {name = "Poing Américain du Mac", hash = "COMPONENT_KNUCKLE_VARMOD_DOLLAR", prix = 1000},
    {name = "Poing Américain des Vagos", hash = "COMPONENT_KNUCKLE_VARMOD_VAGOS", prix = 1000},
    {name = "Poing Américain du Rageux", hash = "COMPONENT_KNUCKLE_VARMOD_HATE", prix = 1000},
    {name = "Poing Américain du Joueur", hash = "COMPONENT_KNUCKLE_VARMOD_DIAMOND", prix = 1000},
    {name = "Poing Américain Bling bling", hash = "COMPONENT_KNUCKLE_VARMOD_PIMP", prix = 1000},
    {name = "Poing Américain du Roi", hash = "COMPONENT_KNUCKLE_VARMOD_KING", prix = 1000},
    {name = "Poing Américain des Ballas", hash = "COMPONENT_KNUCKLE_VARMOD_BALLAS", prix = 1000},
    {name = "Poing Américain Base Skin", hash = "COMPONENT_KNUCKLE_VARMOD_BASE", prix = 1000},
    
    --Cran d’arrêt
    {name = "Cran d’arrêt Standard", hash = "COMPONENT_SWITCHBLADE_VARMOD_VAR1", prix = 1000},
    {name = "Cran d’arrêt Base Skin", hash = "COMPONENT_SWITCHBLADE_VARMOD_BASE", prix = 1000},
    {name = "Cran d’arrêt Variante 2", hash = "COMPONENT_SWITCHBLADE_VARMOD_VAR2", prix = 1000},

    --Pistolet
    {name = "Chargeur standard",  hash = "COMPONENT_PISTOL_CLIP_01", prix = 1000},
    {name = "Chargeur étendu", hash = "COMPONENT_PISTOL_CLIP_02", prix = 1000},
    {name = "Suppresseur", hash = "COMPONENT_AT_PI_SUPP_02", prix = 5000},
    {name = "Lampe", hash = "COMPONENT_AT_PI_FLSH", prix = 1000},
   -- {name = "Finition de luxe", hash = "COMPONENT_PISTOL_VARMOD_LUXE", prix = 1000},
    
    --Pistolet cal.50
    {name = "Chargeur standard", hash = "COMPONENT_PISTOL50_CLIP_01", prix = 1000},
    {name = "Chargeur étendu", hash = "COMPONENT_PISTOL50_CLIP_02", prix = 1000},
    {name = "Suppresseur", hash = "COMPONENT_AT_AR_SUPP_02", prix = 5000},
    --{name = "Finition de luxe", hash = "COMPONENT_PISTOL50_VARMOD_LUXE", prix = 1000},
    
    --Pistolet de combat 
    {name = "Chargeur standard", hash = "COMPONENT_COMBATPISTOL_CLIP_01", prix = 1000},
    {name = "Chargeur étendu", hash = "COMPONENT_COMBATPISTOL_CLIP_02", prix = 1000},
    {name = "Suppresseur", hash = "COMPONENT_AT_PI_SUPP", prix = 5000},
    --{name = "Lowrider", hash = "COMPONENT_COMBATPISTOL_VARMOD_LOWRIDER", prix = 1000},
    
    --Pistolet Perforant
    {name = "Chargeur standard", hash = "COMPONENT_APPISTOL_CLIP_01", prix = 1000},
    {name = "Chargeur étendu", hash = "COMPONENT_APPISTOL_CLIP_02", prix = 1000},
   -- {name = "Finition de luxe", hash = "COMPONENT_APPISTOL_VARMOD_LUXE", prix = 1000},
    
    --Pistolet Lourd
    {name = "Chargeur standard", hash = "COMPONENT_HEAVYPISTOL_CLIP_01", prix = 1000},
    {name = "Chargeur étendu", hash = "COMPONENT_HEAVYPISTOL_CLIP_02", prix = 1000},
   -- {name = "Finition de luxe", hash = "COMPONENT_HEAVYPISTOL_VARMOD_LUXE", prix = 1000},
    
    --Pistolet Vintage   
    {name = "Chargeur standard", hash = "COMPONENT_VINTAGEPISTOL_CLIP_01", prix = 1000},
    {name = "Chargeur étendu", hash = "COMPONENT_VINTAGEPISTOL_CLIP_02", prix = 1000},

    --SMG
    {name = "Chargeur standard", hash = "COMPONENT_SMG_CLIP_01", prix = 1000},
    {name = "Chargeur étendu", hash = "COMPONENT_SMG_CLIP_02", prix = 1000},
    {name = "Chargeur très étendu", hash = "COMPONENT_SMG_CLIP_03", prix = 1000},
    {name = "Lampe", hash = "COMPONENT_AT_AR_FLSH", prix = 1000},
    {name = "Viseur", hash = "COMPONENT_AT_SCOPE_MACRO_02", prix = 1000},
    --{name = "Finition de luxe", hash = "COMPONENT_SMG_VARMOD_LUXE", prix = 1000},

    --Micro SMG
    {name = "Chargeur standard", hash = "COMPONENT_MICROSMG_CLIP_01", prix = 1000},
    {name = "Chargeur étendu", hash = "COMPONENT_MICROSMG_CLIP_02", prix = 1000},
   -- {name = "Finition de luxe", hash = "COMPONENT_MICROSMG_VARMOD_LUXE", prix = 1000},

    --SMG d’Assaut
    {name = "Chargeur standard", hash = "COMPONENT_ASSAULTSMG_CLIP_01", prix = 1000},
    {name = "Chargeur étendu", hash = "COMPONENT_ASSAULTSMG_CLIP_02", prix = 1000},
    --{name = "Lowrider", hash = "COMPONENT_ASSAULTSMG_VARMOD_LOWRIDER", prix = 1000},

    --Fusil d’Assaut
    {name = "Chargeur standard", hash = "COMPONENT_ASSAULTRIFLE_CLIP_01", prix = 1000},
    {name = "Chargeur étendu", hash = "COMPONENT_ASSAULTRIFLE_CLIP_02", prix = 1000},
    {name = "Chargeur XXL", hash = "COMPONENT_ASSAULTRIFLE_CLIP_03", prix = 1000},
   -- {name = "Finition de luxe", hash = "COMPONENT_ASSAULTRIFLE_VARMOD_LUXE", prix = 1000},

    --M16
    {name = "Chargeur standard", hash = "COMPONENT_CARBINERIFLE_CLIP_01", prix = 1000},
    {name = "Chargeur étendu", hash = "COMPONENT_CARBINERIFLE_CLIP_02", prix = 1000},
    {name = "Chargeur XXL", hash = "COMPONENT_CARBINERIFLE_CLIP_03", prix = 1000},
   --{name = "Finition de luxe", hash = "COMPONENT_CARBINERIFLE_VARMOD_LUXE", prix = 1000},

    --Assault shotgun
    {name = "Chargeur standard", hash = "COMPONENT_ASSAULTSHOTGUN_CLIP_01", prix = 1000},
    {name = "Chargeur étendu", hash = "COMPONENT_ASSAULTSHOTGUN_CLIP_02", prix = 1000},
    {name = "Grip", hash = "COMPONENT_AT_AR_AFGRIP", prix = 1000},

    --Sawn Off Shotgun
    --{name = "Finition de luxe", hash = "COMPONENT_SAWNOFFSHOTGUN_VARMOD_LUXE", prix = 1000},

    --Sniper rifle
    {name = "Lunette", hash = "COMPONENT_AT_SCOPE_LARGE", prix = 1000},
    {name = "Lunette de précision", hash = "COMPONENT_AT_SCOPE_MAX", prix = 1000},
   -- {name = "Finition de luxe", hash = "COMPONENT_SNIPERRIFLE_VARMOD_LUXE", prix = 1000},
}