local vocalLevel = 2

AddEventHandler('onClientMapStart', function()
	vocalLevel = 2
	NetworkSetTalkerProximity(10.001)
end)

function ShowNotif(text)
	SetNotificationTextEntry("STRING")
	AddTextComponentString(text)
	DrawNotification(false, false)
end

local keyPressed = false
local once = true
Citizen.CreateThread(function()
	while true do
		Wait(0)
		if once then
			once = false
			NetworkSetVoiceActive(1)
		end

		while IsControlPressed(1, 167) and keyPressed do
			Wait(10)
		end
		if IsControlPressed(1, 167) and not keyPressed then
			keyPressed = true
			vocalLevel = vocalLevel + 1
			if vocalLevel > 3 then
				vocalLevel = 1
			end
			--if vocalLevel < 1 then
			--	vocalLevel = 3
			--end
            local plyPos = GetEntityCoords(GetPlayerPed(-1), true)
            local i = 0
			if vocalLevel == 1 then
				NetworkSetTalkerProximity(3.001)
				ShowNotif("Vous ~h~~b~ écoutez proche")
				DrawMarker(1, plyPos.x, plyPos.y, plyPos.z - 1, 0, 0, 0, 0, 0, 0, 3.0, 3.0, 1.0, 10, 235, 10, 200, 0, 0, 2, 0, 0, 0, 0)
			elseif vocalLevel == 2 then
				NetworkSetTalkerProximity(10.001)
				ShowNotif("Vous écoutez ~h~~b~Normalement")
				DrawMarker(1, plyPos.x, plyPos.y, plyPos.z - 1, 0, 0, 0, 0, 0, 0, 10.0, 10.0, 1.0, 10, 235, 10, 200, 0, 0, 2, 0, 0, 0, 0)
			elseif vocalLevel == 3 then
				NetworkSetTalkerProximity(20.091)
				ShowNotif("Vous ~h~~b~écoutez loin")
				DrawMarker(1, plyPos.x, plyPos.y, plyPos.z - 1, 0, 0, 0, 0, 0, 0, 20.0, 20.0, 1.0, 10, 235, 10, 200, 0, 0, 2, 0, 0, 0, 0)
			end
			Wait(200)
		elseif not IsControlPressed(1, 167) and keyPressed then
			keyPressed = false
		end
	end
end)
