local disableKeys = { 19, 20, 43, 48, 187, 233, 309, 311, 85, 74, 21, 73, 121, 45, 80, 140, 170, 177, 194, 202, 225, 263}

function DisableControls()
	for i = 1, #disableKeys do
		DisableControlAction(0,  disableKeys[i],  1)
	end
end

RegisterNetEvent('chatCommandEntered')

local guiEnabled = false

function DisplayNotification(text)
	SetNotificationTextEntry("STRING")
	AddTextComponentString(text)
	DrawNotification(false, false)
end

function PrintChatMessage(text)
    TriggerEvent('chatMessage', "system", { 255, 0, 0 }, text)
end

function EnableGui(enable)
    SetNuiFocus(enable)
    guiEnabled = enable

    SendNUIMessage({
        type = "enableui",
        enable = enable
    })
end

RegisterNetEvent("ccreation:menu")
AddEventHandler('ccreation:menu', function()
    Citizen.Wait(1000)
    EnableGui(true)
end)

RegisterNUICallback('escape', function(data, cb)
    EnableGui(false)
    cb('ok')
end)

RegisterNUICallback('login', function(data, cb)
		if string.len(data.username) > 0 and string.len(data.password) > 0 then
	    TriggerServerEvent("es:updateName", data.username, data.password)
            local target = ""
            DisplayNotification("Bienvenue ~b~".. data.username .." " .. data.password)
            EnableGui(false)
            for i=0,31 do
                EnableAllControlActions(i)
            end
            Citizen.Wait(2000)
		else
			TriggerEvent("es_freeroam:notif", "~r~Remplisser tous les champs")
		end
    cb('ok')
end)

Citizen.CreateThread(function()
    while true do
        if guiEnabled then

            for i=0,31 do
                DisableAllControlActions(i)
            end
            if IsDisabledControlJustReleased(0, 142) then -- MeleeAttackAlternate
                SendNUIMessage({
                    type = "click"
                })
            end
        end
        Citizen.Wait(0)
    end
end)
