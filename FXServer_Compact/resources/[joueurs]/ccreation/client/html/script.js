var documentWidth = document.documentElement.clientWidth;
var documentHeight = document.documentElement.clientHeight;

var cursor = document.getElementById("cursor");
var cursorX = documentWidth / 2;
var cursorY = documentHeight / 2;

function UpdateCursorPos() {
    cursor.style.left = cursorX;
    cursor.style.top = cursorY;
}

function Click(x, y) {
    var element = $(document.elementFromPoint(x, y));
    element.focus().click();
    return true;
}

$(function() {
    window.addEventListener('message', function(event) {
        var item = event.data;
        if (item.type == "enableui") {
            cursor.style.display = item.enable ? "block" : "none";
            document.body.style.display = item.enable ? "block" : "none";
        }
        if (item.type == "click") {
            // Avoid clicking the cursor itself, click 1px to the top/left;
            Click(cursorX - 1, cursorY - 1);
        }
    });

    $(document).mousemove(function(event) {
        cursorX = event.pageX;
        cursorY = event.pageY;
        UpdateCursorPos();
    });

    document.onkeyup = function (data) {
        // if (data.which == 27) { // Escape key
        //     $.post('http://ccreation/escape', JSON.stringify({}));
        // }
    };

    $(".dialog-selectorf").fadeOut(10);

    var sickvar = true;

    $("#login-form").submit(function(e) {
        e.preventDefault(); // Prevent form from submitting

        $.post('http://ccreation/login', JSON.stringify({
            username: $("#username").val().trim(),
            password: $("#password").val().trim()
        }));
    });
});
