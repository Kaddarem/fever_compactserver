local peds = {
  {type=4, modelped = 's_f_y_shop_low', x = 24.510189056396, y = -1347.0377197266, z = 29.502490997314, a = 266.82464599609},
  {type=4, modelped = 's_f_m_sweatshop_01', x = -705.79522705078, y = -914.55267333984, z = 19.215591430664, a = 87.01318359375 },
  {type=4, modelped = 's_f_y_sweatshop_01', x = -1222.1662597656, y = -908.62371826172, z = 12.3263463974, a = 1 },
  {type=4, modelped = 's_f_y_shop_low', x = 372.52584838867, y = 326.53286743164, z = 103.56636810303, a = 251.52012634277 },
  {type=4, modelped = 's_f_m_sweatshop_01', x = 549.32391357422, y = 2671.3012695313, z = 42.156490325928, a = 94.315902709961  },
  {type=4, modelped = 's_f_y_sweatshop_01', x = 1392.0661621094, y = 3606.2924804688, z = 34.980922698975, a = 191.2900390625  },
  {type=5, modelped = 'mp_m_shopkeep_01', x = 1959.8677978516, y = 3739.654296875, z = 32.343746185303, a = 301.51791381836  },
  }

AddEventHandler('onClientMapStart', function()
    for _, item in pairs(peds) do
        RequestModel(item.modelped)
        while not HasModelLoaded(item.modelped) do
            Wait(1)
        end
    end

    for _, item in pairs(peds) do
        local hash = GetHashKey(item.modelped)
        ped = CreatePed(item.type, hash, item.x, item.y, item.z, item.a, false, true)
        SetPedCombatAttributes(ped, 46, true)
        SetPedFleeAttributes(ped, 0, 0)
        SetPedArmour(ped, 100)
        SetPedMaxHealth(ped, 100)
        SetPedRelationshipGroupHash(ped, GetHashKey("GANG_1"))
        TaskStartScenarioInPlace(ped, "WORLD_HUMAN_STAND_IMPATIENT", 0, true)
        SetPedCanRagdoll(ped, false)
        SetPedDiesWhenInjured(ped, false)
    end
end)
        
