RegisterServerEvent("cocasee")
AddEventHandler("cocasee", function(prix)
	TriggerEvent("es:getPlayerFromId", source, function(user)
	    if (tonumber(user.money) >= prix) then
            user.removeMoney(prix)
            TriggerClientEvent("es_freeroam:notify", source, "CHAR_PROPERTY_BAR_MIRROR_PARK", 1, "Magasin", false, "Coca ~g~+1 !\n")
            TriggerClientEvent("player:receiveItem",source, 31, 1)
		else
		  TriggerClientEvent("es_freeroam:notify", source, "CHAR_PROPERTY_BAR_MIRROR_PARK", 1, "Magasin", false, "~r~Tu n'as pas suffisamment d'argent !\n")
		end
	end)
end)

RegisterServerEvent("Sandwichs")
AddEventHandler("Sandwichs", function(prix)
	TriggerEvent("es:getPlayerFromId", source, function(user)
	    if (tonumber(user.money) >= prix) then
            user.removeMoney(prix)
            TriggerClientEvent("es_freeroam:notify", source, "CHAR_PROPERTY_BAR_MIRROR_PARK", 1, "Magasin", false, "Sandwich ~g~+1 !\n")
            TriggerClientEvent("player:receiveItem",source, 30, 1)
		else
		TriggerClientEvent("es_freeroam:notify", source, "CHAR_PROPERTY_BAR_MIRROR_PARK", 1, "Magasin", false, "~r~Tu n'as pas suffisamment d'argent !\n")
		end
	end)
end)

RegisterServerEvent("telephone")
AddEventHandler("telephone", function(id,prix)
	TriggerEvent("es:getPlayerFromId", source, function(user)
	    if (tonumber(user.money) >= prix) then
            user.removeMoney(prix)
            TriggerClientEvent("es_freeroam:notify", source, "CHAR_PROPERTY_BAR_MIRROR_PARK", 1, "Magasin", false, "Voici votre ~g~téléphone !\n")
            TriggerClientEvent("player:receiveItem",source,id, 1)
		else
		TriggerClientEvent("es_freeroam:notify", source, "CHAR_PROPERTY_BAR_MIRROR_PARK", 1, "Magasin", false, "~r~Tu n'as pas suffisamment d'argent !\n")
		end
	end)
end)

RegisterServerEvent("cigarette")
AddEventHandler("cigarette", function(id,prix)
	TriggerEvent("es:getPlayerFromId", source, function(user)
	    if (tonumber(user.money) >= prix) then
            user.removeMoney(prix)
            TriggerClientEvent("es_freeroam:notify", source, "CHAR_PROPERTY_BAR_MIRROR_PARK", 1, "Magasin", false, "Cigarettes ~g~+20 !\n")
            TriggerClientEvent("player:receiveItem",source,id, 20)
		else
		TriggerClientEvent("es_freeroam:notify", source, "CHAR_PROPERTY_BAR_MIRROR_PARK", 1, "Magasin", false, "~r~Tu n'as pas suffisamment d'argent !\n")
		end
	end)
end)

RegisterServerEvent("Achat:coffre")
AddEventHandler("Achat:coffre", function(prix)
	TriggerEvent("es:getPlayerFromId", source, function(user)
	    if (tonumber(user.money) >= prix) then
            user.removeMoney(prix)
            TriggerClientEvent("es_freeroam:notify", source, "CHAR_PROPERTY_BAR_MIRROR_PARK", 1, "Magasin", false, "Voici un coffre déplacable !\n")
            TriggerClientEvent("player:receiveItem",source, 48, 1)
		else
		TriggerClientEvent("es_freeroam:notify", source, "CHAR_PROPERTY_BAR_MIRROR_PARK", 1, "Magasin", false, "~r~Tu n'as pas suffisamment d'argent !\n")
		end
	end)
end)