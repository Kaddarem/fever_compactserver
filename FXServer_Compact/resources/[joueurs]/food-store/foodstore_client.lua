function changemodel(model)
	
	local modelhashed = GetHashKey(model)

	RequestModel(modelhashed)
	while not HasModelLoaded(modelhashed) do 
	    RequestModel(modelhashed)
	    Citizen.Wait(0)
	end

	SetPlayerModel(PlayerId(), modelhashed)
	local a = "" -- nil doesnt work
	SetPedRandomComponentVariation(GetPlayerPed(-1), true)
	SetModelAsNoLongerNeeded(modelhashed)
end

function Notify(text)
    SetNotificationTextEntry('STRING')
    AddTextComponentString(text)
    DrawNotification(false, false)
end

function DisplayHelpText(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

function Main()
    VMenu.curItem = 1
    VMenu.ResetMenu()
    VMenu.EditHeader('Ressources achetables')
    VMenu.AddFunc('Téléphone (100$)',"Telephone",{32,100},"Acheter")
    VMenu.AddFunc("Sandwich (40$)", "Sandwich", 40,"Acheter")
    VMenu.AddFunc("Coca (40$)", "cocase", 40,"Acheter")
    VMenu.AddFunc("Paquet de Cigarette (250$)", "Cigarette", {15,250},"Acheter")
    VMenu.AddFunc("Coffre (10.000$)", "Coffre", 10000,"Acheter")
end

------------------------------
--FONCTIONS
-------------------------------
local twentyfourseven_shops = {
	{ ['x'] = 1961.1140136719, ['y'] = 3741.4494628906, ['z'] = 32.34375 , header = "foodshop"},
	{ ['x'] = 1392.4129638672, ['y'] = 3604.47265625, ['z'] = 34.980926513672 , header = "foodshop" },
	{ ['x'] = 546.98962402344, ['y'] = 2670.3176269531, ['z'] = 42.156539916992 , header = "foodshop" },
	{ ['x'] = 2556.2534179688, ['y'] = 382.876953125, ['z'] = 108.62294769287 , header = "foodshop" },
	{ ['x'] = -1821.9542236328, ['y'] = 792.40191650391, ['z'] = 138.13920593262 , header = "foodshop" },
	{ ['x'] = 128.1410369873, ['y'] = -1286.1120605469, ['z'] = 29.281036376953 , header = "foodshop" },
	{ ['x'] = -1223.6690673828, ['y'] = -906.67517089844, ['z'] = 12.326356887817 , header = "foodshop" },
	{ ['x'] = -708.19256591797, ['y'] = -914.65264892578, ['z'] = 19.215591430664 , header = "foodshop" },
	{ ['x'] = 26.419162750244, ['y'] = -1347.5804443359, ['z'] = 29.497024536133 , header = "foodshop" },
    { ['x'] = 374.33697509766, ['y'] = 325.66787719727, ['z'] = 103.56637573242 , header = "foodshop" },
    { ['x'] = -407.01229858398, ['y'] = 6062.7783203125, ['z'] = 31.500108718872 , header = "foodshop" },
}

Citizen.CreateThread(function()
	for k,v in ipairs(twentyfourseven_shops)do
		if k ~= 8 and k ~= 10 then
            local blip = AddBlipForCoord(v.x, v.y, v.z)
            SetBlipSprite(blip, 52)
            SetBlipScale(blip, 0.8)
            SetBlipAsShortRange(blip, true)
            BeginTextCommandSetBlipName("STRING")
            AddTextComponentString("Magasin")
            EndTextCommandSetBlipName(blip)
        end
	end
end)

function Sandwich(prix)
    TriggerServerEvent("Sandwichs",prix)
    VMenu.visible = true
end

function cocase(prix)
    TriggerServerEvent("cocasee",prix)
end

function Cigarette(args)
    local id = args[1]
    local prix = args[2]
    TriggerServerEvent('cigarette',id,prix)
    VMenu.visible = true
end

function Telephone(args)
    local id = args[1]
    local prix = args[2]
    if exports.menu_personnel:getQuantity(id) < 1 then
        TriggerServerEvent('telephone',id,prix)
    else
       TriggerEvent("es_freeroam:notify", "CHAR_PROPERTY_BAR_MIRROR_PARK", 1, "Magasin", false, "Vous avez déjà un téléphone")
    end
end

function Coffre(prix)
    TriggerServerEvent('Achat:coffre',prix)
end

-------------------------
---INVENTAIRE
-------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Press F2 to open menu
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Citizen.CreateThread(function()
    VMenu.AddMenu( 'Ressources achetables', 'foodshop',247,123,29)
    local BoucleLent = 5000
    local BoucleCourt = 5
    local TempsBoucle = BoucleCourt
	while true do
		Citizen.Wait(TempsBoucle)
        TempsBoucle = BoucleLent
		local pos = GetEntityCoords(GetPlayerPed(-1), false)
		for k,v in ipairs(twentyfourseven_shops) do
			if(Vdist(v.x, v.y, v.z, pos.x, pos.y, pos.z) < 20.0)then
                TempsBoucle = BoucleCourt
				if(Vdist(v.x, v.y, v.z, pos.x, pos.y, pos.z) < 2.0)then
					DisplayHelpText("Appuyer sur ~INPUT_CONTEXT~ pour ~g~acheter.")
					if IsControlJustPressed(1, 51) then
                        if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                        VMenu.curItem = 1
                        Main()
                        if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
                        VMenu.visible = not VMenu.visible
				    end
                else
                    VMenu.visible = false
                end
            end
		end
        VMenu.Show()
	end
end)
