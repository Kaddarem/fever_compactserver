ITEMS = {}
INV = {}
-- flag to keep track of whether player died to prevent
maxCapacity = 30
UltimeCapacity = 200
poids = 0
refreshTime = 60000

-- register events, only needs to be done once

RegisterNetEvent("player:receiveItem")
RegisterNetEvent("player:looseItem")
RegisterNetEvent("player:sellItem")


------------------------- EVENTS -------------------------

-- handles when a player spawns either from joining or after death

AddEventHandler('onClientResourceStart', function(res)
    if res =="menu_personnel" then
        TriggerServerEvent("item:getItems")
    end
end)

RegisterNetEvent("gui:getItems")
AddEventHandler("gui:getItems", function(THEITEMS,THEINV)
    if THEITEMS ~= nil then
        ITEMS = {}
        ITEMS = THEITEMS
    end
    if THEINV ~= nil then
        INV = {}
        INV = THEINV
    end
    for id, group_item in pairs(INV) do
        for id2, item in pairs(group_item) do
            if item.poids == nil then
                INV[tonumber(id)][tonumber(id2)].poids = ITEMS[tonumber(id)].poids
            end
        end
    end
end)

AddEventHandler("player:receiveItem", function(item, qty,extra,_poids)
    local item = tonumber(item)
    if INV[item] == nil then
        INV[item] = {}
    end
    local poids = ITEMS[item].poids
    if _poids ~= nil then
        poids = _poids    
    end
    if ITEMS[item].isArme then
        if extra == nil then
            extra = {}
        end
        table.insert(INV[item],{quantite = 1, extra = extra,poids = poids})
        equiperarme({id = item, extra = extra})
    elseif item == 47 or item == 48 then
        if extra == nil then
            extra = 0
        end
        table.insert(INV[item],{quantite = 1, extra = extra,poids = poids})
    else
        if INV[item][1] == nil then
            INV[item][1] = {quantite = qty,poids = poids}
        else
            INV[item][1].quantite = INV[item][1].quantite + qty
        end
    end
    TriggerServerEvent("item:updateInv", INV)
    TriggerEvent('menu:updateInv')
end)

RegisterNetEvent('player:addweapacc')
AddEventHandler('player:addweapacc',function(item,ligne,acc)
    table.insert(INV[item][ligne].extra,acc)  
    TriggerServerEvent("item:updateInv", INV)  
    TriggerEvent('menu:updateInv')
end)

AddEventHandler("player:looseItem", function(item, ligne, qty)
    local item = tonumber(item)
    local ligne = tonumber(ligne)
    if qty == nil then
        qty = ligne
        ligne = 1
    end
    if ITEMS[item].isArme then
        RemoveWeaponFromPed(GetPlayerPed(-1),GetHashKey(ITEMS[item].hash)) 
    end  
    INV[item][ligne].quantite = INV[item][ligne].quantite - qty
    if INV[item][ligne].quantite == 0 then
        table.remove(INV[item],ligne)
    end
    if #INV[item] == 0 then
        INV[item] = nil 
    end
    TriggerServerEvent("item:updateInv", INV)
    TriggerEvent('menu:updateInv')
end)

AddEventHandler("player:sellItem", function(item, price,_ligne)
    local item = tonumber(item)
    local price = price
    local ligne = 1
    if _ligne ~= nil then
        ligne = _ligne
    end
    TriggerEvent('player:looseItem',item,ligne,1)
    TriggerServerEvent("item:sell", price)
    if ITEMS[item].isArme then
        RemoveWeaponFromPed(GetPlayerPed(-1),GetHashKey(ITEMS[item].hash)) 
    end  
end)

RegisterNetEvent('player:viderinv')
AddEventHandler('player:viderinv', function()
    INV = {}
    RemoveAllPedWeapons(GetPlayerPed(-1))
    TriggerServerEvent("item:updateInv", INV)
    TriggerEvent('menu:updateInv')
end)


------------------------- EXPORTS METHODS -------------------------

function getQuantity(itemId)
    local quantity = 0
    if INV[itemId] ~= nil then
        for _,item in pairs(INV[itemId]) do
           quantity = quantity + item.quantite 
        end
    end
    return quantity
end

RegisterNetEvent('item:getQuantity')
AddEventHandler('item:getQuantity', function(itemId,Retour)
    local quantity = 0
    if INV[itemId] ~= nil then
        for _,items in pairs(INV[itemId]) do
            quantity = quantity + item.quantite 
        end
    end
    Retour(quantity)
end)

function getPoidsItem(itemId)
    local quantity = 0
    for _,item in pairs(ITEMS[itemId]) do
       quantity = quantity + item.quantite 
    end
    return quantity*ITEMS[itemId].poids
end

function getItemsList()
    return ITEMS
end

function getINV()
    return INV
end

function getPods()
    local pods = 0
    for id, group_item in pairs(INV) do
        for _, item in pairs(group_item) do
            pods = pods + item.quantite * item.poids
        end
    end
    return pods
end

------------------------- GENERAL METHODS -------------------------

function getPlayers()
    local playerList = {}
    for i = 0, 500 do
        local player = GetPlayerFromServerId(i)
        if NetworkIsPlayerActive(player) then
            table.insert(playerList, player)
        end
    end
    return playerList
end

function getNearPlayer()
    local players = getPlayers()
    local pos = GetEntityCoords(GetPlayerPed(-1))
    local pos2
    local distance
    local minDistance = 3
    local playerNear
    for _, player in pairs(players) do
        pos2 = GetEntityCoords(GetPlayerPed(player))
        distance = GetDistanceBetweenCoords(pos["x"], pos["y"], pos["z"], pos2["x"], pos2["y"], pos2["z"], true)
        if (pos ~= pos2 and distance < minDistance) then
            playerNear = player
            minDistance = distance
        end
    end
    if (minDistance < 3) then
        return playerNear
    end
end

function VehicleInFront()
    local pos = GetEntityCoords(GetPlayerPed(-1))
    local entityWorld = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, 3.0, 0.0)
    local rayHandle = CastRayPointToPoint(pos.x, pos.y, pos.z, entityWorld.x, entityWorld.y, entityWorld.z, 10, GetPlayerPed(-1), 0)
    local a, b, c, d, result = GetRaycastResult(rayHandle)
    return GetVehicleNumberPlateText(result)
end

function Chat(debugg)
    TriggerEvent("chatMessage", '', { 0, 0x99, 255 }, tostring(debugg))
end
