local openkey = 311
local craft = false
local craftIllegal = false
local jobID = 0
local EmplacementVente = false


User = {
	Spawned = false,
	Loaded = false,
	group = "0",
	permission_level = 0,
	money = 0,
	dirtymoney = 0,
	job = 0,
	police = 0,
	enService = 0,
	nom = "",
	prenom = "",
	vehicle = "",
	identifier = nil,
	telephone = "",
    compte = 0,
    gunlicense = 0
}

RegisterNetEvent('recolt:updateJobs')
AddEventHandler('recolt:updateJobs', function(newjob)
    jobID = newjob
end)

--------------------------
---------- MAIN ----------
--------------------------
function Main()
    VMenu.ResetMenu('Menu personnel')
    VMenu.ResetCursor()
    VMenu.AddFunc("Animations", "menuAnim", nil, "Accéder")
    VMenu.AddFunc("Inventaire", "menuitems", nil)
    VMenu.AddFunc("~r~Fouiller","fouiller",nil)
    VMenu.AddFunc("Craft", "craftMenu", nil)
    VMenu.AddFunc("Carte d'identité", "menuid", nil)
	VMenu.AddFunc("~g~Sauvegarder ma position", "menuposition", nil)
	VMenu.AddFunc("Donner de l'argent", "menugiveCash", nil)
    if craftIllegal then
        VMenu.AddFunc("~r~Menu Receleur","menuReceleur",nil)    
    end
    if User.group == "owner" then
        VMenu.AddFunc("~r~Menu Admin","menuAdmin",nil)
    end
    VMenu.AddFunc("Vêtement","GererVetement",nil)
    VMenu.AddFunc("Affichage","AffichageEcran",nil)
    
    if VMenu.curItem > #VMenu.items[2] then
        VMenu.curItem = 1
        VMenu.scroll = 0
    end
end
local IDcarteOn = false

function GererVetement()
    VMenu.curItem = 1
    VMenu.ResetMenu()
    VMenu.AddPrev('Main')
    VMenu.EditHeader("Vêtement")
    VMenu.AddBool("Masque","actionmasque","masque",masqueutiliser())
    VMenu.AddBool("Lunette","actionlunette","lunette",lunetteutiliser())
    VMenu.AddBool("Chapeau","actionchapeau","chapeau",chapeauutiliser())
end

function menuid()
    VMenu.curItem = 1
    VMenu.ResetMenu()
    VMenu.AddPrev('Main')
    VMenu.EditHeader("Carte d'identite")
    if IDcarteOn then
        VMenu.AddFunc("Ranger","rangerID",nil)
    else
        VMenu.AddFunc("Afficher","afficherID",nil)
    end
    VMenu.AddFunc("Montrer","montrerID",nil)
end

function menuitems()
    VMenu.ResetMenu()
    VMenu.curItem = 2
    VMenu.AddPrev('Main')
    VMenu.EditHeader("Inventaire")
    VMenu.AddSep("Capacité : " .. (getPods() or 0) .. "/" .. maxCapacity.."kg")
    local count = 0
    local libelle = ""
    for id, group_item in pairs(INV) do
        for ligne,item in pairs (group_item) do
            if (item.quantite > 0) then
                count = count +1
                if ITEMS[id].isIllegal then
                    if tonumber(id) == 41 then
                        libelle = "~r~"..tostring(ITEMS[id].libelle).." : $" .. tostring(comma_value(item.quantite))
                    else
                        libelle = "~r~"..tostring(ITEMS[id].libelle).." : " .. tostring(comma_value(item.quantite))
                    end
                elseif tonumber(id) == 47 then
                    libelle =tostring(ITEMS[id].libelle).." "..exports.plongee:GetNomCouleur(item.extra)
                else
                    libelle =tostring(ITEMS[id].libelle).." : " .. tostring(comma_value(item.quantite))
                end
                if ITEMS[id].isArme then
                    VMenu.AddWeap(libelle, "ItemMenu", {['item'] = item,['ligne'] = ligne,['id'] = id},item.poids,ITEMS[id].hash,item.extra)
                else
                    VMenu.AddPoids(libelle, "ItemMenu", {['item'] = item,['ligne'] = ligne,['id'] = id},item.poids)
                end
            end
        end
    end
    if count == 0 then
        VMenu.AddFunc("Vide","Main",'Main',"Retour")
    end
end

function comma_value(n)
	local left,num,right = string.match(n,'^([^%d]*%d)(%d*)(.-)$')
	return left..(num:reverse():gsub('(%d%d%d)','%1 '):reverse())..right
end

RegisterNetEvent('menu:updateInv')
AddEventHandler('menu:updateInv', function()
    if VMenu.visible and VMenu.menus.name == 'Inventaire' then  
        menuitems()    
    end
end)

RegisterNetEvent('ammunation:emplacementvente')
AddEventHandler('ammunation:emplacementvente', function(valeur)
    EmplacementVente = valeur    
end)

local TenuePlongee = false
function ItemMenu(infoitem)
    VMenu.ResetMenu()
    VMenu.AddPrev('menuitems')
    local itemId = tonumber(infoitem.id)
    VMenu.AddFunc("Donner", "give", infoitem)
    if itemId == 15 then
	    VMenu.AddFunc("Fumer", "fumer", infoitem)
    elseif itemId == 14 then
	    VMenu.AddFunc("Utiliser", "kitrepair", infoitem)   
    elseif itemId == 46 then
        VMenu.AddFunc("Utiliser","CarJack",infoitem)
    elseif itemId == 47 then
        if not TenuePlongee then
            VMenu.AddChoix("Equiper","EqTenuePlongee","Plongee",{"Complète","Minimum"},infoitem,nil,0.06)
        else
           VMenu.AddFunc("Déséquiper","EqTenuePlongee",{nil,infoitem}) 
        end
    elseif itemId == 48 then
        VMenu.AddFunc('Poser',"PoserCoffre",infoitem)
    end
    if ITEMS[itemId].isArme then
       VMenu.AddFunc("Equiper","equiperarme",{id = infoitem.id,extra = infoitem.item.extra}) 
       VMenu.AddFunc("Démonter un accessoire","deleteaccessoirearme",infoitem) 
    end
    if EmplacementVente and ITEMS[itemId].isArme then
        VMenu.AddFunc("Vendre","vendrearme",infoitem)  
    end
    local food = tonumber(ITEMS[itemId].food)
    local water = tonumber(ITEMS[itemId].water) 
    local sommeinfo = food + water
    if sommeinfo > 0 then
        if food < water then
	       VMenu.AddFunc("Boire", "invboire", infoitem,"Utiliser")
        else
	       VMenu.AddFunc("Manger", "invmanger", infoitem,"Utiliser")
        end        
    end
	VMenu.AddFunc("~r~Jeter", "invjeter", infoitem)
    if VMenu.curItem > tonumber(#VMenu.items) then
        VMenu.ResetCursor()
    end
end

TeintBdd = 0
RegisterNetEvent('Ammu:SetWeaponTint')
AddEventHandler('Ammu:SetWeaponTint', function(droitTint,tint)
    TeintBdd = tint
end)

function equiperarme(item)
    GiveWeaponToPed(GetPlayerPed(-1),GetHashKey(ITEMS[item.id].hash), 1000, false, true) 
    SetPedWeaponTintIndex(GetPlayerPed(-1),GetHashKey(ITEMS[item.id].hash),TeintBdd)
    if item.extra ~= nil then
        for _, acc in pairs (item.extra) do
            GiveWeaponComponentToPed(GetPlayerPed(-1),GetHashKey(ITEMS[item.id].hash),GetHashKey(acc))
        end
    end
end

function vendrearme(infoitem)
    TriggerEvent('ammunation:vente',infoitem)
    VMenu.visible = false
end

function afficherID()
    IDcarteOn = true
    TriggerEvent('identite:draw',User)
    menuid()
end

function rangerID()
    IDcarteOn = false
    TriggerEvent('identite:ranger')
    menuid()
end

function lunetteutiliser()
    local valeurmask=GetPedPropIndex(GetPlayerPed(-1),1)
    if valeurmask == -1 then
        return false
    else
        return true
    end
end

function chapeauutiliser()
    local valeurmask=GetPedPropIndex(GetPlayerPed(-1),0)
    if valeurmask == -1 then
        return false
    else
        return true
    end
end

function masqueutiliser()
    local valeurmask=GetPedDrawableVariation(GetPlayerPed(-1),1)
    if valeurmask == 0 then
        return false
    else
        return true
    end
end

function actionmasque(valeur)
    if valeur then
        TriggerServerEvent('menu:loadmasque')
    else
        local inAnim = 'take_off_mask_ps'
        local dict = "misscommon@std_take_off_masks"
        RequestAnimDict(dict)
        while not HasAnimDictLoaded(dict) do
            Citizen.Wait(10)
        end
        TaskPlayAnim(GetPlayerPed(-1), dict, inAnim, 4.0, -1, -1, 50, 0, false, false, false)
        Wait(500)
        SetPedComponentVariation(GetPlayerPed(-1), 1, 0, 0, 0)
        Wait(1000)
        ClearPedTasks(GetPlayerPed(-1)) 
        Citizen.Wait(200)
    end
end

function actionchapeau(valeur)
    if valeur then
        TriggerServerEvent('menu:loadchapeau')
    else
        ClearPedProp(GetPlayerPed(-1),0)
    end
    Citizen.Wait(200)
end

function actionlunette(valeur)
    if valeur then
        TriggerServerEvent('menu:loadlunette')
    else
        ClearPedProp(GetPlayerPed(-1),1)
    end
    Citizen.Wait(200)
end

RegisterNetEvent("menu:mettremasque")
AddEventHandler("menu:mettremasque", function(args)
	local inAnim = 'put_on_mask_ps'
    local dict = "misscommon@van_put_on_masks"
	RequestAnimDict(dict)
	while not HasAnimDictLoaded(dict) do
		Citizen.Wait(10)
	end
	TaskPlayAnim(GetPlayerPed(-1), dict, inAnim, 4.0, -1, -1, 50, 0, false, false, false)
    Wait(1000)
    SetPedComponentVariation(GetPlayerPed(-1), 1, tonumber(args[1]), tonumber(args[2]), 0)
    Wait(1000)
    ClearPedTasks(GetPlayerPed(-1)) 
end)

function menuposition()
	LastPosX, LastPosY, LastPosZ = table.unpack(GetEntityCoords(GetPlayerPed(-1), true))
	local LastPosH = GetEntityHeading(GetPlayerPed(-1))
	TriggerServerEvent("project:savelastpos", LastPosX , LastPosY , LastPosZ, LastPosH)
    TriggerServerEvent('es:savemoney')
    TriggerEvent("hud:NotifColor", "Position sauvegardée",200)
end

RegisterNetEvent("menu:mettrechapeau")
AddEventHandler("menu:mettrechapeau", function(args)
    SetPedPropIndex(GetPlayerPed(-1), 0, tonumber(args[1]), tonumber(args[2]), 0)  
end)

RegisterNetEvent("menu:mettrelunette")
AddEventHandler("menu:mettrelunette", function(args)
    SetPedPropIndex(GetPlayerPed(-1), 1, tonumber(args[1]), tonumber(args[2]), 0)  
end)
----------------------------
-------- INVENTAIRE --------
----------------------------

local Prop = nil

function montrerID()
    taketarget()
    if Target ~= -1 then
        animation("mp_common","givetake2_a")
        newObject("prop_cs_swipe_card")
		TriggerServerEvent('identite:montrercarte',GetPlayerServerId(Target),User)
        TriggerEvent('hud:NotifColor',"Carte d'identité donnée",141)
        Citizen.Wait(1600)
	    DeleteEntity(Prop)
	else
		TriggerEvent('hud:NotifColor', "Personne devant vous",6)
	end
end

function fumer(infoitem)
    TriggerEvent('player:looseItem',infoitem.id,infoitem.ligne,1)
    menuitems()
    VMenu.visible = false
    FumerInAnim()
end

local maxdamage = 1500

function kitrepair(infoitem)
    local vehFront = VehicleInFront()
    local playerped = GetPlayerPed(-1)
    local coordA = GetEntityCoords(playerped, 1)
    local coordB = GetOffsetFromEntityInWorldCoords(playerped, 0.0, 5.0, 0.0)
    local targetVehicle = getVehicleInDirection(coordA, coordB)
    if targetVehicle ~= 0 then
        VMenu.visible = false
        damage = tonumber(GetVehicleBodyHealth(targetVehicle,false)) + tonumber(GetVehicleEngineHealth(targetVehicle,  false))
        if damage < maxdamage then
            TriggerEvent('hud:NotifColor',"Véhicule trop endommagé",6)
        else
            TriggerEvent('player:looseItem',infoitem.id,infoitem.ligne,1)
            if GetEntityModel(GetPlayerPed(-1)) == GetHashKey("mp_f_freemode_01") then
                TriggerEvent('vmenu:anim','mini@repair','fixing_a_ped')
            else
                local orientation = GetEntityHeading(GetPlayerPed(-1))
                SetEntityHeading(GetPlayerPed(-1),orientation+180.0)
                TaskStartScenarioInPlace(GetPlayerPed(-1), "WORLD_HUMAN_VEHICLE_MECHANIC", 0, 1)
                PlayAmbientSpeech1(GetPlayerPed(-1), "GENERIC_CURSE_MED" ,"SPEECH_PARAMS_FORCE")
            end
            Citizen.Wait(10000)
            SetVehicleFixed(targetVehicle)
            SetVehicleDeformationFixed(targetVehicle)
            SetVehicleUndriveable(targetVehicle, false)
            damage = 2000-damage
            damage = math.floor(damage/10)
            if damage > 100 then
                damage = 100
            end
            TriggerEvent('hud:Notif',"~s~Véhicule réparé.")
            ClearPedTasks(GetPlayerPed(-1))
        end
    else
        TriggerEvent('hud:NotifColor',"Pas de véhicule proche de vous.",6)
    end
    
end

function CarJack(infoitem)
    local pos = GetEntityCoords(GetPlayerPed(-1))
	local entityWorld = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, 1.0, 0.0) 
	 local rayHandle = Citizen.InvokeNative(0x28579D1B8F8AAC80,pos, entityWorld, 1.0, 10, GetPlayerPed(-1), 0)
	local _, _, _, _, vehicleHandle = GetRaycastResult(rayHandle)
	if(DoesEntityExist(vehicleHandle)) then
        VMenu.visible = false
        TriggerEvent('player:looseItem',infoitem.id,infoitem.ligne,1)
        TriggerEvent('vmenu:anim','mini@telescope','enter_front')
        Wait(1800)
        TriggerEvent('vmenu:anim','mini@telescope','idle')
        Wait(7000)
        TriggerEvent('vmenu:anim','mini@telescope','exit_front')
        if math.random() > 0.34 then
            if math.random() < 0.25 then
                SetVehicleAlarm(vehicleHandle,true)
                StartVehicleAlarm(vehicleHandle)
                TriggerEvent('Alerte:vol_voiture',vehicleHandle)
                TriggerEvent('hud:NotifColor',"Véhicule ~h~ouvert~h~",141)
            else               
                TriggerEvent('hud:NotifColor',"Véhicule ~h~ouvert~h~ sans déclencher l'alarme",141) 
                SetVehicleAlarm(vehicleHandle,false)
            end
            SetVehicleDoorsLockedForAllPlayers(vehicleHandle, false)
            SetVehicleDoorsLocked(vehicleHandle,0)
        else
            TriggerEvent('hud:NotifColor',"~h~Echec de crochetage~h~",6)
            SetVehicleAlarm(vehicleHandle,true)
            StartVehicleAlarm(vehicleHandle)
            TriggerEvent('Alerte:vol_voiture',vehicleHandle)
        end
        Wait(1400)
        ClearPedTasks(GetPlayerPed(-1))
    else
        TriggerEvent('hud:NotifColor',"Pas de véhicule proche de vous.",6)
    end    
end

function invboire(infoitem)
    TriggerEvent('player:looseItem',infoitem.id,infoitem.ligne,1)
    TriggerEvent('besoin:addbesoin', tonumber(ITEMS[infoitem.id].food),tonumber(ITEMS[infoitem.id].water))
    if ITEMS[infoitem.id].hash ~= nil then
        newObject(ITEMS[infoitem.id].hash) 
    end
    TriggerEvent("vmenu:animtop", "amb@world_human_drinking@coffee@male@idle_a", "idle_b")
    Citizen.Wait(8000)
    StopAnimTask(GetPlayerPed(-1), "amb@world_human_drinking@coffee@male@idle_a", "idle_b",1.0)
	DeleteEntity(Prop)
    menuitems()
end

function invmanger(infoitem)-- target = Dernier joueur à avoir parlé, pas besoin ici. Mais obligatoire !
    TriggerEvent('player:looseItem',infoitem.id,infoitem.ligne,1)
    TriggerEvent('besoin:addbesoin', tonumber(ITEMS[infoitem.id].food),tonumber(ITEMS[infoitem.id].water))
    if ITEMS[infoitem.id].hash ~= nil then
        newObject(ITEMS[infoitem.id].hash) 
    end
    TriggerEvent("vmenu:animtop", "amb@world_human_seat_wall_eating@male@both_hands@base", "base")
    Citizen.Wait(8000)
    StopAnimTask(GetPlayerPed(-1), "amb@world_human_seat_wall_eating@male@both_hands@base", "base",1.0)
	DeleteEntity(Prop)
    menuitems()
end


function invjeter(infoitem)
    TriggerEvent('DisplayInputHTML',"Entrez le quantité a jeter","number",10, function(qty)
        if qty > 0 and qty <= infoitem.item.quantite  then
            ClearPedTasksImmediately(GetPlayerPed(-1))
            if infoitem.isArme then
                RemoveWeaponFromPed(GetPlayerPed(-1),GetHashKey(ITEMS[infoitem.id].hash)) 
            end
            TriggerEvent('vmenu:animtop','mp_weapon_drop','drop_lh')
            Citizen.Wait(1000)
            StopAnimTask(GetPlayerPed(-1), 'mp_weapon_drop','drop_lh',1.0)
            local extra = {}
            if infoitem.item.extra ~= nil then
               extra = infoitem.item.extra
            end
            TriggerEvent('DropSystem:drop',extra,infoitem.id,qty)
            TriggerEvent('player:looseItem',infoitem.id,infoitem.ligne,qty)
            menuitems()
        else
           TriggerEvent("hud:NotifColor","Erreur de saisie",6) 
        end
    end)
end

function deleteaccessoirearme(infoitem)
    VMenu.ResetMenu("Accessoires équipés")
    VMenu.AddPrev("menuitems")
    VMenu.ResetCursor(2)
    VMenu.AddSep(ITEMS[infoitem.id].libelle)
    local count = 0
    for ligne,acc in pairs (infoitem.item.extra) do
        for _,detail in pairs(ListAccessoire) do
           if acc == detail.hash then
                count = count +1
                VMenu.AddFunc(detail.name,"DeleteAcc",{infoitem,ligne,detail.name},"~r~Enlever l'accessoire")
            end
        end
    end
    if count == 0 then
       VMenu.AddFunc("Aucun accessoire","menuitems",nil,"Retour")
    end
end

function DeleteAcc(arg)
    local infoitem = arg[1]
    local ligne = arg[2]
    local nom = arg[3]
    TriggerEvent('hud:NotifKey',"~INPUT_MP_TEXT_CHAT_TEAM~", "~g~Supprimer le "..nom)
    Wait(100)
    TriggerEvent('hud:NotifKey',"~INPUT_REPLAY_ENDPOINT~", "~r~Annuler")
    while true do
        Wait(5)
		if IsControlJustReleased(1, 246) then
            TriggerEvent('hud:NotifDel')
            local HashArme = ITEMS[infoitem.id].hash
            local HashAcc = INV[infoitem.id][infoitem.ligne]['extra'][ligne]
            RemoveWeaponComponentFromPed(GetPlayerPed(-1),GetHashKey(HashArme),GetHashKey(HashAcc))
            table.remove(INV[infoitem.id][infoitem.ligne].extra,ligne)
            TriggerEvent('hud:NotifColor',nom.." démonté",141)
            TriggerServerEvent("item:updateInv", INV)
            deleteaccessoirearme({['item'] = infoitem.item,['ligne'] = infoitem.ligne,['id'] = infoitem.id})
            break
        end
		if IsControlJustReleased(1, 306) then
            TriggerEvent('hud:NotifDel')
			break
        end
    end
end

function EqTenuePlongee(args)
    local choix = args[1]
    local item = args[2]
    TenuePlongee = not TenuePlongee
    if TenuePlongee then
        TriggerEvent('Plongee:Equiper',choix,item.item.extra)
    else
        TriggerEvent('Plongee:Desequiper')
    end
    ItemMenu(item)
end

function PoserCoffre(ligne)
    VMenu.visible = false
    TriggerEvent('CoffreMove:Poser',ligne)
end
------------------------
------ OBJET -----------
------------------------

function newObject(Model)
	local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1), true))
	RequestModel(Model)
	while not HasModelLoaded(Model) do
		Citizen.Wait(10)
	end
	Citizen.Wait(157)
	Prop = CreateObject(Model, 1.0, 1.0, 1.0, 1, 1, 1)
	local bone = GetPedBoneIndex(GetPlayerPed(-1), 28422)
    if Model == "prop_cs_swipe_card" then
	    AttachEntityToEntity(Prop, GetPlayerPed(-1), bone, 0.03, 0.03, 0.03, 180.0, 180.0, 0.0, 1, 1, 1, 0, 2, 1)
    elseif Model == 'prop_anim_cash_pile_01' then
	    AttachEntityToEntity(Prop, GetPlayerPed(-1), bone,0.04, 0.06, 0.05, 40.0, 40.0, 330.0, 1, 1, 1, 0, 2, 1)
    elseif Model == "prop_food_chips" then
	    AttachEntityToEntity(Prop, GetPlayerPed(-1), bone, -0.0, 0.03, 0.00, 90.0, 0.0, 0.0, 1, 1, 1, 0, 2, 1)
    elseif Model == "ng_proc_coffee_01a" then
	    AttachEntityToEntity(Prop, GetPlayerPed(-1), bone, -0.0, 0.00,- 0.10, 0.0, 0.0, 0.0, 1, 1, 1, 0, 2, 1)
    elseif Model == 'prop_beer_pride' then
	    AttachEntityToEntity(Prop, GetPlayerPed(-1), bone, -0.0, 0.00,- 0.10, 0.0, 0.0, 0.0, 1, 1, 1, 0, 2, 1)
    else
	   AttachEntityToEntity(Prop, GetPlayerPed(-1), bone, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1, 1, 0, 0, 2, 1)
    end
    SetEntityCollision(Prop,false,true)
end
------------------------
---------- CAR ---------
------------------------
function VehicleInFront()
    local pos = GetEntityCoords(GetPlayerPed(-1))
    local entityWorld = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, 3.0, 0.0)
	local rayHandle = Citizen.InvokeNative(0x28579D1B8F8AAC80,pos.x, pos.y, pos.z, entityWorld.x, entityWorld.y, entityWorld.z, 3/2, 10, GetPlayerPed(-1), 0)
    local a, b, c, d, result = GetRaycastResult(rayHandle)
    return GetVehicleNumberPlateText(result)
end

function getVehicleInDirection(coordFrom, coordTo)
	local rayHandle = Citizen.InvokeNative(0x28579D1B8F8AAC80,coordFrom.x, coordFrom.y, coordFrom.z, coordTo.x, coordTo.y, coordTo.z, 5/2, 10, GetPlayerPed(-1), 0)
	local a, b, c, d, vehicle = GetRaycastResult(rayHandle)
	return vehicle
end
------------------------
-------- TARGET --------
------------------------
Target = -1

function taketarget()
    local X = GetEntityForwardX(GetPlayerPed(-1))
    local Y = GetEntityForwardY(GetPlayerPed(-1))
    for i = 0,64 do
        if NetworkIsPlayerConnected(i) then
            if NetworkIsPlayerActive(i) and GetPlayerPed(i) ~= nil then
                if GetPlayerServerId(i) ~= GetPlayerServerId(PlayerId()) then
                    if (GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)).x+X,GetEntityCoords(GetPlayerPed(-1)).y+Y,GetEntityCoords(GetPlayerPed(-1)).z,GetEntityCoords(GetPlayerPed(i)).x,GetEntityCoords(GetPlayerPed(i)).y,GetEntityCoords(GetPlayerPed(i)).z) < 1.0001) then
                        Target = i
                        break
                    end
                Target = -1
                end
            end
        end
    end
end

--------------------------
---------- INFO ----------
--------------------------
RegisterNetEvent("menu:setUser")
AddEventHandler("menu:setUser", function(infos)
	for k,v in pairs(infos) do
		User[k] = v
	end
end)

---------------------------
-------- CRAFT ------------
---------------------------

RegisterNetEvent('craft:activer')
AddEventHandler('craft:activer', function(illegal)
    if illegal then
        craftIllegal = true
    end
end)

function craftMenu()
    VMenu.curItem = 1
    VMenu.ResetMenu()
    VMenu.AddPrev('Main')
    VMenu.EditHeader("Craft")
    local count = 0
    for i,v in pairs (ListCraft) do
        if v.jobs[1] == nil then
            count = count + AddCraft(v,i)
        else
            local bonjob = false
            for _,job in pairs(v.jobs) do
                if jobID == job then
                    bonjob = true
                end
            end
            if bonjob then
                count = count + AddCraft(v,i)
            end
            
        end
    end
    if count < 1 then
       VMenu.AddSep("Aucun Craft") 
    end
end

function AddCraft(infos,i)
    local afficher = false
    if infos.Illegal and craftIllegal then
        afficher = true
    elseif not infos.Illegal then
        afficher = true
    end
    if afficher then
        local info = ""
        for a,b in pairs(infos.need) do
            if info ~= "" then
                info = info .." - "
            end
            info = info .. b[1] .. " " .. ITEMS[b[2]].libelle
        end
        if infos.Illegal then
            VMenu.AddFunc("~r~"..ITEMS[i].libelle,"craftItem",{infos,i},info)
        else
            VMenu.AddFunc(ITEMS[i].libelle,"craftItem",{infos,i},info)
        end
        return 1
    end
    return 0
end

function craftItem(arg)
    local info = arg[1]
    local item = arg[2]
    TriggerEvent('DisplayInputHTML',"Entrez le quantité à crafter","number",10, function(res)
        local necessaire = true
        for a,v in pairs (info.need) do
            if getQuantity(v[2]) < res * v[1] then
                necessaire = false
            end
        end
        if not necessaire then
            TriggerEvent("hud:NotifColor","Vous n'avez pas le nécessaire",6)
            craftMenu()
        else
            for i=1,res do
                TriggerEvent("vmenu:anim", "missmechanic", "work2_base")
                Wait(2000)
                for a,b in pairs(info.need) do
                    TriggerEvent('player:looseItem',b[2],b[1])
                end
                TriggerEvent('player:receiveItem',item,1)
                TriggerEvent('hud:NotifColor',"Craft "..i.."/"..res.." de "..ITEMS[item].libelle,141)
            end
            ClearPedTasks(GetPlayerPed(-1))
        end
    end)
end
--------------------------------
---------- GIVE ARGENT ---------
--------------------------------
local cashconfirmed = 0
local sendTarget = -1

function give(info)
    taketarget()
    if Target ~= -1 then
		VMenu.visible = false
        TriggerEvent('DisplayInputHTML',"Entrez le quantité a donner","number",10, function(res)
            if res > 0 then
                if (INV[info.id][info.ligne].quantite - tonumber(res) >= 0) then
                    TriggerServerEvent("player:giveItem", info.id, info.ligne, ITEMS[info.id].libelle, res, GetPlayerServerId(Target), INV[info.id][info.ligne])
                else
                    TriggerEvent('hud:NotifColor',"Vous n'en avais pas autant sur vous !",6)  
                end
            else
                TriggerEvent('hud:NotifColor',"Erreur de saisie",6)
            end
        end)
    else
		TriggerEvent('hud:NotifColor', "Personne devant vous",6)
    end 
end

function menugiveCash()
    taketarget()
	if Target ~= -1 then
		VMenu.visible = false
		DisplayOnscreenKeyboard(true, "FMMC_KEY_TIP8", "", "", "", "", "", 30)
		cashconfirmed = 1
		sendTarget = Target
	else
		TriggerEvent('hud:NotifColor', "Personne devant vous",6)
	end
end

-------------------------------
-------- fouille --------------
-------------------------------
local FouilleMainEnAir = false
local CibleFouille = -1

function fouiller()
    CibleFouille = -1
    FouilleMainEnAir = false
    taketarget()
	if Target ~= -1 then
        CibleFouille = Target
        if IsEntityDead(GetPlayerPed(Target)) or IsEntityPlayingAnim(GetPlayerPed(Target),"missminuteman_1ig_2","handsup_enter",3) then
            if IsEntityPlayingAnim(GetPlayerPed(Target),"missminuteman_1ig_2","handsup_enter",3) then
                FouilleMainEnAir = true
            end
            TriggerServerEvent('menu:fouilleenvoi',GetPlayerServerId(Target))
            VMenu.ResetMenu()
            VMenu.curItem = 2
            VMenu.AddPrev('Main')
            VMenu.EditHeader("Fouille")
            VMenu.AddSep("Fouille en cours")
        else
            TriggerEvent('hud:NotifColor',"La personne n'est pas inconsciente",6)
            TriggerServerEvent('menu:FouilleNotif',GetPlayerServerId(Target))
        end
	else
		TriggerEvent('hud:NotifColor', "Personne devant vous",6)
	end
end

RegisterNetEvent('menu:fouillerecept')
AddEventHandler('menu:fouillerecept', function(listitems)
    VMenu.ResetMenu()
    VMenu.curItem = 1
    VMenu.AddPrev('Main')
    VMenu.EditHeader("Fouille")
        
    local count = 0
    local libelle = ""
    for id, group_item in pairs(listitems) do
        local id = tonumber(id)
        for ligne,item in pairs (group_item) do
            if (item.quantite > 0) then
                count = count +1
                if ITEMS[id].isIllegal then
                    if tonumber(id) == 41 then
                        libelle = "~r~"..tostring(ITEMS[id].libelle).." : $" .. tostring(comma_value(item.quantite))
                    else
                        libelle = "~r~"..tostring(ITEMS[id].libelle).." : " .. tostring(comma_value(item.quantite))
                    end
                elseif tonumber(id) == 47 then
                    libelle =tostring(ITEMS[id].libelle).." "..exports.plongee:GetNomCouleur(item.extra)
                else
                    libelle =tostring(ITEMS[id].libelle).." : " .. tostring(comma_value(item.quantite))
                end
                if ITEMS[id].isArme then
                    VMenu.AddWeap(libelle, "FouilleTake", {['item'] = item,['ligne'] = ligne,['id'] = id},item.poids,ITEMS[id].hash,item.extra)
                else
                    VMenu.AddPoids(libelle, "FouilleTake", {['item'] = item,['ligne'] = ligne,['id'] = id},item.poids,"Prendre")
                end
            end
        end
    end
    if count == 0 then
        VMenu.AddFunc("Vide","Main",'Main',"Retour")
    end   
end)

RegisterNetEvent('menu:actualiserfouille')
AddEventHandler('menu:actualiserfouille', function()
   Main() 
end)

function FouilleTake(info)
    TriggerEvent('DisplayInputHTML',"Entrez le quantité a prendre","number",10, function(res)
        if (GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)).x,GetEntityCoords(GetPlayerPed(-1)).y,GetEntityCoords(GetPlayerPed(-1)).z,GetEntityCoords(GetPlayerPed(CibleFouille)).x,GetEntityCoords(GetPlayerPed(CibleFouille)).y,GetEntityCoords(GetPlayerPed(CibleFouille)).z) < 1.0001) then
            if res > 0 then
                if (info.item.quantite - tonumber(res) >= 0) then
                    VMenu.ResetMenu()
                    VMenu.curItem = 1
                    VMenu.AddPrev('Main')
                    VMenu.EditHeader("Fouille")
                    VMenu.AddSep("Veuillez patienter")
                    TriggerServerEvent("menu:fouilletake",tonumber(res), info.id,info.ligne, ITEMS[info.id].libelle,GetPlayerServerId(Target),FouilleMainEnAir)
                    CibleFouille = -1
                else
                    TriggerEvent('hud:NotifColor',"Valeur incorrecte",6)
                end
            end
        else
            TriggerEvent('hud:NotifColor',"La personne n'est plus là")
            CibleFouille = -1
        end
    end)
end

function animation(dict,anim)
    Citizen.CreateThread(function()
		Wait(100)
		RequestAnimDict(dict)

		while not HasAnimDictLoaded(dict) do
			Citizen.Wait(0)
		end

		local myPed = PlayerPedId()
		local animation = anim
		local flags = 16 -- only play the animation on the upper body

		TaskPlayAnim(myPed, dict, animation, 8.0, -8, -1, flags, 0, 0, 0, 0)
	end)
end

AddEventHandler('playerSpawned', function()
--AddEventHandler('onClientResourceStart', function(res)
   -- if res == "menu_personnel" then
        TriggerServerEvent("menu:updateUser")
        TriggerServerEvent('craft:check')
        TriggerServerEvent("item:getItems")
        while User.prenom == "" do
            Wait(100)
        end
        VMenu.AddMenuName( 'Menu personnel', 'personnel',244,163,87,User.prenom)
   -- end
end)

local ralentit = false
Citizen.CreateThread(function()
    while true do
        Wait(500)
            poids = getPods()
        end
end)

function InfoDrawText(Text, X, Y, ScX, ScY, Font, Outline, Shadow, Center, RightJustify, R, G, B, A)
	SetTextFont(Font)
	SetTextScale(ScX, ScY)
	SetTextColour(R, G, B, A)
	if Outline then
		SetTextOutline()
	end
	if Shadow then
		SetTextDropShadow()
	end
	SetTextCentre(Center)
    SetTextRightJustify(RightJustify)
	SetTextEntry("STRING")
	AddTextComponentString(Text)
	DrawText(X, Y)
end

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        if IsControlJustPressed(1, openkey) and not IsEntityDead(GetPlayerPed(-1))  then
            TriggerServerEvent("item:GetInv")
            TriggerServerEvent("menu:updateUser")
            VMenu.AddMenuName( 'Menu personnel', 'personnel',244,163,87,User.prenom)
        end
        if IsControlJustPressed(1, openkey) and not IsEntityDead(GetPlayerPed(-1)) then -- Menu to draw
           if not VMenu.visible then TriggerEvent('VMenu.CloseAll') end
            taketarget()
            VMenu.ResetCursor()
            Main()
            VMenu.visible = not VMenu.visible -- Hide/Show the menu
            TriggerEvent('AFKkick:reset')
        end
        VMenu.Show()
            if poids > UltimeCapacity then
                InfoDrawText("~r~Vous êtes ~h~beaucoup~h~ trop lourd : "..poids..'kg', 0.5, 0.92, 0.5, 0.5, 8, false, false, true, false, 255, 0, 0, 255)
                DisableControlAction(0,266,true)  
                DisableControlAction(0,267,true)  
                DisableControlAction(0,268,true)  
                DisableControlAction(0,269,true)
                DisableControlAction(0,30,true)  
                DisableControlAction(0,31,true)   
                DisableControlAction(0,32,true)   
                DisableControlAction(0,33,true)   
                DisableControlAction(0,34,true)   
                DisableControlAction(0,35,true)  
                DisableControlAction(0,36,true)
                if IsPlayerFreeForAmbientTask(PlayerId()) or ralentit then
                    local heading = GetGameplayCamRelativeHeading()
                    TaskAchieveHeading(GetPlayerPed(-1),heading,10000)
                end
                if not ralentit and IsPlayerFreeForAmbientTask(PlayerId()) then
                    TaskForceMotionState(GetPlayerPed(-1),1110276645)
                    ralentit = true
                end
            elseif poids > maxCapacity then
                InfoDrawText("~r~Vous êtes trop lourd : "..poids..'kg', 0.5, 0.92, 0.5, 0.5, 8, false, false, true, false, 255, 0, 0, 255)
                EnableAllControlActions(0)
                if not IsPedSwimming(GetPlayerPed(-1)) then
                    DisableControlAction(0,22,true)
                    DisableControlAction(0,21,true)
                    DisableControlAction(0,36,true)
                    if not ralentit and IsPlayerFreeForAmbientTask(PlayerId()) then
                        TaskForceMotionState(GetPlayerPed(-1),1110276645)
                        ralentit = true
                    end
                end
            elseif ralentit then
                EnableAllControlActions(0)
                ralentit = false
            end
           
        if cashconfirmed == 1 then
			if UpdateOnscreenKeyboard() == 3 then
				cashconfirmed = 0
			elseif UpdateOnscreenKeyboard() == 1 then
				local txt = GetOnscreenKeyboardResult()
				if (string.len(txt) > 0) and (string.match(txt, '%d+')) then -- BEAU REGEX PATTERN EN LUA PARCE QUE C'EST PAUVRE
					txt = tonumber(txt)
					if User.money >= txt then
						if txt > 0 then
							addCash = txt
							cashconfirmed = 2
						else
							TriggerEvent('hud:NotifColor', "Vous devez entrer un nombre positif",6)
							cashconfirmed = 0
							sendTarget = -1
						end
					else
						TriggerEvent('hud:NotifColor', "Vous n'avez pas assez d'argent",6)
						cashconfirmed = 0
						sendTarget = -1
					end
				else
					TriggerEvent('hud:NotifColor', "Entrer un montant valide",6)
					cashconfirmed = 0
					sendTarget = -1
				end
			elseif UpdateOnscreenKeyboard() == 2 then
				cashconfirmed = 0
				sendTarget = -1
			end
		end
		if cashconfirmed == 2 then
            toPlayer = GetPlayerServerId(sendTarget)
            amount = addCash
            TriggerEvent('bank:givecash', toPlayer, amount)
            animation("mp_common","givetake2_a")
            newObject("prop_anim_cash_pile_01")
            Citizen.Wait(1600)
            DeleteEntity(Prop)
			cashconfirmed = 0
			sendTarget = -1
		end
    end
end)
