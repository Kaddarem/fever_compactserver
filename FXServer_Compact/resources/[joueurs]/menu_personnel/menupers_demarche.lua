demarche = {
    {'',-1,'Normal'},
    { "move_ped_crouched",1,"Accroupi" },
	{ "move_m@JOG@",1,"Balade" },
	{ "move_injured_generic",1,"Bléssé" },
	{ "move_heist_lester",1,"Bloqué du dos" },
	{ "move_lester_CaneUp",1,"Boitant (avec cane)" },
    {"MOVE_M@DRUNK@MODERATEDRUNK",1, "Bourré 1" },
    {"MOVE_M@DRUNK@MODERATEDRUNK_HEAD_UP",1,"Bourré 2"  },
    { "MOVE_M@BAIL_BOND_TAZERED",1,"Drogué" },
    { "move_m@bag",1,"Determiné 1" },
	{ "move_ped_mop",1,"Determiné 2" },
	{ "move_ped_bucket",1,"Determiné 3" },
	{ "move_characters@michael@fire",1,"Determiné 4" },
	{ "MOVE_P_M_ONE_BRIEFCASE",1,"Determiné 5" },
	{ "MOVE_P_M_ONE",1,"Determiné 6" },
	{ "MOVE_M@FEMME@",1,"Effeminé" },
	{ "move_m@gangster@var_f",1,"Fatigué" },
    { "MOVE_F@FEMME@",1,"Femme" },
    { "MOVE_F@POSH@",1,"Femme Chic" },
    { "MOVE_F@TOUGH_GUY@",1,"Femme Difficile" },
    { "move_f@scared",1,"Femme Effrayer" },
    { "move_f@flee@a",1,"Femme Fuir" },
    { "MOVE_F@GANGSTER@NG",1,"Femme Gangster" },
    { "move_f@sexy@a",1,"Femme Sexy" },
	{ "FEMALE_FAST_RUNNER",1,"Femme Coureur" },
	{ "MOVE_M@GANGSTER@NG",1,"Fier" },
	{ "move_m@gangster@var_e",1,"Gangster" },
    { "move_m@brave",1,"Homme Brave" },
    { "MOVE_M@POSH@",1,"Homme Chic" },
    { "move_m@casual@d",1,"Homme Decontracté" },
	{ "move_p_m_zero_slow",1,"Homme lent" },
    { "MOVE_M@TOUGH_GUY@",1,"Homme Sexy" },
	{ "move_characters@franklin@fire",1,"Imposant" },
	{ "move_p_m_zero_janitor",1,"Jantior" },
    { "ANIM_GROUP_MOVE_BALLISTIC",1,"Poid Lourd" },
    {"MOVE_M@DRUNK@SLIGHTLYDRUNK",1,"Pompette 1" },
    { "MOVE_M@BAIL_BOND_NOT_TAZERED",1,"Pompette 2" },
	{ "move_m@fire",1,"Pompier" },
	{ "missfbi4prepp1_garbageman",1,"Porte un truc 1" },
	{ "clipset@move@trash_fast_turn",1,"Porte un truc 2" },
	{ "move_characters@Jimmy@slow@",1,"Prend ton temps" },
	{ "move_m@gangster@var_i",1,"Pressé" },
	{ "ANIM_GROUP_MOVE_LEMAR_ALLEY",1,"Roule du cul" },
	{ "MOVE_M@PRISON_GAURD",1,"Tir" },
    { "MOVE_M@DRUNK@VERYDRUNK",1,"Très saoul" },
}

    
function menuDemarche()
    VMenu.curItem = 1
    VMenu.ResetMenu() 
    VMenu.EditHeader("Demarche")
    VMenu.AddPrev('Main')
    for _,dema in pairs(demarche) do
        VMenu.AddFunc(dema[3], "setDemarche", dema,"Jouer")
    end
end
    
function setDemarche(table)
    if(table[2]~=-1)then
        applyDemarche(table)
    else
        removeDemarche()
    end
end

function applyDemarche(table)
    RequestAnimSet(table[1])
	while not HasAnimSetLoaded(table[1]) do 
      Citizen.Wait(0)
    end
    SetPedMovementClipset(GetPlayerPed(-1),table[1],1.0)
end

function removeDemarche()
    ResetPedMovementClipset(GetPlayerPed(-1), 0)
end