
local inAnim = "enter"
local outAnim = "exit"
local waitAnim = "base"

local CigProp = 0
local CigModel = "ng_proc_cigarette01a"

--------------------------------------------------------------------------------
--
--								FUNCTIONS
--
--------------------------------------------------------------------------------
function newCigProp()
	local x,y,z = table.unpack(GetEntityCoords(GetPlayerPed(-1), true))
	RequestModel(CigModel)
	while not HasModelLoaded(CigModel) do
		Citizen.Wait(100)
	end
	return CreateObject(CigModel, 1.0, 1.0, 1.0, 1, 1, 0)
end

function FumerInAnim()
	if IsPlayerDead(PlayerId()) then
		return
	end
	local bone = GetPedBoneIndex(GetPlayerPed(-1), 28422)
	local dict2 = "amb@world_human_smoking@"
	if GetEntityModel(GetPlayerPed(-1)) == GetHashKey("mp_f_freemode_01") then
		dict2 = dict2 .. "female"
	else
       dict2 = dict2 .. "male@male_a" 
    end
    dict = dict2 .. "@enter"
    
	RequestAnimDict(dict)
	while not HasAnimDictLoaded(dict) do
		Citizen.Wait(100)
	end

	TaskPlayAnim(GetPlayerPed(-1), dict, inAnim, 4.0, -1, -1, 50, 0, false, false, false)
	Citizen.Wait(157)
	CigProp = newCigProp()
    SetEntityCollision(CigProp,false,true)
	AttachEntityToEntity(CigProp, GetPlayerPed(-1), bone, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1, 1, 0, 0, 2, 1)
    Citizen.Wait(10000)
    FumerTouch(dict2)
end

function FumerTouch(dict2)
    local dict = dict2 .. "@idle_a"
    local anim = 'idle_b'
    RequestAnimDict(dict)
	while not HasAnimDictLoaded(dict) do
		Citizen.Wait(100)
	end
    local taff = 0
    while true do
        Wait(0)
        if taff < 10  then
            DisplayHelp("Appuyez sur ~INPUT_CONTEXT~ pour fumer et ~INPUT_DETONATE~ pour l'éteindre")
            if IsControlJustPressed(1, 51) then
                taff = taff + 1
                TaskPlayAnim(GetPlayerPed(-1), dict, anim, 4.0, -1, -1, 50, 0, false, false, false)
            end
            if IsControlJustPressed(1,47) then
                FumerOutAnim(dict2)
                return
            end
        else
            FumerOutAnim(dict2)
            return
        end
    end
end


function FumerOutAnim(dict2)
	if IsPlayerDead(PlayerId()) then
		return
	end
    
	local dict = dict2 .. "@exit"

	RequestAnimDict(dict)
	while not HasAnimDictLoaded(dict) do
		Citizen.Wait(1)
	end
	if GetCurrentPedWeapon == 1 then
		ClearPedSecondaryTask(GetPlayerPed(-1))
		return
	end
	TaskPlayAnim(GetPlayerPed(-1), dict, outAnim, 5.0, -1, -1, 50, 0, false, false, false)
	Citizen.Wait(2000)
	DeleteEntity(CigProp)
	Citizen.Wait(500)
	StopAnimTask(GetPlayerPed(-1), dict, outAnim, 1.0)
end