animations = {
{'PROP_HUMAN_BUM_SHOPPING_CART',99,'Accouder au comptoir'},
{'',15,'Air guitar'},
{'',16,'Air piano'},
{'',26,'Allez, salut'},
{'WORLD_HUMAN_CHEERING',99,'Applaudir 1'},
{'',32,'Applaudir 2'},
{'WORLD_HUMAN_GUARD_STAND',99,'Attendre'},
{'CODE_HUMAN_CROSS_ROAD_WAIT',99,'Attendre une voiture'},
{'WORLD_HUMAN_LEANING',99,'Attendre contre un mur'},
{'WORLD_HUMAN_CLIPBOARD',99,'Bloc-note'},
{'WORLD_HUMAN_HANG_OUT_STREET',99,'Croiser les bras'},
{'WORLD_HUMAN_STRIP_WATCH_STAND',99,'Danser 1'},
{'',29,'Danser 2'},
{'',30,'Danser 3'},
{'WORLD_HUMAN_PARTYING',99,'Danser avec une biere'},
{'',31,'Danser DJ'},
{'',33,'Debouche narine'},
{'',24,'Dire Bonjour'},
{'WORLD_HUMAN_BUM_SLUMPED',99,'Dormir'},
{'',12,'Embrasser'},
{'',17,'Face palm'},
{'',28,'Faire des abdos'},
{'',27,'Faire des pompes'},
{'',8,'Faire la belle'},
{'WORLD_HUMAN_HUMAN_STATUE',99,'Faire la statue'},
{'',18,'Faire le dure'},
{'PROP_HUMAN_MUSCLE_CHIN_UPS',99,'Faire des tractions'},
{'WORLD_HUMAN_YOGA',99,'Faire du Yoga'},
{'',34,'Faire le bodybuilder'},
{'WORLD_HUMAN_MOBILE_FILM_SHOCKING',99,'Filmer'},
{'',7,'Humble discours'},
{'',13,'HighFive'},
{'WORLD_HUMAN_MUSICIAN',99,'Jouer de la musique'},
{'WORLD_HUMAN_BUM_FREEWAY',99,'Mendier'},
{'WORLD_HUMAN_MUSCLE_FLEX',99,'Musclor'},
{'world_human_maid_clean',99,'Nettoyer'},
{'WORLD_HUMAN_PAPARAZZI',99,'Prendre des photos'},
{'WORLD_HUMAN_BINOCULARS',99,'Regarder aux jumelles'},
{'WORLD_HUMAN_TOURIST_MAP',99,"Regarder la carte"},
{"",14,"Rock 'n' Roll"},
{'WORLD_HUMAN_PICNIC',99,"S'asseoir au sol"},
{'',22,"S'asseoir sur une chaise"},
{'',19,'Salut anglais'},
{'',20,'Sautiller'},
{'WORLD_HUMAN_JOG_STANDING',99,"S'échauffer"},
{'',21,"S'étirer"},
{'',23,"Se rendre"},
{'',25,"Signe de la main"},
{'WORLD_HUMAN_SUNBATHE_BACK',99,'Sur le dos'},
{'WORLD_HUMAN_SUNBATHE',99,'Sur le ventre'},
{'WORLD_HUMAN_HAMMERING',99,'Taper au marteau'},
{'',10,'Two thumbs up'},
{'',11,'Viens avec moi!'},
{'',9,'Viens, je suis là!'},
}

sautiller = {
    "idle_a",
    "idle_b",
    "idle_c",
    "idle_d",
    "idle_e",
}

function menuAnim()
    math.randomseed(GetNetworkTime())
    Wait(10)
    VMenu.curItem = 1
    VMenu.ResetMenu() 
    VMenu.EditHeader("Animations")
    VMenu.AddPrev('Main')
    ClearPedTasks(GetPlayerPed(-1)) 
	VMenu.AddFunc("~b~Démarche", "menuDemarche", nil, "Accéder")
	VMenu.AddFunc("~r~Pegi21", "menuPegi21", nil, "Accéder")
    VMenu.AddSep("")
    for _,anim in pairs(animations) do
        VMenu.AddFunc(anim[3], "animplay", anim,"Jouer")
    end
    stopSit()
end

function animplay(table)
    menuAnim()
	VMenu.visible = false
	local manSkin = GetHashKey("mp_m_freemode_01")
    local ped = GetPlayerPed(-1);
	
	if table[2] == 99 then
		PlayScenario(table[1],"GENERIC_CURSE_MED" ,"SPEECH_PARAMS_FORCE")
	elseif table[2] == 7 then
		TriggerEvent("vmenu:anim", "missmic4premiere", "prem_4stars_a_benton")
	elseif table[2] == 8 then
		TriggerEvent("vmenu:anim", "missmic4premiere", "prem_actress_star_a")
	elseif table[2] == 9 then
		TriggerEvent("vmenu:anim", "missmic4premiere", "wave_c")
	elseif table[2] == 10 then
		TriggerEvent("vmenu:anim", "mp_action", "thanks_male_06")
	elseif table[2] == 11 then
		TriggerEvent("vmenu:anim", "gestures@m@standing@casual", "gesture_come_here_soft")
	elseif table[2] == 12 then
		TriggerEvent("vmenu:anim", "mp_ped_interaction", "kisses_guy_a")
	elseif table[2] == 13 then
		TriggerEvent("vmenu:anim", "mp_ped_interaction", "highfive_guy_a")
	elseif table[2] == 14 then
		TriggerEvent("vmenu:anim", "mp_player_int_upperock", "mp_player_int_rock_enter")
	elseif table[2] == 15 then
		TriggerEvent("vmenu:anim", "anim@mp_player_intcelebrationfemale@air_guitar", "air_guitar")
	elseif table[2] == 16 then
		TriggerEvent("vmenu:anim", "anim@mp_player_intcelebrationfemale@air_synth", "air_synth")
	elseif table[2] == 17 then
		TriggerEvent("vmenu:anim", "anim@mp_player_intcelebrationfemale@face_palm", "face_palm")
	elseif table[2] == 18 then
		if GetEntityModel(ped) == manSkin then
			TriggerEvent("vmenu:anim", "anim@mp_player_intcelebrationmale@knuckle_crunch", "knuckle_crunch")
		else
			TriggerEvent("vmenu:anim", "anim@mp_player_intcelebrationfemale@knuckle_crunch", "knuckle_crunch")
		end
	elseif table[2] == 19 then
		TriggerEvent("vmenu:anim", "anim@mp_player_intcelebrationmale@wave", "wave")
	elseif table[2] == 20 then
        alea = math.random(1,5)
		TriggerEvent("vmenu:anim", "mini@triathlon", sautiller[alea])
	elseif table[2] == 21 then
		TriggerEvent("vmenu:anim", "mini@triathlon", "idle_f")
    elseif table[2] == 22 then
		TriggerEvent("sit:chaise")
	elseif table[2] == 23 then
		TriggerEvent("vmenu:rendre")      
	elseif table[2] == 24 then
		TriggerEvent("vmenu:anim", "gestures@m@standing@casual", "gesture_hello")
	elseif table[2] == 25 then
		TriggerEvent("vmenu:anim", "missmic4premiere", "wave_a")
	elseif table[2] == 26 then
		TriggerEvent("vmenu:anim", "mp_player_intsalute", "mp_player_int_salute")
	elseif table[2] == 27 then
		TriggerEvent("vmenu:anim", "amb@world_human_push_ups@male@base", "base")
	elseif table[2] == 28 then
		TriggerEvent("vmenu:anim", "amb@world_human_sit_ups@male@base", "base")
	elseif table[2] == 29 then
		if GetEntityModel(ped) == manSkin then
			TriggerEvent("vmenu:anim", "amb@world_human_partying@female@partying_beer@base", "base")
		else
			TriggerEvent("vmenu:anim", "mini@strip_club@private_dance@part2", "priv_dance_p2")
		end
	elseif table[2] == 30 then
		TriggerEvent("vmenu:anim", "misschinese2_crystalmazemcs1_cs", "dance_loop_tao")
	elseif table[2] == 31 then
		TriggerEvent("vmenu:anim", "anim@mp_player_intcelebrationmale@dj", "dj")
	elseif table[2] == 32 then
		if GetEntityModel(ped) == manSkin then
			TriggerEvent("vmenu:anim", "amb@world_human_cheering@male_e", "base")
		else
			TriggerEvent("vmenu:anim", "amb@world_human_cheering@female_d", "base")
		end
	elseif table[2] == 33 then
		TriggerEvent("vmenu:anim", "move_p_m_two_idles@generic", "fidget_blow_snot")
	elseif table[2] == 34 then
		if GetEntityModel(ped) == manSkin then
			TriggerEvent("vmenu:anim", "amb@world_human_muscle_flex@arms_at_side@idle_a", "idle_c")
		else
			TriggerEvent("vmenu:anim", "amb@world_human_muscle_flex@arms_at_side@idle_a", "idle_a")
		end		
	end
end

function PlayScenario(param1, param2, param3)
	Citizen.CreateThread(function()
		TaskStartScenarioInPlace(GetPlayerPed(-1), param1, 0, 1)
		PlayAmbientSpeech1(GetPlayerPed(-1), param2, param3)
	end)
end

RegisterNetEvent("vmenu:animtop")
AddEventHandler("vmenu:animtop", function(dict, anim)
	Citizen.CreateThread(function()
		Wait(100)
		RequestAnimDict(dict)

		while not HasAnimDictLoaded(dict) do
			Citizen.Wait(0)
		end

		local myPed = PlayerPedId()
		local animation = anim
		local flags = 50 -- only play the animation on the upper body

		TaskPlayAnim(myPed, dict, animation, 8.0, -8, -1, flags, 0, 0, 0, 0)
	end)
end)

RegisterNetEvent('vmenu:anim')
AddEventHandler("vmenu:anim", function(dict, anim)
	Citizen.CreateThread(function()
		Wait(100)
		RequestAnimDict(dict)

		while not HasAnimDictLoaded(dict) do
			Citizen.Wait(0)
		end

		local myPed = PlayerPedId()
		local animation = anim
		local flags = 1 -- only play the animation on the upper body

		TaskPlayAnim(myPed, dict, animation, 8.0, -8, -1, flags, 0, 0, 0, 0)
	end)
end)

local assis = false

AddEventHandler('sit:chaise', function()
    local ped = GetPlayerPed(-1)
   
    if ped then
        local distance = -0.5
        local pos = GetEntityCoords(ped)
        local head = GetEntityHeading(ped)
        local rad = math.pi/2+math.rad(head)
        local coord = {
                x = pos.x +distance*math.cos(rad),
                y = pos.y +distance*math.sin(rad),
                z = pos.z  ,
            }
        TaskStartScenarioAtPosition(ped, "PROP_HUMAN_SEAT_CHAIR", coord.x,coord.y,coord.z -0.5, head, 0, 1, 1)
        assis = true
        Wait(1000)
        FreezeEntityPosition(ped,true)
    end
end)

function stopSit()
    local ped = GetPlayerPed(-1)
    if assis then
        FreezeEntityPosition(ped,false)
        local ped = GetPlayerPed(-1)
        Citizen.InvokeNative(0xD01015C7316AE176, ped, "Stop")
        if not IsPedInjured(ped) then
            ClearPedSecondaryTask(ped)
        end
        if not IsPedInAnyVehicle(ped, 1) then
            SetPedCurrentWeaponVisible(ped, 1, 1, 1, 1)
        end
        SetPedConfigFlag(ped, 36, 0)
        ClearPedSecondaryTask(PlayerPedId())
        assis = false
    end
end


RegisterNetEvent( 'vmenu:rendre' )
AddEventHandler( 'vmenu:rendre', function()
    local player = GetPlayerPed( -1 )
   if not IsPedInAnyVehicle(player, false) and not IsPedSwimming(player) and not IsPedShooting(player) and not IsPedClimbing(player) and not IsPedCuffed(player) and not IsPedDiving(player) and not IsPedFalling(player) and not IsPedJumping(player) and not IsPedJumpingOutOfVehicle(player) and IsPedOnFoot(player) and not IsPedRunning(player) and not IsPedUsingAnyScenario(player) and not IsPedInParachuteFreeFall(player) then
	if ( DoesEntityExist( player ) and not IsEntityDead( player )) then 
        RequestAnimDict("random@arrests")
        RequestAnimDict("random@arrests@busted")

		while not HasAnimDictLoaded("random@arrests@busted") and not HasAnimDictLoaded("random@arrests") do
			Citizen.Wait(0)
		end
		if ( IsEntityPlayingAnim( player, "random@arrests@busted", "idle_a", 3 ) ) then 
			TaskPlayAnim( player, "random@arrests@busted", "exit", 8.0, 1.0, -1, 2, 0, 0, 0, 0 )
			Wait (3000)
            TaskPlayAnim( player, "random@arrests", "kneeling_arrest_get_up", 8.0, 1.0, -1, 128, 0, 0, 0, 0 )
        else
            TaskPlayAnim( player, "random@arrests", "idle_2_hands_up", 8.0, 1.0, -1, 2, 0, 0, 0, 0 )
			Wait (4000)
            TaskPlayAnim( player, "random@arrests", "kneeling_arrest_idle", 8.0, 1.0, -1, 2, 0, 0, 0, 0 )
			Wait (500)
			TaskPlayAnim( player, "random@arrests@busted", "enter", 8.0, 1.0, -1, 2, 0, 0, 0, 0 )
			Wait (1000)
			TaskPlayAnim( player, "random@arrests@busted", "idle_a", 8.0, 1.0, -1, 9, 0, 0, 0, 0 )
        end     
   end
	end
end )