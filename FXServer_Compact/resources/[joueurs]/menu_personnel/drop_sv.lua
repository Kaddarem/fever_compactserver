local bag = {}

RegisterServerEvent('DropSystem:create')
AddEventHandler('DropSystem:create', function(bags, extra,id,isArme, count)
    if bag[bags] == nil then
        bag[bags] = {}
    end
    local presence = 0
    for ligne,info in pairs(bag[bags]) do
        if info.item == id and not isArme and info.item ~= 47  then
            presence = ligne
            bag[bags][ligne].count = bag[bags][ligne].count + count
        end
    end
    if presence == 0 then
        table.insert(bag[bags],{item = id,extra = extra, count = count})
    end
    TriggerClientEvent('DropSystem:createForAll', -1, bags)
end)



RegisterServerEvent('DropSystem:drop')
AddEventHandler('DropSystem:drop', function(bag, item, count)
    TriggerClientEvent('DropSystem:createForAll', -1, bag)
end)


RegisterServerEvent('DropSystem:take')
AddEventHandler('DropSystem:take', function(bags,qty,ligne)
    local bags = bags
    local source = source
    local qty = qty
    local ligne = ligne
    if bag[bags][ligne].count >= qty then
        bag[bags][ligne].count = bag[bags][ligne].count - qty
        local somme = 0
        for _,item in pairs (bag[bags]) do
            somme = somme + item.count
        end
        if somme == 0 then
            TriggerClientEvent('DropSystem:remove', -1, bags)
        end
        TriggerClientEvent('player:receiveItem',source, bag[bags][ligne].item,qty,bag[bags][ligne].extra)
        TriggerClientEvent('hud:NotifColor',source,"Op, dans la poche !",141)  
    else
        TriggerClientEvent('hud:NotifColor',source, "Il n'y en a pas autant",6)
    end
end)

RegisterServerEvent('DropSystem:Look')
AddEventHandler('DropSystem:Look', function(bags)
    local source = source
    TriggerClientEvent('DropSystem:MenuLook',source,bags,bag[bags]) 
end)

function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

-- gets the actual player id unique to the player,
-- independent of whether the player changes their screen name
function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end