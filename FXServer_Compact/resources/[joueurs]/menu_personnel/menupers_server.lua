RegisterServerEvent("item:getItems")
AddEventHandler("item:getItems", function()
    local source = source
    local items = {}
    local inv = {}
    local player = getPlayerID(source)
    MySQL.Async.fetchAll('SELECT * FROM items ORDER BY id',{}, function(result)
        for _, v in pairs(result) do
            t = {["libelle"] = v.libelle, ['poids'] = v.poids,["limitation"]= v.limitation, ["food"] = v.food, ["water"] = v.water, ["needs"] = v.needs, ["item_id"]= v.id, ['isIllegal'] = v.isIllegal, ['isArme'] = v.isArme, ['venteArme'] = v.VenteArme, ['hash'] = v.hash }
            items[tonumber(v.id)] = t
        end
        MySQL.Async.fetchAll("SELECT * FROM user_inventory WHERE user_id = @username", { ['@username'] = player }, function(result2)
            if result2[1] ~= nil then
                inv = NumberKey(json.decode(result2[1].inventaire))
            end
            TriggerClientEvent("gui:getItems", source, items,inv)
        end)
    end)
end)

function NumberKey(t)
    local t2 = {}
        for k,v in pairs(t) do
            --check if string key can be turned into a number
            if tonumber(k) and type(k) ~= "number" then
                t2[tonumber(k)] = v
            else
                t2[k] = v
            end
        end
    return t2
end

RegisterServerEvent('item:GetInv')
AddEventHandler('item:GetInv',function()
    local source = source
    MySQL.Async.fetchAll("SELECT * FROM user_inventory WHERE user_id = @username", { ['@username'] = player }, function(result2)
        if result2[1] ~= nil then
            inv = json.decode(result2[1].inventaire)
        end
        TriggerClientEvent("gui:getItems", source, items,inv)
    end)
end)

RegisterServerEvent("item:setItem")
AddEventHandler("item:setItem", function(item, quantity)
    local source = source
    local player = getPlayerID(source)
    local qantity = quantity
    local item = item
    MySQL.Sync.execute("INSERT INTO user_inventory (`user_id`, `item_id`, `quantity`) VALUES (@player, @item, @qty)",{['@player'] = player, ['@item'] = item, ['@qty'] = quantity })
end)

RegisterServerEvent('item:updateInv')
AddEventHandler('item:updateInv',function(inv)
    local source = source
    TriggerClientEvent('menu:updateInv',source)
    local inv = inv
    local delete = true
    for _,_ in pairs (inv) do
        delete = false
        break
    end
    if delete then
        MySQL.Async.insert('DELETE FROM user_inventory WHERE user_id = @username', { ['@username'] = getPlayerID(source)})    
    else
        MySQL.Async.insert('REPLACE INTO user_inventory VALUES(@id,@inv)',{['@id'] = getPlayerID(source),['@inv']= json.encode(inv)})
    end
end)


RegisterServerEvent("item:reset")
AddEventHandler("item:reset", function()
    local source = source
    local player = getPlayerID(source)
    TriggerClientEvent('player:viderinv',source)
    MySQL.Async.insert("DELETE FROM user_inventory WHERE `user_id` = @username", { ['@username'] = player})
end)
RegisterServerEvent("item:sell")
AddEventHandler("item:sell", function(price)
    local source = source
    TriggerEvent('es:getPlayerFromId', source, function(user)
        local player = user.identifier
        user.addMoney(tonumber(price))
    end)
end)


RegisterServerEvent("player:giveItem")
AddEventHandler("player:giveItem", function(id, ligne, name, qty, target, info)
    local source = source
    local target = target
    local id = id
    local name = name
    local qty = qty
    local ligne = ligne
    local player = getPlayerID(target)
    TriggerClientEvent("player:looseItem", source, id, ligne,  qty)
    if info.extra == nil then
        TriggerClientEvent("player:receiveItem", target, id, qty)
    else
        TriggerClientEvent("player:receiveItem", target, id, qty,info.extra)    
    end
    TriggerClientEvent("es_freeroam:notify", target, "CHAR_MP_STRIPCLUB_PR", 1, "Mairie", false, "Vous venez de recevoir " .. qty .. " " .. name)
    TriggerEvent('log:print',"[Give] "..getnameRP(source).." a donnée "..qty.." "..name.." à "..getnameRP(target))
    
end)

RegisterServerEvent("player:swapMoney")
AddEventHandler("player:swapMoney", function(amount, target)
    local source = source
    TriggerEvent('es:getPlayerFromId', source, function(user)
        if user.money - amount >= 0 then
            user.removeMoney(amount)
            TriggerEvent('es:getPlayerFromId', target, function(user) user.addMoney(amount) end)
        end
    end)
end)

function RecupNum(ID)
    local num
    local temp = string.sub(ID,7)
    num = tonumber(temp,16)
    return num
end
RegisterServerEvent('menu:updateUser')
AddEventHandler('menu:updateUser', function(openMenu)
	local userInfos = {}
    local source = source
	TriggerEvent('es:getPlayerFromId', source, function(user)
		if user ~= nil then
            userInfos = {
                permission_level = user.permission_level,
                money = user.money,
                job = user.job,
                police = user.police,
                enService = user.enService,
                nom = user.nom,
                prenom = user.prenom,
                vehicle = user.vehicle,
                identifier = user.identifier,
                telephone = user.telephone,
                compte = source,
                numero = RecupNum(getPlayerID(source)),
                gunlicense = user.gunlicense,
                group = user.group.group
            }
		end
	end)
	TriggerClientEvent("menu:setUser", source, userInfos)
end)

RegisterServerEvent('menu:loadmasque')
AddEventHandler('menu:loadmasque', function()
    local source = source
    info = {}
    local player = getPlayerID(source)
    MySQL.Async.fetchAll("SELECT * FROM modelmenu WHERE identifier = @username AND Actif = 1", { ['@username'] = player },function(result)
        if result then
            info[1]=result[1].mask
            info[2]=result[1].mask_txt
            TriggerClientEvent('menu:mettremasque',source,info)
        else
            TriggerClientEvent('hud:NoticColor',source,"Vous n'avez pas de masque",6)
        end
    end)
end)

RegisterServerEvent('menu:loadchapeau')
AddEventHandler('menu:loadchapeau', function()
    local source = source
    info = {}
    local player = getPlayerID(source)
    MySQL.Async.fetchAll("SELECT * FROM modelmenu WHERE identifier = @username AND Actif = 1", { ['@username'] = player }, function(result)
        if result then
            info[1]=result[1].Chapeau
            info[2]=result[1].ChapeauColor
            TriggerClientEvent('menu:mettrechapeau',source,info)
        else
            TriggerClientEvent('hud:NoticColor',source,"Vous n'avez pas de chapeau",6)
        end
    end)
end)

RegisterServerEvent('menu:loadlunette')
AddEventHandler('menu:loadlunette', function()
    local source = source
    info = {}
    local player = getPlayerID(source)
    MySQL.Async.fetchAll("SELECT * FROM modelmenu WHERE identifier = @username AND Actif = 1", { ['@username'] = player }, function(result)
        if result then
            info[1]=result[1].Lun
            info[2]=result[1].LunColor
            TriggerClientEvent('menu:mettrelunette',source,info)
        else
            TriggerClientEvent('hud:NoticColor',source,"Vous n'avez pas de lunette",6)
        end
    end)
end)

RegisterNetEvent('menu:FouilleNotif')
AddEventHandler('menu:FouilleNotif',function(target)
   TriggerClientEvent('hud:NotifColor',target,"~h~On essaye de vous fouiller",6) 
end)

RegisterNetEvent('menu:fouilleenvoi')
AddEventHandler('menu:fouilleenvoi', function(target)
    local source = source
    local items = {}
    local player = getPlayerID(target)
    MySQL.Async.fetchAll("SELECT * FROM user_inventory WHERE user_id = @username", { ['@username'] = player }, function(result2)
        TriggerClientEvent("menu:fouillerecept", source, json.decode(result2[1].inventaire))
    end)
end)

RegisterNetEvent('menu:fouilletake')
AddEventHandler('menu:fouilletake', function(quantite,id,ligne,libelle,target,FouilleMainEnAir)
    local source = source
    local items = {}
    local player = getPlayerID(target)
    local id = id
    local ligne = ligne
    local quantite = quantite
    local verifier = false
    local libelle = libelle
    MySQL.Async.fetchAll("SELECT * FROM user_inventory WHERE user_id = @username", { ['@username'] = player }, function(result2)
        local Inventaire = NumberKey(json.decode(result2[1].inventaire))
        if Inventaire[id][ligne].quantite >= quantite then
            TriggerClientEvent("player:looseItem", target, id,ligne, quantite)
            local extra = {}
            if Inventaire[id][ligne].extra ~= nil then
                extra = Inventaire[id][ligne].extra
            end
            TriggerClientEvent("player:receiveItem", source, id, quantite,extra)
            TriggerClientEvent("hud:NotifColor", source, "Vous avez pris "..quantite.." "..libelle,141)
            if FouilleMainEnAir then
                TriggerClientEvent("hud:NotifColor", target, "On vous avez pris "..quantite.." "..libelle,6)           
            end
            TriggerClientEvent('menu:actualiserfouille',source)
            TriggerEvent('log:print',"[Fouille] "..getnameRP(source).." a pris "..quantite.." "..libelle.." sur "..getnameRP(target))
        else
            TriggerClientEvent('hud:NotifColor',source,"Valeur incorrecte",6)
            TriggerClientEvent('menu:actualiserfouille',source)
        end
    end)    
end)

function getnameRP(joueur)
    local name = tostring(joueur)
    TriggerEvent('es:getPlayerFromId', joueur, function(user)
		if user ~= nil then
            name = user.prenom.." "..user.nom
        end
    end)
    return name
end

-- get's the player id without having to use bugged essentials
function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

-- gets the actual player id unique to the player,
-- independent of whether the player changes their screen name
function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end