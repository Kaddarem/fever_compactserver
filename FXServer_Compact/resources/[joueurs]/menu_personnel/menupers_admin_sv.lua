local frozen = {}
RegisterServerEvent('ADMIN:annonce')
AddEventHandler('ADMIN:annonce',function(annonce)
    print(annonce)
    TriggerClientEvent('chatMessage', -1, "ANNONCE", {255, 0, 0}, annonce)
end)

RegisterServerEvent('ADMIN:freeze')
AddEventHandler('ADMIN:freeze',function(player)
    if(frozen[player])then
	   frozen[player] = false
	else
		frozen[player] = true
	end
    TriggerClientEvent('es_admin:freezePlayer', player, frozen[player])
    local state = "libéré"
	if(frozen[player])then
		state = "freezé"
	end    
    TriggerClientEvent('hud:Notif',source,"Joueur ~g~"..state)
end)

RegisterServerEvent('ADMIN:kick')
AddEventHandler('ADMIN:kick',function(player,raison)
    DropPlayer(player, raison)
    TriggerClientEvent('hud:Notif',"~r~"..player.."~w~ kické")
end)

RegisterServerEvent('ADMIN:tp')
AddEventHandler('ADMIN:tp',function(player)
    TriggerClientEvent('es_admin:teleportUser', player, source)
    TriggerClientEvent('hud:Notif',player,"Vous avez été téléporté")
end)

RegisterServerEvent('ADMIN:kill')
AddEventHandler('ADMIN:kill',function(player)
    TriggerClientEvent('es_admin:kill', player)
    TriggerClientEvent('hud:Notif',source,"Vous avez tué "..player)
end)