RegisterServerEvent('craft:check')
AddEventHandler('craft:check', function()
    local source = source
    local player = getPlayerID(source)
    local result = MySQL.Sync.fetchScalar('SELECT * FROM craft WHERE identifier = @id AND illegal = 1', {['@id'] = player})
    if result ~= nil then
            TriggerClientEvent('craft:activer',source,true)
    else
        TriggerClientEvent('craft:activer',source,false)        
    end
end)

-- get's the player id without having to use bugged essentials
function getPlayerID(source)
    local identifiers = GetPlayerIdentifiers(source)
    local player = getIdentifiant(identifiers)
    return player
end

-- gets the actual player id unique to the player,
-- independent of whether the player changes their screen name
function getIdentifiant(id)
    for _, v in ipairs(id) do
        return v
    end
end