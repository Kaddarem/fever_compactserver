local HUD = {
    visible = true,
    cinema = false,
    besoin = true,
    lordoffever = false,
    appart = false,
}

function AffichageEcran()
    if VMenu.menus.name ~= "Affichage" then
        VMenu.curItem = 1
    end
    VMenu.ResetMenu()
    VMenu.AddPrev('Main')
    VMenu.EditHeader("Affichage")
    VMenu.AddBool("Appartement","GererAppart","appart",HUD.appart)
    VMenu.AddBool("Batiment Achetable","GererLord","lord",HUD.lordoffever)
    VMenu.AddBool("Besoins","GererAffBesoin","besoin",HUD.besoin)
    VMenu.AddBool("HUD Visible","GererHUD","HUD",HUD.visible)
    VMenu.AddBool("Mode Cinema","GererCinema","cinema",HUD.cinema)
end

function GererHUD(value)
    if not value then
        HUD.visible = false
        TriggerEvent('es:setMoneyDisplay',0)
        TriggerEvent('es:setBankDisplay',0)
        TriggerEvent("es:setFoodMode","simple")
    else
        HUD.visible = true
        TriggerEvent('es:setMoneyDisplay',100)
        TriggerEvent('es:setBankDisplay',100)
        TriggerEvent("es:setFoodMode","reset")
        TriggerEvent('es:setFoodDisplay',100)
    end
    DisplayRadar(HUD.visible)
    if HUD.cinema then
        HUD.cinema = false
    end
    AffichageEcran()
end

function GererCinema()
    HUD.cinema = not HUD.cinema
    if HUD.cinema then
        BoucleBande()
    else
        TriggerEvent("es:setFoodMode","reset")
        TriggerEvent('es:setMoneyDisplay',100)
        TriggerEvent('es:setBankDisplay',100)
        DisplayRadar(true)
    end
    AffichageEcran()
end

function GererAffBesoin()
    HUD.besoin = not HUD.besoin
    if HUD.besoin then
        TriggerEvent('es:setFoodDisplay',100)
    else
        TriggerEvent('es:setFoodDisplay',0)
    end
    AffichageEcran()
end

function BoucleBande()
    Citizen.CreateThread(function()
        TriggerEvent('es:setMoneyDisplay',0)
        TriggerEvent('es:setBankDisplay',0)
        DisplayRadar(false)
        TriggerEvent("es:setFoodMode","cinema")
        while HUD.cinema do
            Wait(1)
            DrawRect(0.5,0.05,1.0,0.1,0,0,0,255)
            DrawRect(0.5,0.95,1.0,0.1,0,0,0,255)
        end
    end)
end

function GererLord()
    TriggerEvent("lof:drawBlipToogle")
    HUD.lordoffever = not HUD.lordoffever
    AffichageEcran()
end

function GererAppart()
    TriggerEvent("Appartement:Blips")
    HUD.appart = not HUD.appart
    AffichageEcran()
end