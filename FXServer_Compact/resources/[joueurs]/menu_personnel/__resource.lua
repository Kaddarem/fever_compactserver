server_scripts {
    '@mysql-async/lib/MySQL.lua',
	'menupers_server.lua',
    'menureceleur_sv.lua',
    'craft_sv.lua',
    'drop_sv.lua',
    'menupers_admin_sv.lua'
}
client_script {
    '@hud-event/hudevent_aff.lua',
    '@appmenu/menu_cl.lua',
    'menureceleur_cl.lua',
    'vdkinv.lua',
    'fumer_animation.lua',
	'menupers_client.lua',
	'menupers_anims.lua',
    'pointing.lua',
    'craft_cl.lua',
    'drop_cl.lua',
    'menupers_demarche.lua',
    'menupers_configaff.lua',
    'menupers_admin.lua',
    '@ammunation/gunacc_cl.lua',
    'menupers_pegi21.lua'
}

export 'getQuantity'
export 'getPoidsItem'
export 'getPods'
export 'getItemsList'
export 'getINV'