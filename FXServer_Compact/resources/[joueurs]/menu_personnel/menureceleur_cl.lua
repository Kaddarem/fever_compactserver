function menuReceleur()
    VMenu.curItem = 1
    VMenu.ResetMenu()
    VMenu.AddPrev('Main')
    VMenu.EditHeader("Menu Receleur")
    VMenu.AddFunc("Give Argent propre","RecGivePropre",nil)
    VMenu.AddFunc("Give Item","RecGiveItem",nil)
end

function RecGivePropre()
    TriggerEvent('DisplayInputHTML',"Entrez le quantité a créer","number",10, function(res)
        if res > 0 then
            TriggerServerEvent("MenuReceleur:GivePropre", res)
            TriggerEvent('hud:NotifColor',"Voici "..res.."$ pour vous !",144)
        else
            TriggerEvent('hud:NotifColor',"Erreur de saisie",6)
        end
    end)
end

function RecGiveItem()
    VMenu.curItem = 1
    VMenu.ResetMenu()
    VMenu.AddPrev('menuReceleur')
    VMenu.EditHeader("Liste items")
    for ind, value in pairs(ITEMS) do
        if value.isIllegal then
            VMenu.AddPoids("~r~"..tostring(value.libelle), "RecGive", value,value.poids)
        else
            VMenu.AddPoids(tostring(value.libelle), "RecGive", value,value.poids)
        end
    end
end

function RecGive(value)
    TriggerEvent('DisplayInputHTML',"Entrez le quantité a créer","number",10, function(res)
        res = tonumber(res)
        if res > 0 then
            TriggerEvent('player:receiveItem',value.item_id,res)
            TriggerEvent('hud:NotifColor',"Voici "..res.." "..value.libelle.." pour vous !",144)
        else
            TriggerEvent('hud:NotifColor',"Erreur de saisie",6)
        end
    end)
end