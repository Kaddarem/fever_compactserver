local noclip = false
local delgun = false

function menuAdmin()
    VMenu.ResetCursor()
    VMenu.ResetMenu("Menu administrateur")
    VMenu.AddPrev('Main')
    setOpt("invisible",false)
    setOpt("showid",false)
    setOpt("noclip",false)
    setOpt("delgun",false)
    VMenu.AddBool('Delgun',"Admin_delgun","delgun",getOpt("delgun"))
    VMenu.AddFunc('Freeze un joueur',"Admin_freeze")
    VMenu.AddBool("Invisibilité","Admin_inv","invisible",getOpt("invisible"))
    VMenu.AddFunc('Kicker un joueur',"Admin_kick")
    VMenu.AddBool("Noclip","Admin_noclip","noclip",getOpt("noclip"))
    VMenu.AddFunc('Passer une annonce',"Admin_annonce")
    VMenu.AddFunc('Se téléporter à un joueur',"Admin_goto")
    VMenu.AddFunc("Spawn d'un véhicule","Admin_SpawnCar")
    VMenu.AddFunc('Téléporter un joueur ici',"Admin_tp")
    VMenu.AddFunc('Tuer un joueur',"Admin_kill")
    VMenu.AddFunc("Voir les ids","Admin_showid")
end

function Admin_delgun(value)
    delgun = value
	if value then -- checks if toggle is false
		TriggerEvent('hud:Notif',"~g~Delete Gun Activé!")
		GiveWeaponToPed(GetPlayerPed(-1), GetHashKey("WEAPON_STUNGUN"),true, true)
	else
		TriggerEvent('hud:Notif','~b~Delete Gun désactivé!')
        RemoveWeaponFromPed(GetPlayerPed(-1), GetHashKey("WEAPON_STUNGUN"))
	end
            
    local function getEntity(player) --Function To Get Entity Player Is Aiming At
        local result, entity = GetEntityPlayerIsFreeAimingAt(player)
        return entity
    end
    Citizen.CreateThread(function() -- Creates thread
        while delgun do -- infinite loop
            Citizen.Wait(0) -- wait so it doesnt crash
            if IsPlayerFreeAiming(PlayerId()) then -- checks if player is aiming around
                local entity = getEntity(PlayerId()) -- gets the entity
                if IsPedShooting(GetPlayerPed(-1)) then -- checks if ped is shooting
                    local temp = GetEntityModel(entity)
                    SetEntityAsMissionEntity(entity, true, true) -- sets the entity as mission entity so it can be despawned
                    DeleteEntity(entity) -- deletes the entity
                end
            end
        end
    end)        
end

function Admin_annonce()
    TriggerEvent('DisplayInputHTML',"Annonce","text",60, function(annonce)
        if annonce ~= "" then
            TriggerServerEvent('ADMIN:annonce',annonce)    
        end
    end)
end

function Admin_freeze()
    TriggerEvent('DisplayInputHTML',"Entrer l'id du joueur","number",5, function(id)
        if GetPlayerFromServerId(tonumber(id)) ~= -1 then
            TriggerServerEvent('ADMIN:freeze',id) 
        else
            TriggerEvent('hud:NotifColor',"ID inconnu",6)
        end   
    end)
end
function Admin_inv(value)
    if value then
	   SetEntityVisible(GetPlayerPed(-1),false)
	   SetEntityInvincible(GetPlayerPed(-1),false)
       --SetPlayerInvincible(PlayerId(),false)
       visible = false
    else
       SetEntityVisible(GetPlayerPed(-1),true)
	   SetEntityInvincible(GetPlayerPed(-1),true)
       --SetPlayerInvincible(PlayerId(),true)
       visible = true
    end
end

function Admin_kick()
    TriggerEvent('DisplayInputHTML',"Entrer l'id du joueur","number",5, function(id)
        if GetPlayerFromServerId(tonumber(id)) ~= -1 then
            TriggerEvent('DisplayInputHTML',"Entrer la raison du kick","text",60, function(raison)
                TriggerServerEvent('ADMIN:kick',id,raison)    
            end) 
        else
            TriggerEvent('hud:NotifColor',"ID inconnu",6)
        end
    end)
end


function Admin_noclip(_noclip)
    noclip = _noclip
	if noclip then
		noclip_pos = GetEntityCoords(GetPlayerPed(-1), false)
	end
    
	if noclip then
        TriggerEvent('hud:Notif',"Noclip ~g~activé")
	else
        TriggerEvent('hud:Notif',"Noclip ~r~desactivé")
    end
    
    local heading = GetEntityHeading(GetPlayerPed(-1)) - 180.0
    Citizen.CreateThread(function()
        SetEntityHeading(GetPlayerPed(-1),  heading)
        local coef = 1.0
        while noclip do
            Citizen.Wait(5)
            SetEntityCoordsNoOffset(GetPlayerPed(-1),  noclip_pos.x,  noclip_pos.y,  noclip_pos.z,  0, 0, 0)
            coef = 1.0
            if(IsControlPressed(1,  21))then
                coef = 5.0
            end
            if(IsControlPressed(1,  34))then
                heading = heading + 1.5
                if(heading > 360)then
                    heading = 0
                end
                SetEntityHeading(GetPlayerPed(-1),  heading)
            end
            if(IsControlPressed(1,  9))then
                heading = heading - 1.5
                if(heading < 0)then
                    heading = 360
                end
                SetEntityHeading(GetPlayerPed(-1),  heading)
            end
            if(IsControlPressed(1,  8))then
                noclip_pos = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, coef * 1.0, 0.0)
            end
            if(IsControlPressed(1,  32))then
                noclip_pos = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, coef * -1.0, 0.0)
            end
            if(IsControlPressed(1,  27))then
                noclip_pos = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, 0.0, coef * 0.5)
            end
            if(IsControlPressed(1,  173))then
                noclip_pos = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, 0.0, coef * -0.5)
            end
        end
    end)
end

function Admin_goto()
    TriggerEvent('DisplayInputHTML',"Entrer l'id du joueur","number",5, function(id)
        if GetPlayerFromServerId(tonumber(id)) ~= -1 then
            local pos = GetEntityCoords(GetPlayerPed(GetPlayerFromServerId(tonumber(id))))
            RequestCollisionAtCoord(pos.x, pos.y, pos.z)
            while not HasCollisionLoadedAroundEntity(GetPlayerPed(-1))do
                RequestCollisionAtCoord(pos.x, pos.y, pos.z)
                Citizen.Wait(0)
            end
            SetEntityCoords(GetPlayerPed(-1), pos)
        else
            TriggerEvent('hud:NotifColor',"ID inconnu",6)
        end
    end)
end

function Admin_SpawnCar()
    TriggerEvent('DisplayInputHTML',"Entrer le modèle à faire spawn","text",20, function(modele)
        local carid = GetHashKey(modele)
        if IsModelValid(carid) then
        local playerPed = GetPlayerPed(-1)
            RequestModel(carid)
            while not HasModelLoaded(carid) do
                Citizen.Wait(0)
            end
            local playerCoords = GetEntityCoords(playerPed)
            local heading = GetEntityHeading(playerPed)
            veh = CreateVehicle(carid, playerCoords, heading, true, false)
            TaskWarpPedIntoVehicle(playerPed, veh, -1)
        else
            TriggerEvent('hud:NotifColor',"Ce modèle n'existe pas",6)
        end
    end)
end

function Admin_tp()
    TriggerEvent('DisplayInputHTML',"Entrer l'id du joueur","number",5, function(id)
        if GetPlayerFromServerId(tonumber(id)) ~= -1 then
            TriggerServerEvent('ADMIN:tp',id)    
        else
            TriggerEvent('hud:NotifColor',"ID inconnu",6)
        end
    end)
end

function Admin_kill()
    TriggerEvent('DisplayInputHTML',"Entrer l'id du joueur à tuer","number",5, function(id)
        if GetPlayerFromServerId(tonumber(id)) ~= -1 then
            TriggerServerEvent('ADMIN:kill',id)    
        else
            TriggerEvent('hud:NotifColor',"ID inconnu",6)
        end
    end)
end

function Admin_showid()
    for id = 0, 500 do
        if GetPlayerServerId(id) ~= 0 then
            ped = GetPlayerPed(id)
           -- Create head display (this is safe to be spammed)
            headId = Citizen.InvokeNative( 0xBFEFE3321A3F5015, ped, "- "..GetPlayerServerId(id).." -", false, false, "", false )
 
        end
    end
    TriggerEvent('hud:NotifColor',"IDs affichés",141)
end
