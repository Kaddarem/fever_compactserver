local dropList = {}

local PropObject = "prop_paper_bag_01"

RegisterNetEvent('DropSystem:drop')
AddEventHandler('DropSystem:drop', function(extra,id,amount)
    local bag = 0
    local ped = GetPlayerPed(-1)
    local pedCoord = GetEntityCoords(ped)
    for k,v in pairs(dropList) do
        if DoesObjectOfTypeExistAtCoords(pedCoord["x"], pedCoord["y"], pedCoord["z"], 1.3, GetHashKey(PropObject), true) then
            Bag = GetClosestObjectOfType(pedCoord["x"], pedCoord["y"], pedCoord["z"], 1.3, GetHashKey(PropObject), false, false, false)
            if NetworkGetNetworkIdFromEntity(Bag) == k then
                bag = k
            end
        end
    end
    if bag == 0 then
        bag = SetBagOnGround()
    end
    TriggerServerEvent('DropSystem:create', bag, extra,id,ITEMS[id].isArme, amount)
end)

RegisterNetEvent('DropSystem:remove')
AddEventHandler('DropSystem:remove', function(bag)
    DeleteBag(bag)
    if dropList[bag] ~= nil then
        dropList[bag] = nil
    end
end)

RegisterNetEvent('DropSystem:createForAll')
AddEventHandler('DropSystem:createForAll', function(bag)
  dropList[bag] = true
end)

function SetBagOnGround()
    x, y, z = table.unpack(GetEntityCoords(GetPlayerPed(-1), true))
    Bag = GetHashKey(PropObject)
    RequestModel(Bag)
    while not HasModelLoaded(Bag) do
        Citizen.Wait(1)
    end
    local object = CreateObject(Bag, x, y, z -2, true, true, true) -- x+1
    PlaceObjectOnGroundProperly(object)
    local network = NetworkGetNetworkIdFromEntity(object)
    return network
end

function DeleteBag(Bag)
  BagEntity = NetworkGetEntityFromNetworkId(Bag)
  SetEntityAsMissionEntity(BagEntity, true, true)
  DeleteObject(BagEntity)
end

RegisterNetEvent('DropSystem:MenuLook')
AddEventHandler('DropSystem:MenuLook',function(id,ContenuSac)
   OpenMenuDrop(id,ContenuSac) 
end)

function OpenMenuDrop(id,ContenuSac)
    VMenu.ResetMenu()
    VMenu.curItem = 2
    VMenu.EditHeader('Sac au sol')
    VMenu.AddSep("Capacité poche : " .. (getPods() or 0) .. "/" .. maxCapacity.."kg")
    local count = 0
    for a,b in pairs(ContenuSac) do
        count = count + 1
        if b.count > 0 then
            if ITEMS[b.item].isArme then
                VMenu.AddWeap(ITEMS[b.item].libelle, "TakeItemDrop",{a,id,ContenuSac},ITEMS[b.item].poids,ITEMS[b.item].hash,b.extra)
            elseif tonumber(b.item) == 47 then
                VMenu.AddPoids(ITEMS[b.item].libelle.." "..exports.plongee:GetNomCouleur(b.extra),"TakeItemDrop",{a,id,ContenuSac},ITEMS[b.item].poids,"Prendre") 
            else
                VMenu.AddPoids(ITEMS[b.item].libelle.." : "..b.count,"TakeItemDrop",{a,id,ContenuSac},ITEMS[b.item].poids,"Prendre") 
            end
        end
    end
    if count == 0 then
        VMenu.AddSep("Sac vide")
    end
    VMenu.visible = true
end

function TakeItemDrop(args)
    local ligne = args[1]
    local id = args[2]
    local ContenuSac = args[3]
    TriggerEvent('DisplayInputHTML',"Entrez le quantité a ramasser","number",10, function(qty)
        if qty ~= nil then
            if qty > 0 and qty <= ContenuSac[ligne].count then
                TriggerEvent('hud:Notif',"Take")
                TriggerServerEvent('DropSystem:take',id,qty,ligne,ContenuSac[ligne].extra)
                VMenu.visible = false
            else
                TriggerEvent('hud:NotifColor',"Erreur de saisie",6)
            end
        else
            TriggerEvent('hud:NotifColor',"Erreur de saisie",6)
        end
    end)
end
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(5)
        local ped = GetPlayerPed(-1)
        local pedCoord = GetEntityCoords(ped)
        for k,v in pairs(dropList) do
            if DoesObjectOfTypeExistAtCoords(pedCoord["x"], pedCoord["y"], pedCoord["z"], 1.3, GetHashKey(PropObject), true) then
                Bag = GetClosestObjectOfType(pedCoord["x"], pedCoord["y"], pedCoord["z"], 1.3, GetHashKey(PropObject), false, false, false)
                if NetworkGetNetworkIdFromEntity(Bag) == k then
                    if Vdist(pedCoord,GetEntityCoords(Bag)) < 1.3 then
                        DisplayHelp("Pressez ~INPUT_CONTEXT~ pour ramasser le sac")
                        if IsControlJustPressed(1,51) then
                            if VMenu.visible then
                                VMenu.visible = false
                            else
                                TriggerServerEvent('DropSystem:Look', k)
                                VMenu.curItem = 1
                                VMenu.scroll = 0
                            end    
                        end
                    elseif VMenu.visible then
                        VMenu.visible = false
                    end
                end
            end
        end
    end
end)